{ Zip Based Installer }
program Install;
{$MODE OBJFPC}{$H+}
uses
  Crt, Dos, SysUtils, Zipper, CrtUtil;

const
  ZipFile = 'PETRA.ZIP';

type
  { TUnZipperEx - Extension for unzipper to delete existing files
    and show the extracted filenames }
  TUnZipperEx = class(TUnZipper)
    procedure DoStartFile(Sender: TObject; const AFileName: string);
  end;

var
  Options: array [0..2] of string;
  TargetDir: string = 'C:\PETRA';

{ TUnZipperEx }
procedure TUnZipperEx.DoStartFile(Sender: TObject; const AFileName: string);
begin
  ForceDirectories(ExtractFilePath(AFileName));
  Color(7, 0);Write('  Extracting ');Color(3, 0);Writeln(AFileName);
end;


{ Functions }

// A fatal error happened
procedure Fatal(Msg: string);
begin
  Color(7, 0);
  ClrScr;
  Color(15, 4);Write(' FATAL ERROR: ');
  Color(14, 4);Write(Msg);ClrEol;Writeln;
  Color(7, 0);Writeln;Writeln;
  Writeln('Installation aborted');
  Writeln;
  Halt;
end;

// Perform installation
procedure DoInstall;
var
  UZ: TUnZipperEx;
begin
  // Ensure the target directory exists
  if not ForceDirectories(TargetDir) then
    Fatal('Failed to create ' + TargetDir);
  if not DirectoryExists(TargetDir) then
    Fatal('Target directory ' + TargetDir + ' does not exist');
  // Uncompress the files
  Color(14, 0);ClrScr;
  Writeln('Decompressing ', ZipFile, ' into ', TargetDir);
  Color(7, 0);
  UZ:=TUnZipperEx.Create;
  UZ.OnStartFile:=@UZ.DoStartFile;
  try
    UZ.FileName:=ZipFile;
    UZ.OutputPath:=TargetDir;
    Writeln('  Scanning zipfile...');
    UZ.Examine;
    Writeln('  Decompressing...');
    UZ.UnZipAllFiles;
  except
    Fatal('Failed to extract the archive: ' + Exception(ExceptObject).Message);
  end;
  UZ.Free;
  Color(7, 0);
  ClrScr;
  Color(15, 3);
  Write(' * Installation finished * ');
  Color(7, 0);
  Writeln;Writeln;
  Color(14, 0);Writeln('To configure the game (sound settings, etc) enter:');
  Color(7, 0);
  Writeln;
  if (Length(TargetDir) > 1) and (TargetDir[2]=':') then Writeln('  ', Copy(TargetDir, 1, 2));
  Writeln('  CD ', TargetDir);
  Writeln('  CONFIG');
  Writeln;
  Color(14, 0);Writeln('To play the game:');
  Color(7, 0);
  if (Length(TargetDir) > 1) and (TargetDir[2]=':') then Writeln('  ', Copy(TargetDir, 1, 2));
  Writeln('  CD ', TargetDir);
  Writeln('  PETRA');
  Writeln;
  Halt;
end;

// Change the target directory
procedure ChangeTarget;
var
  S: string;
  I: Integer;
begin
  DrawWindow(4, 9, 77, 14, 'Target Directory (press Enter for ' + TargetDir + ')');
  GotoXY(7, 12);
  Color(14, 1);Write('>');Color(7, 1);
  Readln(S);
  S:=UpperCase(Trim(S));
  for I:=1 to Length(S) do
    if not (S[I] in ['A'..'Z', '0'..'9', '\', '_']) then begin
      if (S[I]=':') and (I=2) then Continue;
      GotoXY(1, 25);Color(15, 4);ClrEol;
      Beep;
      Write('Invalid path!');ReadKey;
      Exit;
    end;
  TargetDir:=UpperCase(ExpandFileName(S));
end;

begin
  // Main menu
  repeat
    DrawBackground('Post Apocalyptic Petra Installer');
    Options[0]:='Install to ' + TargetDir;
    Options[1]:='Change target directory';
    Options[2]:='Exit';
    case RunMenu('Menu', Options) of
      0: DoInstall;
      1: ChangeTarget;
      2: Break;
    end;
  until False;
  Color(7, 0);
  ClrScr;
end.
