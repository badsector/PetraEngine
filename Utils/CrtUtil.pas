{ Crt Utilities }
unit CrtUtil;
{$MODE OBJFPC}{$H+}
interface

// Set the color to use for output
procedure Color(C, B: Byte);
// Draw status line
procedure DrawStatus(Line: string);
// Draw background with the given title
procedure DrawBackground(ATitle: string);
// Draw a window
procedure DrawWindow(X1, Y1, X2, Y2: Integer; Title: string);
// Run a menu
function RunMenu(Title: string; Options: array of string): Integer;
// Display the contents of a text file
procedure DisplayFile(FileName: string);
// Display the files in a directory and provide a menu to pick one
function PickFile(Title, HelpPrefix, Path: string): string;

implementation

uses
  Crt, Dos, SysUtils;

{ Functions }

procedure Color(C, B: Byte);
begin
  TextColor(C);
  TextBackground(B);
end;

procedure DrawStatus(Line: string);
begin
  GotoXY(1, 25);
  Color(15, 4);
  Write(Line);
  ClrEol;
end;

procedure DrawBackground(ATitle: string);
var
  I: Integer;
begin
  Color(15, 4);
  GotoXY(1, 1);
  ClrEol;
  GotoXY(1, 1);
  Write(' ', ATitle);
  DrawStatus('');
  Color(3, 3);
  for I:=2 to 24 do begin
    GotoXY(1, I);
    ClrEol;
  end;
end;

procedure DrawWindow(X1, Y1, X2, Y2: Integer; Title: string);
var
  Y: Integer;
begin
  Color(1, 1);
  for Y:=Y1 + 1 to Y2 - 1 do begin
    GotoXY(X1 + 2, Y);
    Write('':(X2 - X1 - 3));
  end;
  Color(0, 7);
  GotoXY(X1, Y1);Write('':(X2 - X1 + 1));
  GotoXY(X1, Y2);Write('':(X2 - X1 + 1));
  for Y:=Y1 + 1 to Y2 - 1 do begin
    GotoXY(X1, Y);Color(0, 7);Write('  ');
    GotoXY(X2 - 1, Y);Write('  ');Color(0, 0);Write('  ');
  end;
  GotoXY(X2 + 1, Y2);Write('  ');
  GotoXY(X1 + 2, Y2 + 1);Write('':(X2 - X1 + 1));
  Color(15, 2);
  GotoXY(X1 + 2, Y1 + 1);Write('':(X2 - X1 - 3));
  GotoXY(X1 + 2 + ((X2 - X1 - 3) - Length(Title)) div 2, Y1 + 1);
  Write(Title);
end;

function RunMenu(Title: string; Options: array of string): Integer;
var
  X, Y, I, Width, Scroll, MaxDisp: Integer;
  Key: Char;
  Help: array of string;

  // Select next item and scroll the view if necessary
  procedure NextItem(Wrap: Boolean);
  begin
    if Result < High(Options) then begin
      Inc(Result);
      if Result - Scroll >= MaxDisp then Inc(Scroll);
    end else if Wrap then begin
      Result:=0;
      Scroll:=0;
    end;
  end;

  // Select previous item and scroll the view if necessary
  procedure PreviousItem(Wrap: Boolean);
  begin
    if Result > 0 then begin
      Dec(Result);
      if Result - Scroll < 0 then Dec(Scroll);
    end else if Wrap then begin
      Result:=High(Options);
      Scroll:=Length(Options) - MaxDisp;
    end;
  end;

begin
  // Split title and help
  SetLength(Help, Length(Options));
  for I:=0 to High(Options) do
    if Pos(#1, Options[I]) <> 0 then begin
      Help[I]:=Copy(Options[I], Pos(#1, Options[I]) + 1, 80);
      Options[I]:=Copy(Options[I], 1, Pos(#1, Options[I]) - 1);
    end else
      Help[I]:=Options[I];
  // Find option width
  Width:=Length(Title);
  for I:=0 to High(Options) do
    if Width < Length(Options[I]) then
      Width:=Length(Options[I]);
  // Calculate position
  X:=37 - Width div 2;
  Y:=11 - Length(Options) div 2;
  if Y < 3 then Y:=3;
  // Calculate maximum displayed options
  MaxDisp:=Length(Options);
  if MaxDisp > 16 then MaxDisp:=16;
  // Draw menu window
  DrawWindow(X, Y, X + Width + 5, Y + MaxDisp + 4, Title);
  Result:=0;
  Scroll:=0;
  while True do begin
    // Draw menu options
    for I:=0 to MaxDisp - 1 do begin
      if I + Scroll=Result then Color(12, 0) else Color(15, 1);
      GotoXY(X + 2, Y + 3 + I);
      Write(' ', Options[I + Scroll],
        '':(Width - Length(Options[I + Scroll]) + 1));
    end;
    // Draw current option's help
    DrawStatus(' ' + Help[Result]);
    // Draw indicators that there are more items
    if MaxDisp <> Length(Options) then begin
      Color(11, 1);
      GotoXY(X + Width - 4, Y + 2);
      if Scroll > 0 then
        Write('More...')
      else
        Write('       ');
      GotoXY(X + Width - 4, Y + MaxDisp + 3);
      if Scroll < Length(Options) - MaxDisp then
        Write('More...')
      else
        Write('       ');
    end;
    // Move the cursor to the selected option
    GotoXY(X + 3, Y + 3 + Result - Scroll);
    // Handle input
    Key:=ReadKey;
    case Key of
      #0: case ReadKey of
        #79: begin Scroll:=Length(Options) - MaxDisp; Result:=High(Options); end;
        #80: NextItem(True);
        #81: for I:=1 to 15 do NextItem(False);
        #71: begin Scroll:=0; Result:=0; end;
        #72: PreviousItem(True);
        #73: for I:=1 to 15 do PreviousItem(False);
      end;
      'w', 'W': PreviousItem(True);
      's', 'S': NextItem(True);
      #27: Exit(-1);
      #13: Exit;
    end;
  end;
end;

procedure DisplayFile(FileName: string);
var
  F: Text;
  S: string;
  I: Integer;
  Lines: array of string = nil;
begin
  // Read the file contents
  Assign(F, FileName);{$I-}Reset(F);{$I+}
  if IOResult <> 0 then begin
    SetLength(Lines, 1);
    Lines[0]:='Cannot open ' + FileName;
  end else begin
    while not Eof(F) do begin
      Readln(F, S);
      S:=Copy(S, 1, 74); // trim the line
      SetLength(Lines, Length(Lines) + 1);
      Lines[High(Lines)]:=S;
    end;
    Close(F);
  end;
  // Add help suffix for line numbering
  for I:=0 to High(Lines) do
    Lines[I] += #1 + 'Line ' + IntToStr(I + 1) + ' of ' + IntToStr(Length(Lines));
  // Show the lines as a menu
  RunMenu(FileName, Lines);
end;

function PickFile(Title, HelpPrefix, Path: string): string;
var
  Files: array of string = nil;
  FI: TSearchRec;
  R: Integer;
begin
  // Scan for files
  if FindFirst(Path, faAnyFile, FI)=0 then begin
    repeat
      with FI do begin
        if (Attr and faDirectory)=0 then begin
          SetLength(Files, Length(Files) + 1);
          Files[High(Files)]:=Name + #1 + HelpPrefix + ' ' + Name;
        end;
      end;
    until FindNext(FI) <> 0;
    FindClose(FI);
  end;
  // Error if no files were found
  if Length(Files)=0 then begin
    SetLength(Files, 1);
    Files[0]:='Files not found for ' + Path;
    RunMenu('Error', Files);
    Exit('');
  end;
  // Add Cancel option
  SetLength(Files, Length(Files) + 1);
  Files[High(Files)]:='Cancel'#1'Go back to the previous menu';
  // Show menu
  R:=RunMenu(Title, Files);
  if (R=-1) or (R=High(Files)) then
    Result:=''
  else
    Result:=Copy(Files[R], 1, Pos(#1, Files[R]) -1 );
end;

end.
