program PalConv;

{$mode objfpc}{$H+}{$M+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  Classes, SysUtils, CustApp
  { you can add units after this };

type
  { Exceptions }
  EPaletteConversionError = class(Exception);

  { Classes }
  TPaletteIOClass = class of TPaletteIO;

  { TRGBColor - 8bcc RGB color }
  TRGBColor = record
    Red, Green, Blue: Byte;
  end;

  { TRGBPalette - 8bit color palette }
  TRGBPalette = object
    // The palette colors
    Colors: array [0..255] of TRGBColor;
    // Reset the palette to black
    procedure Clear;
  end;

  { TPaletteIO - Base class for palette loaders and writers }
  TPaletteIO = class
  public
    // Load the palette from the given file
    class procedure Load(AFileName: string; var APalette: TRGBPalette); virtual; abstract;
    // Write the palette to the given file
    class procedure Save(AFileName: string; const APalette: TRGBPalette); virtual; abstract;
    // Return the name of the supported file format
    class function GetFormatName: string; virtual; abstract;
    // Return a description for the file format
    class function GetFormatInfo: string; virtual; abstract;
  end;

  { TJASCPaletteIO - JASC palette loader/writer }
  TJASCPaletteIO = class(TPaletteIO)
  public
    // TPaletteIO methods
    class procedure Load(AFileName: string; var APalette: TRGBPalette); override;
    class procedure Save(AFileName: string; const APalette: TRGBPalette); override;
    class function GetFormatName: string; override;
    class function GetFormatInfo: string; override;
  end;

  { TGIMPPaletteIO - GIMP palette (GPL) loader/writer }
  TGIMPPaletteIO = class(TPaletteIO)
  public
    // TPaletteIO methods
    class procedure Load(AFileName: string; var APalette: TRGBPalette); override;
    class procedure Save(AFileName: string; const APalette: TRGBPalette); override;
    class function GetFormatName: string; override;
    class function GetFormatInfo: string; override;
  end;

  { TINCPaletteIO - Free Pascal constant in INClude file writer }
  TINCPaletteIO = class(TPaletteIO)
  public
    // TPaletteIO methods
    class procedure Load(AFileName: string; var APalette: TRGBPalette); override;
    class procedure Save(AFileName: string; const APalette: TRGBPalette); override;
    class function GetFormatName: string; override;
    class function GetFormatInfo: string; override;
  end;

  { TPaletteConversionTool - Tool for converting palettes }
  TPaletteConversionTool = class(TCustomApplication)
  private
    InputIO: TPaletteIOClass;
    OutputIO: TPaletteIOClass;
  protected
    procedure DoRun; override;
  public
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
    procedure WriteHelp; virtual;
  end;

const
  DefaultInputIO: TPaletteIOClass = TGIMPPaletteIO;
  DefaultOutputIO: TPaletteIOClass = TJASCPaletteIO;

var
  PaletteIOClasses: array of TPaletteIOClass;

{ Functions }
procedure RegisterPaletteIOClasses(AClass: array of TPaletteIOClass);
var
  I: Integer;
begin
  for I:=0 to High(AClass) do
    Insert(AClass[I], PaletteIOClasses, Length(PaletteIOClasses));
end;

function FindPaletteIOClassByName(AName: string): TPaletteIOClass;
var
  I: Integer;
begin
  AName:=LowerCase(Trim(AName));
  for I:=0 to High(PaletteIOClasses) do
    if LowerCase(PaletteIOClasses[I].GetFormatName)=AName then
      Exit(PaletteIOClasses[I]);
  Result:=nil;
end;

class procedure TINCPaletteIO.Load(AFileName: string; var APalette: TRGBPalette);
begin
  raise EPaletteConversionError.Create('Cannot read from INC files');
end;

class procedure TINCPaletteIO.Save(AFileName: string; const APalette: TRGBPalette);
var
  F: TextFile;
  I: Integer;
begin
  AssignFile(F, AFileName);
  {$I-}
  Rewrite(F);
  {$I+}
  if IOResult <> 0 then raise EPaletteConversionError.Create('Failed to create ' + AFileName);
  try
    Write(F, 'const PaletteTriplets: array [0..767] of byte = (');
    for I:=0 to 255 do with APalette.Colors[I] do begin
      if (I mod 16)=0 then Writeln(F);
      if I > 0 then Write(F, ',');
      Write(F, Red, ',', Green, ',', Blue);
    end;
    Writeln(F, ');');
  finally
    CloseFile(F);
  end;
end;

class function TINCPaletteIO.GetFormatName: string;
begin
  Result:='INC';
end;

class function TINCPaletteIO.GetFormatInfo: string;
begin
  Result:='Pascal include file';
end;

{ TRGBPalette }

procedure TRGBPalette.Clear;
var
  I: Integer;
begin
  for I:=0 to 255 do with Colors[I] do begin
    Red:=0;
    Green:=0;
    Blue:=0;
  end;
end;

{ TJASCPaletteIO }

class procedure TJASCPaletteIO.Load(AFileName: string; var APalette: TRGBPalette);
var
  F: TextFile;
  S: string;
  I: Integer;
begin
  AssignFile(F, AFileName);
  {$I-}
  Reset(F);
  {$I+}
  if IOResult <> 0 then raise EPaletteConversionError.Create('Failed to open ' + AFileName);
  try
    Readln(F, S);
    if S <> 'JASC-PAL' then raise EPaletteConversionError.Create('Not a JASC palette file: ' + AFileName);
    Readln(F, S);
    if S <> '0100' then raise EPaletteConversionError.Create('Unsupported JASC palette version in ' + AFileName);
    Readln(F, S);
    if S <> '256' then raise EPaletteConversionError.Create(AFileName + ' is not a 256 color JASC palette');
    for I:=0 to 255 do with APalette.Colors[I] do begin
      Readln(F, S);
      S:=Trim(S);
      Red:=StrToIntDef(Copy(S, 1, Pos(' ', S) - 1), 0);
      S:=Trim(Copy(S, Pos(' ', S), Length(S)));
      Green:=StrToIntDef(Copy(S, 1, Pos(' ', S) - 1), 0);
      S:=Trim(Copy(S, Pos(' ', S), Length(S)));
      Blue:=StrToIntDef(S, 0);
    end;
  finally
    CloseFile(F);
  end;
end;

class procedure TJASCPaletteIO.Save(AFileName: string; const APalette: TRGBPalette);
var
  F: TextFile;
  I: Integer;
begin
  AssignFile(F, AFileName);
  {$I-}
  Rewrite(F);
  {$I+}
  if IOResult <> 0 then raise EPaletteConversionError.Create('Failed to create ' + AFileName);
  try
    Write(F, 'JASC-PAL'#13#10);
    Write(F, '0100'#13#10);
    Write(F, '256'#13#10);
    for I:=0 to 255 do
      with APalette.Colors[I] do
        Write(F, Red, ' ', Green, ' ', Blue, #13#10);
  finally
    CloseFile(F);
  end;
end;

class function TJASCPaletteIO.GetFormatName: string;
begin
  Result:='JASC';
end;

class function TJASCPaletteIO.GetFormatInfo: string;
begin
  Result:='JASC Paint Shop Pro Palette';
end;
             
{ TGIMPPaletteIO }

class procedure TGIMPPaletteIO.Load(AFileName: string; var APalette: TRGBPalette);
var
  F: TextFile;
  S: string;
  I: Integer;
begin
  AssignFile(F, AFileName);
  {$I-}
  Reset(F);
  {$I+}
  if IOResult <> 0 then raise EPaletteConversionError.Create('Failed to open ' + AFileName);
  try
    Readln(F, S);
    if S <> 'GIMP Palette' then raise EPaletteConversionError.Create('Not a GIMP palette file ' + AFileName);
    while not EOF(F) do begin
      Readln(F, S);
      S:=Trim(S);
      if S='#' then Break;
    end;
    if EOF(F) then raise EPaletteConversionError.Create('No colors or missing header separator (#) in ' + AFileName);
    I:=0;
    while not EOF(F) do begin
      if I=256 then raise EPaletteConversionError.Create('Too many colors in ' + AFileName);
      Readln(F, S);
      with APalette.Colors[I] do begin
        S:=Trim(S);
        Red:=StrToIntDef(Copy(S, 1, Pos(' ', S) - 1), 0);
        S:=Trim(Copy(S, Pos(' ', S), Length(S)));
        Green:=StrToIntDef(Copy(S, 1, Pos(' ', S) - 1), 0);
        S:=Trim(Copy(S, Pos(' ', S), Length(S)));
        if Pos(' ', S)=0 then
          Blue:=StrToIntDef(S, 0)
        else
          Blue:=StrToIntDef(Copy(S, 1, Pos(' ', S) - 1), 0);
      end;
      Inc(I);
    end;
  finally
    CloseFile(F);
  end;
end;

class procedure TGIMPPaletteIO.Save(AFileName: string; const APalette: TRGBPalette);
var
  F: TextFile;
  I: Integer;
begin
  AssignFile(F, AFileName);
  {$I-}
  Rewrite(F);
  {$I+}
  if IOResult <> 0 then raise EPaletteConversionError.Create('Failed to create ' + AFileName);
  try
    Writeln(F, 'GIMP Palette');
    Writeln(F, 'Name: ', ExtractFileName(AFileName));
    Writeln(F, 'Columns: 16');
    Writeln(F, '#');
    for I:=0 to 255 do
      with APalette.Colors[I] do
        Writeln(F, Red, ' ', Green, ' ', Blue);
  finally
    CloseFile(F);
  end;
end;

class function TGIMPPaletteIO.GetFormatName: string;
begin
  Result:='GIMP';
end;

class function TGIMPPaletteIO.GetFormatInfo: string;
begin
  Result:='GIMP Palette';
end;

{ TPaletteConversionTool }

procedure TPaletteConversionTool.DoRun;
var
  ErrorMsg: String;
  Pal: TRGBPalette;
begin
  // Quick check parameters
  ErrorMsg:=CheckOptions('hi:o:I:O:', ['help', 'input::', 'output::', 'input-format::', 'output-format::']);
  if ErrorMsg<>'' then begin
    ShowException(Exception.Create(ErrorMsg));
    Terminate;
    Exit;
  end;

  // Parse parameters
  if HasOption('h', 'help') then begin
    WriteHelp;
    Terminate;
    Exit;
  end;
  if not (HasOption('i', 'input') and HasOption('o', 'output')) then begin
    Writeln('Missing --input or --output');
    WriteHelp;
    Terminate;
    Exit;
  end;
  InputIO:=TGIMPPaletteIO;
  OutputIO:=TJASCPaletteIO;
  if HasOption('I', 'input-format') then begin
    InputIO:=FindPaletteIOClassByName(Trim(GetOptionValue('I', 'input-format')));
    if not Assigned(InputIO) then begin
      Writeln('Unknown input format');
      WriteHelp;
      Terminate;
      Exit;
    end;
  end;
  if HasOption('O', 'output-format') then begin
    OutputIO:=FindPaletteIOClassByName(Trim(GetOptionValue('O', 'output-format')));
    if not Assigned(OutputIO) then begin
      Writeln('Unknown output format');
      WriteHelp;
      Terminate;
      Exit;
    end;
  end;

  // Do the conversion
  try
    Pal.Clear;
    InputIO.Load(GetOptionValue('i', 'input'), Pal);
    OutputIO.Save(GetOptionValue('o', 'output'), Pal);
  except
    Writeln('Conversion error: ' + Exception(ExceptObject).Message);
    WriteHelp;
  end;

  // Stop program loop
  Terminate;
end;

constructor TPaletteConversionTool.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  StopOnException:=True;
end;

destructor TPaletteConversionTool.Destroy;
begin
  inherited Destroy;
end;

procedure TPaletteConversionTool.WriteHelp;
var
  I: Integer;
begin
  Writeln('Usage: ', ExtractFileName(ExeName), ' [options]');
  Writeln('Options:');
  Writeln('-i <x> or --input=<x>         Input filename');
  Writeln('-o <x> or --output=<x>        Output filename');
  Writeln('-I <x> or --input-format=<x>  Input palette format (see below)');
  Writeln('-O <x> or --output-format=<x> Output palette format');
  Writeln('Formats:');
  for I:=0 to High(PaletteIOClasses) do begin
    Write(PaletteIOClasses[I].GetFormatName,
      '':30-Length(PaletteIOClasses[I].GetFormatName),
      PaletteIOClasses[I].GetFormatInfo);
    if PaletteIOClasses[I]=DefaultInputIO then
      Writeln(' (default input)')
    else if PaletteIOClasses[I]=DefaultOutputIO then
      Writeln(' (default output)')
    else
      Writeln;
  end;
end;

var
  Application: TPaletteConversionTool;
begin
  RegisterPaletteIOClasses([TJASCPaletteIO, TGIMPPaletteIO, TINCPaletteIO]);
  Application:=TPaletteConversionTool.Create(nil);
  Application.Title:='Palette Conversion Tool';
  Application.Run;
  Application.Free;
end.

