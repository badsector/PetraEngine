{ Windows-based configuration utility }
program wincfg;
{$UNITPATH Game}
{$UNITPATH ../Fowl}
{$R WRES.RES}
uses
  Windows, SysUtils, FObjects, Settings, DirectDraw, Direct3D;

const
  GameEXE = 'petra.exe';

type
  { Pointers }
  PDirectDraw3DDeviceInfo = ^TDirectDraw3DDeviceInfo;
  PConfigWindow = ^TConfigWindow;
  
  { TDirectDraw3DDeviceInfo - Contains information about a single enumerated
    DirectDraw device with 3D capabilities and the Direct3D devices it
    exposes }
  TDirectDraw3DDeviceInfo = record
    // The DirectDraw device's GUID
    GUID: TGUID;
    // The names of the Direct3D devices this device exposes
    Names: array of string;
    // The GUIDs of the Direct3D devices this device exposes
    GUIDs: array of TGUID;
    // The previously selected device
    Select3DDev: Integer;
  end;

  { TConfigWindow - Application main window }
  TConfigWindow = object(TWindow)
    DDDevices: array of TDirectDraw3DDeviceInfo;
    DirectDraw3DDevice, Direct3DDevice, TextSpeed: PComboBox;
    ColorDepthAuto, ColorDepth16, ColorDepth32: PRadioButton;
    MouseSpeed: PScrollBar;
    BilinearFilter, DisableSound: PCheckBox;
    UserMap: PComboBox;
    ResetToDefault, SaveAndExit, CancelAndExit: PButton;
    SelectDDev: Integer;
    constructor Init;
    procedure SetupWindow; virtual;
    function ProcessNotification(Child: PWindowsObject; NotifyCode: Integer): Boolean; virtual;
    procedure SaveTheSettings;
  end;

  { TConfigApp - Application type }
  TConfigApp = object(TApplication)
    procedure InitMainWindow; virtual;
  end;
  
{ Functions }
function D3DDevEnumProc(lpDeviceDescription, lpDeviceName: PChar; const Desc: TD3DDeviceDesc7; lpContext: Pointer): HRESULT; stdcall;
begin
  with PDirectDraw3DDeviceInfo(lpContext)^ do begin
    SetLength(Names, Length(Names) + 1);
    SetLength(GUIDs, Length(GUIDs) + 1);
    Names[High(Names)]:=string(lpDeviceDescription) + ' (' + string(lpDeviceName) + ')';
    GUIDs[High(GUIDs)]:=Desc.DeviceGUID;
    if IsEqualGUID(GSettings.D3D3DDevGUID, Desc.DeviceGUID) then
      Select3DDev:=High(Names);
  end;
  Result:=D3DENUMRET_OK;
end;


function DDrawEnumProc(lpGUID: PGUID; lpDriverDesc: PChar; lpDriverName: PChar; lpContext: Pointer; hm: HMonitor): BOOL; stdcall;
var
  DDraw: IDirectDraw7;
  D3D: IDirect3D7;
  Caps: TDDCaps;
begin
  Result:=True;
  DDraw:=nil;
  if Succeeded(DirectDrawCreateEx(lpGUID, DDraw, IID_IDirectDraw7, nil)) then with PConfigWindow(lpContext)^ do begin
    ZeroMemory(@Caps, SizeOf(Caps));
    Caps.dwSize:=SizeOf(TDDCaps);
    if Succeeded(DDraw.GetCaps(@Caps, nil)) then begin
      if (Caps.dwCaps and DDCAPS_3D)=DDCAPS_3D then begin
        D3D:=nil;
        if Succeeded(DDraw.QueryInterface(IID_IDirect3D7, D3D)) then begin
          SetLength(DDDevices, Length(DDDevices) + 1);
          if Assigned(lpGUID) then
            DDDevices[High(DDDevices)].GUID:=lpGUID^
          else
            ZeroMemory(@DDDevices[High(DDDevices)].GUID, SizeOf(TGUID));
          if IsEqualGUID(GSettings.D3DDevGUID, DDDevices[High(DDDevices)].GUID) then
            SelectDDev:=High(DDDevices);
          DDDevices[High(DDDevices)].Names:=nil;
          DDDevices[High(DDDevices)].GUIDs:=nil;
          DDDevices[High(DDDevices)].Select3DDev:=-1;
          D3D.EnumDevices(@D3DDevEnumProc, @DDDevices[High(DDDevices)]);
          if DDDevices[High(DDDevices)].Select3DDev=-1 then
            DDDevices[High(DDDevices)].Select3DDev:=High(DDDevices[High(DDDevices)].Names);
          DirectDraw3DDevice^.AddString(string(lpDriverDesc) + ' (' + string(lpDriverName) + ')');
          D3D:=nil;
        end;
      end;
    end;
    DDraw:=nil;
  end;
end;
  
{ TConfigWindow }
constructor TConfigWindow.Init;
var 
  Text: PStatic;
begin
  inherited Init(nil, 'Post Apocalyptic Petra Settings');
  // Setup window style and area
  Attr.Style:=Attr.Style or ws_DlgFrame;
  Attr.Style -= ws_MaximizeBox or ws_ThickFrame;
  Resize(400, 390);
  MoveToCenter;
  // Create window controls
  New(Text, Init(@Self, 0, 'Direct3D settings:', 10, 10, 240, 19));
  New(Text, Init(@Self, 0, 'DirectDraw 3D Device:', 10, 33, 140, 19));
  New(Text, Init(@Self, 0, 'Direct3D Device:', 10, 58, 140, 19));
  New(Text, Init(@Self, 0, 'Color Depth:', 10, 83, 140, 19));
  DirectDraw3DDevice:=New(PComboBox, Init(@Self, 0, 150, 30, 235, 125, [coDropDownList]));
  Direct3DDevice:=New(PComboBox, Init(@Self, 0, 150, 55, 235, 125, [coDropDownList]));
  ColorDepthAuto:=New(PRadioButton, Init(@Self, 0, 'Auto', 150, 78, 60, 25));
  ColorDepth16:=New(PRadioButton, Init(@Self, 0, '16bit', 220, 78, 60, 25));
  ColorDepth32:=New(PRadioButton, Init(@Self, 0, '32bit', 290, 78, 60, 25));
  New(Text, Init(@Self, 0, 'OpenGL and Direct3D settings:', 10, 118, 240, 19));
  BilinearFilter:=New(PCheckBox, Init(@Self, 0, 'Bilinear texture filtering', 10, 138, 140, 19, [coAutoCheck]));
  New(Text, Init(@Self, 0, 'Game settings:', 10, 178, 240, 19));
  New(Text, Init(@Self, 0, 'Speech text speed:', 10, 201, 140, 19));
  New(Text, Init(@Self, 0, 'Mouse speed:', 10, 226, 140, 19));
  New(Text, Init(@Self, 0, 'User map:', 10, 280, 140, 19));
  TextSpeed:=New(PComboBox, Init(@Self, 0, 150, 198, 235, 80, [coDropDownList]));
  MouseSpeed:=New(PScrollBar, Init(@Self, 0, 150, 223, 235, 19, True));
  DisableSound:=New(PCheckBox, Init(@Self, 0, 'Disable sound', 10, 250, 140, 19, [coAutoCheck]));
  UserMap:=New(PComboBox, Init(@Self, 0, 150, 277, 235, 100, [coDropDownList]));
  ResetToDefault:=New(PButton, Init(@Self, 0, '&Reset to Defaults', 35, 320, 100, 25, False));
  SaveAndExit:=New(PButton, Init(@Self, 0, 'Save and &Exit', 145, 320, 100, 25, True));
  CancelAndExit:=New(PButton, Init(@Self, 0, '&Cancel and Exit', 255, 320, 100, 25, False));
end;

procedure TConfigWindow.SetupWindow;
const
  FirstTime: Boolean = True;
var
  F: TSearchRec;
  R, SelectUserMap: Integer;
begin
  if FirstTime then begin
    inherited SetupWindow;
    FirstTime:=False;
    // Add DirectDraw 3D devices
    SelectDDev:=-1;
    DirectDrawEnumerateEx(@DDrawEnumProc, @Self, DDENUM_ATTACHEDSECONDARYDEVICES or DDENUM_DETACHEDSECONDARYDEVICES or DDENUM_NONDISPLAYDEVICES);
    // Add the text speed options
    TextSpeed^.AddString('Normal');
    TextSpeed^.AddString('Fast');
    TextSpeed^.AddString('Instant');
    // Setup mouse scrollbar
    MouseSpeed^.SetRange(1, 1000);
    // Add user maps
    UserMap^.AddString('(none)');
    R:=FindFirst('User\*.map', faAnyFile, F);
    SelectUserMap:=0;
    if R=0 then begin
      while R=0 do begin
        if GSettings.CustomMap=Copy(F.Name, 1, Pos('.', F.Name) - 1) then
          SelectUserMap:=UserMap^.GetCount;
        UserMap^.AddString(Copy(F.Name, 1, Pos('.', F.Name) - 1));
        R:=FindNext(F);
      end;
      FindClose(F);
    end;
    UserMap^.SetSelIndex(SelectUserMap);
  end;
  // Fill in current settings
  with GSettings do begin
    if BilinearFiltering then
      BilinearFilter^.SetCheck(csChecked)
    else
      BilinearFilter^.SetCheck(csUnchecked);
    if SelectDDev=-1 then SelectDDev:=0;
    DirectDraw3DDevice^.SetSelIndex(SelectDDev);
    ProcessNotification(DirectDraw3DDevice, cn_Changed);
    if D3DColorDepth=16 then
      ColorDepth16^.SetCheck(csChecked)
    else if D3DColorDepth=32 then
      ColorDepth32^.SetCheck(csChecked)
    else
      ColorDepthAuto^.SetCheck(csChecked);
    TextSpeed^.SetSelIndex(Ord(SpeechTextSpeed));
    MouseSpeed^.SetPosition(MouseRotateSpeed);
    if NoSound then
      DisableSound^.SetCheck(csChecked)
    else
      DisableSound^.SetCheck(csUnchecked);
  end;
end;

function TConfigWindow.ProcessNotification(Child: PWindowsObject; NotifyCode: Integer): Boolean;
var
  I: Integer;
begin
  Result:=False;
  // A control was changed
  if NotifyCode=cn_Changed then begin
    if PComboBox(Child)=DirectDraw3DDevice then begin
      Direct3DDevice^.ClearList;
      if DirectDraw3DDevice^.GetSelIndex <> -1 then 
        with DDDevices[DirectDraw3DDevice^.GetSelIndex] do begin
          for I:=0 to High(Names) do
            Direct3DDevice^.AddString(Names[I]);
          Direct3DDevice^.SetSelIndex(DDDevices[DirectDraw3DDevice^.GetSelIndex].Select3DDev);
        end;
    end;
  end;
  // A button was clicked
  if NotifyCode=cn_Clicked then begin
    // Check if it was any of the pushbuttons
    if PButton(Child)=ResetToDefault then begin
      ResetSettings;
      if Length(DDDevices) > 0 then begin
        SelectDDev:=0;
        DDDevices[0].Select3DDev:=High(DDDevices[0].Names);
      end;
      SetupWindow;
    end;
    if (PButton(Child)=SaveAndExit) or (PButton(Child)=CancelAndExit) then begin
      // If Save and Exit was checked, save the settings file
      if PButton(Child)=SaveAndExit then SaveTheSettings;
      // Terminate the application
      Application^.Terminate;
    end;
    // The event was handled
    Result:=True;
  end;
end;

procedure TConfigWindow.SaveTheSettings;
begin
  // Fill settings from UI
  ResetSettings;
  with GSettings do begin
    BilinearFiltering:=BilinearFilter^.GetCheck=csChecked;
    if DirectDraw3DDevice^.GetSelIndex <> -1 then begin
      D3DDevGUID:=DDDevices[DirectDraw3DDevice^.GetSelIndex].GUID;
      if Direct3DDevice^.GetSelIndex <> -1 then
        D3D3DDevGUID:=DDDevices[DirectDraw3DDevice^.GetSelIndex].GUIDs[Direct3DDevice^.GetSelIndex];
    end;
    if ColorDepthAuto^.GetCheck=csChecked then
      D3DColorDepth:=0
    else if ColorDepth16^.GetCheck=csChecked then
      D3DColorDepth:=16
    else if ColorDepth32^.GetCheck=csChecked then
      D3DColorDepth:=32;
    SpeechTextSpeed:=TSpeechTextSpeed(TextSpeed^.GetSelIndex);
    MouseRotateSpeed:=MouseSpeed^.GetPosition;
    NoSound:=DisableSound^.GetCheck <> csUnchecked;
    if UserMap^.GetSelIndex < 1 then
      CustomMap:=''
    else
      CustomMap:=UserMap^.GetSelString;
  end;
  // Save settings file
  SaveSettings;
end;

{ TConfigApp }
procedure TConfigApp.InitMainWindow;
begin
  // Create the main window
  MainWindow:=New(PConfigWindow, Init);
end;

var
  App: TConfigApp;
begin
  // Load settings file
  LoadSettings;
  // Initialize application
  App.Init('Post Apocalyptic Petra Settings');
  // Enter main loop
  App.Run;
  // Shut down application
  App.Done;
end.
