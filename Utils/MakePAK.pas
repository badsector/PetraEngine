{ Create a package file from the Data directory }
program MakePak;
{$MODE OBJFPC}{$H+}
uses
  Classes, SysUtils;

var
  Files: array of string;
  Offsets, Sizes: array of Cardinal;
  Pak: TFileStream;
  Base: Int64;
  I: Integer;

procedure AddFile(AFileName: string);
var
  F: TFileStream;
begin
  Writeln('Adding ', AFileName);
  SetLength(Files, Length(Files) + 1);
  SetLength(Offsets, Length(Files));
  SetLength(Sizes, Length(Files));
  Files[High(Files)]:=StringReplace(AFileName, '\', '/', [rfReplaceAll]);
  Offsets[High(Offsets)]:=Pak.Position;
  F:=TFileStream.Create(AFileName, fmOpenRead);
  Pak.CopyFrom(F, F.Size);
  Sizes[High(Sizes)]:=F.Size;
  F.Free;
end;

procedure Scan(APrefix: string);
var
  Fi: TRawByteSearchRec;
  R: LongInt;
begin
  Writeln('Scanning ', APrefix);
  R:=FindFirst(APrefix + DirectorySeparator + AllFilesMask, faAnyFile, Fi);
  if R <> 0 then Exit;
  while R=0 do begin
    if (Fi.Name='.') or (Fi.Name='..') then begin
      R:=FindNext(Fi);
      Continue;
    end;
    if (Fi.Attr and faDirectory)=faDirectory then
      Scan(APrefix + DirectorySeparator + Fi.Name)
    else
      AddFile(APrefix + DirectorySeparator + Fi.Name);
    R:=FindNext(Fi);
  end;
  FindClose(Fi);
end;

begin
  Pak:=TFileStream.Create('data.pak', fmCreate);
  Pak.WriteBuffer('PEPF', 4);
  Pak.WriteDWord(0);
  if ParamCount > 0 then begin
    for I:=1 to ParamCount do Scan(ParamStr(I));
  end else Scan('Data');
  Base:=Pak.Position;
  Pak.WriteDWord(Length(Files));
  for I:=0 to High(Files) do begin
    Pak.WriteAnsiString(Files[I]);
    Pak.WriteDWord(Offsets[I]);
    Pak.WriteDWord(Sizes[I]);
  end;
  Pak.Position:=4;
  Pak.WriteDWord(Base);
  Pak.Free;
end.

