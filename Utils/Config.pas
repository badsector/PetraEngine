{ DOS/console-based configuration utility }
program Config;
{$MODE OBJFPC}{$H+}
{$UNITPATH Game}
uses
  Crt, Dos, SysUtils, CrtUtil, Settings;

const
  GameEXE = 'petra.exe';

{ Subscreens }

procedure ReadTextFile;
var
  FileName: string;
begin
  FileName:=PickFile('Select a text file', 'Read ', 'User\*.txt');
  if FileName <> '' then DisplayFile('User\' + FileName);
end;

procedure SoundSettings;
var
  Selection: Integer;
  EnableSound: string;
begin
  while True do begin
    // Enable sound menu option
    if GSettings.NoSound then
      EnableSound:='Enable Sound (currently disabled)'
    else
      EnableSound:='Disable sound (currently enabled)';
    // Show menu
    Selection:=RunMenu('Sound Settings', [
      EnableSound,
      'Sound Blaster 16 Port (' + HexStr(GSettings.SB16Port, 3) + ')',
      'Sound Blaster 16 Stereo DMA (' + IntToStr(GSettings.SB16DMA) + ')',
      'Sound Blaster 16 IRQ (' + IntToStr(GSettings.SB16IRQ) + ')',
      'Go Back']);
    case Selection of
      0: GSettings.NoSound:=not GSettings.NoSound;
      1: case RunMenu('Select Port', ['220', '240', '260', '280', 'Cancel']) of
        0: GSettings.SB16Port:=$220;
        1: GSettings.SB16Port:=$240;
        2: GSettings.SB16Port:=$260;
        3: GSettings.SB16Port:=$280;
      end;
      2: case RunMenu('Select DMA', ['5', '6', '7', 'Cancel']) of
        0: GSettings.SB16DMA:=5;
        1: GSettings.SB16DMA:=6;
        2: GSettings.SB16DMA:=7;
      end;
      3: case RunMenu('Select IRQ', ['2', '5', '7', 'Cancel']) of
        0: GSettings.SB16IRQ:=2;
        1: GSettings.SB16IRQ:=5;
        2: GSettings.SB16IRQ:=7;
      end;
      -1, 4: Break;
    end;
  end;
end;

procedure GameplaySettings;
var
  Selection: Integer;
begin
  // Show menu
  Selection:=RunMenu('Gameplay Settings', [
    'Set Text Speed to Normal'#1'Set the speed that text appears in speech boxes to normal',
    'Set Text Speed to Fast'#1'Set the speed that text appears in speech boxes to fast',
    'Set Text Speed to Instant'#1'Make text in speech boxes appear instantly',
    'Go Back']);
  case Selection of
    0: GSettings.SpeechTextSpeed:=stsNormal;
    1: GSettings.SpeechTextSpeed:=stsFast;
    2: GSettings.SpeechTextSpeed:=stsInstant;
  end;
end;

{ Main screen }

var
  Selection: Integer;       // Current selection
  ExitCommand: string = ''; // The command to run at exit
  ExitParams: string = '';  // Parameter to exit command
  {$IFDEF GO32V2}
  F: Text;
  {$ENDIF}

begin
  // Load settings file
  LoadSettings;
  // Initialize
  TextMode(CO80);
  Color(7, 0);
  ClrScr;
  // Special mode for showing the readme file
  if LowerCase(ParamStr(1))='/readme' then begin
    DrawBackground('Post Apocalyptic Petra Configuration Utility');
    DisplayFile('readme.txt');
    TextMode(CO80);
    Color(7, 0);
    ClrScr;
    exit;
  end;
  ExitCommand:='';
  repeat
    // Draw background
    DrawBackground('Post Apocalyptic Petra Configuration Utility');
    // Show the main menu
    Selection:=RunMenu('Main Menu', [
      'Start the game'#1'Save the settings and start playing Petra',
      'Read Me'#1'Display the README.TXT file',
      'Run user map'#1'Save the settings and select a .MAP file from the User directory to play',
      'Read text file'#1'Display the contents of a text file in the User directory',
      'Sound settings'#1'Configure Sound Blasic 16 settings',
      'Gameplay settings'#1'Configure gameplay settings such as text speed',
      'Save and exit'#1'Save the settings and exit from this utility',
      'Exit without save'#1'Exit this utility without saving the settings'
    ]);
    // Handle selection
    case Selection of
      0: begin
        ExitCommand:=GameEXE;
        SaveSettings;
        Break;
      end;
      1: DisplayFile('readme.txt');
      2: begin
        ExitParams:=PickFile('Select map', 'Play map', 'User\*.map');
        if ExitParams <> '' then begin
          ExitCommand:=GameEXE;
          ExitParams:='-map ' + Copy(ExitParams, 1, Length(ExitParams) - 4);
          SaveSettings;
          Break;
        end;
      end;
      3: ReadTextFile;
      4: SoundSettings;
      5: GameplaySettings;
      6: begin
        SaveSettings;
        Break;
      end;
      -1, 7: Break;
    end;
  until False;
  // Erase screen
  TextMode(CO80);
  Color(7, 0);
  ClrScr;
  // Handle exit command
  if ExitCommand <> '' then begin
    // For DOS we need to create a batch file that will be called
    // by the main batch file that calls config
    {$IFDEF GO32V2}
    Assign(F, '_petrun.bat');{$I-}ReWrite(F);{$I+}
    if IOResult=0 then begin
      Writeln(F, '@ECHO OFF');
      Writeln(F, ExitCommand, ' ', ExitParams);
      Close(F);
    end;
    // For other OSes call the executable directly
    {$ELSE}
    Exec(ExitCommand, ExitParams);
    {$ENDIF}
  end;
end.
