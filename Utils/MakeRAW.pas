program MakeRAW;

{$mode objfpc}{$H+}{$M+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  Classes, SysUtils, CustApp,
  FPImage, FPReadBMP, FPReadGif, FPReadJPEG, FPReadPCX, FPReadPNG, FPReadPNM,
  FPReadXPM, FPReadTGA, FPReadTiff
  { you can add units after this };

type
  { Exceptions }
  EImageConversionError = class(Exception);

  { TRGBColor - 8bcc RGB color }
  TRGBColor = record
    Red, Green, Blue: Byte;
  end;

  { TRGBPalette - 8bit color palette }
  TRGBPalette = object
    // The palette colors
    Colors: array [0..255] of TRGBColor;
    // Reset the palette to the default one used by Post Apocalyptic Petra
    procedure Reset;
    // Load the given JASC palette
    procedure LoadJASCPalette(AFileName: string);
    // Find closest color starting at the given index
    function ClosestColor(R, G, B: Byte; Start: Integer): Integer;
  end;

  { TRAWMakerTool - Tool for converting images to 8bit RAW files }
  TRAWMakerTool = class(TCustomApplication)
  protected
    procedure DoRun; override;
  public
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
    procedure WriteHelp; virtual;
  end;

{ Functions }
function ReadImageFromFile(AFileName: string): TFPCustomImage;
var
  I: Integer;
  ReaderClass: TFPCustomImageReaderClass;
  Reader: TFPCustomImageReader;
  F: TFileStream;
  Ok: Boolean;
begin
  for I:=0 to ImageHandlers.Count - 1 do begin
    ReaderClass:=ImageHandlers.ImageReader[ImageHandlers.TypeNames[I]];
    if not Assigned(ReaderClass) then Continue;
    try
      F:=TFileStream.Create(AFileName, fmOpenRead);
      Reader:=ReaderClass.Create;
      Ok:=Reader.CheckContents(F);
    except
      Reader.Free;
      F.Free;
    end;
    if not Ok then begin
      Reader.Free;
      F.Free;
      Continue;
    end;
    try
      F.Position:=0;
      Exit(Reader.ImageRead(F, nil));
    finally
      Reader.Free;
      F.Free;
    end;
  end;
  raise EImageConversionError.Create('Unsupported image format');
end;

{ TRGBPalette }

procedure TRGBPalette.Reset;
const
  PaletteTriplets: array [0..767] of byte = (
0,0,0,0,0,128,0,0,255,170,170,255,0,64,0,0,128,0,0,255,0,64,0,0,128,0,0,255,0,0,0,64,64,0,128,128,128,64,0,255,128,0,255,255,0,255,255,255
,8,8,8,17,17,17,34,34,34,51,51,51,68,68,68,85,85,85,102,102,102,119,119,119,136,136,136,153,153,153,170,170,170,187,187,187,204,204,204,221,221,221,238,238,238,255,255,255
,3,2,1,23,17,14,43,33,27,63,48,41,83,64,54,103,79,67,120,93,79,137,107,92,154,121,104,171,135,117,187,148,129,204,162,142,221,176,154,238,190,167,246,216,192,254,242,218
,85,9,30,109,37,45,132,64,60,156,92,75,180,119,91,204,147,106,227,174,121,251,202,136,251,207,148,252,213,160,252,218,172,252,224,184,253,229,197,253,234,209,254,240,221,254,245,233
,21,0,0,68,0,0,115,0,0,161,0,0,208,0,0,255,0,0,255,72,72,255,143,143,255,215,215,0,17,0,0,76,0,0,136,0,0,196,0,0,255,0,92,255,92,185,255,185
,0,13,13,0,61,61,0,110,110,0,158,158,0,207,207,0,255,255,68,255,255,136,255,255,204,255,255,0,0,15,36,36,55,72,72,95,108,108,135,145,145,175,181,181,215,217,217,255
,0,0,17,0,0,65,0,0,112,0,0,160,0,0,207,0,0,255,66,81,255,133,163,254,199,244,254,18,0,36,41,9,72,64,18,109,86,28,146,109,37,182,132,46,218,155,55,255
,15,1,0,32,11,0,48,20,0,64,30,0,81,40,0,115,57,0,149,74,0,183,91,0,217,108,0,225,128,40,232,149,79,240,169,119,247,190,158,255,210,198,255,222,216,255,235,234
,53,53,53,66,44,44,78,35,35,90,26,26,103,18,18,116,9,9,128,0,0,153,51,0,179,102,0,204,153,0,230,204,0,255,255,0,191,255,64,128,255,128,64,255,191,0,255,255
,20,20,20,59,59,17,98,98,13,138,138,10,177,177,7,216,216,3,41,22,3,84,54,25,127,87,47,169,119,69,212,152,91,255,184,113,255,192,134,254,200,156,254,208,178,254,216,199
,23,0,23,45,0,45,66,0,66,88,0,88,109,0,109,131,0,131,152,0,152,174,0,174,164,0,82,177,18,98,190,37,113,203,55,129,216,73,145,229,91,161,242,110,176,255,128,192
,17,17,17,23,23,33,28,28,49,34,34,65,39,39,80,45,45,96,50,50,112,56,56,128,61,61,144,67,67,160,72,72,176,78,78,192,83,83,207,89,89,223,94,94,239,100,100,255
,128,64,64,119,70,70,111,76,76,102,82,82,94,88,88,85,94,94,77,100,100,68,106,106,60,113,113,51,119,119,43,125,125,34,131,131,26,137,137,17,143,143,9,149,149,0,155,155
,58,7,7,71,19,14,84,31,21,97,42,28,111,54,35,124,66,42,137,78,49,150,90,56,163,101,64,176,113,71,189,125,78,202,137,85,216,149,92,229,160,99,242,172,106,255,184,113
,255,188,121,255,192,128,255,195,136,255,199,144,255,203,151,255,207,159,255,211,167,255,214,174,255,218,182,255,222,190,255,226,198,255,230,205,255,234,213,255,237,221,255,241,228,255,245,236
,1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,7,7,7,8,8,8,10,10,10,11,11,11,12,12,12,13,13,13,14,14,14,15,15,15,16,16,16,113,26,26);
var
  I: Integer;
begin
  for I:=0 to 255 do with Colors[I] do begin
    Red:=PaletteTriplets[I*3];
    Green:=PaletteTriplets[I*3 + 1];
    Blue:=PaletteTriplets[I*3 + 2];
  end;
end;

procedure TRGBPalette.LoadJASCPalette(AFileName: string);
var
  F: TextFile;
  S: string;
  I: Integer;
begin
  Reset;
  AssignFile(F, AFileName);
  {$I-}
  System.Reset(F);
  {$I+}
  if IOResult <> 0 then raise EImageConversionError.Create('Failed to open ' + AFileName);
  try
    Readln(F, S);
    if S <> 'JASC-PAL' then raise EImageConversionError.Create('Not a JASC palette file: ' + AFileName);
    Readln(F, S);
    if S <> '0100' then raise EImageConversionError.Create('Unsupported JASC palette version in ' + AFileName);
    Readln(F, S);
    if S <> '256' then raise EImageConversionError.Create(AFileName + ' is not a 256 color JASC palette');
    for I:=0 to 255 do with Colors[I] do begin
      Readln(F, S);
      S:=Trim(S);
      Red:=StrToIntDef(Copy(S, 1, Pos(' ', S) - 1), 0);
      S:=Trim(Copy(S, Pos(' ', S), Length(S)));
      Green:=StrToIntDef(Copy(S, 1, Pos(' ', S) - 1), 0);
      S:=Trim(Copy(S, Pos(' ', S), Length(S)));
      Blue:=StrToIntDef(S, 0);
    end;
  finally
    CloseFile(F);
  end;
end;

function TRGBPalette.ClosestColor(R, G, B: Byte; Start: Integer): Integer;
var
  Dist, BestDist: Double;
begin
  Result:=Start;
  Dist:=MaxInt;
  BestDist:=Dist;
  while Start < 256 do with Colors[Start] do begin
    Dist:=Sqr((Integer(R) - Integer(Red))) + Sqr((Integer(G) - Integer(Green))) + Sqr((Integer(B) - Integer(Blue)));
    if Dist < BestDist then begin
      BestDist:=Dist;
      Result:=Start;
    end;
    Inc(Start);
  end;
end;

{ TRAWMakerTool }

procedure TRAWMakerTool.DoRun;
var
  ErrorMsg: String;
  Pal: TRGBPalette;
  StartIndex: Integer = 16;
  Image: TFPCustomImage;
  F: TFileStream;
  RGB: TFPColor;
  X, Y: Integer;
begin
  // Quick check parameters
  ErrorMsg:=CheckOptions('hfi:o:p:', ['help', 'full', 'input::', 'output::', 'palette::']);
  if ErrorMsg<>'' then begin
    ShowException(Exception.Create(ErrorMsg));
    Terminate;
    Exit;
  end;

  // Parse parameters
  if HasOption('h', 'help') then begin
    WriteHelp;
    Terminate;
    Exit;
  end;
  if not (HasOption('i', 'input') and HasOption('o', 'output')) then begin
    Writeln('Missing --input or --output');
    WriteHelp;
    Terminate;
    Exit;
  end;
  if HasOption('f', 'full') then StartIndex:=0;

  // Load the image
  try
    Image:=ReadImageFromFile(GetOptionValue('i', 'input'));
  except
    Writeln('Image read error: ' + Exception(ExceptObject).Message);
    Terminate;
    Exit;
  end;

  // Load the palette, if provided
  if HasOption('p', 'palette') then
    Pal.LoadJASCPalette(GetOptionValue('p', 'palette'))
  else
    Pal.Reset;

  // Create the raw file
  try
    try
      F:=TFileStream.Create(GetOptionValue('o', 'output'), fmCreate);
      for Y:=0 to Image.Height - 1 do
        for X:=0 to Image.Width - 1 do begin
          RGB:=Image.Colors[X, Y];
          F.WriteByte(Pal.ClosestColor(RGB.Red shr 8, RGB.Green shr 8, RGB.Blue shr 8, StartIndex));
        end;
    finally
      F.Free;
    end;
  except
    Writeln('Conversion error: ' + Exception(ExceptObject).Message);
    WriteHelp;
  end;

  // Stop program loop
  Terminate;
end;

constructor TRAWMakerTool.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  StopOnException:=True;
end;

destructor TRAWMakerTool.Destroy;
begin
  inherited Destroy;
end;

procedure TRAWMakerTool.WriteHelp;
var
  I: Integer;
begin
  Writeln('Usage: ', ExtractFileName(ExeName), ' [options]');
  Writeln('Options:');
  Writeln('-i <x> or --input=<x>        Input filename');
  Writeln('-o <x> or --output=<x>       Output filename');
  Writeln('-p <x> or --palette=<x>      JASC palette file to use');
  Writeln('-f or --full                 Use full palette (instead of 16..255)');
  Writeln('Supported image formats:');
  for I:=0 to ImageHandlers.Count - 1 do
    if Assigned(ImageHandlers.ImageReader[ImageHandlers.TypeNames[I]]) then
      Write(LowerCase(ImageHandlers.DefaultExtension[ImageHandlers.TypeNames[I]]), ' ');
  Writeln;
end;

var
  Application: TRAWMakerTool;
begin
  Application:=TRAWMakerTool.Create(nil);
  Application.Title:='Palette Conversion Tool';
  Application.Run;
  Application.Free;
end.

