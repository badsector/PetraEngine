#!/usr/bin/env bash

if [ ! -e "PetraEngine/Scripts/new-game.sh" ]
then
    echo "The new-game.sh script must be called from the same directory where the Petra"
    echo "Engine directory (PetraEngine) is placed (e.g. if the engine is in the"
    echo "~/Code/PetraEngine directory then the script must be called from ~/Code)"
    exit 1
fi

gamedir=$1

if [ "$gamedir" = "" ]
then
    echo "Usage: dirname"
    echo "Directory must not exist"
    exit 1
fi

if [ -d "$gamedir" ]
then
    echo "The game directory $gamedir already exists."
    exit 1
fi

mkdir -p $gamedir

cp -R PetraEngine/GameData $gamedir/
cp -R PetraEngine/GameSrc $gamedir/
cp -R PetraEngine/*.sh $gamedir/
