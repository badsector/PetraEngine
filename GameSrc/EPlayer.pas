{ Player entity }
unit EPlayer;

interface

uses
  Classes, Serial, Maths, Engine, Scene, World, InputDrv;

type
  { TPlayer - entity for the player }
  TPlayer = class(TSceneNodeEntity, TInputHandler)
  protected
    // TEntity methods
    procedure AddedToWorld; override;
    procedure RemovedFromWorld; override;
    procedure AddSceneNodes; override;
    procedure RemoveSceneNodes; override;
    procedure Update; override;
    procedure Frame; override;
  protected
    // TSceneNodeEntity methods
    function CreateSceneNode: TSceneNode; override;
  public
    // TEntity methods
    function PerformCommand(ACommand: string): Boolean; override;
  public
    // TInputHandler methods
    function KeyDown(Key: TInputKey): Boolean;
    function KeyUp(Key: TInputKey): Boolean;
    function Mouse(X, Y: Integer): Boolean;
  public
    // Create the entity
    constructor Create; override;
    // Destroy the entity
    destructor Destroy; override;
  end;

implementation

{ TPlayer }
constructor TPlayer.Create;
begin
  inherited Create;
  // Set the entity name
  Name:='Player';
  // Load any resources here
end;

destructor TPlayer.Destroy;
begin
  // Free any resources here
  inherited Destroy;
end;

procedure TPlayer.AddedToWorld;
begin
  inherited AddedToWorld;
  // Add the player as an input handler
  GInput.AddHandler(Self);
  // Store the player entity
  GGame.Player:=Self;
end;

procedure TPlayer.RemovedFromWorld;
begin
  // Remove Petra from being the player entity
  if GGame.Player=Self then GGame.Player:=nil;
  // Remove Petra from being an input handler
  GInput.RemoveHandler(Self);
  inherited RemovedFromWorld;
end;

procedure TPlayer.AddSceneNodes;
begin
  inherited AddSceneNodes;
  // Add scene nodes for the player entity
end;

procedure TPlayer.RemoveSceneNodes;
begin
  // Remove any scene nodes
  inherited RemoveSceneNodes;
end;

procedure TPlayer.Update;
begin
  // Fixed updates
  inherited Update;
  // Set the camera to the player's position
  Scene.Camera.Position:=Vector(PositionX, PositionY, PositionZ);
end;

procedure TPlayer.Frame;
begin
  // Per-frame updates
  inherited Frame;
end;

function TPlayer.CreateSceneNode: TSceneNode;
begin
  // Create a scene node for the entity
  Result:=nil;
end;

function TPlayer.PerformCommand(ACommand: string): Boolean;
begin
  if inherited PerformCommand(ACommand) then Exit(True);
  // Handle commands for the player
  Result:=False;
end;

function TPlayer.KeyDown(Key: TInputKey): Boolean;
begin
  // Player key press handler
  Result:=False;
end;

function TPlayer.KeyUp(Key: TInputKey): Boolean;
begin
  // Player key release handler
  Result:=False;
end;

function TPlayer.Mouse(X, Y: Integer): Boolean;
begin
  Result:=False;
end;

initialization
  // Register the player entity class
  RegisterSerializableClass(TPlayer);
end.
