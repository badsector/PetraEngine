{ Game side for the game }
unit Game;

interface

uses
  DataLoad, Misc, Serial, GameBase, Scripts, InputDrv;

type
  { TGame - Main game class }
  TGame = class(TGameBase,TInputHandler)
  protected
    // TGameBase methods
    procedure InitGame; override;
    procedure ShutdownGame; override;
  public
    // TGameBase methods
    function RunScriptCommand(Script: TScript; Args: TStringArray): Boolean; override;
    procedure Update; override;
    procedure Frame; override;
    procedure PostFrame; override;
  public
    // TInputHandler methods
    function KeyDown(Key: TInputKey): Boolean;
    function KeyUp(Key: TInputKey): Boolean;
    function Mouse(X, Y: Integer): Boolean;
  public
    constructor Create; override;
    destructor Destroy; override;
  end;

implementation

uses
  // Game-specific units go here
  EPlayer,
  // End of game-specific units
  Settings, Engine, GfxDrv, Fonts, Maths;

{ TGame }
constructor TGame.Create;
begin
  inherited Create;
  // Create screens, etc here
end;

destructor TGame.Destroy;
begin
  // Delete screens, etc here
  inherited Destroy;
end;

procedure TGame.InitGame;
{$IFNDEF EDITOR}
var
  InitialMap: string;
  I: Integer;
{$ENDIF}
begin
  // Register input handler
  GInput.AddHandler(Self);
  {$IFDEF EDITOR}
  // Load game color palette if needed for the editor
  GRenderer.ColorPalette.LoadJASCPalette(DataDirectoryPrefix + 'colors.pal');
  GRenderer.ColorPalette.BuildLightTable(0, 0, 0, 1, Vector(1, 1, 1));
  {$ENDIF}

  {$IFNDEF EDITOR}
  // Load inital map
  InitialMap:='map1';
  // Check if a custom map is specified via the settings
  if GSettings.CustomMap <> '' then InitialMap:=GSettings.CustomMap;
  // Check if a custom map is specified in command line
  for I:=1 to ParamCount do
    if (I < ParamCount) and (LowerCase(ParamStr(I))='-map') then
      InitialMap:=ParamStr(I + 1);
  PlayMap(InitialMap);
  {$ENDIF}
end;

procedure TGame.ShutdownGame;
begin
  // Remove input handler
  GInput.RemoveHandler(Self);
  inherited ShutdownGame;
end;

function TGame.RunScriptCommand(Script: TScript; Args: TStringArray): Boolean;
begin
  if inherited RunScriptCommand(Script, Args) then Exit(True);
  // Handle game-wide script commands
  Result:=False;
end;

procedure TGame.Update;
begin
  inherited Update;
  // Put fixed update logic here
end;

procedure TGame.Frame;
begin
  inherited Frame;
  // Put per-frame update logic here that is
  // called during frame updates / 3D rendering
end;

procedure TGame.PostFrame;
begin
  inherited PostFrame;
  // Put per-frame update logic here that is
  // called after frame updates - text can be
  // drawn here too, e.g.

  if Assigned(GResourceManager.DefaultFont) then
    GRenderer.DrawText(10, VRes - 23, 
      'Petra Engine Template',
      TBitFont(GResourceManager.DefaultFont), 15, 0);

  // You could also draw HUD elements here too
  // using GRenderer.DrawHUDHudImage(...)
end;

function TGame.KeyDown(Key: TInputKey): Boolean;
begin
  // Global key press handling
  if Key=ikF10 then begin
    QuitEngine;
    Result:=True;
  end;
  Result:=False;
end;

function TGame.KeyUp(Key: TInputKey): Boolean;
begin
  // Global key release handling
  Result:=False;
end;

function TGame.Mouse(X, Y: Integer): Boolean;
begin
  // Global mouse motion handling
  Result:=False;
end;

initialization
  RegisterSerializableClass(TGame);
end.
