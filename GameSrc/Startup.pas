{ Game startup }
program Startup;

uses
  GameBoot;

begin
  BootGame('Petra Engine Template Game', 'GameData');
  RunGame;
  ShutdownGame;
end.
