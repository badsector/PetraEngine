unit UEdTableEditor;

{$mode objfpc}{$H+}

interface

uses
  Misc, Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ComCtrls, World, GameBase, Engine;

type

  { TTableEditor - Data table editor}
  TTableEditor = class(TForm)
    rbWorld: TRadioButton;
    rbGame: TRadioButton;
    Label1: TLabel;
    lvTable: TListView;
    btAdd: TButton;
    btEdit: TButton;
    btRename: TButton;
    btDelete: TButton;
    btClose: TButton;
    procedure rbWorldChange(Sender: TObject);
    procedure btAddClick(Sender: TObject);
    procedure btEditClick(Sender: TObject);
    procedure btRenameClick(Sender: TObject);
    procedure btDeleteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    // Current table
    Table: PStringKeyValueSet;
    // Select and read the data from the table
    procedure ReadTable;
  public

  end;

var
  TableEditor: TTableEditor;

implementation

{$R *.lfm}

{ TTableEditor }

procedure TTableEditor.rbWorldChange(Sender: TObject);
begin
  ReadTable;
end;

procedure TTableEditor.btAddClick(Sender: TObject);
var
  VName, Value: string;
  I: Integer;
begin
  // Ask for name
  VName:=Trim(LowerCase(InputBox('New Value', 'Enter name, leave empty to cancel. Value box will follow next.', '')));
  if VName='' then Exit;
  // Check if there is another key with that name
  I:=Table^.IndexOfKey(VName);
  if I <> -1 then begin
    ShowMessage('There is already another value with that key');
    Exit;
  end;
  // Ask for value
  Value:=InputBox('New Value', 'Enter value for ' + VName + ', leave empty to cancel.', '');
  if Value='' then Exit;
  // Set value and rebuild table
  Table^.SetValue(VName, Value);
  ReadTable;
end;

procedure TTableEditor.btEditClick(Sender: TObject);
var
  Index: Integer;
  Value: string;
begin
  if lvTable.ItemIndex=-1 then Exit;
  // Find key
  Index:=Table^.IndexOfKey(lvTable.Items[lvTable.ItemIndex].Caption);
  if Index=-1 then Exit;
  // Ask for value
  Value:=InputBox('New Value', 'Enter value for ' + Table^.Keys[Index] + ', set empty to delete.', Table^.Values[Index]);
  // If the value was empty, delete the value
  if Value='' then
    Table^.RemoveKeyAt(Index)
  else
    Table^.Values[Index]:=Value;
  ReadTable;
end;

procedure TTableEditor.btRenameClick(Sender: TObject);
var
  Index, I: Integer;
  Value: string;
begin
  if lvTable.ItemIndex=-1 then Exit;
  // Find key
  Index:=Table^.IndexOfKey(lvTable.Items[lvTable.ItemIndex].Caption);
  if Index=-1 then Exit;
  // Ask for new name
  Value:=LowerCase(InputBox('New Name', 'Enter new name for ' + Table^.Keys[Index] + '.', Table^.Keys[Index]));
  if Value=Table^.Keys[Index] then Exit;
  // Check if there is another key with that name
  I:=Table^.IndexOfKey(Value);
  if (I <> -1) and (I <> Index) then begin
    ShowMessage('There is already another value with that key');
    Exit;
  end;
  // Set the new name
  Table^.Keys[Index]:=Value;
  ReadTable;
end;

procedure TTableEditor.btDeleteClick(Sender: TObject);
var
  Index: Integer;
begin
  if lvTable.ItemIndex=-1 then Exit;
  // Find key
  Index:=Table^.IndexOfKey(lvTable.Items[lvTable.ItemIndex].Caption);
  if Index=-1 then Exit;
  Table^.RemoveKeyAt(Index);
  ReadTable;
end;

procedure TTableEditor.FormCreate(Sender: TObject);
begin
  ReadTable;
end;

procedure TTableEditor.ReadTable;
var
  Item: TListItem;
  I: Integer;
begin
  // Select proper table
  if rbWorld.Checked then Table:=GGame.World.DataTable else Table:=GGame.DataTable;
  // Fill the list
  lvTable.Clear;
  for I:=0 to High(Table^.Keys) do begin
    Item:=lvTable.Items.Add;
    Item.Caption:=Table^.Keys[I];
    Item.SubItems.Add(Table^.Values[I]);
  end;
end;

end.

