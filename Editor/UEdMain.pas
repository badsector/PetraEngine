unit UEdMain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, Menus, ExtCtrls,
  StdCtrls, ComCtrls, RTTIGrids, Engine, World, GridMap, Maths,
  Scene, Serial, Cameras, Game, ScrEnt, Types;

type

  { TWallTextureInfo - Information about a wall texture }
  TWallTextureInfo = record
    // The texture index
    Index: Integer;
    // The texture position in the textures tab
    X, Y: Integer;
    // Texture bitmap
    Bitmap: TBitmap;
  end;

  { TGridCellProxy - Proxy object for a grid cell }
  TGridCellProxy = class(TSerializable)
  private
    FGrid: TGrid;
    FCellX: Integer;
    FCellY: Integer;
    FFloorTexture: Integer;
    FCeilingTexture: Integer;
    FUpperTexture: Integer;
    FLowerTexture: Integer;
    FCellPart: TCellPart;
    function GetCeilingRotation: Integer;
    function GetFloorHeight: Single;
    function GetFloorRotation: Integer;
    function GetLowerRotation: Integer;
    function GetUpperRotation: Integer;
    procedure SetCeilingRotation(ACeilingRotation: Integer);
    procedure SetFloorHeight(AValue: Single);
    function GetCeilingHeight: Single;
    procedure SetCeilingHeight(AValue: Single);
    function GetCellOptions: TCellOptions;
    procedure SetCellOptions(AValue: TCellOptions);
    procedure SetFloorRotation(AFloorRotation: Integer);
    procedure SetLowerRotation(ALowerRotation: Integer);
    procedure SetUpperRotation(AUpperRotation: Integer);
  published
    property Grid: TGrid read FGrid;
    property CellX: Integer read FCellX;
    property CellY: Integer read FCellY;
    property FloorTexture: Integer read FFloorTexture;
    property CeilingTexture: Integer read FCeilingTexture;
    property UpperTexture: Integer read FUpperTexture;
    property LowerTexture: Integer read FLowerTexture;
    property FloorHeight: Single read GetFloorHeight write SetFloorHeight;
    property CeilingHeight: Single read GetCeilingHeight write SetCeilingHeight;
    property CellPart: TCellPart read FCellPart;
    property CellOptions: TCellOptions read GetCellOptions write SetCellOptions;
    property FloorRotation: Integer read GetFloorRotation write SetFloorRotation;
    property LowerRotation: Integer read GetLowerRotation write SetLowerRotation;
    property UpperRotation: Integer read GetUpperRotation write SetUpperRotation;
    property CeilingRotation: Integer read GetCeilingRotation write SetCeilingRotation;
  end;

  { TMain }
  TMain = class(TForm)
    MainMenu1: TMainMenu;
    MenuItem2: TMenuItem;
    mPlayPreviewAnimations: TMenuItem;
    mToolsShowOcclusionTests: TMenuItem;
    mToolsShowPortals: TMenuItem;
    mToolsCameraSequenceFinishAndCopyToClipboard: TMenuItem;
    mToolsCameraSequenceAddPosition: TMenuItem;
    mToolsCameraSequenceStart: TMenuItem;
    mToolsLockVisibilityUpdates: TMenuItem;
    mToolsForcePortalRecalculation: TMenuItem;
    mToolsUsePortals: TMenuItem;
    mToolsRenderEverything: TMenuItem;
    mToolsDisableTrilines: TMenuItem;
    mToolsShowStats: TMenuItem;
    mToolsWireframeMode: TMenuItem;
    mFile: TMenuItem;
    mFileNew: TMenuItem;
    mFileSave: TMenuItem;
    mFileSaveAs: TMenuItem;
    mFileExit: TMenuItem;
    mFileOpen: TMenuItem;
    MenuItem7: TMenuItem;
    plRenderContainer: TPanel;
    plRenderWindow: TPanel;
    ApplicationProperties1: TApplicationProperties;
    Panel2: TPanel;
    Separator1: TMenuItem;
    Separator2: TMenuItem;
    TIPropertyGrid1: TTIPropertyGrid;
    Splitter1: TSplitter;
    Label1: TLabel;
    lbInsertables: TListBox;
    Button1: TButton;
    Timer1: TTimer;
    mPlay: TMenuItem;
    mPlayPlayMode: TMenuItem;
    mPlayFreeCamera: TMenuItem;
    MenuItem10: TMenuItem;
    mPlayLookAtPlayer: TMenuItem;
    btSelectGrid: TButton;
    btNewGrid: TButton;
    btRelight: TButton;
    btSelectWorld: TButton;
    pcSidebar: TPageControl;
    tsObjects: TTabSheet;
    tsTextures: TTabSheet;
    pbTextures: TPaintBox;
    sbTextures: TScrollBar;
    pmTextures: TPopupMenu;
    pmTexturesSetToFloor: TMenuItem;
    pmTexturesSetToCeiling: TMenuItem;
    pmTexturesSetToUpper: TMenuItem;
    pmTexturesSetToLower: TMenuItem;
    mEntity: TMenuItem;
    mEntityCamera: TMenuItem;
    mEntityCameraFromView: TMenuItem;
    mEntityCameraToView: TMenuItem;
    mEntityEditScript: TMenuItem;
    mPlayPlayAtSelection: TMenuItem;
    cbCamGridOnly: TCheckBox;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    lbSelObj: TLabel;
    mGame: TMenuItem;
    mGameEditDataTables: TMenuItem;
    MenuItem1: TMenuItem;
    mPlayPreviewSounds: TMenuItem;
    mBookmarks: TMenuItem;
    mBookmarksDelete: TMenuItem;
    mBookmarksAdd: TMenuItem;
    mBookmarksBar: TMenuItem;
    tsEntities: TTabSheet;
    Label2: TLabel;
    lbEntityClasses: TListBox;
    cbListCamGridEntities: TCheckBox;
    lbEntityList: TListBox;
    mEntityMoveCameraToSelection: TMenuItem;
    MenuItem3: TMenuItem;
    cbListOnlyWithName: TCheckBox;
    MenuItem4: TMenuItem;
    pmTexturesPasteFromClipboard: TMenuItem;
    mTools: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ApplicationProperties1Idle(Sender: TObject; var Done: Boolean);
    procedure mPlayPreviewAnimationsClick(Sender: TObject);
    procedure mToolsCameraSequenceAddPositionClick(Sender: TObject);
    procedure mToolsCameraSequenceFinishAndCopyToClipboardClick(Sender: TObject);
    procedure mToolsCameraSequenceStartClick(Sender: TObject);
    procedure mToolsDisableTrilinesClick(Sender: TObject);
    procedure mToolsForcePortalRecalculationClick(Sender: TObject);
    procedure mToolsRenderEverythingClick(Sender: TObject);
    procedure mToolsShowOcclusionTestsClick(Sender: TObject);
    procedure mToolsShowPortalsClick(Sender: TObject);
    procedure mToolsShowStatsClick(Sender: TObject);
    procedure mToolsWireframeModeClick(Sender: TObject);
    procedure plRenderWindowClick(Sender: TObject);
    procedure mFileSaveClick(Sender: TObject);
    procedure mFileOpenClick(Sender: TObject);
    procedure plRenderWindowMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Button1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure mPlayPlayModeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var {%H-}CloseAction: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word; {%H-}Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word; {%H-}Shift: TShiftState);
    procedure plRenderWindowMouseMove(Sender: TObject; {%H-}Shift: TShiftState; X,
      Y: Integer);
    procedure plRenderWindowMouseUp(Sender: TObject; Button: TMouseButton;
      {%H-}Shift: TShiftState; {%H-}X, {%H-}Y: Integer);
    procedure mPlayFreeCameraClick(Sender: TObject);
    procedure mPlayLookAtPlayerClick(Sender: TObject);
    procedure btSelectGridClick(Sender: TObject);
    procedure btNewGridClick(Sender: TObject);
    procedure btRelightClick(Sender: TObject);
    procedure btSelectWorldClick(Sender: TObject);
    procedure pbTexturesPaint(Sender: TObject);
    procedure sbTexturesChange(Sender: TObject);
    procedure pbTexturesMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure pmTexturesSetToLowerClick(Sender: TObject);
    procedure pmTexturesSetToFloorClick(Sender: TObject);
    procedure pmTexturesSetToCeilingClick(Sender: TObject);
    procedure pmTexturesSetToUpperClick(Sender: TObject);
    procedure pmTexturesPasteFromClipboardClick(Sender: TObject);
    procedure mEntityCameraFromViewClick(Sender: TObject);
    procedure mEntityCameraToViewClick(Sender: TObject);
    procedure mEntityEditScriptClick(Sender: TObject);
    procedure mPlayPlayAtSelectionClick(Sender: TObject);
    procedure mFileNewClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure mFileSaveAsClick(Sender: TObject);
    procedure mFileExitClick(Sender: TObject);
    procedure mGameEditDataTablesClick(Sender: TObject);
    procedure mPlayPreviewSoundsClick(Sender: TObject);
    procedure mBookmarksClick(Sender: TObject);
    procedure mBookmarksAddClick(Sender: TObject);
    procedure mBookmarksDeleteClick(Sender: TObject);
    procedure pbTexturesMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure pcSidebarChange(Sender: TObject);
    procedure cbListCamGridEntitiesChange(Sender: TObject);
    procedure lbEntityClassesSelectionChange(Sender: TObject; User: boolean);
    procedure mEntityMoveCameraToSelectionClick(Sender: TObject);
    procedure lbEntityListSelectionChange(Sender: TObject; User: boolean);
    procedure lbEntityListDblClick(Sender: TObject);
    procedure cbListOnlyWithNameChange(Sender: TObject);
  private
    // Property storage
    FSelectedObject: TSerializable;
    FEditorCamera: TCamera;
    FCurrentFileName: string;
    procedure SetSelectedObject(AValue: TSerializable);
    procedure SetCurrentFileName(AValue: string);
    // Event handler for bookmark menu item
    procedure OnBookmarkClick(ASender: TObject);
  private
    // Proxy for grid cells
    CellProxy: TGridCellProxy;
    // Last selection AA box
    SelAABox: TAABox;
    // Real edited game instance (used to store the instance in play mode)
    RealGame: TGame;
    // True if we are in "looking around" mode
    LookAround: Boolean;
    // Stored mouse coordinates for various modes
    MouseX, MouseY: Integer;
    // Last mouse coordinates for various modes
    LastX, LastY: Integer;
    // If true, the editor camera locks to player character in free camera mode
    LockToPlayerInFreeCamera: Boolean;
    // If true, the selected object is being moved around
    Moving: Boolean;
    // The moving plane
    MovingPlane: TPlane;
    // The original intersection point on the plane
    MovingPlaneIP: TVector;
    // The original position for the object before started to move
    OriginalPosition: TVector;
    // Last selected grid cell settings
    LastGridCell: TCell;
    // Wall textures
    WallTextures: array of TWallTextureInfo;
    // Currently selected wall texture
    SelectedWallTexture: Integer;
    // Ignore startup script (this is set when playing teleported somewhere)
    IgnoreStartupScript: Boolean;
    // Last position the editor camera was in when a check for entity filtering
    // was performed
    LastEditorCamPosGridFilterCheck: TVector;
    // Holds the value of cbListCamGridEntities.Checked to avoid going through
    // the widgetset every tick
    ListCamGridEntitiesChecked: Boolean;
    // Camera sequence string
    CameraSequence: string;
    // Update wall textures
    procedure UpdateWallTextures;
    // Arrange wall textures
    procedure ArrangeWallTextures;
    // Restart animated entities
    procedure RestartAnimatedEntities;
  public
    // New world file
    procedure NewWorldFile;
    // Save the current world to the given file
    procedure SaveWorldFile(AFileName: string);
    // Load a world file from the given file
    procedure LoadWorldFile(AFileName: string);
    // Calculate portals
    procedure RecalculatePortals;
    // Relight the world
    procedure Relight;
    // Update the entity list
    procedure UpdateEntityList;
    // Update the entity list only if it is visible
    procedure UpdateEntityListIfVisible; inline;
    // Create a new grid at the given coordinates with the given dimensions
    // (note that the origin coordinates will be rounded to ensure the grid is
    // integer aligned)
    function CreateGridAt(AOrigin: TVector; AWidth, AHeight: Integer): TGrid;
    // Create a new entity of the given class at the given coordinates
    function CreateEntityAt(AEntityClass: TEntityClass; APosition: TVector): TEntity;
    // Called by the engine to render the editor-specific stuff
    procedure Render;
  public
    // Current filename
    property CurrentFileName: string read FCurrentFileName write SetCurrentFileName;
    // Currently selected object
    property SelectedObject: TSerializable read FSelectedObject write SetSelectedObject;
    // The editor camera
    property EditorCamera: TCamera read FEditorCamera;
  end;

var
  Main: TMain;

implementation

uses
  LCLIntf, LCLType, Textures, UEdInput, UEdGfx, CamEnt, UEdMultilineTextBox,
  UEdTableEditor, SndEmit, Clipbrd, FBRaster, Renderer;

{$R *.lfm}

{ TGridCellProxy }

function TGridCellProxy.GetCeilingHeight: Single;
begin
  Result:=Grid.CellHeight - Grid.Cells[CellX, CellY]^.CeilingHeight/100;
end;

function TGridCellProxy.GetFloorHeight: Single;
begin
  Result:=Grid.Cells[CellX, CellY]^.FloorHeight/100;
end;

function TGridCellProxy.GetCeilingRotation: Integer;
var
  FloorRot, LowerRot, UpperRot, CeilingRot: Integer;
begin
  Grid.Cells[CellX, CellY]^.GetRotations(FloorRot, LowerRot, UpperRot, CeilingRot);
  Result:=CeilingRot;
end;

function TGridCellProxy.GetFloorRotation: Integer;
var
  FloorRot, LowerRot, UpperRot, CeilingRot: Integer;
begin
  Grid.Cells[CellX, CellY]^.GetRotations(FloorRot, LowerRot, UpperRot, CeilingRot);
  Result:=FloorRot;
end;

function TGridCellProxy.GetLowerRotation: Integer;
var
  FloorRot, LowerRot, UpperRot, CeilingRot: Integer;
begin
  Grid.Cells[CellX, CellY]^.GetRotations(FloorRot, LowerRot, UpperRot, CeilingRot);
  Result:=LowerRot;
end;

function TGridCellProxy.GetUpperRotation: Integer;
var
  FloorRot, LowerRot, UpperRot, CeilingRot: Integer;
begin
  Grid.Cells[CellX, CellY]^.GetRotations(FloorRot, LowerRot, UpperRot, CeilingRot);
  Result:=UpperRot;
end;

procedure TGridCellProxy.SetCeilingRotation(ACeilingRotation: Integer);
var
  FloorRot, LowerRot, UpperRot, CeilingRot: Integer;
begin
  Grid.Cells[CellX, CellY]^.GetRotations(FloorRot, LowerRot, UpperRot, CeilingRot);
  Grid.Cells[CellX, CellY]^.SetRotations(FloorRot, LowerRot, UpperRot, ACeilingRotation);
  Grid.UpdateSceneNode;
end;

procedure TGridCellProxy.SetCeilingHeight(AValue: Single);
begin
  AValue:=(Grid.CellHeight - AValue)*100;
  Grid.Cells[CellX, CellY]^.CeilingHeight:=Clamp(Round(AValue), -32767, 32767);
  Grid.UpdateSceneNode;
end;

function TGridCellProxy.GetCellOptions: TCellOptions;
begin
  Result:=Grid.Cells[CellX, CellY]^.CellOptions;
end;

procedure TGridCellProxy.SetCellOptions(AValue: TCellOptions);
begin
  Grid.Cells[CellX, CellY]^.CellOptions:=AValue;
  Grid.UpdateSceneNode;
end;

procedure TGridCellProxy.SetFloorRotation(AFloorRotation: Integer);
var
  FloorRot, LowerRot, UpperRot, CeilingRot: Integer;
begin
  Grid.Cells[CellX, CellY]^.GetRotations(FloorRot, LowerRot, UpperRot, CeilingRot);
  Grid.Cells[CellX, CellY]^.SetRotations(AFloorRotation, LowerRot, UpperRot, CeilingRot);
  Grid.UpdateSceneNode;
end;

procedure TGridCellProxy.SetLowerRotation(ALowerRotation: Integer);
var
  FloorRot, LowerRot, UpperRot, CeilingRot: Integer;
begin
  Grid.Cells[CellX, CellY]^.GetRotations(FloorRot, LowerRot, UpperRot, CeilingRot);
  Grid.Cells[CellX, CellY]^.SetRotations(FloorRot, ALowerRotation, UpperRot, CeilingRot);
  Grid.UpdateSceneNode;
end;

procedure TGridCellProxy.SetUpperRotation(AUpperRotation: Integer);
var
  FloorRot, LowerRot, UpperRot, CeilingRot: Integer;
begin
  Grid.Cells[CellX, CellY]^.GetRotations(FloorRot, LowerRot, UpperRot, CeilingRot);
  Grid.Cells[CellX, CellY]^.SetRotations(FloorRot, LowerRot, AUpperRotation, CeilingRot);
  Grid.UpdateSceneNode;
end;

procedure TGridCellProxy.SetFloorHeight(AValue: Single);
begin
  Grid.Cells[CellX, CellY]^.FloorHeight:=Clamp(Round(AValue*100), -32767, 32767);
  Grid.UpdateSceneNode;
end;

{ TMain }

procedure TMain.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  // Initialize the engine
  InitializeEngine;
  // Initialize the game
  GGame.Initialize;
  // Setup property grid with non-visual stuff
  TIPropertyGrid1.GutterColor:=clWindow;
  TIPropertyGrid1.GutterEdgeColor:=DecColor(clWindow, 64);
  TIPropertyGrid1.ShowGutter:=False;
  // Fill insertable and filterable classes
  lbEntityClasses.Items.AddObject('(None)', nil);
  for I:=0 to High(SerializableClasses) do
    if SerializableClasses[I].InheritsFrom(TEntity) then begin
      lbInsertables.Items.Add(SerializableClasses[I].ClassName);
      lbEntityClasses.Items.AddObject(SerializableClasses[I].ClassName, TObject(SerializableClasses[I]));
    end;
  lbInsertables.ItemIndex:=0;
  lbEntityClasses.ItemIndex:=0;
  // Fill entity classes
  for I:=0 to High(SerializableClasses) do
    if SerializableClasses[I].InheritsFrom(TEntity) then
  lbInsertables.ItemIndex:=0;
  // Create the proxy object for grid cells
  CellProxy:=TGridCellProxy.Create;
  // Create an editor camera
  FEditorCamera:=TCamera.Create;
  FEditorCamera.Position:=Vector(0, 2, 0);
  FEditorCamera.FarPlane:=FEditorCamera.FarPlane * 4;
  // Update wall textures
  UpdateWallTextures;
  // Make sure the objects tab is active
  pcSidebar.ActivePage:=tsObjects;
end;

procedure TMain.FormDestroy(Sender: TObject);
var
  I: Integer;
begin
  // Free the camera
  FEditorCamera.Free;
  // Free the cell proxy
  CellProxy.Free;
  // Free the wall texture bitmaps
  for I:=0 to High(WallTextures) do
    WallTextures[I].Bitmap.Free;
  // Shutdown the engine
  ShutdownEngine;
end;

procedure TMain.ApplicationProperties1Idle(Sender: TObject; var Done: Boolean);
begin
  // Update global flags
  GLockVisibilityUpdates:=mToolsLockVisibilityUpdates.Checked;
  if mToolsRenderEverything.Checked then GGame.Scene.VisibilityMode:=svmAlways;
  if mToolsUsePortals.Checked then GGame.Scene.VisibilityMode:=svmPortals;
  GShowPortals:=mToolsShowPortals.Checked;
  GShowOcclusionTests:=mToolsShowOcclusionTests.Checked;
  // Update the engine
  if EngineRunning then begin
    // If we're not in play mode use the editor camera
    if GPlayMode <> pmPlaying then GRenderer.Camera:=FEditorCamera;
    // Check if we need to update the entity list based on the camera positon
    if ListCamGridEntitiesChecked then begin
      // Check if the position changed
      if GRenderer.Camera.Position <> LastEditorCamPosGridFilterCheck then begin
        LastEditorCamPosGridFilterCheck:=GRenderer.Camera.Position;
        UpdateEntityListIfVisible;
      end;
    end;
    // Force aspect ratio
    EditorCamera.Aspect:=plRenderWindow.Width/plRenderContainer.Height;
    GGame.Scene.Camera.Aspect:=plRenderWindow.Width/plRenderContainer.Height;
    // Run a cycle
    RunEngineCycle;
  end;
  // The game requested to quit
  if GQuitRequested then begin
    GQuitRequested:=False;
    mPlayPlayMode.Click;
  end;
  // Keep calling this event
  Done:=False;
end;

procedure TMain.mPlayPreviewAnimationsClick(Sender: TObject);
begin
  GUpdateEditorAnimations:=not GUpdateEditorAnimations;
  mPlayPreviewAnimations.Checked:=GUpdateEditorAnimations;
  // Restart animations if preview is enabled
  if GUpdateEditorAnimations then RestartAnimatedEntities;
end;

procedure TMain.mToolsCameraSequenceAddPositionClick(Sender: TObject);
var
  FS: TFormatSettings;
begin
  FS:=DefaultFormatSettings;
  FS.DecimalSeparator:='.';
  if CameraSequence <> '' then CameraSequence += ',' + LineEnding;
  with EditorCamera do
    CameraSequence += Format('%f, %f, %f, %f, %f, %f', [PositionX, PositionY, PositionZ, DirectionX, DirectionY, DirectionZ], FS);
  CameraSequence:=Trim(CameraSequence);
  Clipboard.AsText:=CameraSequence;
end;

procedure TMain.mToolsCameraSequenceFinishAndCopyToClipboardClick(Sender: TObject);
begin
  CameraSequence:=Trim(CameraSequence);
  Clipboard.AsText:=CameraSequence;
end;

procedure TMain.mToolsCameraSequenceStartClick(Sender: TObject);
begin
  CameraSequence:='';
end;

procedure TMain.mToolsDisableTrilinesClick(Sender: TObject);
begin
  mToolsDisableTrilines.Checked:=not mToolsDisableTrilines.Checked;
  GDisableTriLines:=mToolsDisableTrilines.Checked;
end;

procedure TMain.mToolsForcePortalRecalculationClick(Sender: TObject);
begin
  RecalculatePortals;
end;

procedure TMain.mToolsRenderEverythingClick(Sender: TObject);
begin
  GEditorRects:=nil;
end;

procedure TMain.mToolsShowOcclusionTestsClick(Sender: TObject);
begin
  GEditorRects:=nil;
end;

procedure TMain.mToolsShowPortalsClick(Sender: TObject);
begin
  GEditorRects:=nil;
end;

procedure TMain.mToolsShowStatsClick(Sender: TObject);
begin
  mToolsShowStats.Checked:=not mToolsShowStats.Checked;
  GShowStats:=mToolsShowStats.Checked;
end;

procedure TMain.mToolsWireframeModeClick(Sender: TObject);
begin
  mToolsWireframeMode.Checked:=not mToolsWireframeMode.Checked;
  GWireframeMode:=mToolsWireframeMode.Checked;
end;

procedure TMain.plRenderWindowClick(Sender: TObject);
begin
  plRenderWindow.SetFocus;
end;

procedure TMain.mFileSaveClick(Sender: TObject);
begin
  // Show Save As if there isn't a filename set
  if CurrentFileName='' then begin
    mFileSaveAs.Click;
    Exit;
  end;
  // Save to the current file
  SaveWorldFile(CurrentFileName);
end;

procedure TMain.mFileOpenClick(Sender: TObject);
begin
  // Confirm
  if MessageDlg('Open World File', 'Any unsaved changes to the current world might be lost, are you sure that you want to continue?',
    mtConfirmation, mbYesNo, 0) <> mrYes then Exit;
  // Ask for filename and open the file
  OpenDialog1.FileName:=CurrentFileName;
  if OpenDialog1.Execute then begin
    LoadWorldFile(OpenDialog1.FileName);
  end;
end;

procedure TMain.plRenderWindowMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  Ray: TRay;
  Obj: TSerializable;
  T: TTransform;
  Info: TWorldRayHitInfo;
  Entity: TEntity;
begin
  plRenderWindow.SetFocus;
  // Left click
  if Button=mbLeft then begin
    // If we are in free camera mode and Alt was also pressed (ie. Alt+Click)
    // then teleport the player entity to the clicked position
    if (GPlayMode=pmFreeCamera) and (ssAlt in Shift) then begin
      // Construct a ray
      Ray:=GRenderer.Camera.CalcRay((X/plRenderWindow.Width)*2-1, (Y/plRenderWindow.Height)*2-1);
      // Perform rayhit check
      Info.Reset;
      if GGame.World.RayHit(Ray, Info) then begin
        // Teleport player
        T:=GGame.Player.Transform;
        T.Translation:=Info.IP;
        T.Translation.Y += 0.05; // add a bit of space to avoid collision issues
        GGame.Player.Transform:=T;
        GGame.Player.PerformCommand('toytotransform'); // EPetra height hack
      end;
      Exit;
    end;
    // Construct a ray
    Ray:=GRenderer.Camera.CalcRay((X/plRenderWindow.Width)*2-1, (Y/plRenderWindow.Height)*2-1);
    // Prepare the ray hit check
    Info.Reset;
    // If "Cam grid only" is checked, limit the search to the grid the camera is in
    if cbCamGridOnly.Checked then
      Info.GridHit.LimitGrid:=GGame.World.GridMap.GridAt(GRenderer.Camera.Position);
    // For Alt+Click (moving entity) ignore the selected entity
    if (SelectedObject is TEntity) and (Shift=[ssLeft, ssAlt]) then
      Info.IgnoreEntity:=TEntity(SelectedObject);
    // Check if the ray hits anything in the world
    Obj:=nil;
    if GGame.World.RayHit(Ray, Info) then begin
      case Info.HitType of
        rhEntity: Obj:=Info.Entity;
        rhGridCell: begin
          CellProxy.FGrid:=Info.GridHit.Grid;
          CellProxy.FCellX:=Info.GridHit.CellX;
          CellProxy.FCellY:=Info.GridHit.CellY;
          CellProxy.FFloorTexture:=0;
          CellProxy.FCeilingTexture:=0;
          CellProxy.FUpperTexture:=0;
          CellProxy.FLowerTexture:=0;
          CellProxy.FCellPart:=Info.GridHit.CellPart;
          Obj:=CellProxy;
        end;
      end;
    end;
    // Nothing was clicked, ignore
    if not Assigned(Obj) then Exit;
    // Alt+Click => Move selected object to the clicked point
    if (GPlayMode=pmEditor) and (Shift=[ssAlt, ssLeft]) then begin
      if SelectedObject is TEntity then begin
        T:=TEntity(SelectedObject).Transform;
        T.Translation:=Info.IP;
        // Adjust position if the ray hits the grid
        if Obj is TGridCellProxy then with TGridCellProxy(Obj) do begin
          case CellPart of
            // If we hit the floor, use the floor height
            cpFloor: T.Translation.Y:=Grid.OriginY + Grid.Cells[CellX, CellY]^.FloorHeight/100;
            // If we hit the ceiling, adjust the entity's height to snap below
            cpCeiling: begin
              T.Translation.Y:=Grid.OriginY + Grid.CellHeight - Grid.Cells[CellX, CellY]^.CeilingHeight/100;
              if SelectedObject is TSceneNodeEntity then
                T.Translation.Y -= TSceneNodeEntity(SelectedObject).SceneNode.AABox.Height;
            end;
          end;
        end;
        TEntity(SelectedObject).Transform:=T;
        TIPropertyGrid1.RefreshPropertyValues;
      end;
    end;
    // Shift+Click => Start moving an object
    if Shift=[ssShift, ssLeft] then begin
      Moving:=False;
      // Entity?
      if SelectedObject is TEntity then begin
        OriginalPosition:=TEntity(SelectedObject).Transform.Translation;
        Moving:=True;
      end;
      // Grid?
      if SelectedObject is TGrid then begin
        OriginalPosition:=TGrid(SelectedObject).Origin;
        Moving:=True;
      end;
      // If we started moving, calculate a moving plane and plane point
      if Moving then begin
        MovingPlane.FromPointAndNormal(OriginalPosition, Vector(0, 1, 0));
        Ray.PlaneHit(MovingPlane, MovingPlaneIP);
      end;
    end;
    // Click => Select an object
    if Shift=[ssLeft] then begin
      if (Obj <> SelectedObject) or (Obj is TGridCellProxy) then begin
        SelectedObject:=nil;
        SelectedObject:=Obj;
      end;
      // If C is pressed and we selected a grid cell, copy the last cell
      // settings, otherwise store them for future use
      if SelectedObject is TGridCellProxy then with TGridCellProxy(SelectedObject) do begin
        if (GetKeyState(VK_C) and $8000) <> 0 then begin
          Grid.Cells[CellX, CellY]^:=LastGridCell;
          Grid.UpdateSceneNode;
          TIPropertyGrid1.RefreshPropertyValues;
        end else begin
          LastGridCell:=Grid.Cells[CellX, CellY]^;
        end;
      end;
    end;
  end;
  // Right click (look around)
  if Button=mbRight then begin
    // Ignore if play mode is active (but keep going if freecam is active)
    if GPlayMode=pmPlaying then Exit;
    // Enable lookaround mode
    LookAround:=True;
    MouseX:=X;
    MouseY:=Y;
  end;
  // Ignore the rest if we are in gameplay mode
  if GPlayMode <> pmEditor then Exit;
  // Ctrl+Click => Duplicate selected entity
  if Shift=[ssCtrl, ssLeft] then begin
    if not (SelectedObject is TEntity) then Exit;
    // Remove the entity from the world temporarily so that it wont refer to it
    GGame.World.RemoveEntity(TEntity(SelectedObject));
    // Clone the entity
    Entity:=TEntity(TEntity(SelectedObject).CreateDeepClone);
    // Put the original object back to the world
    GGame.World.AddEntity(TEntity(SelectedObject));
    // Move the new entity to the clicked position and put it in the world
    T:=Entity.Transform;
    T.Translation:=Info.IP;
    Entity.Transform:=T;
    GGame.World.AddEntity(Entity);
    // Select the new entity
    SelectedObject:=Entity;
  end;
end;

procedure TMain.Button1Click(Sender: TObject);
var
  Cls: TPersistentClass;
  Pos: TVector;
begin
  Cls:=GetClass(lbInsertables.Items[lbInsertables.ItemIndex]);
  if Cls=nil then Exit;
  Pos:=SelAABox.Center;
  Pos.Y:=SelAABox.Max.Y;
  CreateEntityAt(TEntityClass(Cls), Pos);
end;

procedure TMain.Timer1Timer(Sender: TObject);
var
  FloorHeight, CeilingHeight: Single;
  Speed: Single;
begin
  // Ignore if we are in play mode
  if GPlayMode=pmPlaying then Exit;
  // Ignore if the render window is not focused
  if not plRenderWindow.Focused then Exit;
  // Enter play mode if requested
  if GPlayMapOnEditorStart <> '' then begin
    // Load the map
    LoadWorldFile(GPlayMapOnEditorStart);
    if GPlayMapOnEditorStart=CurrentFileName then begin
      GPlayMapOnEditorStart:='';
      IgnoreStartupScript:=GPlayMapOnEditorNoStartup;
      GPlayMapOnEditorNoStartup:=False;
      mPlayPlayMode.Click;
    end;
    Exit;
  end;
  // Camera control
  if ssShift in GetKeyShiftState then Speed:=0.2 else Speed:=0.08;
  if (GetKeyState(VK_A) and $8000) <> 0 then begin
    EditorCamera.Position:=EditorCamera.Position.Added(EditorCamera.ViewMatrix.Inverted.XAxis.Scaled(Speed));
  end;
  if (GetKeyState(VK_D) and $8000) <> 0 then begin
    EditorCamera.Position:=EditorCamera.Position.Added(EditorCamera.ViewMatrix.Inverted.XAxis.Scaled(-Speed));
  end;
  if ((GetKeyState(VK_W) and $8000) <> 0) and ((GetKeyState(VK_MENU) and $8000)=0) then begin
    EditorCamera.Position:=EditorCamera.Position.Added(EditorCamera.Direction.Scaled(Speed));
  end;
  if (GetKeyState(VK_S) and $8000) <> 0 then begin
    EditorCamera.Position:=EditorCamera.Position.Added(EditorCamera.Direction.Scaled(-Speed));
  end;
  if (GetKeyState(VK_Q) and $8000) <> 0 then begin
    EditorCamera.Position:=EditorCamera.Position.Added(Vector(0, Speed, 0));
  end;
  if (GetKeyState(VK_E) and $8000) <> 0 then begin
    EditorCamera.Position:=EditorCamera.Position.Added(Vector(0, -Speed, 0));
  end;
  // Look at player if enabled
  if LockToPlayerInFreeCamera then begin
    EditorCamera.Direction:=Interpolate(EditorCamera.Direction,
      Direction(EditorCamera.Position, GGame.Player.Transform.Translation),
      0.15).Normalized;
  end;
  // Ignore if the game is being played
  if GPlayMode <> pmEditor then Exit;
  // Raise selected object
  if (GetKeyState(VK_NUMPAD9) and $8000) <> 0 then begin
    // Entity?
    if SelectedObject is TEntity then begin
      TEntity(SelectedObject).PositionY:=TEntity(SelectedObject).PositionY + 0.05;
      TIPropertyGrid1.RefreshPropertyValues;
    end;
    // Grid?
    if SelectedObject is TGrid then begin
      TGrid(SelectedObject).OriginY:=TGrid(SelectedObject).OriginY + 0.05;
      TIPropertyGrid1.RefreshPropertyValues;
    end;
    // Grid cell?
    if SelectedObject is TGridCellProxy then with TGridCellProxy(SelectedObject) do begin
      case CellPart of
        cpNone: ;
        cpFloor, cpLower:
          Grid.Cells[CellX, CellY]^.FloorHeight:=Min(Integer(Trunc(Grid.CellHeight*100)), Integer(Grid.Cells[CellX, CellY]^.FloorHeight + 5));
        cpCeiling, cpUpper:
          Grid.Cells[CellX, CellY]^.CeilingHeight:=Max(0, Grid.Cells[CellX, CellY]^.CeilingHeight - 5);
      end;
      Grid.UpdateSceneNode;
      TIPropertyGrid1.RefreshPropertyValues;
    end;
  end;
  // Lower selected object
  if (GetKeyState(VK_NUMPAD3) and $8000) <> 0 then begin
    // Entity?
    if SelectedObject is TEntity then begin
      TEntity(SelectedObject).PositionY:=TEntity(SelectedObject).PositionY - 0.05;
      TIPropertyGrid1.RefreshPropertyValues;
    end;
    // Grid?
    if SelectedObject is TGrid then begin
      TGrid(SelectedObject).OriginY:=TGrid(SelectedObject).OriginY - 0.05;
      TIPropertyGrid1.RefreshPropertyValues;
    end;
    // Grid cell?
    if SelectedObject is TGridCellProxy then with TGridCellProxy(SelectedObject) do begin
      case CellPart of
        cpFloor, cpLower:
          Grid.Cells[CellX, CellY]^.FloorHeight:=Max(0, Grid.Cells[CellX, CellY]^.FloorHeight - 5);
        cpCeiling, cpUpper:
          Grid.Cells[CellX, CellY]^.CeilingHeight:=Min(Integer(Trunc(Grid.CellHeight*100)), Integer(Grid.Cells[CellX, CellY]^.CeilingHeight + 5));
      end;
      Grid.UpdateSceneNode;
      TIPropertyGrid1.RefreshPropertyValues;
    end;
  end;
  // Drop selected object to floor
  if (GetKeyState(VK_NUMPAD1) and $8000) <> 0 then begin
    if SelectedObject is TEntity then begin
      GGame.World.GridMap.GetFloorAndCeilingHeightsAt(TEntity(SelectedObject).Transform.Translation,
        FloorHeight, CeilingHeight);
      TEntity(SelectedObject).PositionY:=FloorHeight;
      TIPropertyGrid1.RefreshPropertyValues;
    end;
  end;
  // Place selected object to ceiling
  if (GetKeyState(VK_NUMPAD7) and $8000) <> 0 then begin
    if SelectedObject is TEntity then begin
      GGame.World.GridMap.GetFloorAndCeilingHeightsAt(TEntity(SelectedObject).Transform.Translation,
        FloorHeight, CeilingHeight);
      if SelectedObject is TEntity then
        TEntity(SelectedObject).PositionY:=CeilingHeight - TEntity(SelectedObject).AABox.Height;
      TIPropertyGrid1.RefreshPropertyValues;
    end;
  end;
  // Move selected object left
  if (GetKeyState(VK_NUMPAD4) and $8000) <> 0 then begin
    if SelectedObject is TEntity then begin
      TEntity(SelectedObject).PositionX:=TEntity(SelectedObject).PositionX - 0.05;
      TIPropertyGrid1.RefreshPropertyValues;
    end;
  end;
  // Move selected object right
  if (GetKeyState(VK_NUMPAD6) and $8000) <> 0 then begin
    if SelectedObject is TEntity then begin
      TEntity(SelectedObject).PositionX:=TEntity(SelectedObject).PositionX + 0.05;
      TIPropertyGrid1.RefreshPropertyValues;
    end;
  end;
  // Move selected object far
  if (GetKeyState(VK_NUMPAD8) and $8000) <> 0 then begin
    if SelectedObject is TEntity then begin
      TEntity(SelectedObject).PositionZ:=TEntity(SelectedObject).PositionZ - 0.05;
      TIPropertyGrid1.RefreshPropertyValues;
    end;
  end;
  // Move selected object near
  if (GetKeyState(VK_NUMPAD2) and $8000) <> 0 then begin
    if SelectedObject is TEntity then begin
      TEntity(SelectedObject).PositionZ:=TEntity(SelectedObject).PositionZ + 0.05;
      TIPropertyGrid1.RefreshPropertyValues;
    end;
  end;
end;

procedure TMain.mPlayPlayModeClick(Sender: TObject);
begin
  // Remove any selection
  SelectedObject:=nil;
  // Toggle play mode
  if GPlayMode=pmEditor then begin
    // Change mode
    GPlayMode:=pmPlaying;
    // Ensure there are valid portals
    RecalculatePortals;
    // Store current game instance
    RealGame:=GGame;
    // Remove world scene nodes to avoid any clashes (especially since the
    // AddSceneNodes/RemoveSceneNodes methods are also uses for non-scene
    // registration/unregistration, like in sound emitters)
    GGame.World.RemoveSceneNodes;
    // Create a new game instance
    GGame:=TGame(GGame.CreateDeepClone);
    // Workaround: TPetra will set itself to RealGame.Player instead of
    // the cloned copy since GGame will point to that by the time it is added
    // to the world.  Make sure the cloned TGame instance refers to the proper
    // player instance
    GGame.Player:=RealGame.Player;
    // Initialize the game
    GGame.Initialize;
    // Initialize the world;
    GGame.InitWorldForPIE;
    // Run the startup script
    if not IgnoreStartupScript then GGame.World.RunStartupScript;
  end else begin
    // Shutdown the game
    GGame.Shutdown;
    // Change mode
    GPlayMode:=pmEditor;
    // Exit from any active screen
    GScreenManager.Screen:=nil;
    // Kill the current game
    GGame.Free;
    GGame:=RealGame;
    GGame.Player:=nil;
    RealGame:=nil;
    // Disable looking to player camera
    LockToPlayerInFreeCamera:=False;
    mPlayLookAtPlayer.Checked:=False;
    // Add scene nodes back
    GGame.World.AddSceneNodes;
    // Restart editor animations if they are enabled
    if GUpdateEditorAnimations then RestartAnimatedEntities;
  end;
  // Reset the IgnoreStartupScript flag
  IgnoreStartupScript:=False;
  // Update UI
  mPlayPlayMode.Checked:=GPlayMode <> pmEditor;
  mPlayFreeCamera.Checked:=False;
end;

procedure TMain.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  // Exit play mode
  if GPlayMode <> pmEditor then mPlayPlayMode.Click;
end;

procedure TMain.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  H: Single;
  IH, FloorRot, LowerRot, UpperRot, CeilingRot: Integer;
  Info: TWorldRayHitInfo;
  Picking, Flipping: Boolean;
  Ray: TRay;
begin
  // Ignore if the render window is not focused
  if not plRenderWindow.Focused then Exit;
  // Forward key event if we are in play mode
  if GPlayMode <> pmEditor then begin
    // When in free camera mode, ignore WASDQE events
    if (GPlayMode=pmFreeCamera) and (Key in [VK_W, VK_A, VK_S, VK_D, VK_Q, VK_E]) then Exit;
    // Forward event
    TEditorInputDriver(GInput).OnKeyEvent(Key, True);
    // Ignore alt, control, etc
    if (Key in [VK_MENU, VK_CONTROL, VK_TAB]) or (ssAlt in Shift) or (ssCtrl in Shift) then Key:=0;
  end
  // Handle editor shortcut
  else case Key of
    // Raise selected object by 25cm/50cm
    VK_R: begin
      if ssShift in Shift then H:=0.25 else H:=0.50;
      IH:=Round(H*100);
      // Entity?
      if SelectedObject is TEntity then begin
        TEntity(SelectedObject).PositionY:=TEntity(SelectedObject).PositionY + H;
        TIPropertyGrid1.RefreshPropertyValues;
      end;
      // Grid?
      if SelectedObject is TGrid then begin
        TGrid(SelectedObject).OriginY:=TGrid(SelectedObject).OriginY + H*4;
        TIPropertyGrid1.RefreshPropertyValues;
      end;
      // Grid cell?
      if SelectedObject is TGridCellProxy then with TGridCellProxy(SelectedObject) do begin
        case CellPart of
          cpFloor, cpLower:
            Grid.Cells[CellX, CellY]^.FloorHeight:=Grid.Cells[CellX, CellY]^.FloorHeight + IH;
          cpCeiling, cpUpper:
            Grid.Cells[CellX, CellY]^.CeilingHeight:=Grid.Cells[CellX, CellY]^.CeilingHeight - IH;
        end;
        Grid.UpdateSceneNode;
        TIPropertyGrid1.RefreshPropertyValues;
      end;
    end;
    // Lower selected object by 25cm/50cm
    VK_F: begin
      if ssShift in Shift then H:=0.25 else H:=0.50;
      IH:=Round(H*100);
      // Entity?
      if SelectedObject is TEntity then begin
        TEntity(SelectedObject).PositionY:=TEntity(SelectedObject).PositionY - H;
        TIPropertyGrid1.RefreshPropertyValues;
      end;
      // Grid?
      if SelectedObject is TGrid then begin
        TGrid(SelectedObject).OriginY:=TGrid(SelectedObject).OriginY - H*4;
        TIPropertyGrid1.RefreshPropertyValues;
      end;
      // Grid cell?
      if SelectedObject is TGridCellProxy then with TGridCellProxy(SelectedObject) do begin
        case CellPart of
          cpFloor, cpLower:
            Grid.Cells[CellX, CellY]^.FloorHeight:=Grid.Cells[CellX, CellY]^.FloorHeight - IH;
          cpCeiling, cpUpper:
            Grid.Cells[CellX, CellY]^.CeilingHeight:=Grid.Cells[CellX, CellY]^.CeilingHeight + IH;
        end;
        Grid.UpdateSceneNode;
        TIPropertyGrid1.RefreshPropertyValues;
      end;
    end;
    // Delete selected object
    VK_DELETE: if Assigned(SelectedObject) and
      (MessageDlg('Delete', 'Are you sure you want to delete the selected ' +
         SelectedObject.ClassName + '?', mtConfirmation, mbYesNo, 0)=mrYes) then begin
      // Clear the property grid so that it wont try to access the object
      TIPropertyGrid1.TIObject:=nil;
      TIPropertyGrid1.ShowGutter:=False;
      // Entity?
      if SelectedObject is TEntity then begin
        GGame.World.DeleteEntity(TEntity(SelectedObject));
        UpdateEntityListIfVisible;
        SelectedObject:=nil;
      end;
      // Grid?
      if SelectedObject is TGrid then begin
        // Remove the grid scene nodes
        GGame.World.GridMap.RemoveSceneNodes(GGame.Scene);
        // Delete the grid
        GGame.World.GridMap.DeleteGrid(TGrid(SelectedObject));
        SelectedObject:=nil;
        // Add the remaining grid scene nodes
        GGame.World.GridMap.AddSceneNodes(GGame.Scene);
      end;
      // Grid cell?
      if SelectedObject is TGridCellProxy then with TGridCellProxy(SelectedObject) do begin
        // Cannot delete the grid cell, just reset it
        Grid.Cells[CellX, CellY]^.Reset;
        TIPropertyGrid1.TIObject:=SelectedObject;
        TIPropertyGrid1.ShowGutter:=True;
      end;
    end;
    // Set or get texture
    VK_T: begin
      // Ctrl+T => Picking mode
      Picking:=ssCtrl in GetKeyShiftState;
      // Construct a ray towards the mouse
      Ray:=GRenderer.Camera.CalcRay((LastX/plRenderWindow.Width)*2-1, (LastY/plRenderWindow.Height)*2-1);
      // Perform a ray hit check
      Info.Reset;
      if not GGame.World.RayHit(Ray, Info) then Exit;
      // Hit a grid cell?
      if Info.HitType=rhGridCell then with Info.GridHit do begin
        case CellPart of
          cpLower:
            if Picking then
              SelectedWallTexture:=Grid.Cells[CellX, CellY]^.Lower
            else
              Grid.Cells[CellX, CellY]^.Lower:=SelectedWallTexture;
          cpFloor:
            if Picking then
              SelectedWallTexture:=Grid.Cells[CellX, CellY]^.Floor
            else
              Grid.Cells[CellX, CellY]^.Floor:=SelectedWallTexture;
          cpCeiling:
            if Picking then
              SelectedWallTexture:=Grid.Cells[CellX, CellY]^.Ceiling
            else
              Grid.Cells[CellX, CellY]^.Ceiling:=SelectedWallTexture;
          cpUpper:
            if Picking then
              SelectedWallTexture:=Grid.Cells[CellX, CellY]^.Upper
            else
              Grid.Cells[CellX, CellY]^.Upper:=SelectedWallTexture;
        end;
        // Update UI or grid
        if Picking then pbTextures.Invalidate else Grid.UpdateSceneNode;
      end;
    end;
    // Rotate or flip texture
    VK_G: begin
      // Shift+G => Flip instead of rotating
      Flipping:=ssShift in GetKeyShiftState;
      // Construct a ray towards the mouse
      Ray:=GRenderer.Camera.CalcRay((LastX/plRenderWindow.Width)*2-1, (LastY/plRenderWindow.Height)*2-1);
      // Perform a ray hit check
      Info.Reset;
      if not GGame.World.RayHit(Ray, Info) then Exit;
      // Hit a grid cell?
      if Info.HitType=rhGridCell then with Info.GridHit do begin
        Grid.Cells[CellX, CellY]^.GetRotations(FloorRot, LowerRot, UpperRot, CeilingRot);
        case CellPart of
          cpLower:
            if Flipping then
              Grid.Cells[CellX, CellY]^.CellOptions:=Grid.Cells[CellX, CellY]^.CellOptions >< [coFlipLowerTexture]
            else
              LowerRot:=(LowerRot + 1) mod 4;
          cpFloor:
            if Flipping then
              Grid.Cells[CellX, CellY]^.CellOptions:=Grid.Cells[CellX, CellY]^.CellOptions >< [coFlipFloorTexture]
            else
              FloorRot:=(FloorRot + 1) mod 4;
          cpCeiling:
            if Flipping then
              Grid.Cells[CellX, CellY]^.CellOptions:=Grid.Cells[CellX, CellY]^.CellOptions >< [coFlipCeilingTexture]
            else
              CeilingRot:=(CeilingRot + 1) mod 4;
          cpUpper:
            if Flipping then
              Grid.Cells[CellX, CellY]^.CellOptions:=Grid.Cells[CellX, CellY]^.CellOptions >< [coFlipUpperTexture]
            else
              UpperRot:=(UpperRot + 1) mod 4;
        end;
        // Update grid
        Grid.Cells[CellX, CellY]^.SetRotations(FloorRot, LowerRot, UpperRot, CeilingRot);
        Grid.UpdateSceneNode;
        // Refresh property editor's values if we modified the selected cell
        if SelectedObject is TGridCellProxy then
          if (TGridCellProxy(SelectedObject).CellX=CellX) and
             (TGridCellProxy(SelectedObject).CellY=CellY) then
               TIPropertyGrid1.RefreshPropertyValues;
      end;
    end;
  end;
end;

procedure TMain.FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  // Ignore if the render window is not focused
  if not plRenderWindow.Focused then Exit;
  // Forward key event if we are in play mode
  if GPlayMode <> pmEditor then begin
    // When in free camera mode, ignore WASDQE events
    if (GPlayMode=pmFreeCamera) and (Key in [VK_W, VK_A, VK_S, VK_D, VK_Q, VK_E]) then Exit;
    TEditorInputDriver(GInput).OnKeyEvent(Key, False);
    // Ignore alt, control, etc
    if (Key in [VK_MENU, VK_CONTROL]) or (ssAlt in Shift) or (ssCtrl in Shift) then Key:=0;
  end;
end;

procedure TMain.plRenderWindowMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
  Mtx: TMatrix;
  Ray: TRay;
  IP, Delta: TVector;
  T: TTransform;
begin
  // Store mouse coordinates
  LastX:=X;
  LastY:=Y;
  // Moving selected object
  if Moving then begin
    // Construct a ray
    Ray:=GRenderer.Camera.CalcRay((X/plRenderWindow.Width)*2-1, (Y/plRenderWindow.Height)*2-1);
    // Check if the ray hits the moving plane
    if not Ray.PlaneHit(MovingPlane, IP) then Exit;
    // Calculate motion delta
    Delta:=IP.Subbed(MovingPlaneIP);
    // Ignore height
    Delta.Y:=0;
    // Snap to smaller increments
    Delta.X:=Round(Delta.X*10)/10;
    Delta.Z:=Round(Delta.Z*10)/10;
    // If the delta is too big (moved to horizon?) ignore the motion
    if Delta.Length > 100 then Exit;
    // Entity?
    if SelectedObject is TEntity then begin
      T:=TEntity(SelectedObject).Transform;
      T.Translation:=OriginalPosition.Added(Delta);
      TEntity(SelectedObject).Transform:=T;
      TIPropertyGrid1.RefreshPropertyValues;
    end;
    // Grid?
    if SelectedObject is TGrid then begin
      // Only allow whole integer deltas
      Delta.X:=Round(Delta.X);
      Delta.Z:=Round(Delta.Z);
      TGrid(SelectedObject).Origin:=OriginalPosition.Added(Delta);
      TIPropertyGrid1.RefreshPropertyValues;
    end;
  end;
  // Looking around
  if LookAround then begin
    Mtx.YRotation(-(X - MouseX)*0.01);
    EditorCamera.Direction:=Mtx.Transformed(EditorCamera.Direction).Normalized;
    Mtx.Rotation(EditorCamera.ViewMatrix.Inverted.XAxis, (Y - MouseY)*0.01);
    EditorCamera.Direction:=Mtx.Transformed(EditorCamera.Direction).Normalized;
    MouseX:=X;
    MouseY:=Y;
  end;
end;

procedure TMain.plRenderWindowMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  // Left click
  if Button=mbLeft then begin
    Moving:=False;
  end;
  // Right click
  if Button=mbRight then begin
    LookAround:=False;
  end;
end;

procedure TMain.mPlayFreeCameraClick(Sender: TObject);
begin
  if GPlayMode in [pmPlaying, pmFreeCamera] then begin
    // Toggle free camera mode
    if GPlayMode=pmPlaying then begin
      GPlayMode:=pmFreeCamera;
      // Update editor camera to be the same as the game camera
      FEditorCamera.Position:=GGame.Scene.Camera.Position;
      FEditorCamera.Direction:=GGame.Scene.Camera.Direction;
    end else
      GPlayMode:=pmPlaying;
  end;
  mPlayFreeCamera.Checked:=GPlayMode=pmFreeCamera;
end;

procedure TMain.mPlayLookAtPlayerClick(Sender: TObject);
begin
  LockToPlayerInFreeCamera:=not LockToPlayerInFreeCamera;
  mPlayLookAtPlayer.Checked:=LockToPlayerInFreeCamera;
end;

procedure TMain.btSelectGridClick(Sender: TObject);
begin
  // Ignore in gameplay mode
  if GPlayMode <> pmEditor then Exit;
  // If there is no selection, select the grid the camera is in
  if not Assigned(SelectedObject) then begin
    SelectedObject:=GGame.World.GridMap.GridAt(EditorCamera.Position);
    Exit;
  end;
  // If there is already a grid selected then ignore this
  if SelectedObject is TGrid then Exit;
  // If there is an entity selected, select the grid the entity is in
  if SelectedObject is TEntity then begin
    SelectedObject:=GGame.World.GridMap.GridAt(TEntity(SelectedObject).Transform.Translation);
    Exit;
  end;
  // If there is a cell selected, select the cell's grid
  if SelectedObject is TGridCellProxy then begin
    SelectedOBject:=TGridCellProxy(SelectedObject).Grid;
    Exit;
  end;
end;

procedure TMain.btNewGridClick(Sender: TObject);
var
  GridSizeStr: string;
  P: SizeInt;
  W, H: LongInt;
  Grid: TGrid;
  Origin: TVector;
begin
  // Ask for size
  GridSizeStr:=InputBox('New Grid', 'Grid size (separate with space):', '10 10').Trim;
  if GridSizeStr='' then Exit;
  P:=Pos(' ', GridSizeStr);
  if P=0 then begin
    W:=StrToIntDef(GridSizeStr, 0);
    if W=0 then Exit;
    H:=W;
  end else begin
    W:=StrToIntDef(Copy(GridSizeStr, 1, P - 1), 0);
    H:=StrToIntDef(Copy(GridSizeStr, P + 1, MaxInt), 0);
    if (W=0) or (H=0) then Exit;
  end;
  // Calculate origin
  Origin:=EditorCamera.Position.Subbed(Vector(W*GridCellSize/2, 1.5, H*GridCellSize/2));
  // Create the grid
  CreateGridAt(Origin, W, H);
end;

procedure TMain.btRelightClick(Sender: TObject);
begin
  Relight;
end;

procedure TMain.btSelectWorldClick(Sender: TObject);
begin
  SelectedObject:=GGame.World;
end;

procedure TMain.pbTexturesPaint(Sender: TObject);
var
  I: Integer;
begin
  pbTextures.Canvas.Pen.Color:=clRed;
  pbTextures.Canvas.Rectangle(-1, -1, Width, Height);
  for I:=0 to High(WallTextures) do begin
    if I=SelectedWallTexture then
      pbTextures.Canvas.Rectangle(WallTextures[I].X - 1,
                                  WallTextures[I].Y - 1 - sbTextures.Position,
                                  WallTextures[I].X + 65,
                                  WallTextures[I].Y + 65 - sbTextures.Position);
    pbTextures.Canvas.Draw(WallTextures[I].X,
                           WallTextures[I].Y - sbTextures.Position,
                           WallTextures[I].Bitmap);
  end;
end;

procedure TMain.sbTexturesChange(Sender: TObject);
begin
  pbTextures.Invalidate;
end;

procedure TMain.pbTexturesMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  I: Integer;
begin
  if Button <> mbLeft then Exit;
  Y += sbTextures.Position;
  for I:=0 to High(WallTextures) do
    if (X >= WallTextures[I].X) and (Y >= WallTextures[I].Y) and
       (X <= WallTextures[I].X + 64) and (Y <= WallTextures[I].Y + 64) then begin
      SelectedWallTexture:=I;
      pbTextures.Invalidate;
      Exit;
    end;
end;

procedure TMain.pmTexturesSetToLowerClick(Sender: TObject);
var
  X, Y: Integer;
begin
  if not (SelectedObject is TGrid) then begin
    ShowMessage('No selected grid');
    Exit;
  end;
  with TGrid(SelectedObject) do begin
    for Y:=0 to Height - 1 do
      for X:=0 to Width - 1 do
        Cells[X, Y]^.Lower:=SelectedWallTexture;
    UpdateSceneNode;
  end;
end;

procedure TMain.pmTexturesSetToFloorClick(Sender: TObject);
var
  X, Y: Integer;
begin
  if not (SelectedObject is TGrid) then begin
    ShowMessage('No selected grid');
    Exit;
  end;
  with TGrid(SelectedObject) do begin
    for Y:=0 to Height - 1 do
      for X:=0 to Width - 1 do
        Cells[X, Y]^.Floor:=SelectedWallTexture;
    UpdateSceneNode;
  end;
end;

procedure TMain.pmTexturesSetToCeilingClick(Sender: TObject);
var
  X, Y: Integer;
begin
  if not (SelectedObject is TGrid) then begin
    ShowMessage('No selected grid');
    Exit;
  end;
  with TGrid(SelectedObject) do begin
    for Y:=0 to Height - 1 do
      for X:=0 to Width - 1 do
        Cells[X, Y]^.Ceiling:=SelectedWallTexture;
    UpdateSceneNode;
  end;
end;

procedure TMain.pmTexturesSetToUpperClick(Sender: TObject);
var
  X, Y: Integer;
begin
  if not (SelectedObject is TGrid) then begin
    ShowMessage('No selected grid');
    Exit;
  end;
  with TGrid(SelectedObject) do begin
    for Y:=0 to Height - 1 do
      for X:=0 to Width - 1 do
        Cells[X, Y]^.Upper:=SelectedWallTexture;
    UpdateSceneNode;
  end;
end;

procedure TMain.pmTexturesPasteFromClipboardClick(Sender: TObject);
var
  Bitmap: TBitmap;
  Texels: array [0..4095] of Byte;
  X, Y: Integer;
begin
  if SelectedWallTexture < 0 then Exit;
  // Check the data format in the clipboard
  if not Clipboard.HasFormat(PredefinedClipboardFormat(pcfBitmap)) then begin
    ShowMessage('No bitmap in the clipboard');
    Exit;
  end;
  // Try to paste the bitmap
  Bitmap:=TBitmap.Create;
  try
    Bitmap.LoadFromClipboardFormat(PredefinedClipboardFormat(pcfBitmap));
  except
    ShowMessage('Failed to paste bitmap');
    Bitmap.Free;
    Exit;
  end;
  // Check the bitmap's size
  if (Bitmap.Width <> 64) or (Bitmap.Height <> 64) then begin
    ShowMessage('Bitmap must be 64x64');
    Bitmap.Free;
    Exit;
  end;
  // Convert to 8bit image
  for Y:=0 to 63 do
    for X:=0 to 63 do
      Texels[Y*64 + X]:=GRenderer.ColorPalette.GetClosestColor(
        Red(Bitmap.Canvas.Pixels[X, Y]),
        Green(Bitmap.Canvas.Pixels[X, Y]),
        Blue(Bitmap.Canvas.Pixels[X, Y]));
  // Update textures
  TTexture(GResourceManager.WallTextures[SelectedWallTexture]).SetTexels(Texels);
  UpdateWallTextures;
end;

procedure TMain.mEntityCameraFromViewClick(Sender: TObject);
begin
  if SelectedObject is TCameraEntity then begin
    TCameraEntity(SelectedObject).Camera.Direction:=EditorCamera.Direction;
    TCameraEntity(SelectedObject).PositionX:=EditorCamera.PositionX;
    TCameraEntity(SelectedObject).PositionY:=EditorCamera.PositionY;
    TCameraEntity(SelectedObject).PositionZ:=EditorCamera.PositionZ;
    TIPropertyGrid1.RefreshPropertyValues;
  end else
    ShowMessage('The selected object is not a TCameraEntity');
end;

procedure TMain.mEntityCameraToViewClick(Sender: TObject);
begin
  if SelectedObject is TCameraEntity then begin
    EditorCamera.Position:=TCameraEntity(SelectedObject).Transform.Translation;
    EditorCamera.Direction:=TCameraEntity(SelectedObject).Camera.Direction;
  end else
    ShowMessage('The selected object is not a TCameraEntity');
end;

procedure TMain.mEntityEditScriptClick(Sender: TObject);
begin
  if not (SelectedObject is TScriptEntity) then begin
    ShowMessage('The selected object is not a TScriptEntity');
    Exit;
  end;
  TScriptEntity(SelectedObject).ScriptCode:=
    MultilineTextDialog('Edit script code', TScriptEntity(SelectedObject).ScriptCode);
  TIPropertyGrid1.RefreshPropertyValues;
end;

procedure TMain.mPlayPlayAtSelectionClick(Sender: TObject);
var
  T: TTransform;
  FloorHeight, CeilingHeight: Single;
begin
  // Make sure we're in editor mode
  if GPlayMode <> pmEditor then begin
    ShowMessage('Not in editor mode');
    Exit;
  end;
  // Make sure we have a selection
  if not Assigned(SelectedObject) then begin
    ShowMessage('There is no object selected');
    Exit;
  end;
  // Start play mode without running the startup script
  IgnoreStartupScript:=True;
  mPlayPlayMode.Click;
  // Move the player to the selection
  T:=GGame.Player.Transform;
  T.Translation:=SelAABox.Center;
  GGame.World.GetFloorAndCeilingHeightsAt(T.Translation, FloorHeight, CeilingHeight);
  T.Translation.Y:=FloorHeight;
  GGame.Player.Transform:=T;
  GGame.Player.PerformCommand('toytotransform');
end;

procedure TMain.mFileNewClick(Sender: TObject);
begin
  // Confirm
  if MessageDlg('New World File', 'Any unsaved changes to the current world might be lost, are you sure that you want to continue?',
    mtConfirmation, mbYesNo, 0) <> mrYes then Exit;
  // Make brand new world
  NewWorldFile;
end;

procedure TMain.FormCloseQuery(Sender: TObject; var CanClose: boolean);
begin
  // Confirm
  if MessageDlg('Exit', 'Any unsaved changes to the current world might be lost, are you sure that you want to continue?',
    mtConfirmation, mbYesNo, 0) <> mrYes then begin
    CanClose:=False;
    Exit;
  end;
  CanClose:=True;
end;

procedure TMain.mFileSaveAsClick(Sender: TObject);
begin
  // Ask for filename and save the file
  SaveDialog1.FileName:=CurrentFileName;
  if SaveDialog1.Execute then begin
    SaveWorldFile(SaveDialog1.FileName);
  end;
end;

procedure TMain.mFileExitClick(Sender: TObject);
begin
  Close;
end;

procedure TMain.mGameEditDataTablesClick(Sender: TObject);
begin
  Application.CreateForm(TTableEditor, TableEditor);
  TableEditor.ShowModal;
  TableEditor.Free;
end;

procedure TMain.mPlayPreviewSoundsClick(Sender: TObject);
begin
  GUpdateEditorSounds:=not GUpdateEditorSounds;
  mPlayPreviewSounds.Checked:=GUpdateEditorSounds;
end;

procedure TMain.mBookmarksClick(Sender: TObject);
var
  Item: TMenuItem;
  I: Integer;
begin
  // Delete existing bookmark menu items
  while mBookmarks.Count > 3 do
    mBookmarks.Delete(3);
  // Create new menu items for the bookmarks
  for I:=0 to High(GGame.World.Bookmarks.Bookmarks) do
    with GGame.World.Bookmarks.Bookmarks[I] do begin
      Item:=TMenuItem.Create(mBookmarks);
      Item.Caption:=Name;
      Item.Tag:=I;
      Item.OnClick:=@OnBookmarkClick;
      mBookmarks.Add(Item);
    end;
end;

procedure TMain.mBookmarksAddClick(Sender: TObject);
var
  BookmarkName: String;
begin
  BookmarkName:=Trim(InputBox('Add Bookmark', 'Enter name for the new bookmark', ''));
  if BookmarkName='' then Exit;
  SetLength(GGame.World.Bookmarks.Bookmarks, Length(GGame.World.Bookmarks.Bookmarks) + 1);
  with GGame.World.Bookmarks.Bookmarks[High(GGame.World.Bookmarks.Bookmarks)] do begin
    Name:=BookmarkName;
    CameraPosition:=GRenderer.Camera.Position;
    CameraDirection:=GRenderer.Camera.Direction;
  end;
end;

procedure TMain.mBookmarksDeleteClick(Sender: TObject);
var
  Names: array of string;
  I, Idx: Integer;
begin
  SetLength(Names, Length(GGame.World.Bookmarks.Bookmarks));
  for I:=0 to High(Names) do
    Names[I]:=GGame.World.Bookmarks.Bookmarks[I].Name;
  Idx:=InputCombo('Delete Bookmark', 'Select a bookmark to delete', Names);
  if Idx=-1 then Exit;
  for I:=Idx to High(Names) - 1 do
    GGame.World.Bookmarks.Bookmarks[I]:=GGame.World.Bookmarks.Bookmarks[I + 1];
  SetLength(GGame.World.Bookmarks.Bookmarks, High(Names));
end;

procedure TMain.pbTexturesMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
  sbTextures.Position:=sbTextures.Position - WheelDelta;
end;

procedure TMain.pcSidebarChange(Sender: TObject);
begin
  UpdateEntityListIfVisible;
end;

procedure TMain.cbListCamGridEntitiesChange(Sender: TObject);
begin
  ListCamGridEntitiesChecked:=cbListCamGridEntities.Checked;
  UpdateEntityList;
end;

procedure TMain.lbEntityClassesSelectionChange(Sender: TObject; User: boolean);
begin
  UpdateEntityList;
end;

procedure TMain.mEntityMoveCameraToSelectionClick(Sender: TObject);
begin
  EditorCamera.Position:=SelAABox.Center;
  EditorCamera.PositionY:=SelAABox.Max.Y + 0.1;
end;

procedure TMain.lbEntityListSelectionChange(Sender: TObject; User: boolean);
begin
  // Ignore programmatic changes
  if not User then Exit;
  // Select the object the user selected
  if lbEntityList.ItemIndex <> -1 then
    SelectedObject:=TSerializable(lbEntityList.Items.Objects[lbEntityList.ItemIndex]);
end;

procedure TMain.lbEntityListDblClick(Sender: TObject);
begin
  // If there is a selected object (which will be the item the user double
  // clicked on since the first click will select it), move the camera there
  if lbEntityList.ItemIndex <> -1 then
    mEntityMoveCameraToSelection.Click;
end;

procedure TMain.cbListOnlyWithNameChange(Sender: TObject);
begin
  UpdateEntityListIfVisible;
end;

procedure TMain.SetSelectedObject(AValue: TSerializable);
begin
  FSelectedObject:=AValue;
  TIPropertyGrid1.TIObject:=AValue;
  TIPropertyGrid1.ShowGutter:=Assigned(AValue);
  if Assigned(AValue) then begin
    if AValue is TGridCellProxy then
      lbSelObj.Caption:='Cell ' + IntToStr(TGridCellProxy(AValue).CellX) + ',' +
        IntToStr(TGridCellProxy(AValue).CellY) + ' of grid ' +
        IntToStr(GGame.World.GridMap.IndexOfGrid(TGridCellProxy(AValue).Grid)) + ' properties:'
    else if AValue is TGrid then
      lbSelObj.Caption:='Grid ' + IntToStr(GGame.World.GridMap.IndexOfGrid(TGrid(AValue))) + ' properties:'
    else
      lbSelObj.Caption:='Object ' + AValue.ClassName + ' properties:';
  end else
    lbSelObj.Caption:='No selected object';
end;

procedure TMain.SetCurrentFileName(AValue: string);
begin
  if FCurrentFileName=AValue then Exit;
  FCurrentFileName:=AValue;
  if AValue='' then Caption:='World Editor' else Caption:='World Editor - ' + AValue;
end;

procedure TMain.OnBookmarkClick(ASender: TObject);
begin
  with GGame.World.Bookmarks.Bookmarks[TMenuItem(ASender).Tag] do begin
    EditorCamera.Position:=CameraPosition;
    EditorCamera.Direction:=CameraDirection;
  end;
end;

procedure TMain.NewWorldFile;
begin
  SelectedObject:=nil;
  GGame.LoadWorld('');
  CurrentFileName:='';
  UpdateEntityListIfVisible;
end;

procedure TMain.SaveWorldFile(AFileName: string);
begin
  RecalculatePortals;
  GGame.SaveWorld(AFileName);
  CurrentFileName:=AFileName;
end;

procedure TMain.LoadWorldFile(AFileName: string);
begin
  SelectedObject:=nil;
  GGame.LoadWorld(AFileName);
  if GGame.World.DeserializedVersion < 7 then RecalculatePortals;
  EditorCamera.Position:=Vector(0, 2, 0);
  CurrentFileName:=AFileName;
  UpdateEntityListIfVisible;
end;

procedure TMain.Relight;

  // Calculate lighting at the given point
  function LightAt(P: TVector; N: PVector=nil): Single;
  var
    LightPos: TVector;
    I: Integer;
    LightRadius, LightInt, D, Dist: Single;
    Ray: TRay;
    Info: TWorldRayHitInfo;
    Grid: TGrid;
  begin
    // If shift is pressed, return full light
    if (GetKeyState(VK_SHIFT) and $8000) <> 0 then Exit(1);
    // Ambient
    Grid:=GGame.World.GridMap.GridAt(P);
    if Assigned(Grid) then Result:=Grid.Ambient/255 else Result:=0.1;
    // Go through all lights in the world
    for I:=0 to GGame.World.EntityCount - 1 do begin
      if not (GGame.World.Entities[I] is TLight) then Continue;
      LightPos:=TLight(GGame.World.Entities[I]).Transform.Translation;
      LightRadius:=TLight(GGame.World.Entities[I]).Radius;
      LightInt:=TLight(GGame.World.Entities[I]).Light;
      // Check distance between point and light
      Dist:=Distance(LightPos, P);
      if Dist <= LightRadius then begin
        // Check if there is a clear line between the light and the point
        Ray.FromSegment(LightPos, P);
        Info.Reset;
        Info.IgnoreAllEntities:=True;
        Info.GridHit.IgnoreHoles:=True;
        // Check for shadows
        D:=1;
        if TLight(GGame.World.Entities[I]).CastShadows then begin
          if GGame.World.RayHit(Ray, Info) then begin
            // Check if the hit is between the light and the point
            if Dist >= Distance(LightPos, Info.IP) then
              D:=0;
          end;
        end;
        // If a normal is provided, use it to shade the surface
        if Assigned(N) then begin
          D *= Clamp(N^.Dot(Direction(P, LightPos)));
        end;
        Result += ((1 - (Dist/LightRadius))*LightInt)*D;
      end;
    end;
    // Clamp the result
    Result:=Clamp(Result);
  end;

  // Relight the given grid
  procedure RelightGrid(Grid: TGrid);
  var
    X, Y, SX, SY: Integer;
    Final: Single;
  begin
    // Relight cells
    for Y:=0 to Grid.Height - 1 do
      for X:=0 to Grid.Width - 1 do begin
        Final:=0.0;
        for SX:=-1 to 1 do
          for SY:=-1 to 1 do begin
            Final += LightAt(
              Grid.Origin.Added(Vector(X*GridCellSize + GridCellSize/2 + SX*GridCellSize,
                                       Grid.CellHeight/2,
                                       Y*GridCellSize + GridCellSize/2 + SY*GridCellSize)));
          end;
        Final /= 9;
        Grid.Cells[X, Y]^.Light:=Round(255*Final);
      end;
    // Update scene node
    Grid.UpdateSceneNode;
  end;

  // Relight the given entity
  procedure RelightEntity(Entity: TEntity);

    // Relight the entity as a static mesh
    procedure RelightStaticMesh;
    var
      Lighting: array of Byte;
      I: Integer;
      N, TA, TB, TC: TVector;
      Mtx: TMatrix;
    begin
      with TStaticMeshEntity(Entity) do begin
        case LightingType of
          smltFullbright:
            SetLighting([]);
          smltConstant:
            SetLighting([Round(255*Clamp(LightAt(Transform.ToMatrix.Transformed(Mesh.AABox.Center))))]);
          smltPerVertex: begin
            SetLength(Lighting, Mesh.TriangleCount*3);
            Mtx:=Transform.ToMatrix;
            for I:=0 to Mesh.TriangleCount - 1 do begin
              TA:=Mtx.Transformed(Mesh.Triangles[I].A);
              TB:=Mtx.Transformed(Mesh.Triangles[I].B);
              TC:=Mtx.Transformed(Mesh.Triangles[I].C);
              N:=TriangleNormal(TA, TB, TC);
              Lighting[I*3]:=Round(255*Clamp(LightAt(TA, @N)));
              Lighting[I*3 + 1]:=Round(255*Clamp(LightAt(TB, @N)));
              Lighting[I*3 + 2]:=Round(255*Clamp(LightAt(TC, @N)));
            end;
            SetLighting(Lighting);
          end;
        end;
      end;
    end;

  begin
    // Static mesh?
    if Entity is TStaticMeshEntity then
      RelightStaticMesh;
  end;

var
  I: Integer;
begin
  // Relight grids
  for I:=0 to GGame.World.GridMap.GridCount - 1 do
    RelightGrid(GGame.World.GridMap.Grids[I]);
  // Relight entities
  for I:=0 to GGame.World.EntityCount - 1 do
    RelightEntity(GGame.World.Entities[I]);
end;

procedure TMain.RecalculatePortals;

  // Generate portals for the given grid
  procedure PortalsForGrid(Grid: TGrid; GridIndex: Integer);
  var
    CY, CX: Integer;
    FloorY, CeilingY: Single;

    // Check if a portal can be placed at the given coordinates
    procedure ConsiderPortal(const A, B, C, D: TVector);
    var
      Probe: TVector;
      ProbedGrid: TGrid;
      ProbedFloorHeight, ProbedCeilingHeight: Single;
    begin
      // Probe the world for another grid behind the portal
      Probe:=A.Added(B).Added(C).Added(D).Scaled(0.25).Subbed(TriangleNormal(A, B, C).Added(TriangleNormal(A, C, D)).Normalized.Scaled(0.01));
      ProbedGrid:=GGame.World.GridMap.GridAt(Probe, Grid);
      // Abort if no probe was found or the probe pointed to the same grid
      if not Assigned(ProbedGrid) then Exit;
      // Abort if whatever is behind the portal is solid
      ProbedGrid.GetFloorAndCeilingHeightsAt(Probe.X, Probe.Z, ProbedFloorHeight, ProbedCeilingHeight);
      if ProbedFloorHeight + 0.1 > ProbedCeilingHeight then Exit;
      // Add portal
      Grid.AddPortal(A, B, C, D, ProbedGrid);
    end;

  begin
    // Remove any existing portals
    Grid.RemovePortals;
    // Floor and ceiling portals
    for CY:=0 to Grid.Height - 1 do
      for CX:=0 to Grid.Width - 1 do with Grid.Cells[CX, CY]^ do begin
        // Floor portals
        if Floor=0 then begin
          ConsiderPortal(
            Vector(Grid.OriginX + CX*GridCellSize, Grid.OriginY + FloorHeight/100, Grid.OriginZ + CY*GridCellSize),
            Vector(Grid.OriginX + CX*GridCellSize, Grid.OriginY + FloorHeight/100, Grid.OriginZ + CY*GridCellSize + GridCellSize),
            Vector(Grid.OriginX + CX*GridCellSize + GridCellSize, Grid.OriginY + FloorHeight/100, Grid.OriginZ + CY*GridCellSize + GridCellSize),
            Vector(Grid.OriginX + CX*GridCellSize + GridCellSize, Grid.OriginY + FloorHeight/100, Grid.OriginZ + CY*GridCellSize));
        end;
        // Ceiling portals
        if Ceiling=0 then begin
          ConsiderPortal(
            Vector(Grid.OriginX + CX*GridCellSize, Grid.OriginY + Grid.CellHeight - CeilingHeight/100, Grid.OriginZ + CY*GridCellSize),
            Vector(Grid.OriginX + CX*GridCellSize + GridCellSize, Grid.OriginY + Grid.CellHeight - CeilingHeight/100, Grid.OriginZ + CY*GridCellSize),
            Vector(Grid.OriginX + CX*GridCellSize + GridCellSize, Grid.OriginY + Grid.CellHeight - CeilingHeight/100, Grid.OriginZ + CY*GridCellSize + GridCellSize),
            Vector(Grid.OriginX + CX*GridCellSize, Grid.OriginY + Grid.CellHeight - CeilingHeight/100, Grid.OriginZ + CY*GridCellSize + GridCellSize));
        end;
      end;
    // Left edge portals
    for CY:=0 to Grid.Height - 1 do with Grid.Cells[0, CY]^ do begin
      Grid.GetFloorAndCeilingHeightsForCell(Grid.Cells[0, CY], FloorY, CeilingY);
      if Floor=0 then FloorY:=Grid.AABox.Min.Y;
      if Ceiling=0 then CeilingY:=Grid.AABox.Max.Y;
      if FloorY + 0.1 > CeilingY then Continue;
      ConsiderPortal(
        Vector(Grid.OriginX, CeilingY, Grid.OriginZ + CY*GridCellSize + GridCellSize),
        Vector(Grid.OriginX, FloorY, Grid.OriginZ + CY*GridCellSize + GridCellSize),
        Vector(Grid.OriginX, FloorY, Grid.OriginZ + CY*GridCellSize),
        Vector(Grid.OriginX, CeilingY, Grid.OriginZ + CY*GridCellSize));
    end;
    // Right edge portals
    for CY:=0 to Grid.Height - 1 do with Grid.Cells[0, CY]^ do begin
      Grid.GetFloorAndCeilingHeightsForCell(Grid.Cells[Grid.Width - 1, CY], FloorY, CeilingY);
      if Floor=0 then FloorY:=Grid.AABox.Min.Y;
      if Ceiling=0 then CeilingY:=Grid.AABox.Max.Y;
      if FloorY + 0.1 > CeilingY then Continue;
      ConsiderPortal(
        Vector(Grid.OriginX + Grid.Width*GridCellSize, CeilingY, Grid.OriginZ + CY*GridCellSize),
        Vector(Grid.OriginX + Grid.Width*GridCellSize, FloorY, Grid.OriginZ + CY*GridCellSize),
        Vector(Grid.OriginX + Grid.Width*GridCellSize, FloorY, Grid.OriginZ + CY*GridCellSize + GridCellSize),
        Vector(Grid.OriginX + Grid.Width*GridCellSize, CeilingY, Grid.OriginZ + CY*GridCellSize + GridCellSize));
    end;
    // Back edge portals
    for CX:=0 to Grid.Width - 1 do with Grid.Cells[CX, 0]^ do begin
      Grid.GetFloorAndCeilingHeightsForCell(Grid.Cells[CX, 0], FloorY, CeilingY);
      if Floor=0 then FloorY:=Grid.AABox.Min.Y;
      if Ceiling=0 then CeilingY:=Grid.AABox.Max.Y;
      if FloorY + 0.1 > CeilingY then Continue;
      ConsiderPortal(
        Vector(Grid.OriginX + CX*GridCellSize, CeilingY, Grid.OriginZ),
        Vector(Grid.OriginX + CX*GridCellSize, FloorY, Grid.OriginZ),
        Vector(Grid.OriginX + CX*GridCellSize + GridCellSize, FloorY, Grid.OriginZ),
        Vector(Grid.OriginX + CX*GridCellSize + GridCellSize, CeilingY, Grid.OriginZ));
    end;
    // Front edge portals
    for CX:=0 to Grid.Width - 1 do with Grid.Cells[CX, Grid.Height - 1]^ do begin
      Grid.GetFloorAndCeilingHeightsForCell(Grid.Cells[CX, Grid.Height - 1], FloorY, CeilingY);
      if Floor=0 then FloorY:=Grid.AABox.Min.Y;
      if Ceiling=0 then CeilingY:=Grid.AABox.Max.Y;
      if FloorY + 0.1 > CeilingY then Continue;
      ConsiderPortal(
        Vector(Grid.OriginX + CX*GridCellSize + GridCellSize, CeilingY, Grid.OriginZ + Grid.Height*GridCellSize),
        Vector(Grid.OriginX + CX*GridCellSize + GridCellSize, FloorY, Grid.OriginZ + Grid.Height*GridCellSize),
        Vector(Grid.OriginX + CX*GridCellSize, FloorY, Grid.OriginZ + Grid.Height*GridCellSize),
        Vector(Grid.OriginX + CX*GridCellSize, CeilingY, Grid.OriginZ + Grid.Height*GridCellSize));
    end;
  end;

var
  I: Integer;
begin
  GEditorQuads:=nil;
  // Ensure the scene is in portal mode
  GGame.Scene.VisibilityMode:=svmPortals;
  // Generate portals
  for I:=0 to GGame.World.GridMap.GridCount - 1 do
    PortalsForGrid(GGame.World.GridMap.Grids[I], I);
  // Update grid scene ndoes
  for I:=0 to GGame.World.GridMap.GridCount - 1 do
    GGame.World.GridMap.Grids[I].UpdateSceneNode;  end;

procedure TMain.UpdateWallTextures;
var
  I, X, Y: Integer;
  Bmp: TBitmap;
  Tex: TTexture;
begin
  // Free previous bitmaps
  for I:=0 to High(WallTextures) do
    WallTextures[I].Bitmap.FreeImage;
  SelectedWallTexture:=0;
  // Find available textures
  SetLength(WallTextures, 0);
  for I:=0 to 255 do
    if GResourceManager.WallTextures[I] <> GResourceManager.ErrorTexture then begin
      // Create the texture bitmap
      Tex:=TTexture(GResourceManager.WallTextures[I]);
      Bmp:=TBitmap.Create;
      Bmp.Width:=64;
      Bmp.Height:=64;
      Bmp.PixelFormat:=pf24bit;
      for Y:=0 to 63 do
        for X:=0 to 63 do with GRenderer.ColorPalette do
          Bmp.Canvas.Pixels[X, Y]:=RGBToColor(
            Red[Tex.Texels[X, Y]],
            Green[Tex.Texels[X, Y]],
            Blue[Tex.Texels[X, Y]]);
      // Add the texture to the array
      SetLength(WallTextures, Length(WallTextures) + 1);
      WallTextures[High(WallTextures)].Index:=I;
      WallTextures[High(WallTextures)].Bitmap:=Bmp;
    end;
  // Arrange the textures
  ArrangeWallTextures;
end;

procedure TMain.ArrangeWallTextures;
var
  X, Y, I: Integer;
begin
  // Arrange textures
  X:=5;
  Y:=5;
  for I:=0 to High(WallTextures) do begin
    if X + 66 >= pbTextures.Width then begin
      X:=5;
      Y += 66;
    end;
    WallTextures[I].X:=X;
    WallTextures[I].Y:=Y;
    Inc(X, 66);
  end;
  // Update scrollbar range
  if X > 5 then Inc(Y, 65);
  sbTextures.Max:=Y;
  // Refresh textures
  pbTextures.Invalidate;
end;

procedure TMain.RestartAnimatedEntities;
var
  I: Integer;
begin
  for I:=0 to GGame.World.EntityCount - 1 do
    if GGame.World.Entities[I] is TPieceModelEntity then
      TPieceModelEntity(GGame.World.Entities[I]).RestartAnimation;
end;

procedure TMain.UpdateEntityList;
var
  I: Integer;
  ClassFilter: TClass = nil;
  PreviousSelection: TObject = nil;
  CamGrid: TGrid = nil;
  OnlyWithName: Boolean;

  // Return a description for an entity
  function GetDesc(E: TEntity): string;
  begin
    // If the entity has a name, use it up front
    if E.Name <> '' then
      Result:=E.Name + ' '
    else
      Result:='';
    // If this is a sound emitter or mesh, mention its resource
    if E is TStaticMeshEntity then
      Result += '[' + TStaticMeshEntity(E).MeshName + '] '
    else if E is TSoundEmitter then
      Result += '[' + TSoundEmitter(E).SoundFile + '] ';
    // Trail with class name
    Result += '(' + E.ClassName + ')';
  end;

begin
  // Check if there is a class filter
  if lbEntityClasses.ItemIndex > 0 then
    ClassFilter:=TClass(lbEntityClasses.Items.Objects[lbEntityClasses.ItemIndex]);
  // Check if ther is a camera grid filter
  if ListCamGridEntitiesChecked then
    CamGrid:=GGame.World.GridMap.GridAt(GRenderer.Camera.Position);
  // Check if only named entities are wanted
    OnlyWithName:=cbListOnlyWithName.Checked;
  // Save previous selection, if any
  if lbEntityList.ItemIndex <> -1 then
    PreviousSelection:=lbEntityList.Items.Objects[lbEntityList.ItemIndex];
  // Fill new items
  lbEntityList.Items.BeginUpdate;
  lbEntityList.Items.Clear;
  for I:=0 to GGame.World.EntityCount - 1 do begin
    // Filter out unnamed entities
    if OnlyWithName and (GGame.World.Entities[I].Name='') then Continue;
    // Filter out unwanted classes
    if Assigned(ClassFilter) and not (GGame.World.Entities[I] is ClassFilter) then
      Continue;
    // Filter out entites outside of the grid, if set
    if Assigned(CamGrid) and not CamGrid.AABox.Overlaps(GGame.World.Entities[I].AABox) then
      Continue;
    // Add entry for this item
    lbEntityList.Items.AddObject(GetDesc(GGame.World.Entities[I]), GGame.World.Entities[I]);
  end;
  // Restore selection
  if Assigned(PreviousSelection) then
    lbEntityList.ItemIndex:=lbEntityList.Items.IndexOfObject(PreviousSelection);
  // Done
  lbEntityList.Items.EndUpdate;
end;

procedure TMain.UpdateEntityListIfVisible;
begin
  if pcSidebar.ActivePage=tsEntities then UpdateEntityList;
end;

function TMain.CreateGridAt(AOrigin: TVector; AWidth, AHeight: Integer): TGrid;
begin
  // Need some minimum size
  AWidth:=Max(AWidth, 1);
  AHeight:=Max(AHeight, 1);
  // Round the origin position
  AOrigin.X:=Round(AOrigin.X);
  AOrigin.Y:=Round(AOrigin.Y);
  AOrigin.Z:=Round(AOrigin.Z);
  // Create the grid
  Result:=TGrid.Create;
  Result.Init(AWidth, AHeight);
  Result.Origin:=AOrigin;
  GGame.World.GridMap.AddGrid(Result);
  Result.UpdateAABox;
  // Create its scene nodes
  GGame.World.GridMap.RemoveSceneNodes(GGame.Scene);
  GGame.World.GridMap.AddSceneNodes(GGame.Scene);
end;

function TMain.CreateEntityAt(AEntityClass: TEntityClass; APosition: TVector): TEntity;
var
  T: TTransform;
begin
  // Create the entity
  if AEntityClass=nil then Exit;
  Result:=TEntity(AEntityClass.NewInstance);
  Result.Create;
  // Setup the transformation
  T.Reset;
  T.Translation:=APosition;
  Result.Transform:=T;
  // Add the entity to the world
  GGame.World.AddEntity(Result);
  UpdateEntityListIfVisible;
end;

procedure TMain.Render;
var
  Box: TAABox;
begin
  // Render selection box
  if not Assigned(SelectedObject) then Exit;
  // Scene node entity?
  if SelectedObject is TEntity then begin
    Box:=TEntity(SelectedObject).CollisionBox;
    // Grow non-scene node entities a bit
    if not (SelectedObject is TSceneNodeEntity) then
      Box.Grow(0.1);
  end
  // Grid?
  else if SelectedObject is TGrid then
    Box:=TGrid(SelectedObject).AABox
  // Grid cell?
  else if SelectedObject is TGridCellProxy then begin
    with TGridCellProxy(SelectedObject) do begin
      Box.Min.X:=Grid.Origin.X + CellX*GridCellSize;
      Box.Min.Y:=Grid.Origin.Y + FloorHeight;
      Box.Min.Z:=Grid.Origin.Z + CellY*GridCellSize;
      Box.Max.X:=Grid.Origin.X + CellX*GridCellSize + GridCellSize;
      Box.Max.Z:=Grid.Origin.Z + CellY*GridCellSize + GridCellSize;
      case CellPart of
        cpNone: Exit;
        cpLower: begin
          Box.Min.Y:=Grid.Origin.Y;
          Box.Max.Y:=Grid.Origin.Y + FloorHeight - 0.01;
        end;
        cpFloor: begin
          Box.Min.Y:=Grid.Origin.Y + FloorHeight;
          Box.Max.Y:=Grid.Origin.Y + FloorHeight + 0.05;
        end;
        cpCeiling: begin
          Box.Min.Y:=Grid.Origin.Y + CeilingHeight - 0.05;
          Box.Max.Y:=Grid.Origin.Y + CeilingHeight;
        end;
        cpUpper: begin
          Box.Min.Y:=Grid.Origin.Y + CeilingHeight + 0.01;
          Box.Max.Y:=Grid.Origin.Y + grid.CellHeight;
        end;
      end;
      Box.Grow(0.02);
      // Draw extents
      GRenderer.RenderTriLine(Box.Min, Box.Max);
    end;
  end;
  // Draw the box
  GRenderer.RenderAABox(Box);
  // Store it for further use
  SelAABox:=Box;
end;

end.

