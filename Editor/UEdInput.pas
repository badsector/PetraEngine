{ Editor input driver }
unit UEdInput;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, InputDrv;

type
  { TEditorInputDriver }
  TEditorInputDriver = class(TInputDriver)
  public
    // TInputDriver methods
    function IsLive: Boolean; override;
  public
    // Called by main for key events
    procedure OnKeyEvent(Key: DWORD; Down: Boolean);
  end;

implementation

uses
  UEdMain;

{ Functions }

// Returns the input key for the given virtual key code
function GetKeyFor(VKey: DWORD): TInputKey;
begin
  case VKey of
    27: Result:=ikEscape;
    112: Result:=ikF1;
    113: Result:=ikF2;
    114: Result:=ikF3;
    115: Result:=ikF4;
    116: Result:=ikF5;
    117: Result:=ikF6;
    118: Result:=ikF7;
    119: Result:=ikF8;
    120: Result:=ikF9;
    121: Result:=ikF10;
    122: Result:=ikF11;
    123: Result:=ikF12;
    145: Result:=ikScrollLock;
    19: Result:=ikPause;
    192: Result:=ikBackQuote;
    48: Result:=ik0;
    49: Result:=ik1;
    50: Result:=ik2;
    51: Result:=ik3;
    52: Result:=ik4;
    53: Result:=ik5;
    54: Result:=ik6;
    55: Result:=ik7;
    56: Result:=ik8;
    57: Result:=ik9;
    189: Result:=ikMinus;
    187: Result:=ikEquals;
    8: Result:=ikBackspace;
    45: Result:=ikInsert;
    36: Result:=ikHome;
    33: Result:=ikPageUp;
    34: Result:=ikPageDown;
    9: Result:=ikTab;
    65: Result:=ikA;
    66: Result:=ikB;
    67: Result:=ikC;
    68: Result:=ikD;
    69: Result:=ikE;
    70: Result:=ikF;
    71: Result:=ikG;
    72: Result:=ikH;
    73: Result:=ikI;
    74: Result:=ikJ;
    75: Result:=ikK;
    76: Result:=ikL;
    77: Result:=ikM;
    78: Result:=ikN;
    79: Result:=ikO;
    80: Result:=ikP;
    81: Result:=ikQ;
    82: Result:=ikR;
    83: Result:=ikS;
    84: Result:=ikT;
    85: Result:=ikU;
    86: Result:=ikV;
    87: Result:=ikW;
    88: Result:=ikX;
    89: Result:=ikY;
    90: Result:=ikZ;
    219: Result:=ikLeftBracket;
    221: Result:=ikRightBracket;
    220: Result:=ikBackSlash;
    13: Result:=ikEnter;
    20: Result:=ikCapsLock;
    186: Result:=ikSemiColon;
    222: Result:=ikQuote;
    16: Result:=ikShift;
    188: Result:=ikComma;
    190: Result:=ikPeriod;
    191: Result:=ikSlash;
    17: Result:=ikControl;
    18: Result:=ikAlt;
    91: Result:=ikLeftMeta;
    92: Result:=ikRightMeta;
    32: Result:=ikSpace;
    46: Result:=ikDelete;
    35: Result:=ikEnd;
    37: Result:=ikLeft;
    39: Result:=ikRight;
    38: Result:=ikUp;
    40: Result:=ikDown;
    141: Result:=ikNumLock;
    111: Result:=ikDivide;
    106: Result:=ikMultiply;
    109: Result:=ikSubtract;
    107: Result:=ikAdd;
    96: Result:=ikKeypad0;
    97: Result:=ikKeypad1;
    98: Result:=ikKeypad2;
    99: Result:=ikKeypad3;
    100: Result:=ikKeypad4;
    101: Result:=ikKeypad5;
    102: Result:=ikKeypad6;
    103: Result:=ikKeypad7;
    104: Result:=ikKeypad8;
    105: Result:=ikKeypad9;
    110: Result:=ikKeypadPoint;
    10000: Result:=ikLeftButton;
    10001: Result:=ikMiddleButton;
    10002: Result:=ikRightButton;
    10003: Result:=ikWheelUp;
    10004: Result:=ikWheelDown;
    else Result:=ikUnknown;
  end;
end;

{ TEditorInputDriver }
function TEditorInputDriver.IsLive: Boolean;
begin
  Result:=Main.Visible;
end;

procedure TEditorInputDriver.OnKeyEvent(Key: DWORD; Down: Boolean);
begin
  NotifyHandlersForKey(GetKeyFor(Key), Down);
end;

end.

