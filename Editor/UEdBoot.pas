{ Editor boot-up - this unit is meant to be included by the game-specific editor
  project source code, usually called Editor.lpr }
unit UEdBoot;

{$mode objfpc}{$H+}
{$WARN 5023 off : Unit "$1" not used in $2}

interface

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, Menus,
  DataLoad, UEdMain, UEdSys, UEdInput, UEdGfx, Serial, GameBase,
  Game, ResMan, CamEnt, ScrEnt, UIScrns, Scripts, SndEmit,
  UEdMultilineTextBox, UEdTableEditor, UEdSnd
  { you can add units after this };

// Boot the editor for the given game using data from the given directory
procedure BootEditor(AGameTitle, ADataDir: string);
// Run the editor normally
procedure RunEditor;
// Run the editor and enter play mode for the given map file. If IgnoreStartupScript
// is True then do not run the script in the script entity named 'startup'
procedure RunEditorAndPlayMap(AMapName: string; IgnoreStartupScript: Boolean=False);
// Register a custom entry for a tool in the given menubar menu (if the menu
// does not exist it will be created) - plain procedure version
procedure RegisterEditorTool(AMenuName, AToolName: string; AddSeparator: Boolean; AToolProc: TProcedure);
// Register a custom entry for a tool in the given menubar menu (if the menu
// does not exist it will be created) - method version
procedure RegisterEditorTool(AMenuName, AToolName: string; AddSeparator: Boolean; AToolProc: TProcedureOfObject);
// Register a custom entry for a tool in the given menubar menu (if the menu
// does not exist it will be created) - form version, this will *create* a new
// form of the given class and show it as a modal form depending on the Modal
// parameter
procedure RegisterEditorTool(AMenuName, AToolName: string; AddSeparator: Boolean; AToolForm: TFormClass; Modal: Boolean=True);

implementation

uses
  Engine;

type
  { TMenuHandler - Information about a registered handler }
  TMenuHandler = record
    Proc: TProcedure;
    Method: TProcedureOfObject;
    FormClass: TFormClass;
    Modal: Boolean;
  end;

  { TMenuEventContainer - TMenuItem needs a method as a handler so all this
    class does is to provide that }
  TMenuEventContainer = class
  private
    procedure OnClick(Sender: TObject);
  end;

var
  // Known handlers
  LMenuHandlers: array of TMenuHandler;
  // TMenuEventContainer instance
  LMenuEventContainer: TMenuEventContainer;

{ Local Functions }

function FindOrCreateMainMenuItem(AMenuName, AToolName: string; AddSeparator: Boolean): TMenuItem;
var
  MenuBarItem: TMenuItem;
begin
  // Find or create menu bar item
  MenuBarItem:=Main.MainMenu1.Items.Find(AMenuName);
  if not Assigned(MenuBarItem) then begin
    MenuBarItem:=TMenuItem.Create(Main.MainMenu1);
    MenuBarItem.Caption:=AMenuName;
    Main.MainMenu1.Items.Add(MenuBarItem);
  end;
  // Add separator if requested
  if AddSeparator and (MenuBarItem.Count > 0) then
    MenuBarItem.AddSeparator;
  // Create menu item for the tool
  Result:=TMenuItem.Create(Main.MainMenu1);
  Result.Caption:=AToolName;
  Result.OnClick:=@LMenuEventContainer.OnClick;
  Result.Tag:=Length(LMenuHandlers);
  MenuBarItem.Add(Result);
  // Register a new handler for the menu item (empty, will be filled later)
  SetLength(LMenuHandlers, Length(LMenuHandlers) + 1);
  with LMenuHandlers[High(LMenuHandlers)] do begin
    Proc:=nil;
    Method:=nil;
    FormClass:=nil;
    Modal:=False;
  end;
end;

{ Functions }

procedure BootEditor(AGameTitle, ADataDir: string);
begin
  // Store title
  GGameTitle:=AGameTitle;
  // Store data directory
  DataDirectoryPrefix:=ADataDir;
  if DataDirectoryPrefix <> '/' then DataDirectoryPrefix += '/';  

  RequireDerivedFormResource:=True;
  Application.Title:='PetraWorldEditor';
  Application.Initialize;
  Application.CreateForm(TMain, Main);
end;

procedure RunEditor;
begin
  Application.Run;
end;

procedure RunEditorAndPlayMap(AMapName: string; IgnoreStartupScript: Boolean);
begin
  GPlayMapOnEditorStart:=AMapName;
  GPlayMapOnEditorNoStartup:=IgnoreStartupScript;
  Application.Run;
end;

procedure RegisterEditorTool(AMenuName, AToolName: string; AddSeparator: Boolean; AToolProc: TProcedure);
var
  MenuItem: TMenuItem;
begin
  // Create menu item
  MenuItem:=FindOrCreateMainMenuItem(AMenuName, AToolName, AddSeparator);
  // Set handler
  LMenuHandlers[MenuItem.Tag].Proc:=AToolProc;
end;

procedure RegisterEditorTool(AMenuName, AToolName: string; AddSeparator: Boolean; AToolProc: TProcedureOfObject);
var
  MenuItem: TMenuItem;
begin
  // Create menu item
  MenuItem:=FindOrCreateMainMenuItem(AMenuName, AToolName, AddSeparator);
  // Set handler
  LMenuHandlers[MenuItem.Tag].Method:=AToolProc;
end;

procedure RegisterEditorTool(AMenuName, AToolName: string; AddSeparator: Boolean; AToolForm: TFormClass; Modal: Boolean);
var
  MenuItem: TMenuItem;
begin
  // Create menu item
  MenuItem:=FindOrCreateMainMenuItem(AMenuName, AToolName, AddSeparator);
  // Set handler
  LMenuHandlers[MenuItem.Tag].FormClass:=AToolForm;
  LMenuHandlers[MenuItem.Tag].Modal:=Modal;
end;

{ TMenuEventContainer }

procedure TMenuEventContainer.OnClick(Sender: TObject);
var
  Form: TForm;
begin
  with LMenuHandlers[TMenuItem(Sender).Tag] do begin
    if Assigned(Proc) then begin
      Proc;
      Exit;
    end;
    if Assigned(Method) then begin
      Method;
      Exit;
    end;
    if Assigned(FormClass) then begin
      Application.CreateForm(FormClass, Form);
      if Modal then
        Form.ShowModal
      else begin
        Form.Show;
        Form.BringToFront;
      end;
      Form.Free;
      Exit;
    end;
  end;
end;

initialization
  LMenuEventContainer:=TMenuEventContainer.Create;
finalization
  LMenuEventContainer.Free;
end.

