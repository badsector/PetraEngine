unit UEdMultilineTextBox;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, SynEdit;

type

  { TMultilineTextBox }

  TMultilineTextBox = class(TForm)
    SynEdit1: TSynEdit;
    btOk: TButton;
    btCancel: TButton;
  private

  public

  end;

var
  MultilineTextBox: TMultilineTextBox;

function MultilineTextDialog(ACaption, AText: string): string;

implementation

{$R *.lfm}

{ Functions }
function MultilineTextDialog(ACaption, AText: string): string;
begin
  Application.CreateForm(TMultilineTextBox, MultilineTextBox);
  MultilineTextBox.Caption:=ACaption;
  MultilineTextBox.SynEdit1.Text:=AText;
  if MultilineTextBox.ShowModal=mrOK then
    Result:=MultilineTextBox.SynEdit1.Text
  else
    Result:=AText;
end;

end.

