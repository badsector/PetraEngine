{ Editor sound driver }
unit UEdSnd;

{$mode ObjFPC}{$H+}

interface

{$IFDEF WINDOWS}
{$UnitPath ..\Engine\Windows}
{$ENDIF}

uses
  Classes, SysUtils, SndDrv, {$IFDEF WINDOWS}WinSnd{$ELSE}OpenAL{$ENDIF};

type

  { TEditorSoundDriver }
  {$IFNDEF WINDOWS}
  TEditorSoundDriver = class(TSoundDriver)
  private
    SilenceData: array [0..SoundBufferSize - 1] of SmallInt;
    function GetBufferPointer(Index: Integer): Pointer;
    procedure Mix(Index: Integer); inline;
  protected
    // TSoundDriver methods
    procedure InitializeDriver; override;
    procedure LockBuffers; override;
    procedure UnlockBuffers; override;
  public
    destructor Destroy; override;
  end;
  {$ELSE}
  TEditorSoundDriver = TWinSoundDriver;
  {$ENDIF}

implementation

{$IFNDEF WINDOWS}
uses
  Engine;

type
  { TSoundThread - Thread that feeds OpenAL with buffer data }
  TSoundThread = class(TThread)
  protected
    // TThread methods
    procedure Execute; override;
  end;

var
  // Sound thread
  SoundThread: TSoundThread;
  // Critical section for the sound thread
  SoundCrit: TRTLCriticalSection;
  // Flags for the thread
  SoundRunning: Boolean;
  // OpenAL device
  ALDevice: PALCDevice;
  // OpenAL context
  ALContext: PALCcontext;
  // OpenAL buffers
  ALBuffers: array [0..MaxSoundBuffers - 1] of ALuint;
  // OpenAL source
  ALSource: ALuint;
  // The next buffer to put in OpenAL source queue
  NextToEnqeue: Integer;

{ TSoundThread }
procedure TSoundThread.Execute;
var
  Processed: ALint;
begin
  while True do begin
    // Check if the thread should exit
    EnterCriticalSection(SoundCrit);
    if not SoundRunning then begin
      LeaveCriticalSection(SoundCrit);
      Break;
    end;
    LeaveCriticalSection(SoundCrit);
    // Check if more buffers need to be queued
    Processed:=0;
    alGetSourcei(ALSource, AL_BUFFERS_PROCESSED, Processed);
    while Processed > 0 do begin
      alSourceUnqueueBuffers(ALSource, 1, @ALBuffers[NextToEnqeue]);
      EnterCriticalSection(SoundCrit);
      TEditorSoundDriver(GSound).Mix(NextToEnqeue);
      alBufferData(ALBuffers[NextToEnqeue], AL_FORMAT_STEREO16, TEditorSoundDriver(GSound).GetBufferPointer(NextToEnqeue), SoundBufferSize*2, 22050);
      LeaveCriticalSection(SoundCrit);
      alSourceQueueBuffers(ALSource, 1, @ALBuffers[NextToEnqeue]);
      NextToEnqeue:=(NextToEnqeue + 1) mod MaxSoundBuffers;
      Dec(Processed);
    end;
    // Let other threads do stuff
    Sleep(1);
  end;
end;

{ TEditorSoundDriver }
destructor TEditorSoundDriver.Destroy;
begin
  if Assigned(SoundThread) then begin
    // Inform the thread to exit
    EnterCriticalSection(SoundCrit);
    SoundRunning:=False;
    LeaveCriticalSection(SoundCrit);
    SoundThread.WaitFor;
    FreeAndNil(SoundThread);
  end;
  if Assigned(ALContext) then begin
    alcMakeContextCurrent(nil);
    alcDestroyContext(ALContext);
  end;
  if Assigned(ALDevice) then alcCloseDevice(ALDevice);
  inherited Destroy;
end;

function TEditorSoundDriver.GetBufferPointer(Index: Integer): Pointer;
begin
  if (SilentCounter=0) and not EditorSilence then Result:=@Buffers[Index][0] else Result:=@SilenceData;
end;

procedure TEditorSoundDriver.Mix(Index: Integer);
begin
  NextBuffer:=Index;
  MixNow;
  Dec(Mixed);
end;

procedure TEditorSoundDriver.InitializeDriver;
var
  I: Integer;
begin
  Available:=False;
  // Initialize OpenAL
  ALDevice:=alcOpenDevice(nil);
  if not Assigned(ALDevice) then Exit;
  ALContext:=alcCreateContext(ALDevice, nil);
  if not Assigned(ALContext) then begin
    alcCloseDevice(ALDevice);
    ALDevice:=nil;
    Exit;
  end;
  if not alcMakeContextCurrent(ALContext) then begin
    alcDestroyContext(ALContext);
    alcCloseDevice(ALDevice);
    ALContext:=nil;
    ALDevice:=nil;
    Exit;
  end;
  alGetError;
  alGenBuffers(MaxSoundBuffers, @ALBuffers);
  if alGetError <> AL_NO_ERROR then Exit;
  alGenSources(1, @ALSource);
  if alGetError <> AL_NO_ERROR then Exit;
  alDistanceModel(AL_NONE);
  // Queue all buffers
  NextToEnqeue:=0;
  for I:=0 to MaxSoundBuffers - 1 do begin
    FillChar(Buffers[I], SizeOf(Buffers[I]), 0);
    alBufferData(ALBuffers[I], AL_FORMAT_STEREO16, @Buffers[I], SizeOf(Buffers[I]), 22050);
    alSourceQueueBuffers(ALSource, 1, @ALBuffers[I]);
  end;
  alSourcePlay(ALSource);
  // Sound?
  Available:=alGetError=AL_NO_ERROR;
  // Start sound thread
  if Available then begin
    SoundRunning:=True;
    SoundThread:=TSoundThread.Create(False);
  end;
end;

procedure TEditorSoundDriver.LockBuffers;
begin
  EnterCriticalSection(SoundCrit);
end;

procedure TEditorSoundDriver.UnlockBuffers;
begin
  LeaveCriticalSection(SoundCrit);
end;

initialization
  InitCriticalSection(SoundCrit);
finalization
  DoneCriticalSection(SoundCrit);
{$ENDIF}
end.

