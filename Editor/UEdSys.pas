{ Editor system driver }
unit UEdSys;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, SysDrv;

type
  { TEditorSystemDriver }
  TEditorSystemDriver = class(TSystemDriver)
  private
    Base: QWord;
  protected
    function GetMilliseconds: Integer; override;
  public
    constructor Create;
  end;

implementation

{ TEditorSystemDriver }
constructor TEditorSystemDriver.Create;
begin
  inherited Create;
  Base:=GetTickCount64;
end;

function TEditorSystemDriver.GetMilliseconds: Integer;
begin
  Result:=GetTickCount64 - Base;
end;

end.

