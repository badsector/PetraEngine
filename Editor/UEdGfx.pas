{ Editor graphics driver }
unit UEdGfx;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Graphics, LCLType, GraphType, GfxDrv, Colors, Raster,
  FBRaster, LCLIntf, IntfGraphics;

type

  { TEditorGraphicsDriver }
  TEditorGraphicsDriver = class(TGraphicsDriver)
  private
    // Frame pixels
    FrameData: array [0..320*200 - 1] of Byte;
    // Rasterizer
    Rasterizer: TFramebufferRasterizer;
    // Bitmap to use for drawing the frame
    Bitmap: TBitmap;
    // Color palette
    ColorPalette: array [0..255] of Cardinal;
  public
    // TGraphicsDriver methods
    procedure Initialize; override;
    procedure Shutdown; override;
    function BeginFrame: PFrameData; override;
    procedure FinishFrame; override;
    procedure SetColorPalettePart(const APalette: TColorPalette; First, Last: Integer); override;
    procedure SetGamma(AGamma: TGammaValue); override;
    function GetRasterizer: TRasterizer; override;
  end;

implementation

uses
  UEdMain;

{ TEditorGraphicsDriver }

procedure TEditorGraphicsDriver.Initialize;
begin
  Rasterizer:=TFramebufferRasterizer.Create;
  // Create a temp bitmap
  Bitmap:=TBitmap.Create;
  Bitmap.PixelFormat:={$IFDEF WINDOWS}pf32bit{$ELSE}pf24bit{$ENDIF};
  Bitmap.SetSize(1, 1);
  Bitmap.BeginUpdate();
  Bitmap.EndUpdate();
end;

procedure TEditorGraphicsDriver.Shutdown;
begin
  Bitmap.Free;
  Rasterizer.Free;
end;

function TEditorGraphicsDriver.BeginFrame: PFrameData;
begin
  Result:=@FrameData[0];
end;

procedure TEditorGraphicsDriver.FinishFrame;
var
  Tgt: PCardinal;
  X, Y, XD, YD, XR, YR: Integer;
  RawImage: TRawImage;
  Src: PByte;
begin
  // Recreate the bitmap if the render size changed
  if (Bitmap.Width <> Main.plRenderWindow.Width) or (Bitmap.Height <> Main.plRenderWindow.Height) then begin
    Bitmap.Free;
    Bitmap:=TBitmap.Create;
    Bitmap.PixelFormat:={$IFDEF WINDOWS}pf32bit{$ELSE}pf24bit{$ENDIF};
    Bitmap.SetSize(Main.plRenderWindow.Width, Main.plRenderWindow.Height);
  end;
  // Stretch to the output
  XD:=Trunc(319/Bitmap.Width*256);
  YD:=Trunc(199/Bitmap.Height*256);
  YR:=0;
  Bitmap.BeginUpdate();
  RawImage:=Bitmap.RawImage;
  for Y:=0 to Bitmap.Height - 1 do begin
    Src:=@FrameData[(YR shr 8)*320];
    Tgt:=PCardinal(RawImage.GetLineStart(Y));
    XR:=0;
    for X:=0 to Bitmap.Width - 1 do begin
      Tgt^:=ColorPalette[Src[XR shr 8]];
      Inc(Tgt);
      Inc(XR, XD);
    end;
    Inc(YR, YD);
  end;
  Bitmap.EndUpdate();
  // Show the bitmap
  Main.plRenderWindow.Canvas.Draw(0, 0, Bitmap);
end;

procedure TEditorGraphicsDriver.SetColorPalettePart(const APalette: TColorPalette; First, Last: Integer);
var
  I: Integer;
begin
  for I:=First to Last do with APalette do with Bitmap.RawImage.Description do
    ColorPalette[I]:=(Integer(Red[I]) shl Integer(RedShift)) or
                     (Integer(Green[I]) shl Integer(GreenShift)) or
                     (Integer(Blue[I]) shl Integer(BlueShift));
end;

procedure TEditorGraphicsDriver.SetGamma(AGamma: TGammaValue);
begin
  // Not implemented
end;

function TEditorGraphicsDriver.GetRasterizer: TRasterizer;
begin
  Result:=Rasterizer;
end;

end.

