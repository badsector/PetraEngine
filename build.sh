#!/usr/bin/env bash

# Game executable
exe=pgame
# Path to FPC 2.2.4 which is needed for Win9x-compatible builds
if [ -e "/c/FPC/2.2.4/bin/i386-win32/fpc.exe" ]
then
    fpc224="/c/FPC/2.2.4/bin/i386-win32/fpc.exe"
else
    fpc224="/cygdrive/c/FPC/2.2.4/bin/i386-win32/fpc.exe"
fi
# Default fpc
fpcdef="$FPC"
if [ "$fpcdef" = "" ]
then
    fpcdef="fpc"
fi
# Lazbuild path
lazbuildat=~/Code/lazarus/lazbuild

t=$1
m=debug

if [ "$t" = "" ]
then
    echo "Usage: build.sh [dos|ddraw|d3d|wgl|gdi|sdl|sdlgl|editor|utils] [release]"
    exit 1
fi

if [ "$2" = "release" ]
then
    m=release
fi

if [ "$t" = "editor" ]
then
    $lazbuildat --build-mode=$m GameSrc/Editor.lpi
    exit 0
fi

if [ "$t" = "utils" ]
then
    mkdir -p Build
    fpc -B -FEBuild ../PetraEngine/Utils/MakePAK.pas
    fpc -B -FEBuild ../PetraEngine/Utils/MakeRAW.pas
    fpc -B -FEBuild ../PetraEngine/Utils/PalConv.pas
    exit 0
fi

binsuffix=''
if [[ "$OSTYPE" == "msys"* ]]
then
    binsuffix='.exe'
fi
if [[ "$OSTYPE" == "cygwin"* ]]
then
    binsuffix='.exe'
fi

declare -A oname=(
    [dos]="$exe.exe"
    [ddraw]="win$exe.exe"
    [d3d]="d3d$exe.exe"
    [wgl]="gl$exe.exe"
    [gdi]="gdi$exe.exe"
    [sdl]="sdl$exe$binsuffix"
    [sdlgl]="sgl$exe$binsuffix"
)

if [ "${oname[$t]}" = "" ]
then
    echo "Unknown target"
    exit 1
fi

declare -A tflags=(
    [dos]='-Tgo32v2 -dPETRADOS -Pi386 -CfX87 -CpPENTIUM -OpPENTIUM -Fu../PetraEngine/Engine/DOS'
    [ddraw]='-WG -dPETRADDRAW -Pi386 -CpPENTIUM -OpPENTIUM -Fu..\FPC_DirectX92 -Fu../PetraEngine/Engine/Windows'
    [d3d]='-WG -dPETRAHW -dPETRAD3D -Pi386 -CpPENTIUM -OpPENTIUM -Fu..\FPC_DirectX92 -Fu../PetraEngine/Engine/\Windows'
    [wgl]='-WG -dPETRAHW -dPETRAGL -Pi386 -CpPENTIUM -OpPENTIUM -Fu../PetraEngine/Engine/Windows'
    [gdi]='-WG -dPETRADGDI -Pi386 -CpPENTIUM -OpPENTIUM -Fu../PetraEngine/Engine/Windows'
    [sdl]='-dPETRASDL -Fu../PetraEngine/Engine/SDL'
    [sdlgl]='-dPETRAHW -dPETRAGL -dPETRASDL -Fu../PetraEngine/Engine/SDL'
)

declare -A mflags=(
    [debug]='-gw2 -gl -gh'
    [release]='-O2 -Ci- -Co- -CO- -Cr- -Ct- -g- -Si -OoFASTMATH'
)

declare -A fpcver=(
    [ddraw]=$fpc224
    [d3d]=$fpc224
    [wgl]=$fpc224
    [gdi]=$fpc224
)

usefpc=${fpcver[$t]}

if [ "$usefpc" = "" ]
then
    usefpc=$fpcdef
fi

flags="-B -Mobjfpc -vn- -Sc -Sh -Fu../PetraEngine/Engine -FuGameSrc ${tflags[$t]} ${mflags[$m]} -o${oname[$t]}"

mkdir -p Build
$usefpc -FEBuild $flags GameSrc/Startup.pas
