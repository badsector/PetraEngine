{ Piece model scene nodes and animation }
unit PieceMdl;
{$WARN 5057 off : Local variable "$1" does not seem to be initialized}
interface

uses
  Maths, ResMan, Renderer, Textures, Meshes, Scene;

type
  // Forward class declarations
  TPieceModelNode = class;
  TPieceModelPieceNode = class;

  { TPieceModelPiece - A single piece of a piece model }
  TPieceModelPiece = class
  private
    // Piece name
    Name: string;
    // Piece mesh
    Mesh: TMesh;
    // Piece children
    Children: array of TPieceModelPiece;
    // Translation of the piece relative to its parent
    Translation: TVector;
  public
    destructor Destroy; override;
  end;

  { TPieceModelAnimation - A piece model animation }
  TPieceModelAnimation = record
    // Animation name
    Name: string;
    // Rotations for all frames and pieces (frames*number_of_pieces)
    Rotations: array of TVector;
    // Root positions (frames)
    RootPositions: array of TVector;
    // Number of frames
    Frames: Integer;
    // Frames per second
    FPS: Integer;
    // True if the animation is looping
    Looping: Boolean;
  end;

  { TPieceModel - A full piece model }
  TPieceModel = class(TResource)
  private
    // Property storage
    FTexture: TTexture;
    FPieceCount: Integer;
  private
    // Root piece
    Root: TPieceModelPiece;
    // Animations
    Animations: array of TPieceModelAnimation;
  public
    // Create an empty piece model (use LoadPMFModel to load a model)
    constructor Create;
    // Destroy the model
    destructor Destroy; override;
    // Load the given PMF model and texture
    procedure LoadPMFModel(AFileName: string;
      ATextureFileName: string; AWidth, AHeight: Integer; Scale: Single=1);
    // Create a new scene node hierarchy for the piece model
    function CreateSceneNode: TPieceModelNode;
    // Return the index of the animation with the given name
    function IndexOfAnimation(AName: string): Integer;
    // Return the piece scene node for the piece of the given name under the
    // given piece model node - the model must have been created using
    // this object's CreateSceneNode
    function GetPieceSceneNode(AName: string;
      Node: TPieceModelNode): TPieceModelPieceNode;
    // Apply the transformations for the given animation and the given
    // time to the given scene node that was created through this object
    // Returns the index of the frame that was just applied or -1 if the
    // end of the animation was reached
    function ApplyAnimation(AnimIndex: Integer; Seconds: Single;
      Node: TPieceModelNode; Blend: Single): Integer;
    // Override the texture with the given one, pass nil to restore the original
    // texture (note that this will affect all instances of this model)
    procedure OverrideTexture(ATexture: TTexture);
    // The model texture
    property Texture: TTexture read FTexture;
    // Number of pieces in the model
    property PieceCount: Integer read FPieceCount;
  end;

  { TPieceModelPieceNode - Scene node for a single piece of a piece model }
  TPieceModelPieceNode = class(TMeshNode)
  private
    // Piece rotation
    Rotation: TVector;
  protected
  public
    constructor Create;
    // Apply light level
    procedure ApplyLightLevel(ALight: Byte);
    // Apply depth bias
    procedure ApplyDepthBias(ADepthBias: Byte);
  end;

  { TPieceModelNode - Scene node for the full piece model }
  TPieceModelNode = class(TSceneNode)
  private
    // Property storage
    FRoot: TPieceModelPieceNode;
  public
    constructor Create;
    // Root piece
    property Root: TPieceModelPieceNode read FRoot;
  end;

implementation

uses
  Classes, DataLoad;

{ TPieceModelPiece }
destructor TPieceModelPiece.Destroy;
var
  I: Integer;
begin
  for I:=0 to High(Children) do Children[I].Free;
  Mesh.Free;
end;

{ TPieceModel }
constructor TPieceModel.Create;
begin
  FTexture:=TTexture.Create(1, 1);
end;

destructor TPieceModel.Destroy;
begin
  FTexture.Free;
  if Assigned(Root) then Root.Free;
end;

procedure TPieceModel.LoadPMFModel(AFileName: string; ATextureFileName: string;
  AWidth, AHeight: Integer; Scale: Single);
var
  F: TStream;
  Magic: array [0..3] of Char;
  Name31: string[31];

  // Read a piece and its children and create the appropriate piece nodes
  procedure ReadPiece(Piece: TPieceModelPiece);
  var
    N: TVector;
    TS, TT: Single;
    Tri: TRenderTri;
    I: Integer;
    Child: TPieceModelPiece;
  begin
    // Read the piece's name
    F.ReadBuffer(Name31, 32);
    Piece.Name:=Name31;
    // Read the node's translation
    F.ReadBuffer(Piece.Translation, 12);
    Piece.Translation.Scale(Scale);
    // Read the mesh data
    F.ReadBuffer(I, 4); // triangle count
    if I > 0 then begin
      Piece.Mesh:=TMesh.Create;
      Piece.Mesh.Texture:=Texture;
      for I:=0 to (I div 3)-1 do begin
        F.ReadBuffer(Tri.A, 12); // Vertex A position
        Tri.A.Scale(Scale);
        F.ReadBuffer(N, 12); // Vertex A normal
        F.ReadBuffer(TS, 4); // Normalized texture coordinates
        F.ReadBuffer(TT, 4);
        Tri.SA:=Round(TS*(AWidth - 1));
        Tri.TA:=Round(TT*(AHeight - 1));
        Tri.LA:=Round(Clamp(N.Z+N.Y*0.5, 0.75, 1)*255);
        F.ReadBuffer(Tri.B, 12); // Vertex B position
        Tri.B.Scale(Scale);
        F.ReadBuffer(N, 12); // Vertex B normal
        F.ReadBuffer(TS, 4); // Normalized texture coordinates
        F.ReadBuffer(TT, 4);
        Tri.SB:=Round(TS*(AWidth - 1));
        Tri.TB:=Round(TT*(AHeight - 1));
        Tri.LB:=Round(Clamp(N.Z+N.Y*0.5, 0.75, 1)*255);
        F.ReadBuffer(Tri.C, 12); // Vertex A position
        Tri.C.Scale(Scale);
        F.ReadBuffer(N, 12); // Vertex A normal
        F.ReadBuffer(TS, 4); // Normalized texture coordinates
        F.ReadBuffer(TT, 4);
        Tri.SC:=Round(TS*(AWidth - 1));
        Tri.TC:=Round(TT*(AHeight - 1));
        Tri.LC:=Round(Clamp(N.Z+N.Y*0.5, 0.75, 1)*255);
        Tri.Texture:=Texture;
        Tri.DepthBias:=0;
        {$IFNDEF PETRAHW}
        Tri.Occluder:=False;
        {$ENDIF}
        Piece.Mesh.AddTriangle(Tri);
      end;
      Piece.Mesh.UpdateAABox;
    end;
    // Read the children
    F.ReadBuffer(I, 4); // child count
    SetLength(Piece.Children, I);
    for I:=0 to I - 1 do begin
      // Create the child node
      Child:=TPieceModelPiece.Create;
      Piece.Children[I]:=Child;
      Inc(FPieceCount);
      // Read the child and its own children
      ReadPiece(Child);
    end;
  end;

  procedure ReadAnimations;
  var
    I, J, K: Integer;
    B: Byte;
    W: Word;
  begin
    // Read animation count
    F.ReadBuffer(I, 4);
    SetLength(Animations, I);
    // Read animations
    for I:=0 to I - 1 do with Animations[I] do begin
      // Animation name
      F.ReadBuffer(Name31, 32);
      Name:=Name31;
      // Animation flags
      F.ReadBuffer(B, 1);
      Looping:=(B and 1)=1;
      // Frames per second
      F.ReadBuffer(B, 1);
      FPS:=B;
      // Frame count
      F.ReadBuffer(W, 2);
      Frames:=W;
      // Read frames
      SetLength(RootPositions, Frames);
      SetLength(Rotations, PieceCount*Frames);
      for J:=0 to W - 1 do begin
        // Frame flags (ignored for now)
        F.ReadBuffer(B, 1);
        // Read root node position
        F.ReadBuffer(RootPositions[J], 12);
        RootPositions[J].Scale(Scale);
        // Rotations for each piece
        for K:=0 to PieceCount - 1 do begin
          F.ReadBuffer(Rotations[J*PieceCount + K], 12);
          Rotations[J*PieceCount + K].X:=Rotations[J*PieceCount + K].X*PI/180;
          Rotations[J*PieceCount + K].Y:=Rotations[J*PieceCount + K].Y*PI/180;
          Rotations[J*PieceCount + K].Z:=Rotations[J*PieceCount + K].Z*PI/180;
        end;
      end;
    end;
  end;

begin
  // Release any existing data
  if Assigned(Root) then Root.Free;
  Root:=nil;
  // Load the texture
  FTexture.SetSize(AWidth, AHeight);
  FTexture.LoadRAWFile(ATextureFileName);
  // Open the file
  F:=GetDataStream(AFileName);
  if not Assigned(F) then Exit;
  F.ReadBuffer(Magic, 4);
  if Magic <> 'PMF1' then begin
    F.Free;
    Exit;
  end;
  // Create the root node
  Root:=TPieceModelPiece.Create;
  FPieceCount:=1;
  // Recursively read all pieces and animations
  ReadPiece(Root);
  ReadAnimations;
  // Done with the file
  F.Free;
end;

function TPieceModel.CreateSceneNode: TPieceModelNode;

  procedure CreateNodes(SceneNode: TPieceModelPieceNode; Piece: TPieceModelPiece);
  var
    I: Integer;
    ChildPiece: TPieceModelPieceNode;
  begin
    SceneNode.Mesh:=Piece.Mesh;
    SceneNode.LocalMatrix.SetTranslation(Piece.Translation);
    for I:=0 to High(Piece.Children) do begin
      ChildPiece:=TPieceModelPieceNode.Create;
      SceneNode.AddChild(ChildPiece);
      CreateNodes(ChildPiece, Piece.Children[I]);
    end;
  end;

begin
  Result:=TPieceModelNode.Create;
  if not Assigned(Root) then Exit;
  // Create the root node under the model node
  Result.FRoot:=TPieceModelPieceNode.Create;
  Result.AddChild(Result.Root);
  // Create the rest of the hierarchy
  CreateNodes(Result.Root, Root);
end;

function TPieceModel.IndexOfAnimation(AName: string): Integer;
var
  I: Integer;
begin
  for I:=0 to High(Animations) do
    if Animations[I].Name=AName then Exit(I);
  Result:=-1;
end;

function TPieceModel.GetPieceSceneNode(AName: string; Node: TPieceModelNode): TPieceModelPieceNode;

  function Scan(Piece: TPieceModelPiece; SceneNode: TPieceModelPieceNode): TPieceModelPieceNode;
  var
    I: Integer;
  begin
    if Piece.Name=AName then Exit(SceneNode);
    for I:=0 to High(Piece.Children) do begin
      Result:=Scan(Piece.Children[I], TPieceModelPieceNode(SceneNode.Children[I]));
      if Assigned(Result) then Exit;
    end;
    Result:=nil;
  end;

begin
  Result:=Scan(Root, Node.Root);
end;

function TPieceModel.ApplyAnimation(AnimIndex: Integer; Seconds: Single;
  Node: TPieceModelNode; Blend: Single): Integer;
var
  BaseFrame, NextFrame: Integer;
  SubFrame: Single;
  BaseRotationIndex, NextRotationIndex: Integer;

  // Recursively apply the frame rotations
  procedure ApplyRotations(Piece: TPieceModelPiece;
    PieceNode: TPieceModelPieceNode);
  var
    T: TVector;
    I: Integer;
  begin
    T:=PieceNode.LocalMatrix.Translation;
    with Animations[AnimIndex] do begin
      PieceNode.Rotation:=Interpolate(
        PieceNode.Rotation,
        Interpolate(Rotations[BaseRotationIndex],
                    Rotations[NextRotationIndex],
                    SubFrame),
        Blend);
      PieceNode.LocalMatrix.XYZRotation(PieceNode.Rotation);
    end;
    PieceNode.LocalMatrix.SetTranslation(T);
    Inc(BaseRotationIndex);
    Inc(NextRotationIndex);
    for I:=0 to High(Piece.Children) do
      ApplyRotations(Piece.Children[I],
        TPieceModelPieceNode(PieceNode.Children[I]));
  end;

begin
  with Animations[AnimIndex] do begin
    // Calculate the current and next frame as well as the interpolation
    // between the two
    BaseFrame:=Trunc(Seconds*FPS);
    SubFrame:=Seconds*FPS - BaseFrame;
    if BaseFrame < 0 then BaseFrame:=0;
    if BaseFrame >= Frames then begin
      if Looping then
        BaseFrame:=BaseFrame mod Frames
      else
        BaseFrame:=Frames - 1;
    end;
    NextFrame:=BaseFrame + 1;
    if NextFrame=Frames then begin
      if Looping then
        NextFrame:=0
      else
        NextFrame:=BaseFrame;
    end;
    // Apply position transformation to the root node
    Node.Root.LocalMatrix.SetTranslation(
      Interpolate(Node.Root.LocalMatrix.Translation,
        Interpolate(RootPositions[BaseFrame],
                    RootPositions[NextFrame],
                    SubFrame),
        Blend));
    // Apply rotations
    BaseRotationIndex:=BaseFrame*PieceCount;
    NextRotationIndex:=NextFrame*PieceCount;
    ApplyRotations(Root, Node.Root);
    // Return the current frame or -1 if we reached the end of the animation
    if Looping then
      Result:=BaseFrame
    else
      if (BaseFrame=Frames - 1) and (SubFrame > 0.5) then
        Result:=-1
      else
        Result:=BaseFrame;
  end;
end;

procedure TPieceModel.OverrideTexture(ATexture: TTexture);

  procedure ApplyOverride(APiece: TPieceModelPiece);
  var
    I: Integer;
  begin
    APiece.Mesh.Texture:=ATexture;
    for I:=0 to High(APiece.Children) do
      ApplyOverride(APiece.Children[I]);
  end;

begin
  if not Assigned(ATexture) then ATexture:=Texture;
  ApplyOverride(Root);
end;

{ TPieceModelPieceNode }
constructor TPieceModelPieceNode.Create;
begin
  inherited Create;
  TestOcclusion:=False;
end;

procedure TPieceModelPieceNode.ApplyLightLevel(ALight: Byte);
var
  I: Integer;
begin
  Light:=ALight;
  // Apply to all children
  for I:=0 to ChildCount - 1 do
    if Children[I] is TPieceModelPieceNode then
      TPieceModelPieceNode(Children[I]).ApplyLightLevel(ALight);
end;

procedure TPieceModelPieceNode.ApplyDepthBias(ADepthBias: Byte);
var
  I: Integer;
begin
  DepthBias:=ADepthBias;
  // Apply to all children
  for I:=0 to ChildCount - 1 do
    if Children[I] is TPieceModelPieceNode then
      TPieceModelPieceNode(Children[I]).ApplyDepthBias(ADepthBias);
end;

{ TPieceModelNode }
constructor TPieceModelNode.Create;
begin
  inherited Create;
  TestOcclusion:=True;
end;

end.
