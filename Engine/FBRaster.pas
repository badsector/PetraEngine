// Framebuffer rasterizer
unit FBRaster;
{$ifdef CPU386}{$asmmode att}{$endif}
{ $DEFINE FORCEWIRE}
interface

uses
  Colors, Raster, Textures, GfxDrv;

type
  { TFramebufferRasterizer - a simple framebuffer-based rasterizer }
  TFramebufferRasterizer = class(TRasterizer)
  private
    FrameData: PBYTE;
    Palette: TColorPalette;
    TintedPalette: TColorPalette;
    LT: PByte;
    TW, TH, WS: Integer;
    TT: PByte;
    PaletteNeedsUpdate: Boolean;
    LastTintR, LastTintG, LastTintB: Byte;
  public
    // TRasterizer methods
    function BeginFrame(APalette: TColorPalette; AFrameData: Pointer): Boolean; override;
    procedure FinishFrame; override;
    procedure Clear(Color: Byte); override;
    procedure SetPixel(X, Y: Integer; Color: Byte); override;
    procedure SetPixels(X1, Y1, X2, Y2: Integer; var Data); override;
    procedure GetPixels(X1, Y1, X2, Y2: Integer; var Data); override;
    procedure SetTexture(const Texture: TTexture); override;
    procedure DrawTriangle(A, B, C: PRasterVertex; Occluder: Boolean; TriZ: Single); override;
    procedure DrawHUDImage(X, Y: Integer; Texture: TTexture; SX, SY, SW, SH: Integer); override;
    function WasRectOccluded(AX1, AY1, AX2, AY2: Integer; AZ: Single): Boolean; override;
  public
    constructor Create;
    destructor Destroy; override;
  end;

{$IFDEF EDITOR}
var
  GWireframeMode: Boolean;
{$ENDIF}

implementation

uses
  Engine;

type
  { TScanSpan - A single span in a scanline }
  TScanSpan = record
    CX1, CX2: Integer;
    Z: Single;
  end;

  { TScanline - The spans in a scanline }
  PScanline = ^TScanline;
  TScanline = object
    // The spans in this scanline
    Span: array of TScanSpan;
    Spans, LastSpans: Integer;
    NSpan: ^TScanSpan;
    // Maximum Z value for this scanline
    MaxSpanZ, LastMaxSpanZ: Single;
    // Splits used for when checking rects against the scaline
    Split: array of record SX1, SX2: Integer end;
    // Clear the spans
    procedure Clear; inline;
    // Add a new span
    procedure AddSpan(AX1, AX2: Integer; AZ: Single);
  end;

var
  // The scanlines for the frame
  LScanlines: array of TScanline;

procedure TScanline.Clear;
begin
  LastSpans:=Spans;
  LastMaxSpanZ:=MaxSpanZ;
  if Length(Split) < Spans*2 then SetLength(Split, Spans*2);
  Spans:=0;
  if Length(Span) > 0 then NSpan:=@Span[0];
  MaxSpanZ:=-100000;
end;

procedure TScanline.AddSpan(AX1, AX2: Integer; AZ: Single); inline;
begin
  // Register span
  if Length(Span)=Spans then begin
    SetLength(Span, Spans + 32);
    NSpan:=@Span[Spans];
  end;
  with NSpan^ do begin
    CX1:=AX1;
    CX2:=AX2;
    Z:=AZ;
  end;
  Inc(Spans);
  Inc(NSpan);
  if MaxSpanZ < AZ then MaxSpanZ:=AZ;
end;

{ TFramebufferRasterizer }
constructor TFramebufferRasterizer.Create;
begin
  inherited Create;
  TintedPalette:=TColorPalette.Create;
  PaletteNeedsUpdate:=True;
end;

destructor TFramebufferRasterizer.Destroy;
begin
  TintedPalette.Free;;
  inherited Destroy;
end;

function TFramebufferRasterizer.BeginFrame(APalette: TColorPalette; AFrameData: Pointer): Boolean;
var
  STintR, STintG, STintB: Single;
  I: Integer;
begin
  Result:=AFrameData <> nil;
  FrameData:=AFrameData;
  Palette:=APalette;
  LT:=@Palette.LightTable[0];
  SetLength(LScanlines, VRes);
  // Apply the color palette
  if PaletteNeedsUpdate or (LastTintR <> TintR) or (LastTintG <> TintG) or (LastTintB <> TintB) then begin
    STintR:=TintR/255;
    STintG:=TintG/255;
    STintB:=TintB/255;
    for I:=0 to 15 do begin
      TintedPalette.Red[I]:=APalette.Red[I];
      TintedPalette.Green[I]:=APalette.Green[I];
      TintedPalette.Blue[I]:=APalette.Blue[I];
    end;
    for I:=16 to 255 do begin
      TintedPalette.Red[I]:=Trunc(APalette.Red[I]*STintR);
      TintedPalette.Green[I]:=Trunc(APalette.Green[I]*STintG);
      TintedPalette.Blue[I]:=Trunc(APalette.Blue[I]*STintB);
    end;
    GRenderer.GraphicsDriver.SetColorPalette(TintedPalette);
    PaletteNeedsUpdate:=False;
    LastTintR:=TintR;
    LastTintG:=TintG;
    LastTintB:=TintB;
  end;
end;

procedure TFramebufferRasterizer.FinishFrame;
{var
  I: Integer;
  X, Y, C: Integer;}
begin
{  for Y:=0 to VRes - 1 do with LScanlines[Y] do begin
    for I:=0 to Spans - 1 do with Span[I] do begin
      C:=(Round(Z*255) mod 255) + 1;
      for X:=X1 to X2 do begin
        FrameData[Y*320 + X]:=C;
      end;
    end;
  end;}
end;

procedure TFramebufferRasterizer.Clear(Color: Byte);
var
  DColor: DWord;
  I: Integer;
begin
  DColor:=Color or (Color shl 8) or (Color shl 16) or (Color shl 24);
  FillDWord(FrameData^, (HRes*VRes) >> 2, DColor);
  for I:=0 to VRes - 1 do LScanlines[I].Clear;
end;

procedure TFramebufferRasterizer.SetPixel(X, Y: Integer; Color: Byte);
begin
  if (X >= 0) and (Y >= 0) and (X < HRes) and (Y < VRes) then
    FrameData[Y*HRes + X]:=Color;
end;

procedure TFramebufferRasterizer.SetPixels(X1, Y1, X2, Y2: Integer; var Data);
var
  RealData: TFrameData absolute Data;
  X, Y, W: Integer;
begin
  W:=X2 - X1 + 1;
  for Y:=Y1 to Y2 do
    for X:=X1 to X2 do
      RealData[(Y - Y1)*W + X - X1]:=FrameData[Y*HRes + X];
end;

procedure TFramebufferRasterizer.GetPixels(X1, Y1, X2, Y2: Integer; var Data);
var
  RealData: TFrameData absolute Data;
  X, Y, W: Integer;
begin
  W:=X2 - X1 + 1;
  for Y:=Y1 to Y2 do
    for X:=X1 to X2 do
      FrameData[Y*HRes + X]:=RealData[(Y - Y1)*W + X - X1];
end;

procedure TFramebufferRasterizer.SetTexture(const Texture: TTexture);

  procedure CalcShl(V: Integer; var VS: Integer);
  var
    I: Integer;
  begin
    I:=1;
    VS:=0;
    while I < 256 do begin
      if I=V then Exit;
      Inc(VS);
      I:=I*2;
    end;
  end;

begin
  TW:=Texture.Width;
  TH:=Texture.Height;
  CalcShl(TW, WS);
  TT:=Texture.TexelData;
end;

var
  {$IFDEF CPU386}TTPtr, {$ENDIF}LightTable: PByte;
procedure TFramebufferRasterizer.DrawTriangle(A, B, C: PRasterVertex; Occluder: Boolean; TriZ: Single);
var
  TmpVertex: PRasterVertex;
  LongXDelta, LongYDelta, LongSDelta, LongTDelta, LongLDelta, LongX, LongS, LongT, LongL: Integer;
  ShortXDelta, ShortYDelta, ShortSDelta, ShortTDelta, ShortLDelta, ShortX, ShortS, ShortT, ShortL: Integer;
  Y, Y2: Integer;
  YAddr: PByte;

  procedure DrawScanlineLTR(X1, X2, S1, S2, T1, T2, L1, L2: Integer; Addr: PByte);
  {$IFDEF CPU386}
  label StartOfLoop;
  {$ENDIF}
  var
    {$IFNDEF CPU386}XDelta,{$ENDIF} SDelta, TDelta, LDelta: Integer;
  begin
    // Clip out of screen
    if (X2 < 0) or (X1 >= HRes) then Exit;
    // Calculate deltas
    {$IFDEF CPU386}
    asm
      movl X2, %ecx
      subl X1, %ecx
      movl S2, %eax
      subl S1, %eax
      cltd
      idiv %ecx
      mov %eax, SDelta
      movl T2, %eax
      subl T1, %eax
      cltd
      idiv %ecx
      mov %eax, TDelta
      movl L2, %eax
      subl L1, %eax
      cltd
      idiv %ecx
      mov %eax, LDelta
    end ['eax', 'ecx', 'edx'];
    {$ELSE}
    XDelta:=X2 - X1;
    SDelta:=(S2 - S1) div XDelta;
    TDelta:=(T2 - T1) div XDelta;
    LDelta:=(L2 - L1) div XDelta;
    {$ENDIF}
    // Clip sides
    if X1 < 0 then begin
      Inc(S1, -X1*SDelta);
      Inc(T1, -X1*TDelta);
      Inc(L1, -X1*LDelta);
      X1:=0;
    end;
    if X2 > HRes then X2:=HRes;
    if Occluder then LScanlines[Y].AddSpan(X1, X2, TriZ);
    {$IFDEF EDITOR}
    if GWireframeMode then Exit;
    {$ENDIF}
    {$IFDEF FORCEWIRE}Exit;{$ENDIF}
    // Fill the scanline
    X2 -= X1;
    {$IFDEF CPU386}
    if X2 > 0 then asm
      movl Addr, %edx
      addl X1, %edx
      movl %edx, %ecx
      addl X2, %ecx
      movl S1, %eax
      movl T1, %ebx
      movl L1, %edi
    StartOfLoop:
      pushl %ebx
      shrl $12, %ebx
      movl %eax, %esi
      shll $6, %ebx
      addl TTPtr, %ebx
      shrl $12, %eax
      movb (%ebx,%eax,1), %al
      andl $0xff, %eax
      movl %edi, %ebx
      shrl $12, %ebx
      addl LDelta, %edi
      shll $8, %ebx
      addl LightTable, %ebx
      movb (%ebx,%eax,1), %al
      movb %al, (%edx)
      incl %edx
      movl %esi, %eax
      popl %ebx
      addl SDelta, %eax
      addl TDelta, %ebx
      cmp %ecx, %edx
      jnz StartOfLoop
    end ['eax', 'ebx', 'ecx', 'edx', 'esi', 'edi'];
    {$ELSE}
    Addr += X1;
    while X2 > 0 do begin
      Addr^:=LightTable[(L1 >> 12 << 8) + TT[((T1 >> 12) shl WS) + (S1 >> 12)]];
      Inc(Addr);
      Inc(S1, SDelta);
      Inc(T1, TDelta);
      Inc(L1, LDelta);
      Dec(X2);
    end;
    {$ENDIF}
  end;

  procedure DrawScanline(X1, X2, S1, S2, T1, T2, L1, L2: Integer; Addr: PByte); inline;
  begin
    if X1 < X2 then
      DrawScanlineLTR(X1, X2, S1, S2, T1, T2, L1, L2, Addr)
    else if X1 > X2 then
      DrawScanlineLTR(X2, X1, S2, S1, T2, T1, L2, L1, Addr);
  end;

  {$IFDEF EDITOR}{$DEFINE DWL}{$ENDIF}
  {$IFDEF FORCEWIRE}{$DEFINE DWL}{$ENDIF}

  {$IFDEF DWL}{$UNDEF DWL}
  procedure DrawWireLine(A, B: PRasterVertex);
  var
    Len, X, Y, DX, DY, I: Integer;
  begin
    Len:=Round(Sqrt(Sqr(B^.X/4096 - A^.X/4096) + Sqr(B^.Y - A^.Y)));
    if Len=0 then Exit;
    X:=A^.X;
    Y:=A^.Y*4096;
    DX:=(B^.X - A^.X) div Len;
    DY:=(B^.Y - A^.Y)*4096 div Len;
    for I:=1 to Len do begin
      SetPixel(X div 4096, Y div 4096, 15);
      Inc(X, DX);
      Inc(Y, DY);
    end;
  end;
  {$ENDIF}

begin
  {$IFDEF EDITOR}
  if GWireframeMode then begin
    DrawWireLine(A, B);
    DrawWireLine(B, C);
    DrawWireLine(C, A);
  end;
  {$ENDIF}
  {$IFDEF FORCEWIRE}
  DrawWireLine(A, B);
  DrawWireLine(B, C);
  DrawWireLine(C, A);
  {$ENDIF}
  // Sort
  if A^.Y > B^.Y then begin
    TmpVertex:=A;
    A:=B;
    B:=TmpVertex;
  end;
  if B^.Y > C^.Y then begin
    TmpVertex:=B;
    B:=C;
    C:=TmpVertex;
  end;
  if A^.Y > B^.Y then begin
    TmpVertex:=A;
    A:=B;
    B:=TmpVertex;
  end;
  // Ignore triangles outside the screen
  if (A^.Y > MaxY) or (C^.Y < 0) then Exit;
  // Ignore single scanline triangles
  if A^.Y=C^.Y then Exit;
  // Store light table pointer
  LightTable:=@Palette.LightTable[0, 0];
  {$IFDEF CPU386}TTPtr:=TT;{$ENDIF}
  // A to C edge (long edge)
  LongYDelta:=C^.Y - A^.Y;
  LongXDelta:=(C^.X - A^.X) div LongYDelta;
  LongSDelta:=((C^.S - A^.S) shl 12) div LongYDelta;
  LongTDelta:=((C^.T - A^.T) shl 12) div LongYDelta;
  LongLDelta:=((C^.L - A^.L) shl 12) div LongYDelta;
  LongX:=A^.X;
  LongS:=A^.S shl 12;
  LongT:=A^.T shl 12;
  LongL:=A^.L shl 12;
  // A to B edge (top triangle part)
  if A^.Y <> B^.Y then begin
    // Short edge for the top part
    ShortYDelta:=B^.Y - A^.Y;
    ShortXDelta:=(B^.X - A^.X) div ShortYDelta;
    ShortSDelta:=((B^.S - A^.S) shl 12) div ShortYDelta;
    ShortTDelta:=((B^.T - A^.T) shl 12) div ShortYDelta;
    ShortLDelta:=((B^.L - A^.L) shl 12) div ShortYDelta;
    ShortX:=LongX;
    ShortS:=LongS;
    ShortT:=LongT;
    ShortL:=LongL;
    // Y clipping
    Y:=A^.Y;
    if Y < 0 then begin
      Inc(ShortX, ShortXDelta*-Y);
      Inc(ShortS, ShortSDelta*-Y);
      Inc(ShortT, ShortTDelta*-Y);
      Inc(ShortL, ShortLDelta*-Y);
      Inc(LongX, LongXDelta*-Y);
      Inc(LongS, LongSDelta*-Y);
      Inc(LongT, LongTDelta*-Y);
      Inc(LongL, LongLDelta*-Y);
      Y:=0;
    end;
    Y2:=B^.Y - 1;
    if Y2 > MaxY then Y2:=MaxY;
    // Fill scanlines for the top part
    YAddr:=@FrameData[Y*HRes];
    for Y:=Y to Y2 do begin
      DrawScanline(
        ShortX div 4096, LongX div 4096,
        ShortS, LongS,
        ShortT, LongT,
        ShortL, LongL,
        YAddr);
      Inc(YAddr, HRes);
      Inc(ShortX, ShortXDelta);
      Inc(ShortS, ShortSDelta);
      Inc(ShortT, ShortTDelta);
      Inc(ShortL, ShortLDelta);
      Inc(LongX, LongXDelta);
      Inc(LongS, LongSDelta);
      Inc(LongT, LongTDelta);
      Inc(LongL, LongLDelta);
    end;
  end else ShortYDelta:=0;
  // Ignore bottom triangle part if it is outside the screen
  if B^.Y > MaxY then Exit;
  // Recalculate long edge if Y was clipped
  if B^.Y < 0 then begin
    LongX:=A^.X + LongXDelta*ShortYDelta;
    LongS:=(Integer(A^.S) shl 12) + LongSDelta*ShortYDelta;
    LongT:=(Integer(A^.T) shl 12) + LongTDelta*ShortYDelta;
    LongL:=(Integer(A^.L) shl 12) + LongLDelta*ShortYDelta;
  end;
  // B to C edge (bottom triangle part)
  if B^.Y <> C^.Y then begin
    // Short edge for the bottom part
    ShortYDelta:=C^.Y - B^.Y;
    ShortXDelta:=(C^.X - B^.X) div ShortYDelta;
    ShortSDelta:=((C^.S - B^.S) shl 12) div ShortYDelta;
    ShortTDelta:=((C^.T - B^.T) shl 12) div ShortYDelta;
    ShortLDelta:=((C^.L - B^.L) shl 12) div ShortYDelta;
    ShortX:=B^.X;
    ShortS:=B^.S shl 12;
    ShortT:=B^.T shl 12;
    ShortL:=B^.L shl 12;
    // Y clipping
    Y:=B^.Y;
    if Y < 0 then begin
      Inc(ShortX, ShortXDelta*-Y);
      Inc(ShortS, ShortSDelta*-Y);
      Inc(ShortT, ShortTDelta*-Y);
      Inc(ShortL, ShortLDelta*-Y);
      Inc(LongX, LongXDelta*-Y);
      Inc(LongS, LongSDelta*-Y);
      Inc(LongT, LongTDelta*-Y);
      Inc(LongL, LongLDelta*-Y);
      Y:=0;
    end;
    Y2:=C^.Y - 1;
    if Y2 > MaxY then Y2:=MaxY;
    // Fill scanlines for the top part
    YAddr:=@FrameData[Y*HRes];
    for Y:=Y to Y2 do begin
      DrawScanline(
        ShortX div 4096, LongX div 4096,
        ShortS, LongS,
        ShortT, LongT,
        ShortL, LongL,
        YAddr);
      Inc(YAddr, HRes);
      Inc(ShortX, ShortXDelta);
      Inc(ShortS, ShortSDelta);
      Inc(ShortT, ShortTDelta);
      Inc(ShortL, ShortLDelta);
      Inc(LongX, LongXDelta);
      Inc(LongS, LongSDelta);
      Inc(LongT, LongTDelta);
      Inc(LongL, LongLDelta);
    end;
  end;
end;

procedure TFramebufferRasterizer.DrawHUDImage(X, Y: Integer; Texture: TTexture; SX, SY, SW, SH: Integer);
var
  Src, Tgt: PByte;
  I: Integer;
begin
  if (X > MaxX) or (Y > MaxY) then Exit;
  if X < 0 then begin
    SX += -X;
    SW -= -X;
    X:=0;
  end;
  if Y < 0 then begin
    SY += -Y;
    SH -= -Y;
    Y:=0;
  end;
  if X + SW > MaxX then SW:=HRes - X;
  if Y + SH > MaxY then SH:=VRes - Y;
  if (SW <= 0) or (SH <= 0) then Exit;
  Src:=Texture.TexelData + SY*Texture.Width + SX;
  Tgt:=FrameData + Y*HRes + X;
  if Texture.HasTranparency then begin
    while SH > 0 do begin
      for I:=1 to SW do begin
        if Src^ <> 255 then Tgt^:=Src^;
        Inc(Src);
        Inc(Tgt);
      end;
      Inc(Src, Texture.Width - SW);
      Inc(Tgt, HRes - SW);
      Dec(SH);
    end;
  end else begin
    while SH > 0 do begin
      Move(Src^, Tgt^, SW);
      Inc(Src, Texture.Width);
      Inc(Tgt, HRes);
      Dec(SH);
    end;
  end;
end;

function TFramebufferRasterizer.WasRectOccluded(AX1, AY1, AX2, AY2: Integer; AZ: Single): Boolean;
const
  AllowError = 2;
label
  NextSplit;
var
  Y, I, Splits, Tmp, VisHits: Integer;
begin
  VisHits:=0;
  for Y:=AY1 to AY2 do with LScanlines[Y] do if LastSpans > 0 then begin
    // All spans in the scanline would obscure this line
    if LastMaxSpanZ < AZ then Continue;
    Splits:=0;
    Split[0].SX1:=AX1;
    Split[0].SX2:=AX2;
    while Splits >= 0 do with Split[Splits] do begin
      for I:=LastSpans - 1 downto 0 do with Span[I] do
        if (AZ > Z) and not ((CX2 < SX1) or (CX1 > SX2)) then begin
          if (CX1 <= SX1) and (CX2 >= SX2) then begin
            Dec(Splits);
            goto NextSplit;
          end;
          if (CX1 > SX1) and (CX2 < SX2) then begin
            Tmp:=SX2;
            SX2:=CX1 - 1;
            Inc(Splits);
            Split[Splits].SX1:=CX2 + 1;
            Split[Splits].SX2:=Tmp;
            goto NextSplit;
          end;
          if CX1 > SX1 then SX2:=CX1 - 1;
          if CX2 < SX2 then SX1:=CX2 + 1;
        end;
      Inc(VisHits);
      Break;
NextSplit:
    end;
    if VisHits >= AllowError then Break;
  end else begin
    Inc(VisHits);
    if VisHits >= AllowError then Break;
  end;
  Result:=VisHits < AllowError;
end;

end.
