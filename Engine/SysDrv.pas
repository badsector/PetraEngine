{ System driver }
unit SysDrv;

interface

type
  { TSystemDriver - base class for generic system-specific functionality }
  TSystemDriver = class
  protected
    function GetMilliseconds: Integer; virtual; abstract;
  public
    // The number of milliseconds since the program started
    property Milliseconds: Integer read GetMilliseconds;
  end;

implementation

end.
