// Common input driver functionality
unit InputDrv;

{$INTERFACES CORBA}

interface

uses
  FGL;

type
  { All input keys }
  TInputKey = (
    ikNone, ikUnknown,
    ikEscape, ikF1, ikF2, ikF3, ikF4, ikF5, ikF6, ikF7, ikF8, ikF9, ikF10,
    ikF11, ikF12, ikScrollLock, ikPause, ikBackQuote, ik0, ik1, ik2, ik3, ik4,
    ik5, ik6, ik7, ik8, ik9, ikMinus, ikEquals, ikBackspace, ikInsert,
    ikHome, ikPageUp, ikPageDown, ikTab, ikA, ikB, ikC, ikD, ikE, ikF, ikG,
    ikH, ikI, ikJ, ikK, ikL, ikM, ikN, ikO, ikP, ikQ, ikR, ikS, ikT, ikU,
    ikV, ikW, ikX, ikY, ikZ, ikLeftBracket, ikRightBracket, ikBackSlash,
    ikEnter, ikCapsLock, ikSemiColon, ikQuote, ikShift, ikComma, ikPeriod,
    ikSlash, ikControl, ikAlt, ikLeftMeta, ikRightMeta, ikSpace, ikDelete,
    ikEnd, ikLeft, ikRight, ikUp, ikDown, ikNumLock, ikDivide, ikMultiply,
    ikSubtract, ikAdd, ikKeypad0, ikKeypad1, ikKeypad2, ikKeypad3,
    ikKeypad4, ikKeypad5, ikKeypad6, ikKeypad7, ikKeypad8, ikKeypad9,
    ikKeypadPoint, ikMouse, ikLeftButton, ikMiddleButton, ikRightButton,
    ikWheelUp, ikWheelDown
  );

  { Base class for input handlers }
  TInputHandler = interface
    // Called when a key is pressed, returns true if handled
    function KeyDown(Key: TInputKey): Boolean;
    // Called when a key is released, returns true if handled
    function KeyUp(Key: TInputKey): Boolean;
    // called when the mouse has moved, returns true if handled
    function Mouse(X, Y: Integer): Boolean;
  end;

  { List of input handlers, used by TInputDriver below }
  TInputHandlerList = specialize TFPGList<TInputHandler>;

  { TInputDriver - provides the input functionality }
  { This is just a base class for the environment-specific code }
  TInputDriver = class
  private
    // Property storage
    FKeyState: array [TInputKey] of Boolean;
    function GetKeyState(Key: TInputKey): Boolean; inline;
    procedure SetExclusiveHandler(AInputHandler: TInputHandler);
  private
    // Handler list
    Handlers: TInputHandlerList;
    FExclusiveHandler: TInputHandler;
  protected
    // Called by subclasses to notify the input handlers for keys
    procedure NotifyHandlersForKey(Key: TInputKey; Down: Boolean);
    // Called by subclasses to notify the input handlers for mouse
    procedure NotifyHandlersForMouse(X, Y: Integer);
  public
    // Constructor
    constructor Create;
    // Destructor
    destructor Destroy; override;
    // Returns true if input handling is live
    function IsLive: Boolean; virtual; abstract;
    // Add an input handler
    procedure AddHandler(Handler: TInputHandler);
    // Remove the given input handler
    procedure RemoveHandler(Handler: TInputHandler);
    // Cause KeyUp calls for any key that is currently down
    procedure ReleaseKeys;
    // The current key state
    property KeyState[Key: TInputKey]: Boolean read GetKeyState;
    // Exclusive handler (set to only forward keys to that handler)
    property ExclusiveHandler: TInputHandler read FExclusiveHandler write SetExclusiveHandler;
  end;

const
  // Input key names
  InputKeyNames: array [TInputKey] of string = (
    'none', 'unknown', 'escape', 'f1', 'f2', 'f3', 'f4', 'f5', 'f6', 'f7',
    'f8', 'f9', 'f10', 'f11', 'f12', 'scroll lock', 'pause', '`', '0', '1',
    '2', '3', '4', '5', '6', '7', '8', '9', '-', '=', 'backspace', 'insert',
    'home', 'page up', 'page down', 'tab', 'A', 'B', 'C', 'D', 'E', 'F', 'G',
    'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
    'W', 'X', 'Y', 'Z', '[', ']', '\', 'enter', 'caps lock', ';', '''',
    'shift', ',', '.', '/', 'control', 'alt', 'left meta', 'right meta',
    'space', 'delete', 'end', 'left', 'right', 'up', 'down', 'num lock',
    'keypad divide', 'keypad multiply', 'keypad subtract', 'keypad add',
    'keypad 0', 'keypad 1', 'keypad 2', 'keypad 3', 'keypad 4', 'keypad 5',
    'keypad 6', 'keypad 7', 'keypad 8', 'keypad 9', 'keypad point',
    'mouse motion', 'left button', 'middle button', 'right button',
    'wheel up', 'wheel down'
  );

implementation

{ TInputDriver }
constructor TInputDriver.Create;
begin
  inherited Create;
  Handlers:=TInputHandlerList.Create;
end;

destructor TInputDriver.Destroy;
begin
  while Handlers.Count > 0 do RemoveHandler(Handlers[Handlers.Count - 1]);
  Handlers.Free;
  inherited Destroy;
end;

function TInputDriver.GetKeyState(Key: TInputKey): Boolean;
begin
  Result:=FKeyState[Key];
end;

procedure TInputDriver.SetExclusiveHandler(AInputHandler: TInputHandler);
begin
  if FExclusiveHandler=AInputHandler then Exit;
  // Force key release events
  ReleaseKeys;
  FExclusiveHandler:=AInputHandler;
end;

procedure TInputDriver.NotifyHandlersForKey(Key: TInputKey; Down: Boolean);
var
  I: Integer;
begin
  // Ignore double requests
  if FKeyState[Key]=Down then Exit;
  // Update current state
  FKeyState[Key]:=Down;
  // Notify handlers
  if Down then begin
    if Assigned(ExclusiveHandler) then begin
      ExclusiveHandler.KeyDown(Key);
      Exit;
    end;
    for I:=Handlers.Count - 1 downto 0 do
      if Handlers[I].KeyDown(Key) then Exit;
  end else begin
    if Assigned(ExclusiveHandler) then begin
      ExclusiveHandler.KeyUp(Key);
      Exit;
    end;
    for I:=Handlers.Count - 1 downto 0 do
      if Handlers[I].KeyUp(Key) then Exit;
  end;
end;

procedure TInputDriver.NotifyHandlersForMouse(X, Y: Integer);
var
  I: Integer;
begin
  if Assigned(ExclusiveHandler) then begin
    ExclusiveHandler.Mouse(X, Y);
    Exit;
  end;
  for I:=Handlers.Count - 1 downto 0 do
    if Handlers[I].Mouse(X, Y) then Exit;
end;

procedure TInputDriver.AddHandler(Handler: TInputHandler);
begin
  Handlers.Add(Handler);
end;

procedure TInputDriver.RemoveHandler(Handler: TInputHandler);
begin
  Handlers.Remove(Handler);
end;

procedure TInputDriver.ReleaseKeys;
var
  Key: TInputKey;
begin
  for Key:=Low(TInputKey) to High(TInputKey) do
    if KeyState[Key] then begin
      NotifyHandlersForKey(Key, False);
      FKeyState[Key]:=False;
    end;
end;

end.
