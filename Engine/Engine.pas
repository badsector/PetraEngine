{ Main engine unit }
unit Engine;

{ $DEFINE SHOWFPS}
interface

uses
  SysDrv, SndDrv, InputDrv, ResMan, Renderer, GameBase, Game, Screens;

type
  // Play modes
  TPlayMode = (pmPlaying, pmFreeCamera, pmEditor);

var
  GGameTitle: string;                   // The game's title
  GSystem: TSystemDriver;               // The system driver
  GInput: TInputDriver;                 // The input driver
  GScreenManager: TScreenManager;       // The screen manager
  GResourceManager: TResourceManager;   // The resource manager
  GRenderer: TRenderer;                 // The renderer
  GSound: TSoundDriver;                 // The sound driver
  GGame: TGame;                         // The game instance
  GBenchmark: Boolean;                  // True if in benchmark mode
  GFinalUpdateCall: Boolean;            // During the update cycle this is set
                                        // to True for the final call to Update
                                        // and False more calls willbe made to
                                        // reach the update frequency
{$IFDEF EDITOR}
  GPlayMode: TPlayMode = pmEditor;      // In editor mode, start in... editor mode
  GPlayMapOnEditorStart: string;        // If set plays the map at editor start
  GPlayMapOnEditorNoStartup: Boolean;   // Do not run startup script for above
  GUpdateEditorSounds: Boolean;         // Update/preview sounds in editor mode
  GUpdateEditorAnimations: Boolean;     // Update/preview animations in editor mode
  GShowStats: Boolean;                  // Show various statistics
  GShowPortals: Boolean;                // Show the portals
  GShowOcclusionTests: Boolean;         // Show box occlusion tests
  GDisableTriLines: Boolean;            // Disable editor trilines
  GLockVisibilityUpdates: Boolean;      // Lock PVS updates
  GQuitRequested: Boolean;              // A request to quit was made
{$ELSE}
const
  GPlayMode = pmPlaying;                // In game builds, the game is always active
{$ENDIF}

// Initialize the engine for the current system
procedure InitializeEngine;
// Shutdown the engine
procedure ShutdownEngine;

// Returns true if the engine should still be running
function EngineRunning: Boolean;
// Request the engine to quit (for the editor this will stop the game)
procedure QuitEngine;
// Runs a single update+render cycle
procedure RunEngineCycle;

implementation

uses
  SndEmit, Fonts, Settings,
  {$IFDEF EDITOR}
    UEdSys, UEdInput, UEdGfx, UEdSnd, UEdMain,
  {$ELSE}
    {$IFDEF WINDOWS}
    {$IFDEF PETRASDL}
    SDLSys, SDLInput, SDLGfx, SDLSnd,
    {$ELSE}
    WinSys, WinInput, WinGfx, WinSnd,
    {$ENDIF}
    {$ENDIF}
    {$IFDEF LINUX}
    SDLSys, SDLInput, SDLGfx, SDLSnd,
    {$ENDIF}
    {$IFDEF DARWIN}
    {$PASCALMAINNAME SDL_main}
    {$LINKLIB SDLmain}
    {$LINKFRAMEWORK Cocoa}
    {$LINKFRAMEWORK SDL}
    {$IFDEF PETRAGL}{$LINKFRAMEWORK OpenGL}{$ENDIF}
    SDLSys, SDLInput, SDLGfx, SDLSnd,
    {$ENDIF}
    {$IFDEF GO32V2}
    DOSSys, DOSInput, DOSGfx, DOSSnd,
    {$ENDIF}
    {$IFDEF ANDROID}
    AndSys, AndInput, AndGfx, AndSnd, AndProcs,
    {$ENDIF}
  {$ENDIF}
  GfxDrv, SysUtils;

var
  // Current graphics driver
  GraphicsDriver: TGraphicsDriver;
  QuitRequested: Boolean;
  LastUpdate: Integer;
  LastFrameMS: Integer;
  {$IFDEF SPEEDSTATS}LastCycleMS: Integer;{$ENDIF}
  {$IFDEF SHOWFPS}LastCycleMS, FrameWait: Integer; TgtShowFPS: Single;{$ENDIF}
  {$IFDEF DUMPFPS}Frames: Integer;{$ENDIF}

{ Local functions }
{$IFDEF DUMPFPS}
var PreTime: Integer;
procedure DumpFPS;
var
  PostTime: Integer;
  F: Text;
begin
  PostTime:=GSystem.Milliseconds;
  IOResult;
  Assign(F, 'fps.txt');{$I-}Rewrite(F);{$I+}
  if IOResult <> 0 then Exit;
  Writeln(F, 'FPS=', Round(Frames/((PostTime - PreTime)/1000)));
  Writeln(F, 'for ', Round((PostTime - PreTime)/1000), ' seconds');
  Close(F);
end;
{$ENDIF}

{ Functions }
procedure InitializeEngine;
var
  I: Integer;
begin
  // Check for benchmark mode
  GBenchmark:=False;
  for I:=1 to ParamCount do if LowerCase(ParamStr(I))='-bench' then
    GBenchmark:=True;
  // Initialize the drivers
  {$IFDEF EDITOR}
    GSystem:=TEditorSystemDriver.Create;
    GraphicsDriver:=TEditorGraphicsDriver.Create;
    GInput:=TEditorInputDriver.Create;
    GSound:=TEditorSoundDriver.Create;
  {$ELSE}
    {$IFDEF WINDOWS}
    {$IFNDEF PETRASDL}
    GSystem:=TWinSystemDriver.Create;
    GraphicsDriver:=TWinGraphicsDriver.Create;
    GInput:=TWinInputDriver.Create;
    GSound:=TWinSoundDriver.Create;
    {$ENDIF}
    {$ENDIF}
    {$IFDEF PETRASDL}
    GSystem:=TSDLSystemDriver.Create;
    GraphicsDriver:=TSDLGraphicsDriver.Create;
    GInput:=TSDLInputDriver.Create;
    GSound:=TSDLSoundDriver.Create;
    {$ENDIF}
    {$IFDEF GO32V2}
    GSystem:=TDOSSystemDriver.Create;
    GraphicsDriver:=TDOSGraphicsDriver.Create;
    GSound:=TDOSSoundDriver.Create;
    {$ENDIF}
    {$IFDEF ANDROID}
    GSystem:=TAndroidSystemDriver.Create;
    GraphicsDriver:=TAndroidGraphicsDriver.Create;
    GInput:=TAndroidInputDriver.Create;
    GSound:=TAndroidSoundDriver.Create;
    {$ENDIF}
  {$ENDIF}
  {$IFDEF DUMPFPS}PreTime:=GSystem.Milliseconds;{$ENDIF}
  GraphicsDriver.Initialize;
  // For DOS we need to initialize the input driver after the graphics mode
  // has been set
  {$IFDEF GO32V2}
  GInput:=TDOSInputDriver.Create;
  {$ENDIF}
  // Create the renderer
  GRenderer:=TRenderer.Create(GraphicsDriver);
  // Create the resource manager
  GResourceManager:=TResourceManager.Create;
  // Create the screen manager
  GScreenManager:=TScreenManager.Create;
  GInput.AddHandler(GScreenManager);
  // Initialize the sound
  GSound.Initialize;
  // Create the game instance
  GGame:=TGame.Create;
  // Unset the quit flag
  QuitRequested:=False;
  // Prepare for the update cycle
  LastUpdate:=GSystem.Milliseconds;
  {$IFDEF SPEEDSTATS}LastCycleMS:=LastUpdate;{$ENDIF}
  {$IFDEF SHOWFPS}LastCycleMS:=LastUpdate;{$ENDIF}
end;

procedure ShutdownEngine;
begin
  // Destroy the game and scene (we do not use FreeAndNil here because we want
  // the GGame reference to be valid since the destructors can still make calls
  // to methods that use it)
  GGame.Free;
  GGame:=nil;
  // Shut down the graphics driver
  GraphicsDriver.Shutdown;
  // Destroy the drivers
  GInput.RemoveHandler(GScreenManager);
  FreeAndNil(GScreenManager);
  FreeAndNil(GRenderer);
  FreeAndNil(GResourceManager);
  FreeAndNil(GSound);
  FreeAndNil(GInput);
  FreeAndNil(GraphicsDriver);
  {$IFDEF DUMPFPS}DumpFPS;{$ENDIF}
  FreeAndNil(GSystem);
end;

function EngineRunning: Boolean;
begin
  if QuitRequested then Exit(False);
  if not (Assigned(GInput) and
          Assigned(GSystem) and
          Assigned(GRenderer)) then Exit(False);
  Result:=GInput.IsLive;
end;

procedure QuitEngine;
begin
  {$IFDEF EDITOR}
  GQuitRequested:=True;
  {$ELSE}
  QuitRequested:=True;
  {$ENDIF}
  {$IFDEF ANDROID}
  Procs.RequestQuit;
  {$ENDIF}
end;

procedure RunEngineCycle;
var
  NowMS, PreUpdateMilliseconds: Integer;
  {$IFDEF SPEEDSTATS}PreRenderMS: Integer;{$ENDIF}
begin
  if QuitRequested then Exit;
  // Update the game at roughly 70Hz (VGA update rate)
  PreUpdateMilliseconds:=GSystem.Milliseconds;
  if PreUpdateMilliseconds - LastUpdate > 14 then begin
    if PreUpdateMilliseconds - LastUpdate > 140 then
      LastUpdate:=PreUpdateMilliseconds - 140;
    while PreUpdateMilliseconds - LastUpdate > 14 do begin
      GFinalUpdateCall:=PreUpdateMilliseconds - LastUpdate - 14 <= 14;
      GResourceManager.Update;
      if not GScreenManager.Pause then begin
        GGame.Update;
        {$IFDEF EDITOR}
        GGame.EditorUpdate;
        {$ENDIF}
      end;
      Inc(LastUpdate, 14);
    end;
  end;
  GFinalUpdateCall:=False;
  // Update sound
  GSound.Update;
  // Render
  {$IFDEF SPEEDSTATS}PreRenderMS:=GSystem.Milliseconds;{$ENDIF}
  if GRenderer.BeginFrame then begin
    // Check if we need to render the scene
    if not GScreenManager.FullScreen then begin
      // Clear the framebuffer
      if Assigned(GGame.World) and Assigned(GGame.Scene) then begin
        {$IFDEF PETRAHW}
        if GGame.Scene.BackgroundColor=0 then
          GRenderer.Rasterizer.Clear(GGame.World.FogRed/255,
                                     GGame.World.FogGreen/255,
                                     GGame.World.FogBlue/255)
        else
          GRenderer.Rasterizer.Clear(GRenderer.ColorPalette.Red[GGame.Scene.BackgroundColor]/255,
                                     GRenderer.ColorPalette.Green[GGame.Scene.BackgroundColor]/255,
                                     GRenderer.ColorPalette.Blue[GGame.Scene.BackgroundColor]/255);
        {$ELSE}
        if GGame.Scene.BackgroundColor=0 then
          GRenderer.Rasterizer.Clear(GGame.World.FogColor)
        else
          GRenderer.Rasterizer.Clear(GGame.Scene.BackgroundColor);
        {$ENDIF}
      end else begin
        {$IFDEF PETRAHW}
        GRenderer.Rasterizer.Clear(0, 0, 0);
        {$ELSE}
        GRenderer.Rasterizer.Clear(0);
        {$ENDIF}
      end;
      // Inform the game we're about to render
      GGame.Frame;
      // Render the editor widgets
      {$IFDEF EDITOR}
      Main.Render;
      {$ENDIF}
      // Finish and present the frame
      GRenderer.FinishFrame;
      // Inform the game we've finished rendering the scene
      GGame.PostFrame;
    end;
    // Draw the current screen
    GScreenManager.Draw;
    {$IFDEF PETRAHW}
    // Draw the shadow textures
    GRenderer.DrawShadowTextures;
    {$ENDIF}
    {$IFDEF SPEEDSTATS}
    PreRenderMS:=GSystem.Milliseconds - PreRenderMS;
    GRenderer.DrawText(0, 0,
       'C:' + IntToStr(GSystem.Milliseconds - LastCycleMS) +
      ' R:' + IntToStr(PreRenderMS) +
      ' F:' + IntToStr(FlushPost-FlushPre) +
        '/' + IntToStr((FlushPost-FlushPre)-(DrawPost-DrawPre)) +
      ' T:' + IntToStr(TransPost-TransPre) +
      ' S:' + IntToStr(SortPost-SortPre) +
      ' D:' + IntToStr(DrawPost-DrawPre) +
      ' P:' + IntToStr(GRenderer.FrameTriangles), TBitFont(GResourceManager.DefaultFont), 15, 0);
    LastCycleMS:=GSystem.Milliseconds;
    {$ENDIF}
    {$IFDEF SHOWFPS}
    Inc(FrameWait);
    if FrameWait >= 5 then begin
      FrameWait:=0;
      TgtShowFPS += (5000/(GSystem.Milliseconds - LastCycleMS) - TgtShowFPS)*0.25;
      LastCycleMS:=GSystem.Milliseconds;
    end;
    GRenderer.DrawText(0, 0,
      IntToStr(Round(TgtShowFPS)),
      TBitFont(GResourceManager.DefaultFont), 14, 0);
    {$ENDIF}
    {$IFDEF EDITOR}
    // Draw stats
    if GShowStats then begin
      GRenderer.DrawText(0, 0, ' Polys:' + IntToStr(GRenderer.FrameTriangles), TBitFont(GResourceManager.DefaultFont), 14, 0);
    end;
    {$ENDIF}
    // Finish the frame for the graphics driver
    GraphicsDriver.FinishFrame;
    {$IFDEF DUMPFPS}Inc(Frames);{$ENDIF}
    if GSettings.MaxFPS > 0 then begin
      while True do begin
        NowMS:=GSystem.Milliseconds;
        if NowMS - LastFrameMS >= 1000/GSettings.MaxFPS then Break;
      end;
      LastFrameMS:=NowMS;
    end;
  end;
end;

initialization
  GGameTitle:='Petra';
end.
