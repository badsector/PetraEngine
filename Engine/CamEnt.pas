{ Camera entities }
unit CamEnt;

interface

uses
  Maths, Serial, World, Cameras;

type
  { TCameraEntity - An entity that simply holds a camera }
  TCameraEntity = class(TEntity)
  private
    FCamera: TCamera;
  protected
    // TSerializable methods
    procedure BeforeDeserialization; override;
    procedure AfterDeserialization; override;
  protected
    // TEntity methods
    procedure TransformChanged; override;
    {$IFDEF EDITOR}
    procedure Frame; override;
    {$ENDIF}
    function GetAABox: TAABox; override;
  public
    constructor Create; override;
    destructor Destroy; override;
  published
    property Camera: TCamera read FCamera;
  end;

implementation

uses
  Engine;

{ TCameraEntity }
constructor TCameraEntity.Create;
begin
  inherited Create;
  FCamera:=TCamera.Create;
end;

destructor TCameraEntity.Destroy;
begin
  FCamera.Free;
  inherited Destroy;
end;

procedure TCameraEntity.BeforeDeserialization;
begin
  FCamera.Free;
  FCamera:=nil;
end;

procedure TCameraEntity.AfterDeserialization;
begin
  inherited AfterDeserialization;
  FCamera.Position:=Transform.Translation;
end;

procedure TCameraEntity.TransformChanged;
begin
  if Assigned(FCamera) then FCamera.Position:=Transform.Translation;
end;

{$IFDEF EDITOR}
procedure TCameraEntity.Frame;
begin
  if GPlayMode=pmEditor then GRenderer.RenderAABox(AABox);
end;
{$ENDIF}

function TCameraEntity.GetAABox: TAABox;
begin
  Result:=inherited GetAABox;
  Result.Grow(0.1);
end;

initialization
  RegisterSerializableClass(TCameraEntity);
end.
