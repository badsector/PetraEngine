{ The grid-based map that is used for the game world }
unit GridMap;

interface

uses
  Classes, Serial, Maths, Scene, Textures, Cameras;

const
  // Size (in the X and Z dimensions) of a single cell in meters
  GridCellSize = 2;

type
  { Forward class declarations }
  TGrid = class;
  TGridMap = class;

  { Pointers }
  PCell = ^TCell;

  { Collections }
  TGridCollection = specialize TSerializableCollection<TGrid>;

  { TGridMeshQuad - A quad in the TGridMeshNode }
  TGridMeshQuad = object
    // Quad vertices
    A, B, C, D: TVector;
    // Midpoint
    M: TVector;
    // Texture
    Texture: TTexture;
    // Light levels
    LA, LB, LC, LD: Byte;
    // Texture coordinates
    SA, TA, SB, TB, SC, TC, SD, TD: Byte;
    {$IFDEF PETRAHW}
    // Subdivide the the quad
    Subdivide: Boolean;
    {$ENDIF}
    // Calculates the midpoint from A,B,C,D
    procedure CalcMidPoint; inline;
  end;

  { TGridMeshNode - Scene node with the mesh of a grid }
  TGridMeshNode = class(TSceneNode)
  private
    // Quads
    Quads: array of TGridMeshQuad;
    // AABox of the quads
    QuadsAABox: TAABox;
    // The grid
    Grid: TGrid;
    // Calculate the quads AA box
    procedure CalcQuadsAABox;
  protected
    // TSceneNode methods
    procedure RenderNode; override;
    function GetOwnLocalAABox: TAABox; override;
  end;

  { TCellPart - A cell part }
  TCellPart = (cpNone, cpLower, cpFloor, cpCeiling, cpUpper);

  { TCellOption - A single cell option }
  TCellOption = (
    coFullbrightFloor,          // Floor is always at full brightness
    coFullbrightCeiling,        // Ceiling is always at full brightness
    coFullbrightWalls,          // Walls are always at full brightness
    coFatalFloor,               // The floor is fatal (game-specific)
    coRunScript,                // Run a script entity when the player enters
    coFlipFloorTexture,         // Flip the floor texture horizontally
    coFlipCeilingTexture,       // Flip the ceiling texture horizontally
    coFlipUpperTexture,         // Flip the upper texture horizontally
    coFlipLowerTexture,         // Flip the lower texture horizontally
    coBlocker                   // The cell acts as if it blocks any passage
  );
  { TCellOptions - A set of TCellOption }
  TCellOptions = set of TCellOption;

  { TCell - A single grid cell }
  TCell = object
    // Textures
    Floor, Ceiling, Upper, Lower: Byte;
    // Floor height in centimeters (grows upwards)
    FloorHeight: SmallInt;
    // Ceiling height in centimeters (grows downwards)
    CeilingHeight: SmallInt;
    // Light value
    Light: Byte;
    // Texture rotations
    TextureRotations: Byte;
    // Cell options
    CellOptions: TCellOptions;
    // Game-specific data (not saved)
    GameData: IntPtr;
    // Reset to default values
    procedure Reset;
    // Set texture rotations
    procedure SetRotations(AFloorRot, ALowerRot, AUpperRot, ACeilingRot: Integer);
    // Get texture rotations
    procedure GetRotations(out AFloorRot, ALowerRot, AUpperRot, ACeilingRot: Integer);
  end;

  { TGridOuterWall - One of grid's outer walls }
  TGridOuterWall = (gowLeft, gowFront, gowRight, gowBack);
  { TGridOuterWalls - Set of outer walls }
  TGridOuterWalls = set of TGridOuterWall;

  { TGridPortal - A portal to another grid }
  TGridPortal = record
    // Portal vertices
    A, B, C, D: TVector;
    // Target grid
    Target: TGrid;
  end;

  { TGrid - A grid in the grid map }
  TGrid = class(TSerializable)
  private
    // Property storage
    FGridMap: TGridMap;
    FName: string;
    FWidth: Integer;
    FHeight: Integer;
    FOrigin: TVector;
    FCellHeight: Single;
    FCells: array of TCell;
    FAABox: TAABox;
    FSceneNode: TGridMeshNode;
    FSceneSector: TSceneSector;
    FAmbient: Byte;
    FLightScale: Single;
    FCameraName: string;
    FCamera: TCamera;
    FGameData: IntPtr;
    FOuterWalls: TGridOuterWalls;
    FSmoothFloor: Boolean;
    FTintOverride: Boolean;
    FTintOverrideBlue: Byte;
    FTintOverrideGreen: Byte;
    FTintOverrideRed: Byte;
    FTintOverrideSpeed: Single;
    function GetCells(X, Y: Integer): PCell; inline;
    function GetSceneNode: TGridMeshNode; inline;
    procedure SetCellHeight(ACellHeight: Single);
    procedure SetWidth(AWidth: Integer);
    procedure SetHeight(AHeight: Integer);
    procedure SetOrigin(AOrigin: TVector); inline;
    procedure SetOriginX(AOriginX: Single); inline;
    procedure SetOriginY(AOriginY: Single); inline;
    procedure SetOriginZ(AOriginZ: Single); inline;
    function GetOriginX: Single; inline;
    function GetOriginY: Single; inline;
    function GetOriginZ: Single; inline;
    procedure SetLightScale(ALightScale: Single);
    procedure SetCameraName(AValue: string);
    procedure SetSmoothFloor(ASmoothFloor: Boolean);
    procedure SetOuterWalls(AOuterWalls: TGridOuterWalls);
  private
    // Portals to other grids
    Portals: array of TGridPortal;
    // Create the scene node for this grid
    procedure CreateSceneNode;
    // Update the grid camera, if set
    procedure UpdateCamera;
  protected
    // TSerializable methods
    procedure AfterDeserialization; override;
    procedure AfterRootDeserialization; override;
    procedure SerializeExtraData(AContext: TSerializationContext); override;
    procedure DeserializeExtraData(AContext: TSerializationContext); override;
  public
    constructor Create; override;
    destructor Destroy; override;
    // Initialize a grid with the given dimensions
    procedure Init(AWidth, AHeight: Integer);
    // Change the size of the grid while preserving its contents
    procedure SetSize(AWidth, AHeight: Integer);
    // Return the cell at the given position
    function GetCellAt(X, Z: Single): PCell;
    // Return the cell coordinates at the given position, returns False if the
    // position is outside the cell
    function GetCellCoordsAt(X, Z: Single; out CellX, CellY: Integer): Boolean;
    // Return the floor and ceiling heights at the given coordinates
    procedure GetFloorAndCeilingHeightsAt(X, Z: Single; out AFloorHeight, ACeilingHeight: Single);
    // Return the floor and ceiling heights for the given cell
    procedure GetFloorAndCeilingHeightsForCell(ACell: PCell; out AFloorHeight, ACeilingHeight: Single);
    // Update the grid's axis aligned bounding box
    procedure UpdateAABox;
    // Update the scene node's geometry
    procedure UpdateSceneNode;
    {$IFDEF EDITOR}
    // Remove all portals
    procedure RemovePortals;
    // Add a new portal
    procedure AddPortal(const A, B, C, D: TVector; Target: TGrid);
    {$ENDIF}
  public
    // The owning gridmap
    property GridMap: TGridMap read FGridMap;
    // The grid's origin
    property Origin: TVector read FOrigin write SetOrigin;
    // The axis aligned bounding box of the entire grid
    property AABox: TAABox read FAABox;
    // The scene node for this grid
    property SceneNode: TGridMeshNode read GetSceneNode;
    // The scene sector for this grid
    property SceneSector: TSceneSector read FSceneSector;
    // The grid cells
    property Cells[X, Y: Integer]: PCell read GetCells;
    // Camera instance referenced to via CameraName
    property Camera: TCamera read FCamera;
    // Game-specific data (not saved)
    property GameData: IntPtr read FGameData write FGameData;
  published
    // The grid's name
    property Name: string read FName write FName;
    // The grid's width in cells (X direction)
    property Width: Integer read FWidth write SetWidth;
    // The grid's height in cells (Z direction)
    property Height: Integer read FHeight write SetHeight;
    // The grid's origin X component
    property OriginX: Single read GetOriginX write SetOriginX;
    // The grid's origin Y component
    property OriginY: Single read GetOriginY write SetOriginY;
    // The grid's origin Z component
    property OriginZ: Single read GetOriginZ write SetOriginZ;
    // The cell height (Y direction)
    property CellHeight: Single read FCellHeight write SetCellHeight;
    // Ambient light
    property Ambient: Byte read FAmbient write FAmbient;
    // Light scale
    property LightScale: Single read FLightScale write SetLightScale;
    // Camera to use when the player is inside the grid
    property CameraName: string read FCameraName write SetCameraName;
    // Enable tint override
    property TintOverride: Boolean read FTintOverride write FTintOverride;
    // Tint override red
    property TintOverrideRed: Byte read FTintOverrideRed write FTintOverrideRed;
    // Tint override green
    property TintOverrideGreen: Byte read FTintOverrideGreen write FTintOverrideGreen;
    // Tint override blue
    property TintOverrideBlue: Byte read FTintOverrideBlue write FTintOverrideBlue;
    // Tint override speed
    property TintOverrideSpeed: Single read FTintOverrideSpeed write FTintOverrideSpeed;
    // Use smooth floor (for terrain, etc)
    property SmoothFloor: Boolean read FSmoothFloor write SetSmoothFloor;
    // Generate outer walls for the grid
    property OuterWalls: TGridOuterWalls read FOuterWalls write SetOuterWalls;
  end;

  { TGridMapRayHitInfo - Information for ray hit checks against the grid map }
  TGridMapRayHitInfo = object
    // True if there was a hit
    Hit: Boolean;
    // Intersection point
    IP: TVector;
    // Grid that was hit
    Grid: TGrid;
    // Index of the grid that was hit
    GridIndex: Integer;
    // Cell coordinates
    CellX, CellY: Integer;
    // Cell part
    CellPart: TCellPart;
    // Ignore holes (texture=0)
    IgnoreHoles: Boolean;
    // Limit ray checks to this grid if not nil
    LimitGrid: TGrid;
    // Reset hit info
    procedure Reset;
  end;

  { TGridMap - Represents a grid-based map used for the game world }
  TGridMap = class(TSerializable)
  private
    // Property storage
    FGrids: TGridCollection;
    function GetGrids(AIndex: Integer): TGrid; inline;
    function GetGridCount: Integer; inline;
  protected
    // TSerializable methods
    procedure SerializeExtraData(AContext: TSerializationContext); override;
    procedure DeserializeExtraData(AContext: TSerializationContext); override;
  public
    constructor Create; override;
    destructor Destroy; override;
    // Add the gridmap scene nodes to the given scene
    procedure AddSceneNodes(Scene: TScene);
    // Remove the gridmap scene nodes from the given scene
    procedure RemoveSceneNodes(Scene: TScene);
    // Add the given grid to the grid map
    procedure AddGrid(AGrid: TGrid);
    // Delete the given grid
    procedure DeleteGrid(AGrid: TGrid);
    // Delete all grids in the grid map
    procedure DeleteAllGrids;
    // Return the grid with the given name
    function FindGrid(AName: string): TGrid;
    // Return the index of the given grid
    function IndexOfGrid(AGrid: TGrid): Integer;
    // Return the grid that contains the given point, optionally ignoring one
    function GridAt(const V: TVector; IgnoreGrid: TGrid=nil): TGrid;
    // Return the grid index and cell coordinates at the given position, returns
    // false if the position is not inside any grid
    function GetGridIndexAndCellCoordsAt(const V: TVector; out GridIndex, CellX, CellY: Integer): Boolean;
    // Return the grid and cell coordinates at the given position, returns nil
    // if the position is not inside any grid
    function GetGridAndCellCoordsAt(const V: TVector; out CellX, CellY: Integer): TGrid;
    // Return the floor and ceiling heights at the given point
    procedure GetFloorAndCeilingHeightsAt(const V: TVector; out FloorHeight, CeilingHeight: Single);
    // Return the floor and ceiling heights for the given box's corners
    procedure GetFloorAndCeilingHeightForBoxCorners(const Box: TAABox; out FloorHeight, CeilingHeight: Single);
    // Return true if the given box can fit in the world (note: only corners tested)
    function CanBoxFit(const Box: TAABox): Boolean;
    // Return the lighting at the given point
    function LightAt(const P: TVector): Byte;
    // Check for a ray hit against the grids
    function RayHit(const ARay: TRay; var Info: TGridMapRayHitInfo): Boolean;
    // Grids in the map
    property Grids[AIndex: Integer]: TGrid read GetGrids;
    // Number of grids
    property GridCount: Integer read GetGridCount;
  end;

implementation

uses
  Renderer, Engine, World, CamEnt;

{ TGridMapRayHitInfo }
procedure TGridMapRayHitInfo.Reset;
begin
  Hit:=False;
  IgnoreHoles:=False;
  LimitGrid:=nil;
end;

{ TGridMeshQuad }
procedure TGridMeshQuad.CalcMidPoint;
begin
  M:=MidpointBetween(MidpointBetween(A, B), MidpointBetween(C, D));
end;

{ TGridMeshNode }
procedure TGridMeshNode.CalcQuadsAABox;
var
  I: Integer;
begin
  QuadsAABox.MakeMaxInverted;
  for I:=0 to High(Quads) do with Quads[I] do begin
    QuadsAABox.Include(A);
    QuadsAABox.Include(B);
    QuadsAABox.Include(C);
    QuadsAABox.Include(D);
  end;
end;

procedure TGridMeshNode.RenderNode;
var
  Tri: PRenderTri;
  I: Integer;
  MAB, MBC, MCD, MDA: TVector;
  LAB, LBC, LCD, LDA, LM: Byte;
  SAB, SBC, SCD, SDA, SM: Byte;
  TAB, TBC, TCD, TDA, TM: Byte;
  DQ: Single;

  procedure AddQuad(const A, B, C, D: TVector; Texture: TTexture; SA, TA, SB, TB, SC, TC, SD, TD, LA, LB, LC, LD: Byte); inline;
  begin
    Tri:=GRenderer.AllocRenderTriangle;
    Tri^.A:=A;
    Tri^.SA:=SA;
    Tri^.TA:=TA;
    Tri^.B:=B;
    Tri^.SB:=SB;
    Tri^.TB:=TB;
    Tri^.C:=C;
    Tri^.SC:=SC;
    Tri^.TC:=TC;
    Tri^.LA:=LA;
    Tri^.LB:=LB;
    Tri^.LC:=LC;
    Tri^.Texture:=Texture;
    Tri^.DepthBias:=0;
    {$IFNDEF PETRAHW}
    Tri^.Occluder:=True;
    {$ENDIF}
    Tri:=GRenderer.AllocRenderTriangle;
    Tri^.A:=A;
    Tri^.SA:=SA;
    Tri^.TA:=TA;
    Tri^.B:=C;
    Tri^.SB:=SC;
    Tri^.TB:=TC;
    Tri^.C:=D;
    Tri^.SC:=SD;
    Tri^.TC:=TD;
    Tri^.LA:=LA;
    Tri^.LB:=LC;
    Tri^.LC:=LD;
    Tri^.Texture:=Texture;
    Tri^.DepthBias:=0;
    {$IFNDEF PETRAHW}
    Tri^.Occluder:=True;
    {$ENDIF}
  end;

begin
  // Ignore the request if the grid's scene sector is not visible
  if (GGame.Scene.VisibilityMode <> svmAlways) and not Grid.SceneSector.Visible then Exit;
  // Render quads
  for I:=0 to High(Quads) do with Quads[I] do begin
    {$IFDEF PETRAHW}
    if Subdivide and (DistanceSq(M, GRenderer.Camera.Position) < 128) then begin
      MAB:=MidpointBetween(A, B);
      MBC:=MidpointBetween(B, C);
      MCD:=MidpointBetween(C, D);
      MDA:=MidpointBetween(D, A);
      LAB:=(LA >> 1) + (LB >> 1);
      LBC:=(LB >> 1) + (LC >> 1);
      LCD:=(LC >> 1) + (LD >> 1);
      LDA:=(LD >> 1) + (LA >> 1);
      LM:=(LA >> 2) + (LB >> 2) + (LC >> 2) + (LD >> 2);
      SAB:=(SA >> 1) + (SB >> 1);
      SBC:=(SB >> 1) + (SC >> 1);
      SCD:=(SC >> 1) + (SD >> 1);
      SDA:=(SD >> 1) + (SA >> 1);
      SM:=(SA >> 2) + (SB >> 2) + (SC >> 2) + (SD >> 2);
      TAB:=(TA >> 1) + (TB >> 1);
      TBC:=(TB >> 1) + (TC >> 1);
      TCD:=(TC >> 1) + (TD >> 1);
      TDA:=(TD >> 1) + (TA >> 1);
      TM:=(TA >> 2) + (TB >> 2) + (TC >> 2) + (TD >> 2);
      AddQuad(A, MAB, M, MDA, Texture, SA, TA, SAB, TAB, SM, TM, SDA, TDA, LA, LAB, LM, LDA);
      AddQuad(M, MBC, C, MCD, Texture, SM, TM, SBC, TBC, SC, TC, SCD, TCD, LM, LBC, LC, LCD);
      AddQuad(MDA, M, MCD, D, Texture, SDA, TDA, SM, TM, SCD, TCD, SD, TD, LDA, LM, LCD, LD);
      AddQuad(MAB, B, MBC, M, Texture, SAB, TAB, SB, TB, SBC, TBC, SM, TM, LAB, LB, LBC, LM);
    end else
      AddQuad(A, B, C, D, Texture, SA, TA, SB, TB, SC, TC, SD, TD, LA, LB, LC, LD);
    {$ELSE}
    DQ:=DistanceSq(M, GRenderer.Camera.Position);
    if DQ > Sqr(GRenderer.Camera.FarPlane + GridCellSize) then Continue;
    if DQ > 32 then begin
      AddQuad(A, B, C, D, Texture, SA, TA, SB, TB, SC, TC, SD, TD, LA, LB, LC, LD);
    end else begin
      MAB:=MidpointBetween(A, B);
      MBC:=MidpointBetween(B, C);
      MCD:=MidpointBetween(C, D);
      MDA:=MidpointBetween(D, A);
      LAB:=(LA >> 1) + (LB >> 1);
      LBC:=(LB >> 1) + (LC >> 1);
      LCD:=(LC >> 1) + (LD >> 1);
      LDA:=(LD >> 1) + (LA >> 1);
      LM:=(LA >> 2) + (LB >> 2) + (LC >> 2) + (LD >> 2);
      SAB:=(SA >> 1) + (SB >> 1);
      SBC:=(SB >> 1) + (SC >> 1);
      SCD:=(SC >> 1) + (SD >> 1);
      SDA:=(SD >> 1) + (SA >> 1);
      SM:=(SA >> 2) + (SB >> 2) + (SC >> 2) + (SD >> 2);
      TAB:=(TA >> 1) + (TB >> 1);
      TBC:=(TB >> 1) + (TC >> 1);
      TCD:=(TC >> 1) + (TD >> 1);
      TDA:=(TD >> 1) + (TA >> 1);
      TM:=(TA >> 2) + (TB >> 2) + (TC >> 2) + (TD >> 2);
      AddQuad(A, MAB, M, MDA, Texture, SA, TA, SAB, TAB, SM, TM, SDA, TDA, LA, LAB, LM, LDA);
      AddQuad(M, MBC, C, MCD, Texture, SM, TM, SBC, TBC, SC, TC, SCD, TCD, LM, LBC, LC, LCD);
      AddQuad(MDA, M, MCD, D, Texture, SDA, TDA, SM, TM, SCD, TCD, SD, TD, LDA, LM, LCD, LD);
      AddQuad(MAB, B, MBC, M, Texture, SAB, TAB, SB, TB, SBC, TBC, SM, TM, LAB, LB, LBC, LM);
    end;
    {$ENDIF}
  end;
end;

function TGridMeshNode.GetOwnLocalAABox: TAABox;
begin
  Result:=QuadsAABox;
end;

{ TCell }
procedure TCell.Reset;
begin
  Floor:=1;
  Ceiling:=1;
  Upper:=1;
  Lower:=1;
  FloorHeight:=0;
  CeilingHeight:=0;
  Light:=255;
  TextureRotations:=0;
  GameData:=0;
end;

procedure TCell.SetRotations(AFloorRot, ALowerRot, AUpperRot, ACeilingRot: Integer);
begin
  AFloorRot:=Clamp(AFloorRot, 0, 3);
  ALowerRot:=Clamp(ALowerRot, 0, 3);
  AUpperRot:=Clamp(AUpperRot, 0, 3);
  ACeilingRot:=Clamp(ACeilingRot, 0, 3);
  TextureRotations:=AFloorRot or (ALowerRot shl 2) or (AUpperRot shl 4) or (ACeilingRot shl 6);
end;

procedure TCell.GetRotations(out AFloorRot, ALowerRot, AUpperRot, ACeilingRot: Integer);
begin
  AFloorRot:=TextureRotations and 3;
  ALowerRot:=(TextureRotations shr 2) and 3;
  AUpperRot:=(TextureRotations shr 4) and 3;
  ACeilingRot:=(TextureRotations shr 6) and 3;
end;

{ TGrid }
constructor TGrid.Create;
begin
  inherited Create;
  FAmbient:=20;
  FLightScale:=1;
  FSceneSector:=TSceneSector.Create;
  FTintOverrideRed:=255;
  FTintOverrideGreen:=255;
  FTintOverrideBlue:=255;
  FTintOverrideSpeed:=0.0625;
end;

destructor TGrid.Destroy;
begin
  // Destroy the scene node if it isn't attached anywhere
  if not Assigned(SceneNode.Parent) then SceneNode.Free;
  FSceneSector.Free;
  inherited Destroy;
end;

function TGrid.GetCells(X, Y: Integer): PCell;
begin
  Result:=@FCells[Y*FWidth + X];
end;

function TGrid.GetSceneNode: TGridMeshNode;
begin
  if not Assigned(FSceneNode) then CreateSceneNode;
  Result:=FSceneNode;
end;

procedure TGrid.SetCellHeight(ACellHeight: Single);
begin
  if FCellHeight=ACellHeight then Exit;
  FCellHeight:=ACellHeight;
  UpdateSceneNode;
end;

procedure TGrid.SetWidth(AWidth: Integer);
begin
  if FWidth=AWidth then Exit;
  SetSize(AWidth, Height);
end;

procedure TGrid.SetHeight(AHeight: Integer);
begin
  if FHeight=AHeight then Exit;
  SetSize(Width, AHeight);
end;

procedure TGrid.SetOrigin(AOrigin: TVector);
begin
  if FOrigin=AOrigin then Exit;
  FOrigin:=AOrigin;
  UpdateAABox;
  UpdateSceneNode;
end;

procedure TGrid.SetOriginX(AOriginX: Single);
begin
  SetOrigin(Vector(AOriginX, Origin.Y, Origin.Z));
end;

procedure TGrid.SetOriginY(AOriginY: Single);
begin
  SetOrigin(Vector(Origin.X, AOriginY, Origin.Z));
end;

procedure TGrid.SetOriginZ(AOriginZ: Single);
begin
  SetOrigin(Vector(Origin.X, Origin.Y, AOriginZ));
end;

function TGrid.GetOriginX: Single;
begin
  Result:=Origin.X;
end;

function TGrid.GetOriginY: Single;
begin
  Result:=Origin.Y;
end;

function TGrid.GetOriginZ: Single;
begin
  Result:=Origin.Z;
end;

procedure TGrid.AfterDeserialization;
begin
  inherited AfterDeserialization;
  UpdateAABox;
end;

procedure TGrid.AfterRootDeserialization;
var
  I, Index: Integer;
begin
  inherited AfterRootDeserialization;
  // Save proper grid targets
  for I:=0 to High(Portals) do with Portals[I] do begin
    Index:=Target.FHeight;
    Target.Free;
    Target:=GridMap.Grids[Index];
  end;
  // Force scene node updates for the grid
  UpdateSceneNode;
  UpdateCamera;
end;

procedure TGrid.CreateSceneNode;
begin
  FSceneNode:=TGridMeshNode.Create;
  FSceneNode.Grid:=Self;
  UpdateSceneNode;
  UpdateCamera;
end;

procedure TGrid.UpdateCamera;
var
  CamEnt: TEntity;
begin
  if (CameraName='') or (not Assigned(GGame)) or (not Assigned(GGame.World)) then begin
    FCamera:=nil;
  end else begin
    CamEnt:=GGame.World.FindEntity(CameraName);
    if CamEnt is TCameraEntity then
      FCamera:=TCameraEntity(CamEnt).Camera
    else
      FCamera:=nil;
  end;
end;

procedure TGrid.SetCameraName(AValue: string);
begin
  if FCameraName=AValue then Exit;
  FCameraName:=AValue;
  UpdateCamera;
end;

procedure TGrid.SetSmoothFloor(ASmoothFloor: Boolean);
begin
  if FSmoothFloor=ASmoothFloor then Exit;
  FSmoothFloor:=ASmoothFloor;
  UpdateSceneNode;
end;

procedure TGrid.SetOuterWalls(AOuterWalls: TGridOuterWalls);
begin
  if FOuterWalls=AOuterWalls then Exit;
  FOuterWalls:=AOuterWalls;
  UpdateSceneNode;
end;

procedure TGrid.SetLightScale(ALightScale: Single);
begin
  if FLightScale=ALightScale then Exit;
  FLightScale:=ALightScale;
  UpdateSceneNode;
end;

procedure TGrid.SerializeExtraData(AContext: TSerializationContext);
var
  I: Integer;
begin
  inherited SerializeExtraData(AContext);
  // Write cells
  for I:=0 to High(FCells) do with AContext do with FCells[I] do begin
    WriteUInt8(Lower);
    WriteUInt8(Floor);
    WriteUInt8(Ceiling);
    WriteUInt8(Upper);
    WriteInt16(FloorHeight);
    WriteInt16(CeilingHeight);
    WriteUInt8(Light);
    Write(CellOptions, SizeOf(CellOptions));
    WriteUInt8(TextureRotations);
  end;
  // Write portals
  with AContext do begin
    WriteUInt8(Length(Portals));
    for I:=0 to High(Portals) do with Portals[I] do begin
      WriteSingle(A.X); WriteSingle(A.Y); WriteSingle(A.Z);
      WriteSingle(B.X); WriteSingle(B.Y); WriteSingle(B.Z);
      WriteSingle(C.X); WriteSingle(C.Y); WriteSingle(C.Z);
      WriteSingle(D.X); WriteSingle(D.Y); WriteSingle(A.Z);
      WriteUInt8(GridMap.IndexOfGrid(Portals[I].Target));
    end;
  end;
end;

procedure TGrid.DeserializeExtraData(AContext: TSerializationContext);
var
  I, J: Integer;
begin
  inherited DeserializeExtraData(AContext);
  // Read cells
  SetLength(FCells, Width*Height);
  for I:=0 to High(FCells) do with AContext do with FCells[I] do begin
    Lower:=ReadUInt8;
    Floor:=ReadUInt8;
    Ceiling:=ReadUInt8;
    Upper:=ReadUInt8;
    FloorHeight:=ReadInt16;
    CeilingHeight:=ReadInt16;
    Light:=ReadUInt8;
    Read(CellOptions, SizeOf(CellOptions));
    if AContext.Version >= 9 then
      TextureRotations:=ReadUInt8
    else
      TextureRotations:=0;
    // Ignore PVS data from version 6
    if AContext.Version=6 then begin
      for J:=0 to ReadUInt8 - 1 do ReadUInt8;
    end;
  end;
  // Read portals
  if AContext.Version >= 7 then begin
    with AContext do begin
      SetLength(Portals, ReadUInt8);
      for I:=0 to High(Portals) do with Portals[I] do begin
        A.X:=ReadSingle; A.Y:=ReadSingle; A.Z:=ReadSingle;
        B.X:=ReadSingle; B.Y:=ReadSingle; B.Z:=ReadSingle;
        C.X:=ReadSingle; C.Y:=ReadSingle; C.Z:=ReadSingle;
        D.X:=ReadSingle; D.Y:=ReadSingle; D.Z:=ReadSingle;
        // Create a dummy grid to hold the index until after root creation
        // (the dummy is needed to be a valid grid object because it is accessed
        // during game deserialization)
        Target:=TGrid.Create;
        Target.FHeight:=ReadUInt8;
      end;
    end;
  end;
end;

procedure TGrid.Init(AWidth, AHeight: Integer);
var
  I: Integer;
begin
  // Allocate cells
  FWidth:=Max(1, AWidth);
  FHeight:=Max(1, AHeight);
  SetLength(FCells, FWidth*FHeight);
  // Make them fullbright and use texture 1
  for I:=0 to High(FCells) do begin
    FCells[I].Floor:=1;
    FCells[I].Ceiling:=1;
    FCells[I].Upper:=1;
    FCells[I].Lower:=1;
    FCells[I].Light:=255;
  end;
  // Initial cell height
  FCellHeight:=5;
end;

procedure TGrid.SetSize(AWidth, AHeight: Integer);
var
  NewCells: array of TCell;
  X, Y: Integer;
begin
  // Allocate new cells
  AWidth:=Max(1, AWidth);
  AHeight:=Max(1, AHeight);
  SetLength(NewCells, AWidth*AHeight);
  // Copy existing cells
  for Y:=0 to AHeight - 1 do
    for X:=0 to AWidth - 1 do
      if (X < FWidth) and (Y < FHeight) then
        NewCells[Y*AWidth + X]:=FCells[Y*FWidth + X]
      else
        NewCells[Y*AWidth + X].Reset;
  // Replace existing cells with the new ones
  FWidth:=AWidth;
  FHeight:=AHeight;
  FCells:=NewCells;
  // Update bounds and, if needed, scene node
  UpdateAABox;
  UpdateSceneNode;
end;

function TGrid.GetCellAt(X, Z: Single): PCell;
var
  CellX, CellY: Integer;
begin
  if (X >= AABox.Min.X) and (X <= AABox.Max.X) and
     (Z >= AABox.Min.Z) and (Z <= AABox.Max.Z) then begin
    CellX:=Trunc((X - Origin.X)/GridCellSize);
    CellY:=Trunc((Z - Origin.Z)/GridCellSize);
    Result:=@Cells[CellX, CellY]^;
  end else
    Result:=nil;
end;

function TGrid.GetCellCoordsAt(X, Z: Single; out CellX, CellY: Integer): Boolean;
begin
  if (X >= AABox.Min.X) and (X <= AABox.Max.X) and
     (Z >= AABox.Min.Z) and (Z <= AABox.Max.Z) then begin
    CellX:=Trunc((X - Origin.X)/GridCellSize);
    CellY:=Trunc((Z - Origin.Z)/GridCellSize);
    Result:=True;
  end else
    Result:=False;
end;

procedure TGrid.GetFloorAndCeilingHeightsAt(X, Z: Single; out AFloorHeight,
  ACeilingHeight: Single);
var
  CellX, CellY: Integer;
  RightHeight, DownHeight, RightDownHeight, FX, FY: Single;
begin
  if (X >= AABox.Min.X) and (X <= AABox.Max.X) and
     (Z >= AABox.Min.Z) and (Z <= AABox.Max.Z) then begin
    CellX:=Trunc((X - Origin.X)/GridCellSize);
    CellY:=Trunc((Z - Origin.Z)/GridCellSize);
    with Cells[CellX, CellY]^ do begin
      if coBlocker in CellOptions then begin
        AFloorHeight:=0;
        ACeilingHeight:=0;
        Exit;
      end;
      if Floor <> 0 then
        AFloorHeight:=Origin.Y + FloorHeight/100
      else
        AFloorHeight:=-1000000;
      if Ceiling <> 0 then
        ACeilingHeight:=Origin.Y + CellHeight - CeilingHeight/100
      else
        ACeilingHeight:=1000000;
      if SmoothFloor then begin
        if CellX < Width - 1 then begin
          RightHeight:=Cells[CellX + 1, CellY]^.FloorHeight/100;
          if CellY < Height - 1 then begin
            DownHeight:=Cells[CellX, CellY + 1]^.FloorHeight/100;
            RightDownHeight:=Cells[CellX + 1, CellY + 1]^.FloorHeight/100;
          end else begin
            DownHeight:=AFloorHeight;
            RightDownHeight:=AFloorHeight;
          end;
        end else begin
          RightHeight:=AFloorHeight;
          RightDownHeight:=AFloorHeight;
          if CellY < Height - 1 then
            DownHeight:=Cells[CellX, CellY + 1]^.FloorHeight/100
          else
            DownHeight:=AFloorHeight;
        end;
        FX:=(X - Origin.X)/GridCellSize - CellX;
        FY:=(Z - Origin.Z)/GridCellSize - CellY;
        AFloorHeight:=Interpolate(
          Interpolate(AFloorHeight, RightHeight, FX),
          Interpolate(DownHeight, RightDownHeight, FX),
          FY);
      end;
    end;
  end else begin
    AFloorHeight:=-1000000;
    ACeilingHeight:=1000000;
  end;
end;

procedure TGrid.GetFloorAndCeilingHeightsForCell(ACell: PCell; out AFloorHeight, ACeilingHeight: Single);
begin
  with ACell^ do begin
    if coBlocker in ACell^.CellOptions then begin
      AFloorHeight:=0;
      ACeilingHeight:=0;
    end else begin
      if Floor <> 0 then
        AFloorHeight:=Origin.Y + FloorHeight/100
      else
        AFloorHeight:=-1000000;
      if Ceiling <> 0 then
        ACeilingHeight:=Origin.Y + CellHeight - CeilingHeight/100
      else
        ACeilingHeight:=1000000;
    end;
  end;
end;

procedure TGrid.UpdateAABox;
var
  X, Y: Integer;
begin
  FAABox.MakeMaxInverted;
  FAABox.Include(Origin);
  FAABox.Include(Origin.Added(Vector(Width*GridCellSize, CellHeight, Height*GridCellSize)));
  for Y:=0 to Height - 1 do
    for X:=0 to Width - 1 do begin
      FAABox.Include(Origin.Added(Vector(0, Cells[X, Y]^.FloorHeight/100, 0)));
      FAABox.Include(Origin.Added(Vector(0, CellHeight - Cells[X, Y]^.CeilingHeight/100, 0)));
    end;
  FSceneSector.AABox:=FAABox;
end;

procedure TGrid.UpdateSceneNode;
var
  X, Z, I: Integer;
  QuadCount, FloorRot, LowerRot, UpperRot, CeilingRot: Integer;
  FloorY: array [0..3] of Single;
  CeilingY, NearY: Single;

  function IsColumn(CX, CY: Integer): Boolean;
  begin
    Result:=Abs(Cells[CX, CY]^.FloorHeight/100 - (CellHeight - Cells[CX, CY]^.CeilingHeight/100)) < 0.1;
  end;

  function LightAt(const V: TVector): Single;
  var
    CX, CY, NCX, NCY: Integer;
    DX, DY, X1Light, X2Light: Single;
  begin
    CX:=Clamp(Round(V.X/GridCellSize), 0, Width - 1);
    CY:=Clamp(Round(V.Z/GridCellSize), 0, Height - 1);
    NCX:=Clamp(CX + 1, 0, Width - 1);
    NCY:=Clamp(CY + 1, 0, Height - 1);
    DX:=Clamp(V.X/GridCellSize - CX);
    DY:=Clamp(V.Y/GridCellSize - CY);
    X1Light:=(Cells[CX, CY]^.Light/255)*(1 - DX) + (Cells[NCX, CY]^.Light/255)*DX;
    X2Light:=(Cells[CX, NCY]^.Light/255)*(1 - DX) + (Cells[NCX, NCY]^.Light/255)*DX;
    Result:=Clamp(LightScale*(X1Light*(1 - DY) + X2Light*DY), 0, 1);
  end;

  procedure AddQuad(const VA, VB, VC, VD: TVector; ATexture: TTexture; Fullbright, Flip: Boolean; Rotation: Integer{$IFDEF PETRAHW}; ASubdivide: Boolean{$ENDIF});
  var
    Tmp: Integer;
  begin
    if Length(FSceneNode.Quads)=QuadCount then
      SetLength(FSceneNode.Quads, QuadCount + 64);
    with FSceneNode.Quads[QuadCount] do begin
      A:=Origin.Added(VA);
      B:=Origin.Added(VB);
      C:=Origin.Added(VC);
      D:=Origin.Added(VD);
      if Fullbright then begin
        LA:=255;
        LB:=255;
        LC:=255;
        LD:=255;
      end else begin
        LA:=Round(LightAt(VA)*(0.45 + Clamp(TriangleNormal(A, B, C).Dot(Vector(1, 3, 2).Normalized))*0.55)*255);
        LB:=Round(LightAt(VB)*(0.45 + Clamp(TriangleNormal(A, B, C).Dot(Vector(1, 3, 2).Normalized))*0.55)*255);
        LC:=Round(LightAt(VC)*(0.45 + Clamp(TriangleNormal(A, B, C).Dot(Vector(1, 3, 2).Normalized))*0.55)*255);
        LD:=Round(LightAt(VD)*(0.45 + Clamp(TriangleNormal(A, B, C).Dot(Vector(1, 3, 2).Normalized))*0.55)*255);
      end;
      CalcMidPoint;
      Texture:=ATexture;
      case Rotation of
        0: begin
          SA:=0;  TA:=0;
          SB:=0;  TB:=63;
          SC:=63; TC:=63;
          SD:=63; TD:=0;
        end;
        1: begin
          SA:=0;  TA:=63;
          SB:=63; TB:=63;
          SC:=63; TC:=0;
          SD:=0;  TD:=0;
        end;
        2: begin
          SA:=63; TA:=63;
          SB:=63; TB:=0;
          SC:=0;  TC:=0;
          SD:=0;  TD:=63;
        end;
        3: begin
          SA:=63; TA:=0;
          SB:=0;  TB:=0;
          SC:=0;  TC:=63;
          SD:=63; TD:=63;
        end;
      end;
      if Flip then begin
        Tmp:=SA; SA:=SC; SC:=Tmp;
        Tmp:=SB; SB:=SD; SD:=Tmp;
      end;
      {$IFDEF PETRAHW}
      Subdivide:=ASubdivide;
      {$ENDIF}
    end;
    Inc(QuadCount);
  end;

  procedure AddWall(VA, VB, VC, VD: TVector; ATexture: TTexture; Fullbright, Flip: Boolean; Rotation: Integer);
  var
    I, WallQuads: Integer;
    Height: Single;
  begin
    Height:=VA.Y - VB.Y;
    WallQuads:=Max(1, Round(Height/GridCellSize + 0.15));
    VB.Y:=VA.Y - Height/WallQuads;
    VC.Y:=VA.Y - Height/WallQuads;
    for I:=0 to WallQuads - 1 do begin
      AddQuad(VA, VB, VC, VD, ATexture, Fullbright, Flip, Rotation{$IFDEF PETRAHW}, False{$ENDIF});
      VA.Y -= Height/WallQuads;
      VB.Y -= Height/WallQuads;
      VC.Y -= Height/WallQuads;
      VD.Y -= Height/WallQuads;
    end;
  end;

begin
  if not Assigned(FSceneNode) then Exit;
  UpdateAABox;
  SetLength(FSceneNode.Quads, Width*Height*2);
  QuadCount:=0;
  for Z:=0 to Height - 1 do
    for X:=0 to Width - 1 do with FCells[Z*Width + X] do begin
      // Get texture rotations
      GetRotations(FloorRot, LowerRot, UpperRot, CeilingRot);
      // Clamp floor/ceiling heights
      FloorHeight:=Min(FloorHeight, Trunc(CellHeight*100) - CeilingHeight);
      // Floor and ceiling heights
      FloorY[0]:=FloorHeight/100;
      if SmoothFloor then begin
        if Z < Height - 1 then FloorY[1]:=FCells[(Z + 1)*Width + X].FloorHeight/100 else FloorY[1]:=FloorY[0];
        if (Z < Height - 1) and (X < Width - 1) then FloorY[2]:=FCells[(Z + 1)*Width + X + 1].FloorHeight/100 else begin
          if X < Width - 1 then FloorY[2]:=FCells[Z*Width + X + 1].FloorHeight/100 else FloorY[3]:=FloorY[2];
        end;
        if X < Width - 1 then FloorY[3]:=FCells[Z*Width + X + 1].FloorHeight/100 else FloorY[3]:=FloorY[0];
      end else begin
        FloorY[1]:=FloorY[0];
        FloorY[2]:=FloorY[0];
        FloorY[3]:=FloorY[0];
      end;
      CeilingY:=CellHeight - CeilingHeight/100;
      // Floor and ceiling
      if FloorY[0] < CeilingY then begin
        // Floor
        if Floor <> 0 then
          AddQuad(Vector(X*GridCellSize, FloorY[0], Z*GridCellSize),
                  Vector(X*GridCellSize, FloorY[1], Z*GridCellSize + GridCellSize),
                  Vector(X*GridCellSize + GridCellSize, FloorY[2], Z*GridCellSize + GridCellSize),
                  Vector(X*GridCellSize + GridCellSize, FloorY[3], Z*GridCellSize),
                  TTexture(GResourceManager.WallTextures[Floor]),
                  coFullbrightFloor in CellOptions,
                  coFlipFloorTexture in CellOptions,
                  FloorRot
                  {$IFDEF PETRAHW}, SmoothFloor{$ENDIF});
        // Ceiling
        if Ceiling <> 0 then
          AddQuad(Vector(X*GridCellSize, CeilingY, Z*GridCellSize + GridCellSize),
                  Vector(X*GridCellSize, CeilingY, Z*GridCellSize),
                  Vector(X*GridCellSize + GridCellSize, CeilingY, Z*GridCellSize),
                  Vector(X*GridCellSize + GridCellSize, CeilingY, Z*GridCellSize + GridCellSize),
                  TTexture(GResourceManager.WallTextures[Ceiling]),
                  coFullbrightCeiling in CellOptions,
                  coFlipCeilingTexture in CellOptions,
                  CeilingRot
                  {$IFDEF PETRAHW}, False{$ENDIF});
      end;
      // Lower walls
      if not SmoothFloor then begin
        // Left wall
        if ((gowLeft in OuterWalls) and (X=0)) or ((X > 0) and (FCells[Z*Width + X - 1].FloorHeight < FloorHeight) and not IsColumn(X - 1, Z)) then begin
          if X > 0 then NearY:=FCells[Z*Width + X - 1].FloorHeight/100 else NearY:=0;
          AddWall(Vector(X*GridCellSize, FloorY[0], Z*GridCellSize),
                  Vector(X*GridCellSize, NearY, Z*GridCellSize),
                  Vector(X*GridCellSize, NearY, Z*GridCellSize + GridCellSize),
                  Vector(X*GridCellSize, FloorY[0], Z*GridCellSize + GridCellSize),
                  TTexture(GResourceManager.WallTextures[Lower]),
                  coFullbrightWalls in CellOptions,
                  coFlipLowerTexture in CellOptions,
                  LowerRot);
        end;
        // Right wall
        if ((gowRight in OuterWalls) and (X=Width - 1)) or ((X < Width - 1) and (FCells[Z*Width + X + 1].FloorHeight < FloorHeight) and not IsColumn(X + 1, Z)) then begin
          if X < Width - 1 then NearY:=FCells[Z*Width + X + 1].FloorHeight/100 else NearY:=0;
          AddWall(Vector(X*GridCellSize + GridCellSize, FloorY[0], Z*GridCellSize + GridCellSize),
                  Vector(X*GridCellSize + GridCellSize, NearY, Z*GridCellSize + GridCellSize),
                  Vector(X*GridCellSize + GridCellSize, NearY, Z*GridCellSize),
                  Vector(X*GridCellSize + GridCellSize, FloorY[0], Z*GridCellSize),
                  TTexture(GResourceManager.WallTextures[Lower]),
                  coFullbrightWalls in CellOptions,
                  coFlipLowerTexture in CellOptions,
                  LowerRot);
        end;
        // Back wall
        if ((gowBack in OuterWalls) and (Z=0)) or ((Z > 0) and (FCells[(Z - 1)*Width + X].FloorHeight < FloorHeight) and not IsColumn(X, Z - 1)) then begin
          if Z > 0 then NearY:=FCells[(Z - 1)*Width + X].FloorHeight/100 else NearY:=0;
          AddWall(Vector(X*GridCellSize + GridCellSize, FloorY[0], Z*GridCellSize),
                  Vector(X*GridCellSize + GridCellSize, NearY, Z*GridCellSize),
                  Vector(X*GridCellSize, NearY, Z*GridCellSize),
                  Vector(X*GridCellSize, FloorY[0], Z*GridCellSize),
                  TTexture(GResourceManager.WallTextures[Lower]),
                  coFullbrightWalls in CellOptions,
                  coFlipLowerTexture in CellOptions,
                  LowerRot);
        end;
        // Front wall
        if ((gowFront in OuterWalls) and (Z=Height - 1)) or ((Z < Height - 1) and (FCells[(Z + 1)*Width + X].FloorHeight < FloorHeight) and not IsColumn(X, Z + 1)) then begin
          if Z < Height - 1 then NearY:=FCells[(Z + 1)*Width + X].FloorHeight/100 else NearY:=0;
          AddWall(Vector(X*GridCellSize, FloorY[0], Z*GridCellSize + GridCellSize),
                  Vector(X*GridCellSize, NearY, Z*GridCellSize + GridCellSize),
                  Vector(X*GridCellSize + GridCellSize, NearY, Z*GridCellSize + GridCellSize),
                  Vector(X*GridCellSize + GridCellSize, FloorY[0], Z*GridCellSize + GridCellSize),
                  TTexture(GResourceManager.WallTextures[Lower]),
                  coFullbrightWalls in CellOptions,
                  coFlipLowerTexture in CellOptions,
                  LowerRot);
        end;
      end;
      // Upper walls
      // Left wall
      if ((gowLeft in OuterWalls) and (X=0)) or ((X > 0) and (FCells[Z*Width + X - 1].CeilingHeight < CeilingHeight) and not IsColumn(X - 1, Z)) then begin
        if X > 0 then NearY:=CellHeight - FCells[Z*Width + X - 1].CeilingHeight/100 else NearY:=CellHeight;
        AddWall(Vector(X*GridCellSize, NearY, Z*GridCellSize),
                Vector(X*GridCellSize, CeilingY, Z*GridCellSize),
                Vector(X*GridCellSize, CeilingY, Z*GridCellSize + GridCellSize),
                Vector(X*GridCellSize, NearY, Z*GridCellSize + GridCellSize),
                TTexture(GResourceManager.WallTextures[Upper]),
                coFullbrightWalls in CellOptions,
                coFlipUpperTexture in CellOptions,
                UpperRot);
      end;
      // Right wall
      if ((gowRight in OuterWalls) and (X=Width - 1)) or ((X < Width - 1) and (FCells[Z*Width + X + 1].CeilingHeight < CeilingHeight) and not IsColumn(X + 1, Z)) then begin
        if X < Width - 1 then NearY:=CellHeight - FCells[Z*Width + X + 1].CeilingHeight/100 else NearY:=CellHeight;
        AddWall(Vector(X*GridCellSize + GridCellSize, NearY, Z*GridCellSize + GridCellSize),
                Vector(X*GridCellSize + GridCellSize, CeilingY, Z*GridCellSize + GridCellSize),
                Vector(X*GridCellSize + GridCellSize, CeilingY, Z*GridCellSize),
                Vector(X*GridCellSize + GridCellSize, NearY, Z*GridCellSize),
                TTexture(GResourceManager.WallTextures[Upper]),
                coFullbrightWalls in CellOptions,
                coFlipUpperTexture in CellOptions,
                UpperRot);
      end;
      // Back wall
      if ((gowBack in OuterWalls) and (Z=0)) or ((Z > 0) and (FCells[(Z - 1)*Width + X].CeilingHeight < CeilingHeight) and not IsColumn(X, Z - 1)) then begin
        if Z > 0 then NearY:=CellHeight - FCells[(Z - 1)*Width + X].CeilingHeight/100 else NearY:=CellHeight;
        AddWall(Vector(X*GridCellSize + GridCellSize, NearY, Z*GridCellSize),
                Vector(X*GridCellSize + GridCellSize, CeilingY, Z*GridCellSize),
                Vector(X*GridCellSize, CeilingY, Z*GridCellSize),
                Vector(X*GridCellSize, NearY, Z*GridCellSize),
                TTexture(GResourceManager.WallTextures[Upper]),
                coFullbrightWalls in CellOptions,
                coFlipUpperTexture in CellOptions,
                UpperRot);
      end;
      // Front wall
      if ((gowFront in OuterWalls) and (Z=Height - 1)) or ((Z < Height - 1) and (FCells[(Z + 1)*Width + X].CeilingHeight < CeilingHeight) and not IsColumn(X, Z + 1)) then begin
        if Z < Height - 1 then NearY:=CellHeight - FCells[(Z + 1)*Width + X].CeilingHeight/100 else NearY:=CellHeight;
        AddWall(Vector(X*GridCellSize, NearY, Z*GridCellSize + GridCellSize),
                Vector(X*GridCellSize, CeilingY, Z*GridCellSize + GridCellSize),
                Vector(X*GridCellSize + GridCellSize, CeilingY, Z*GridCellSize + GridCellSize),
                Vector(X*GridCellSize + GridCellSize, NearY, Z*GridCellSize + GridCellSize),
                TTexture(GResourceManager.WallTextures[Upper]),
                coFullbrightWalls in CellOptions,
                coFlipUpperTexture in CellOptions,
                UpperRot);
      end;
    end;
  SetLength(FSceneNode.Quads, QuadCount);
  FSceneNode.CalcQuadsAABox;
  SceneSector.RemovePortals;
  for I:=0 to High(Portals) do with Portals[I] do
    SceneSector.AddPortal(A, B, C, D, Target.SceneSector);
end;

{$IFDEF EDITOR}
procedure TGrid.RemovePortals;
begin
  Portals:=nil;
end;

procedure TGrid.AddPortal(const A, B, C, D: TVector; Target: TGrid);
begin
  SetLength(Portals, Length(Portals) + 1);
  Portals[High(Portals)].A:=A;
  Portals[High(Portals)].B:=B;
  Portals[High(Portals)].C:=C;
  Portals[High(Portals)].D:=D;
  Portals[High(Portals)].Target:=Target;
end;
{$ENDIF}

{ TGridMap }
destructor TGridMap.Destroy;
begin
  DeleteAllGrids;
  FGrids.Free;
  inherited Destroy;
end;

procedure TGridMap.AddSceneNodes(Scene: TScene);
var
  I: Integer;
begin
  for I:=0 to GridCount - 1 do begin
    Scene.AddChild(Grids[I].SceneNode);
    Scene.AddSector(Grids[I].SceneSector);
  end;
end;

procedure TGridMap.RemoveSceneNodes(Scene: TScene);
var
  I: Integer;
begin
  for I:=0 to GridCount - 1 do begin
    Scene.RemoveSector(Grids[I].SceneSector);
    Scene.RemoveChild(Grids[I].SceneNode);
  end;
end;

function TGridMap.GetGridCount: Integer;
begin
  Result:=FGrids.Count;
end;

procedure TGridMap.SerializeExtraData(AContext: TSerializationContext);
begin
  inherited SerializeExtraData(AContext);
  // Write grids
  AContext.WriteObject(FGrids);
end;

procedure TGridMap.DeserializeExtraData(AContext: TSerializationContext);
var
  I: Integer;
begin
  inherited DeserializeExtraData(AContext);
  // Read grids
  DeleteAllGrids;
  FGrids.Free;
  FGrids:=TGridCollection(AContext.ReadObject(TGridCollection));
  for I:=0 to FGrids.Count - 1 do FGrids[I].FGridMap:=Self;
end;

constructor TGridMap.Create;
begin
  inherited Create;
  FGrids:=TGridCollection.Create;
end;

procedure TGridMap.AddGrid(AGrid: TGrid);
begin
  FGrids.Add(AGrid);
  AGrid.FGridMap:=Self;
  AGrid.UpdateAABox;
end;

procedure TGridMap.DeleteGrid(AGrid: TGrid);
begin
  FGrids.Delete(AGrid);
end;

procedure TGridMap.DeleteAllGrids;
begin
  FGrids.DeleteAllAndClear;
end;

function TGridMap.FindGrid(AName: string): TGrid;
var
  I: Integer;
begin
  for I:=0 to GridCount - 1 do
    if Grids[I].Name=AName then Exit(Grids[I]);
  Result:=nil;
end;

function TGridMap.IndexOfGrid(AGrid: TGrid): Integer;
begin
  Result:=FGrids.IndexOf(AGrid);
end;

function TGridMap.GridAt(const V: TVector; IgnoreGrid: TGrid): TGrid;
var
  I: Integer;
begin
  for I:=0 to GridCount - 1 do
    if (Grids[I] <> IgnoreGrid) and Grids[I].AABox.Includes(V) then
      Exit(Grids[I]);
  Result:=nil;
end;

function TGridMap.GetGridIndexAndCellCoordsAt(const V: TVector; out GridIndex, CellX, CellY: Integer): Boolean;
var
  I: Integer;
begin
  for I:=0 to GridCount - 1 do
    if Grids[I].AABox.Includes(V) then begin
      GridIndex:=I;
      Exit(Grids[I].GetCellCoordsAt(V.X, V.Z, CellX, CellY));
    end;
  Result:=False;
end;

function TGridMap.GetGridAndCellCoordsAt(const V: TVector; out CellX, CellY: Integer): TGrid;
var
  I: Integer;
begin
  for I:=0 to GridCount - 1 do
    if Grids[I].AABox.Includes(V) then begin
      Result:=Grids[I];
      if not Result.GetCellCoordsAt(V.X, V.Z, CellX, CellY) then Result:=nil;
      Exit;
    end;
  Result:=nil;
end;

procedure TGridMap.GetFloorAndCeilingHeightsAt(const V: TVector; out
  FloorHeight, CeilingHeight: Single);
var
  Grid: TGrid;
  I: Integer;
  GFloorHeight, GCeilingHeight: Single;
begin
  // Find the grid at the given position
  Grid:=GridAt(V);
  // Use extreme valus if the grid was not found
  if not Assigned(Grid) then begin
    FloorHeight:=-1000000;
    CeilingHeight:=1000000;
    Exit;
  end;
  // Get the floor and ceiling heights at the point
  Grid.GetFloorAndCeilingHeightsAt(V.X, V.Z, FloorHeight, CeilingHeight);
  // If these are not extreme heights, we're done
  if (FloorHeight > -999999) and (CeilingHeight < 999999) then Exit;
  // We've found a hole in the grid, check grids above and below the grid
  // that has the given point
  for I:=0 to GridCount - 1 do if Grids[I] <> Grid then with Grids[I] do
    if (V.X >= AABox.Min.X) and (V.Z >= AABox.Min.Z) and
       (V.X <= AABox.Max.X) and (V.Z <= AABox.Max.Z) then begin
      GetFloorAndCeilingHeightsAt(V.X, V.Z, GFloorHeight, GCeilingHeight);
      // If this grid's floor isn't above the current grid, consider it
      if GFloorHeight < Grid.AABox.Max.Y then
        FloorHeight:=Max(FloorHeight, GFloorHeight);
      // If this grid's ceiling isn't below the current grid, consider it
      if GCeilingHeight > Grid.AABox.Min.Y then
        CeilingHeight:=Min(CeilingHeight, GCeilingHeight);
    end;
end;

procedure TGridMap.GetFloorAndCeilingHeightForBoxCorners(const Box: TAABox; out
  FloorHeight, CeilingHeight: Single);
var
  FH, CH: Single;
begin
  GetFloorAndCeilingHeightsAt(Box.Min, FloorHeight, CeilingHeight);
  GetFloorAndCeilingHeightsAt(Box.Max, FH, CH);
  FloorHeight:=Max(FH, FloorHeight);
  CeilingHeight:=Min(CH, CeilingHeight);
  GetFloorAndCeilingHeightsAt(Vector(Box.Min.X, Box.Min.Y, Box.Max.Z), FH, CH);
  FloorHeight:=Max(FH, FloorHeight);
  CeilingHeight:=Min(CH, CeilingHeight);
  GetFloorAndCeilingHeightsAt(Vector(Box.Max.X, Box.Min.Y, Box.Min.Z), FH, CH);
  FloorHeight:=Max(FH, FloorHeight);
  CeilingHeight:=Min(CH, CeilingHeight);
end;

function TGridMap.CanBoxFit(const Box: TAABox): Boolean;
var
  FloorHeight, CeilingHeight: Single;
begin
  GetFloorAndCeilingHeightForBoxCorners(Box, FloorHeight, CeilingHeight);
  Result:=(Box.Min.Y >= FloorHeight) and (Box.Max.Y <= CeilingHeight);
end;

function TGridMap.LightAt(const P: TVector): Byte;
var
  Grid: TGrid;
  CX, CY: Integer;
begin
  Grid:=GridAt(P);
  if Assigned(Grid) then begin
    CX:=Clamp(Round((P.X - Grid.OriginX)/GridCellSize), 0, Grid.Width - 1);
    CY:=Clamp(Round((P.Z - Grid.OriginZ)/GridCellSize), 0, Grid.Height - 1);
    Result:=Grid.Cells[CX, CY]^.Light;
  end else
    Result:=20;
end;

function TGridMap.RayHit(const ARay: TRay; var Info: TGridMapRayHitInfo
  ): Boolean;
var
  CurDist, BestDist: Single;
  I: Integer;
  IP: TVector;

  procedure CheckGrid(Index: Integer; Grid: TGrid);
  var
    X, Y: Integer;
  begin
    with Grid do begin
      // Check if the ray hits the grid's bounding box
      if not ARay.AABoxHit(AABox) then Exit;
      // Check cells
      for Y:=0 to Height - 1 do
        for X:=0 to Width - 1 do with Cells[X, Y]^ do begin
          // Check lower part
          if ARay.AABoxHit(AABoxProper(
            OriginX + X*GridCellSize,
            AABox.Min.Y,
            OriginZ + Y*GridCellSize,
            OriginX + X*GridCellSize + GridCellSize,
            OriginY + FloorHeight/100,
            OriginZ + Y*GridCellSize + GridCellSize), IP) then begin
            // Check distance
            CurDist:=DistanceSq(IP, ARay.O);
            if CurDist < BestDist then begin
              if SmoothFloor or (Abs(IP.Y - (OriginY + FloorHeight/100)) < 0.01) then begin
                if Info.IgnoreHoles and (Floor=0) then Continue;
                Info.CellPart:=cpFloor;
              end else begin
                if Info.IgnoreHoles and (Lower=0) then Continue;
                Info.CellPart:=cpLower;
              end;
              BestDist:=CurDist;
              Info.Hit:=True;
              Info.Grid:=Grid;
              Info.GridIndex:=Index;
              Info.CellX:=X;
              Info.CellY:=Y;
              Info.IP:=IP;
            end;
          end
          // Check upper part
          else if ARay.AABoxHit(AABoxProper(
            OriginX + X*GridCellSize,
            OriginY + CellHeight - CeilingHeight/100,
            OriginZ + Y*GridCellSize,
            OriginX + X*GridCellSize + GridCellSize,
            AABox.Max.Y,
            OriginZ + Y*GridCellSize + GridCellSize), IP) then begin
            // Check distance
            CurDist:=DistanceSq(IP, ARay.O);
            if CurDist < BestDist then begin
              if Abs(IP.Y - (OriginY + CellHeight - CeilingHeight/100)) < 0.01 then begin
                if Info.IgnoreHoles and (Ceiling=0) then Continue;
                Info.CellPart:=cpCeiling;
              end else begin
                if Info.IgnoreHoles and (Upper=0) then Continue;
                Info.CellPart:=cpUpper;
              end;
              BestDist:=CurDist;
              Info.Hit:=True;
              Info.Grid:=Grid;
              Info.GridIndex:=Index;
              Info.CellX:=X;
              Info.CellY:=Y;
              Info.IP:=IP;
            end;
          end;
        end;
    end;
  end;

begin
  // Initialize
  Info.Hit:=False;
  BestDist:=$FFFFFFFF;
  // Check if there is a specific grid specified to check against
  if Assigned(Info.LimitGrid) then
    CheckGrid(IndexOfGrid(Info.LimitGrid), Info.LimitGrid)
  else begin
    // Check all grids
    for I:=0 to GridCount - 1 do
      CheckGrid(I, Grids[I]);
  end;
  // Done
  Result:=Info.Hit;
end;

function TGridMap.GetGrids(AIndex: Integer): TGrid;
begin
  Result:=FGrids[AIndex];
end;

initialization
  RegisterSerializableClass(TGrid);
  RegisterSerializableClass(TGridCollection);
  RegisterSerializableClass(TGridMap);
end.

