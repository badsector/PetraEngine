// Generic 3D math stuff
unit Maths;

interface

type
  { Pointers }
  PVector = ^TVector;

  { Axises }
  TAxis = (axX, axY, axZ);

  { TVector - A vector in 3D space (methods) }
  TVector = packed object
    // Vector coordinates
    X, Y, Z: Single;
    { Set all components to zero }
    procedure Zero; inline;
    { Copy the given vector to this one }
    procedure Copy(const Src: TVector); inline;
    { Normalize this vector }
    procedure Normalize; inline;
    { Return this vector normalized }
    function Normalized: TVector; inline;
    { Invert the vector }
    procedure Invert; inline;
    { Return this vector inverted }
    function Inverted: TVector; inline;
    { Return the squared length of this vector }
    function LengthSq: Single; inline;
    { Return the length of this vector }
    function Length: Single; inline;
    { Return the vector's major axis (the component with the largest
      absolute value) }
    function GetMajorAxis: TAxis; inline;
    { Return the dot product of this vector and the given vector }
    function Dot(const V: TVector): Single; inline;
    { Add the given vector to this vector }
    procedure Add(const V: TVector); inline;
    { Subtract the given vector from this vector }
    procedure Sub(const V: TVector); inline;
    { Return the given vector added to this vector }
    function Added(const V: TVector): TVector; inline;
    { Return the given vector subtracted from this vector }
    function Subbed(const V: TVector): TVector; inline;
    { Put the cross product between this vector and the given vector in r }
    procedure Cross(const V: TVector; out r: TVector); inline;
    { Return the cross product between this vector and the given vector }
    function Crossed(const V: TVector): TVector; inline;
    { Scale this vector by s }
    procedure Scale(S: Single); inline;
    { Return this vector scaled by s }
    function Scaled(S: Single): TVector; inline;
    { Multiply this vector by the given vector }
    procedure Multiply(const V: TVector); inline;
    { Return this vector multiplied by the given vector }
    function Multiplied(const V: TVector): TVector; inline;
    { Add the given vector scaled by s }
    procedure AddAndScale(const V: TVector; S: Single); inline;
    { Subtract the given vector scaled by s }
    procedure SubAndScale(const V: TVector; S: Single); inline;
  end;

  { TMatrix - A 4x4 matrix }
  TMatrix = packed object
    // Matrix components
    M11, M21, M31, M41,
    M12, M22, M32, M42,
    M13, M23, M33, M43,
    M14, M24, M34, M44: Single;

    { Make an identity matrix }
    procedure Identity;
    { Make a rotation matrix around the X axis by the given angle }
    procedure XRotation(Angle: Single);
    { Make a rotation matrix around the Y axis by the given angle }
    procedure YRotation(Angle: Single);
    { Make a rotation matrix around the Z axis by the given angle }
    procedure ZRotation(Angle: Single);
    { Make a rotation matrix around the axis specified by x,y,z by the given angle }
    procedure Rotation(X, Y, Z, Angle: Single);
    { Make a rotation matrix around the given axis by the given angle }
    procedure Rotation(const N: TVector; Angle: Single);
    { Make a rotation matrix around X, Y and Z axises by the given angles }
    procedure XYZRotation(Xa, Ya, Za: Single);
    { Make a rotation matrix around X, Y and Z axises by angles in vector }
    procedure XYZRotation(const A: TVector);
    { Make a translation matrix }
    procedure Translation(X, Y, Z: Single);
    { Make a uniform scaling matrix }
    procedure Scaling(S: Single);
    { Make a scaling matrix }
    procedure Scaling(X, Y, Z: Single);
    { Make a rotation matrix for looking towards a target }
    procedure LookAt(const Position, Target, Up: TVector);
    { Make a rotation matrix for the given direction }
    procedure Direction(const Dir: TVector);
    { Make a perspective projection matrix }
    procedure Perspective(Fov, Aspect, ZNear, ZFar: Single);
    { Make an orthographic projection matrix }
    procedure Orthographic(Left, Right, Top, Bottom, ZNear, ZFar: Single);

    { Transform the given vector }
    procedure Transform(var V: TVector);
    { Put the given vector transformed into r }
    procedure Transformed(const V: TVector; out R: TVector);
    { Transform the given vector while ignoring translation }
    procedure TransformWithoutTranslation(var V: TVector);
    { Put the given vector transformed into r while ignoring translation }
    procedure TransformedWithoutTranslation(const V: TVector; out R: TVector);
    { Return the given vector transformed }
    function Transformed(const V: TVector): TVector;
    { Return the given vector transformed while ignoring translation }
    function TransformedWithoutTranslation(const V: TVector): TVector;
    { Transform the given vector for projection }
    procedure TransformProj(var V: TVector);
    { Put the given vector transformed for projection into r }
    procedure TransformedProj(const V: TVector; out R: TVector);
    { Return the given vector transformed for projection }
    function TransformedProj(const V: TVector): TVector;

    { Multiply this matrix with the given matrix }
    procedure Multiply(const Src: TMatrix);
    { Return this matrix multiplied with the given matrix }
    function Multiplied(const Src: TMatrix): TMatrix;
    { Multiply this matrix with the given matrix in a reversed order }
    procedure SwapMultiply(const Src: TMatrix);
    { Return this matrix multiplied with the given matrix in a reversed order }
    function SwapMultiplied(const Src: TMatrix): TMatrix;

    { Return the determinant of this matrix }
    function Determinant: Single;
    { Invert this matrix }
    procedure Invert;
    { Return this matrix inverted }
    function Inverted: TMatrix;

    { Transpose the matrix }
    procedure Transpose;
    { Return this matrix transposed }
    function Transposed: TMatrix;

    { Normalize the axis vectors of this matrix }
    procedure NormalizeAxes;

    { Return the X axis vector }
    function XAxis: TVector; inline;
    { Return the Y axis vector }
    function YAxis: TVector; inline;
    { Return the Z axis vector }
    function ZAxis: TVector; inline;
    { Return the translation part }
    function Translation: TVector; inline;
    { Return the position this matrix represents (inverted Translation) }
    function Position: TVector; inline;

    { Set the X axis vector to the given vector and w component }
    procedure SetXAxis(const V: TVector; W: Single=0); inline;
    { Set the X axis vector to the given x,y,z,w values }
    procedure SetXAxis(X, Y, Z: Single; W: Single=0); inline;
    { Set the Y axis vector to the given vector and w component }
    procedure SetYAxis(const V: TVector; W: Single=0); inline;
    { Set the Y axis vector to the given x,y,z,w values }
    procedure SetYAxis(X, Y, Z: Single; W: Single=0); inline;
    { Set the Z axis vector to the given vector and w component }
    procedure SetZAxis(const V: TVector; W: Single=0); inline;
    { Set the Z axis vector to the given x,y,z,w values }
    procedure SetZAxis(X, Y, Z: Single; W: Single=0); inline;
    { Set the translation to the given vector and w component }
    procedure SetTranslation(const V: TVector; W: Single=1); inline;
    { Set the translation to the given x,y,z,w values }
    procedure SetTranslation(X, Y, Z: Single; W: Single=1); inline;
    { Set all the axes, translation and W components }
    procedure SetAxes(X, Y, Z, W: TVector; XW: Single=0; YW: Single=0; ZW: Single=0; WW: Single=1); inline;
    { Set all the axes and W components (leaves translation intact) }
    procedure SetAxes(X, Y, Z: TVector; XW: Single=0; YW: Single=0; ZW: Single=0); inline;
    { Remove the translation (set it to 0,0,0,1) }
    procedure RemoveTranslation; inline;
  end;

  { TPlane - A plane represented by a normal and distance from the origin }
  TPlane = object
    N: TVector;
    D: Single;
    { Construct the plane from the given three points }
    procedure FromThreePoints(const A, B, C: TVector); inline;
    { Construct the ray from the given point and normal }
    procedure FromPointAndNormal(const P, Normal: TVector); inline;
    { Invert the plane }
    procedure Invert; inline;
    { Return the signed distance the point is from the plane }
    function SignDist(const P: TVector): Single; inline;
    { Return true if the given point is in front of the plane }
    function Front(const P: TVector): Boolean; inline;
    { Project the given point on the plane }
    procedure Project(var P: TVector); inline;
    { Return the given point projected on the plane }
    function Projected(const P: TVector): TVector; inline;
    { Return the given normal projected on the plane (ignores distance) }
    function ProjectedNormal(const P: TVector): TVector; inline;
    { Flip the given vector along the plane }
    procedure Flip(var P: TVector); inline;
    { Return the given vector flipped along the plane }
    function Flipped(const P: TVector): TVector; inline;
    { Return true if the given segment intersects the plane, endpoints included }
    function Intersect(const A, B: TVector; out P: TVector): Boolean;
    { Return true if the given segment intersects the plane, endpoints excluded }
    function IntersectNEP(const A, B: TVector; out P: TVector): Boolean;
    { Normalize the plane }
    procedure Normalize;
  end;

  { TAABox - Represents an axis-aligned bounding box}
  TAABox = object
  private
    function GetExtent: TVector; inline;
    function GetHeight: Single; inline;
    function GetLength: Single; inline;
    function GetWidth: Single; inline;
    procedure SetExtent(AValue: TVector); inline;
    procedure SetHeight(AValue: Single); inline;
    procedure SetLength(AValue: Single); inline;
    procedure SetWidth(AValue: Single); inline;
  public
    // Extents
    Min, Max: TVector;

    { Zero out the box }
    procedure Zero;
    { Make the box an invalid box where Max < Min }
    procedure MakeMaxInverted; inline;
    { Returns true if the box is valid }
    function IsValid: Boolean; inline;
    { Expand the box to include the given box }
    procedure Include(const Box: TAABox); inline;
    { Expand the box to include the given point }
    procedure Include(const V: TVector); inline;
    { Ensure Min < Max }
    procedure MakeProper; inline;
    { Return true if the given point is inside the box }
    function Includes(const V: TVector): Boolean; inline;
    { Return true if the given box is fully inside this box }
    function Includes(const Box: TAABox): Boolean; inline;
    { Returns true if the given box is fully or partially inside this box }
    function Overlaps(const Box: TAABox): Boolean; inline;
    { Grow the box by the given units }
    procedure Grow(V: Single); inline;
    { Grow the box by the given axises }
    procedure Grow(const V: TVector); inline;
    { Return the box grow the box by the given units }
    function Grown(V: Single): TAABox; inline;
    { Return the box grow the box by the given axises }
    function Grown(const V: TVector): TAABox; inline;
    { Move the box to the given point }
    procedure MoveTo(const V: TVector); inline;
    { Move the box by the given delta }
    procedure MoveBy(const Delta: TVector); inline;
    { Return the box's bounding sphere }
    procedure GetBoundingSphere(out Center: TVector; out Radius: Single); inline;
    { Return the box's corner points:                      }
    {  maxA------maxB                   ^                  }
    {    |\      |\                     | y-axis           }
    {    | \     | \      minA=a    |\  |                  }
    {    |maxC----maxD    maxD=b  z-axis|                  }
    {  minA |---minB|                  \|                  }
    {     \ |     \ |                   *---------> x-axis }
    {      \|      \|       (z towards the back)           }
    {     minC----minD                                     }
    procedure GetCorners(out MinA, MinB, MinC, MinD, MaxA, MaxB, MaxC, MaxD: TVector); inline;
    { Return the squared distance to the given point (return 0 if the point is }
    { inside the box)                                                          }
    function DistanceSq(V: TVector): Single; inline;
    { Return the distance to the given point or 0 if the point is inside the box }
    function Distance(V: TVector): Single; inline;
    { The box's width }
    property Width: Single read GetWidth write SetWidth; // X size
    { The box's height }
    property Height: Single read GetHeight write SetHeight; // Y size
    { The box's length }
    property Length: Single read GetLength write SetLength; // Z size
    { The box's extent }
    property Extent: TVector read GetExtent write SetExtent;
    { Calculate the box's center }
    function Center: TVector; inline;
  end;

  { TRay - A ray represented by origin and direction }
  TRay = object
    O, D: TVector;
    { Construct the ray from the segment defined by the given two endpoints }
    procedure FromSegment(const A, B: TVector); inline;
    { Check if the ray hits a sphere at the given center and radius }
    function SphereHit(const SC: TVector; SR: Single; out IP: TVector): Boolean; inline;
    { Check if the ray hits the given plane }
    function PlaneHit(const Plane: TPlane; out IP: TVector): Boolean; inline;
    { Check if the ray hits a triangle defined by the given vertices }
    function TriangleHit(const A, B, C: TVector; out IP: TVector): Boolean; inline;
    { Check if the ray hits a triangle defined by the given vertices }
    function TriangleHit(const A, AB, AC: TVector; const P: TPlane; UU, UV, VV, DD: Single; out ip: TVector): Boolean; inline;
    { Check if the ray hits the given axis aligned box }
    function AABoxHit(const AABox: TAABox): Boolean; inline;
    { Check if the ray hits the given axis aligned box }
    function AABoxHit(const AABox: TAABox; out T: Single): Boolean; inline;
    { Check if the ray hits the given axis aligned box }
    function AABoxHit(const AABox: TAABox; out IP: TVector): Boolean; inline;
  end;

  { TTransform - Represents a transformation made by the combination of translation and rotation }
  TTransform = object
  public
    // Transform components
    Translation: TVector;
    Rotation: TVector;
    { Reset the transformation }
    procedure Reset;
    { Convert the given matrix to a transformation }
    procedure FromMatrix(AMatrix: TMatrix);
    { Convert the transformation to a matrix }
    function ToMatrix: TMatrix;
  end;

var
  Origin: TVector;      // Global (0,0,0) vector
  Identity: TMatrix;    // Global identity matrix

// Create a vector
function Vector(X, Y, Z: Single): TVector; inline;
// Create a plane with the given normal an distance
function Plane(const N: TVector; D: Single): TPlane; inline;
// Create a plane with the given normal an distance
function Plane(NX, NY, NZ, D: Single): TPlane; inline;
// Create a plane using the given point and normal
function Plane(const P, N: TVector): TPlane; inline;
// Create a plane using the given three points
function Plane(const A, B, C: TVector): TPlane; inline;
// Create an axis aligned box
function AABox(const Min, Max: TVector): TAABox; inline;
// Create an axis aligned box
function AABox(MinX, MinY, MinZ, MaxX, MaxY, MaxZ: Single): TAABox; inline;
// Create an axis aligned box and ensure that Min < Max
function AABoxProper(MinX, MinY, MinZ, MaxX, MaxY, MaxZ: Single): TAABox; inline;
// Create a transform with the given translation and rotation
function Transform(const ATranslation, ARotation: TVector): TTransform;
// Return the direction from Source to Destination
function Direction(const Source, Destination: TVector): TVector; inline;
// Return the distance between a and b
function Distance(const A, B: TVector): Single; inline;
// Return the squared distance between a and b
function DistanceSq(const A, B: TVector): Single; inline;
// Return the midpoint between a and b
function MidpointBetween(const A, B: TVector): TVector; inline;
// Return the normal vector between a and b (cross product normalized)
function NormalBetween(const A, B: TVector): TVector; inline;
// Return the reflection of v on a plane with normal n
function Reflection(const V, N: TVector): TVector; inline;
// Return the inner angle between the given two normals
function InnerAngleBetweenNormals(const A, B: TVector): Single; inline;
// Return the angle between two coplanar vectors
function AngleBetweenVectorsOnPlane(const A, B, PN: TVector): Single; inline;
// Return the normal of the triangle defined by the given three vectors
function TriangleNormal(const A, B, C: TVector): TVector; inline;
// Return the interpolation of P from A to B (that is, plugging the returned
// value in Interpolate with A and B will give P)
function Interpolation(const A, B, P: TVector): Single; inline;
// Interpolate the given vectors so that A*(1-Int) + B*Int
function Interpolate(const A, B: TVector; Int: Single): TVector; inline;
// Linearly interpolate the given transform
function Interpolate(const A, B: TTransform; Int: Single): TTransform; inline;
// Interpolate the given numbers so that A*(1-Int) + B*Int
function Interpolate(const A, B, Int: Single): Single; inline;
// Interpolate the given Catmull-Rom spline numbers
function CatmullRomInterpolate(const A, B, C, D, Int: Single): Single; inline;
// Interpolate the given Catmull-Rom spline vectors
function CatmullRomInterpolate(const A, B, C, D: TVector; Int: Single): TVector; inline;
// Calculate the aabox of the given aabox transformed by the given matrix
function AABoxOfTransformedAABox(const Box: TAABox; const M: TMatrix): TAABox; inline;
// Return the modulo of a by b
function ExtMod(A, B: Single): Single; inline;
// Return the maximum value between a and b
function Max(A, B: Single): Single; inline;
// Return the maximum value between a, b and c
function Max(A, B, C: Single): Single; inline;
// Return the minimum value between a and b
function Min(A, B: Single): Single; inline;
// Return the minimum value between a, b and c
function Min(A, B, C: Single): Single; inline;
// Return the maximum value between a and b
function Max(A, B: Integer): Integer; inline;
// Return the maximum value between a, b and c
function Max(A, B, C: Integer): Integer; inline;
// Return the minimum value between a and b
function Min(A, B: Integer): Integer; inline;
// Return the minimum value between a, b and c
function Min(A, B, C: Integer): Integer; inline;
// Clamp the given value between Min and Max
function Clamp(V: Single; Min: Single=0; Max: Single=1): Single; inline;
// Clamp the given value between Min and Max
function Clamp(V: Integer; Min: Integer=0; Max: Integer=MaxInt): Integer; inline;
// Return -1 if v < 0, 1 if v > 0 or 0 if v = 0
function Sign(V: Single): Single; inline;
// Snap the given angle to an axis aligned one
function AxisAlignedAngleSnap(A: Single): Single;

// Vector comparison operator
operator=(const A, B: TVector): Boolean; inline;
// Plane comparison operator
operator=(const A, B: TPlane): Boolean; inline;
// Transform comparison operator
operator=(const A, B: TTransform): Boolean; inline;

implementation

uses
  Math;

operator=(const A, B: TVector): Boolean;
begin
  Result:=(A.X=B.X) and (A.Y=B.Y) and (A.Z=B.Z);
end;

operator=(const A, B: TPlane): Boolean;
begin
  Result:=(A.N=B.N) and (A.D=B.D);
end;

operator=(const A, B: TTransform): Boolean;
begin
  Result:=(A.Translation=B.Translation) and
          (A.Rotation=B.Rotation);
end;

{ Functions }
function Vector(X, Y, Z: Single): TVector;
begin
  Result.X:=X;
  Result.Y:=Y;
  Result.Z:=Z;
end;

function Plane(const N: TVector; D: Single): TPlane;
begin
  Result.N:=N;
  Result.D:=D;
end;

function Plane(NX, NY, NZ, D: Single): TPlane;
begin
  Result.N.X:=NX;
  Result.N.Y:=NY;
  Result.N.Z:=NZ;
  Result.D:=D;
end;

function Plane(const P, N: TVector): TPlane;
begin
  Result.FromPointAndNormal(P, N);
end;

function Plane(const A, B, C: TVector): TPlane;
begin
  Result.FromThreePoints(A, B, C);
end;

function AABox(const Min, Max: TVector): TAABox;
begin
  Result.Min:=Min;
  Result.Max:=Max;
end;

function AABox(MinX, MinY, MinZ, MaxX, MaxY, MaxZ: Single): TAABox;
begin
  Result.Min:=Vector(MinX, MinY, MinZ);
  Result.Max:=Vector(MaxX, MaxY, MaxZ);
end;

function AABoxProper(MinX, MinY, MinZ, MaxX, MaxY, MaxZ: Single): TAABox;
begin
  Result.Min:=Vector(MinX, MinY, MinZ);
  Result.Max:=Vector(MaxX, MaxY, MaxZ);
  Result.MakeProper;
end;

function Transform(const ATranslation, ARotation: TVector): TTransform;
begin
  with Result do begin
    Translation:=ATranslation;
    Rotation:=ARotation;
  end;
end;

function Direction(const Source, Destination: TVector): TVector;
begin
  Result.X:=Destination.X - Source.X;
  Result.Y:=Destination.Y - Source.Y;
  Result.Z:=Destination.Z - Source.Z;
  Result.Normalize;
end;

function Distance(const A, B: TVector): Single;
begin
  Result:=sqrt(sqr(A.X - b.X) + sqr(A.Y - b.Y) + sqr(A.Z - b.Z));
end;

function DistanceSq(const A, B: TVector): Single;
begin
  Result:=sqr(A.X - b.X) + sqr(A.Y - b.Y) + sqr(A.Z - b.Z);
end;

function MidpointBetween(const A, B: TVector): TVector;
begin
  Result:=A.Added(b.Subbed(A).Scaled(0.5));
end;

function NormalBetween(const A, B: TVector): TVector;
begin
  A.Cross(b, Result);
  Result.Normalize;
end;

function Reflection(const V, N: TVector): TVector;
var
  d: Single;
begin
  d:=V.Dot(N);
  Result.X:=V.X - 2*d*N.X;
  Result.Y:=V.Y - 2*d*N.Y;
  Result.Z:=V.Z - 2*d*N.Z;
end;

function InnerAngleBetweenNormals(const A, B: TVector): Single;
begin
  Result:=arccos(A.Dot(b));
end;

function AngleBetweenVectorsOnPlane(const A, B, PN: TVector): Single;
begin
  Result:=arctan2(
    // Determinant
    A.X*b.Y*PN.Z + A.Z*b.X*PN.Y + A.Y*b.Z*PN.X - A.Z*b.Y*PN.X - A.X*b.Z*PN.Y - A.Y*b.X*PN.Z,
    // Dot product
    A.Dot(b));
end;

function TriangleNormal(const A, B, C: TVector): TVector;
var
  ab, ac: TVector;
begin
  ab:=b;
  ab.Sub(A);
  ac:=C;
  ac.Sub(A);
  Result:=NormalBetween(ab, ac);
end;

function Interpolation(const A, B, P: TVector): Single;
begin
  Result:=Distance(A, P)/Distance(A, B);
end;

function Interpolate(const A, B: TVector; Int: Single): TVector;
begin
  Result.X:=A.X + (B.X - A.X)*Int;
  Result.Y:=A.Y + (B.Y - A.Y)*Int;
  Result.Z:=A.Z + (B.Z - A.Z)*Int;
end;

function Interpolate(const A, B: TTransform; Int: Single): TTransform;
begin
  Result.Translation:=Interpolate(A.Translation, B.Translation, Int);
  Result.Rotation:=Interpolate(A.Rotation, B.Rotation, Int);
end;

function Interpolate(const A, B, Int: Single): Single;
begin
  Result:=A + (B - A)*Int;
end;

function CatmullRomInterpolate(const A, B, C, D, Int: Single): Single;
var
  A0, A1, A2, Int2: Single;
begin
  Int2:=Int*Int;
  A0:=-0.5*A + 1.5*B - 1.5*C + 0.5*D;
  A1:=A - 2.5*B + 2*C - 0.5*D;
  A2:=-0.5*A + 0.5*C;
  Result:=A0*Int*Int2+A1*Int2+A2*Int+B;
end;

function CatmullRomInterpolate(const A, B, C, D: TVector; Int: Single): TVector;
begin
  Result.X:=CatmullRomInterpolate(A.X, B.X, C.X, D.X, Int);
  Result.Y:=CatmullRomInterpolate(A.Y, B.Y, C.Y, D.Y, Int);
  Result.Z:=CatmullRomInterpolate(A.Z, B.Z, C.Z, D.Z, Int);
end;

function AABoxOfTransformedAABox(const Box: TAABox; const M: TMatrix): TAABox;
var
  A, B, C, D, E, F, G: TVector;
begin
  Box.GetCorners(A, B, C, D, E, F, G, Result.Min);
  Result.Min:=M.Transformed(Result.Min);
  Result.Max:=Result.Min;
  Result.Include(M.Transformed(A));
  Result.Include(M.Transformed(B));
  Result.Include(M.Transformed(C));
  Result.Include(M.Transformed(D));
  Result.Include(M.Transformed(E));
  Result.Include(M.Transformed(F));
  Result.Include(M.Transformed(G));
end;

function ExtMod(A, B: Single): Single;
begin
  Result:=A - Round(A/B)*B;
end;

function Max(A, B: Single): Single;
begin
  if A > B then Result:=A else Result:=B;
end;

function Max(A, B, C: Single): Single;
begin
  Result:=Max(Max(A, B), Max(B, C));
end;

function Min(A, B: Single): Single;
begin
  if A < B then Result:=A else Result:=B;
end;

function Min(A, B, C: Single): Single;
begin
  Result:=Min(Min(A, B), Min(B, C));
end;

function Max(A, B: Integer): Integer;
begin
  if A > B then Result:=A else Result:=B;
end;

function Max(A, B, C: Integer): Integer;
begin
  Result:=Max(Max(A, B), Max(B, C));
end;

function Min(A, B: Integer): Integer;
begin
  if A < B then Result:=A else Result:=B;
end;

function Min(A, B, C: Integer): Integer;
begin
  Result:=Min(Min(A, B), Min(B, C));
end;

function Sign(V: Single): Single;
begin
  if V > 0 then Sign:=1 else
  if V < 0 then Sign:=-1 else
  Sign:=0;
end;

function AxisAlignedAngleSnap(A: Single): Single;
var
  I: Integer;
begin
  I:=Round(A*180/PI);
  I:=I mod 360;
  if A < 0 then I += 360;
  I:=(I + 45) div 90 * 90;
  Result:=I*PI/180;
end;

function Clamp(v, Min, Max: Integer): Integer;
begin
  if v < Min then v:=Min;
  if v > Max then v:=Max;
  Result:=v;
end;

function Clamp(v, Min, Max: Single): Single;
begin
  if v < Min then v:=Min;
  if v > Max then v:=Max;
  Result:=v;
end;


{ TVector }
procedure TVector.Zero;
begin
  X:=0;
  Y:=0;
  Z:=0;
end;

procedure TVector.Copy(const Src: TVector);
begin
  X:=Src.X;
  Y:=Src.Y;
  Z:=Src.Z;
end;

procedure TVector.Normalize;
var
  l: Single;
begin
  l:=Length;
  if l > 0.0 then begin
    X:=X/l;
    Y:=Y/l;
    Z:=Z/l;
  end;
end;

function TVector.Normalized: TVector;
begin
  Result:=Self;
  Result.Normalize;
end;

procedure TVector.Invert;
begin
  X:=-X;
  Y:=-Y;
  Z:=-Z;
end;

function TVector.Inverted: TVector;
begin
  Result:=Vector(-X, -Y, -Z);
end;

function TVector.LengthSq: Single;
begin
  Result:=X*X + Y*Y +Z*Z;
end;

function TVector.Length: Single;
begin
  Result:=Sqrt(X*X + Y*Y +Z*Z);
end;

function TVector.GetMajorAxis: TAxis;
var
  ax, ay, az: Single;
begin
  ax:=Abs(X);
  ay:=Abs(Y);
  az:=Abs(Z);
  if ax > ay then begin
    if ax > az then Exit(axX) else Exit(axZ);
  end else begin
    if ay > az then Exit(axY) else Exit(axZ);
  end;
end;

function TVector.Dot(const V: TVector): Single;
begin
  Result:=X*V.X + Y*V.Y + Z*V.Z;
end;

procedure TVector.Add(const V: TVector);
begin
  X += V.X;
  Y += V.Y;
  Z += V.Z;
end;

procedure TVector.Sub(const V: TVector);
begin
  X -= V.X;
  Y -= V.Y;
  Z -= V.Z;
end;

function TVector.Added(const V: TVector): TVector;
begin
  Result.X:=X + V.X;
  Result.Y:=Y + V.Y;
  Result.Z:=Z + V.Z;
end;

function TVector.Subbed(const V: TVector): TVector;
begin
  Result.X:=X - V.X;
  Result.Y:=Y - V.Y;
  Result.Z:=Z - V.Z;
end;

procedure TVector.Cross(const V: TVector; out r: TVector);
begin
  r.X:=Y*V.Z - Z*V.Y;
  r.Y:=Z*V.X - X*V.Z;
  r.Z:=X*V.Y - Y*V.X;
end;

function TVector.Crossed(const V: TVector): TVector;
begin
  Result.X:=Y*V.Z - Z*V.Y;
  Result.Y:=Z*V.X - X*V.Z;
  Result.Z:=X*V.Y - Y*V.X;
end;

procedure TVector.Scale(S: Single);
begin
  X:=X*S;
  Y:=Y*S;
  Z:=Z*S;
end;

function TVector.Scaled(S: Single): TVector;
begin
  Result.X:=X*S;
  Result.Y:=Y*S;
  Result.Z:=Z*S;
end;

procedure TVector.Multiply(const V: TVector);
begin
  X:=X*V.X;
  Y:=Y*V.Y;
  Z:=Z*V.Z;
end;

function TVector.Multiplied(const V: TVector): TVector;
begin
  Result.X:=X*V.X;
  Result.Y:=Y*V.Y;
  Result.Z:=Z*V.Z;
end;

procedure TVector.AddAndScale(const V: TVector; S: Single);
begin
  X:=X + V.X*S;
  Y:=Y + V.Y*S;
  Z:=Z + V.Z*S;
end;

procedure TVector.SubAndScale(const V: TVector; S: Single);
begin
  X:=X - V.X*S;
  Y:=Y - V.Y*S;
  Z:=Z - V.Z*S;
end;

{ TMatrix }
procedure TMatrix.Identity;
begin
  FillChar(Self, SizeOf(TMatrix), 0);
  M11:=1.0;
  M22:=1.0;
  M33:=1.0;
  M44:=1.0;
end;

procedure TMatrix.XRotation(Angle: Single);
begin
  Rotation(1, 0, 0, Angle);
end;

procedure TMatrix.YRotation(Angle: Single);
begin
  Rotation(0, 1, 0, Angle);
end;

procedure TMatrix.ZRotation(Angle: Single);
begin
  Rotation(0, 0, 1, Angle);
end;

procedure TMatrix.Rotation(X, Y, Z, Angle: Single);
var
  t, s, c: Single;
begin
  s:=sin(Angle);
  c:=cos(Angle);
  t:=1-c;
  M11:=t*X*X + c;
  M12:=t*X*Y - Z*s;
  M13:=t*X*Z + Y*s;
  M14:=0;
  M21:=t*X*Y + Z*s;
  M22:=t*Y*Y + c;
  M23:=t*Y*Z - X*s;
  M24:=0;
  M31:=t*X*Z - Y*s;
  M32:=t*Y*Z + X*s;
  M33:=t*Z*Z + c;
  M34:=0;
  M41:=0;
  M42:=0;
  M43:=0;
  M44:=1;
end;

procedure TMatrix.Rotation(const N: TVector; Angle: Single);
begin
  Rotation(N.X, N.Y, N.Z, Angle);
end;

procedure TMatrix.XYZRotation(Xa, Ya, Za: Single);
var
  Tmp: TMatrix;
begin
  Identity;
  Tmp.XRotation(Xa);
  Multiply(Tmp);
  Tmp.YRotation(Ya);
  Multiply(Tmp);
  Tmp.ZRotation(Za);
  Multiply(Tmp);
end;

procedure TMatrix.XYZRotation(const A: TVector);
var
  Tmp: TMatrix;
begin
  Identity;
  Tmp.XRotation(A.X);
  Multiply(Tmp);
  Tmp.YRotation(A.Y);
  Multiply(Tmp);
  Tmp.ZRotation(A.Z);
  Multiply(Tmp);
end;

procedure TMatrix.Translation(X, Y, Z: Single);
begin
  FillChar(Self, SizeOf(TMatrix), 0);
  M11:=1.0;
  M22:=1.0;
  M33:=1.0;
  M44:=1.0;
  M14:=X;
  M24:=Y;
  M34:=Z;
end;

procedure TMatrix.Scaling(S: Single);
begin
  FillChar(Self, SizeOf(TMatrix), 0);
  M11:=S;
  M22:=S;
  M33:=S;
  M44:=1.0;
end;

procedure TMatrix.Scaling(X, Y, Z: Single);
begin
  FillChar(Self, SizeOf(TMatrix), 0);
  M11:=X;
  M22:=Y;
  M33:=Z;
  M44:=1.0;
end;

procedure TMatrix.LookAt(const Position, Target, Up: TVector);
var
  F, S, U: TVector;
  Tmp: TMatrix;
begin
  F:=Target.Subbed(Position).Normalized;
  S:=F.Crossed(Up).Normalized;
  U:=S.Crossed(F);
  M11:=S.X;
  M12:=S.Y;
  M13:=S.Z;
  M14:=0;
  M21:=U.X;
  M22:=U.Y;
  M23:=U.Z;
  M24:=0;
  M31:=-F.X;
  M32:=-F.Y;
  M33:=-F.Z;
  M34:=0;
  M41:=0;
  M42:=0;
  M43:=0;
  M44:=1.0;
  Tmp.Translation(-Position.X, -Position.Y, -Position.Z);
  Multiply(Tmp);
end;

procedure TMatrix.Direction(const Dir: TVector);
var
  U, XNormal, YNormal: TVector;
begin
  if Abs(Dir.Y) > 0.8 then begin
    U:=Vector(0, 0, -1);
    XNormal:=U.Crossed(Dir);
    YNormal:=Dir.Crossed(XNormal);
  end else begin
    U:=Vector(0, 1, 0);
    XNormal:=U.Crossed(Dir);
    YNormal:=Dir.Crossed(XNormal);
  end;
  M11:=XNormal.X; M21:=XNormal.Y; M31:=XNormal.Z; M41:=0;
  M12:=YNormal.X; M22:=YNormal.Y; M32:=YNormal.Z; M42:=0;
  M13:=Dir.X; M23:=Dir.Y; M33:=Dir.Z; M43:=0;
  M14:=0; M24:=0; M34:=0; M44:=1;
end;

procedure TMatrix.Perspective(Fov, Aspect, ZNear, ZFar: Single);
var
  F: Single;
begin
  F:=1.0/Tan(Fov/2.0);
  Identity;
  M44:=0;
  M11:=F/Aspect;
  M22:=F;
  M33:=(ZFar + ZNear)/(ZNear - ZFar);
  M34:=(2.0*ZFar*ZNear)/(ZNear - ZFar);
  M43:=-1.0;
end;

procedure TMatrix.Orthographic(Left, Right, Top, Bottom, ZNear, ZFar: Single);
begin
  Identity;
  M11:=2/(Right - Left);
  M22:=2/(Top - Bottom);
  M33:=-2/(ZFar - ZNear);
  M14:=-(Right + Left)/(Right - Left);
  M24:=-(Top + Bottom)/(Top - Bottom);
  M34:=-(ZFar + ZNear)/(ZFar - ZNear);
end;

procedure TMatrix.Transform(var V: TVector);
var
  nx, ny: Single;
begin
  nx:=V.X*M11 + V.Y*M12 + V.Z*M13 + M14;
  ny:=V.X*M21 + V.Y*M22 + V.Z*M23 + M24;
  V.Z:=V.X*M31 + V.Y*M32 + V.Z*M33 + M34;
  V.X:=nx;
  V.Y:=ny;
end;

procedure TMatrix.Transformed(const V: TVector; out R: TVector);
begin
  R.X:=V.X;
  R.Y:=V.Y;
  R.Z:=V.Z;
  Transform(R);
end;

procedure TMatrix.TransformWithoutTranslation(var V: TVector);
var
  nx, ny: Single;
begin
  nx:=V.X*M11 + V.Y*M12 + V.Z*M13;
  ny:=V.X*M21 + V.Y*M22 + V.Z*M23;
  V.Z:=V.X*M31 + V.Y*M32 + V.Z*M33;
  V.X:=nx;
  V.Y:=ny;
end;

procedure TMatrix.TransformedWithoutTranslation(const V: TVector; out R: TVector);
begin
  R.X:=V.X;
  R.Y:=V.Y;
  R.Z:=V.Z;
  Transform(R);
end;

function TMatrix.Transformed(const V: TVector): TVector;
begin
  Result.X:=V.X*M11 + V.Y*M12 + V.Z*M13 + M14;
  Result.Y:=V.X*M21 + V.Y*M22 + V.Z*M23 + M24;
  Result.Z:=V.X*M31 + V.Y*M32 + V.Z*M33 + M34;
end;

function TMatrix.TransformedWithoutTranslation(const V: TVector): TVector;
begin
  Result.X:=V.X*M11 + V.Y*M12 + V.Z*M13;
  Result.Y:=V.X*M21 + V.Y*M22 + V.Z*M23;
  Result.Z:=V.X*M31 + V.Y*M32 + V.Z*M33;
end;

procedure TMatrix.TransformProj(var V: TVector);
var
  nx, ny, nw: Single;
begin
  nx:=V.X*M11 + V.Y*M12 + V.Z*M13 + M14;
  ny:=V.X*M21 + V.Y*M22 + V.Z*M23 + M24;
  nw:=1/(V.X*M41 + V.Y*M42 + V.Z*M43 + M44);
  V.Z:=(V.X*M31 + V.Y*M32 + V.Z*M33 + M34)*nw;
  V.X:=nx*nw;
  V.Y:=ny*nw;
end;

procedure TMatrix.TransformedProj(const V: TVector; out R: TVector);
begin
  R.X:=V.X;
  R.Y:=V.Y;
  R.Z:=V.Z;
  TransformProj(R);
end;

function TMatrix.TransformedProj(const V: TVector): TVector;
var
  nx, ny, nz, nw: Single;
begin
  nx:=V.X*M11 + V.Y*M12 + V.Z*M13 + M14;
  ny:=V.X*M21 + V.Y*M22 + V.Z*M23 + M24;
  nz:=V.X*M31 + V.Y*M32 + V.Z*M33 + M34;
  nw:=V.X*M41 + V.Y*M42 + V.Z*M43 + M44;
  {if nw=0 then begin
    Result.X:=0;
    Result.Y:=0;
    Result.Z:=0;
  end else begin}
    Result.X:=nx/nw;
    Result.Y:=ny/nw;
    Result.Z:=nz/nw;
  //end;
end;

procedure TMatrix.Multiply(const Src: TMatrix);
var
  Tmp: TMatrix;
begin
  Tmp.M11:=M11*Src.M11 + M12*Src.M21 + M13*Src.M31 + M14*Src.M41;
  Tmp.M12:=M11*Src.M12 + M12*Src.M22 + M13*Src.M32 + M14*Src.M42;
  Tmp.M13:=M11*Src.M13 + M12*Src.M23 + M13*Src.M33 + M14*Src.M43;
  Tmp.M14:=M11*Src.M14 + M12*Src.M24 + M13*Src.M34 + M14*Src.M44;
  Tmp.M21:=M21*Src.M11 + M22*Src.M21 + M23*Src.M31 + M24*Src.M41;
  Tmp.M22:=M21*Src.M12 + M22*Src.M22 + M23*Src.M32 + M24*Src.M42;
  Tmp.M23:=M21*Src.M13 + M22*Src.M23 + M23*Src.M33 + M24*Src.M43;
  Tmp.M24:=M21*Src.M14 + M22*Src.M24 + M23*Src.M34 + M24*Src.M44;
  Tmp.M31:=M31*Src.M11 + M32*Src.M21 + M33*Src.M31 + M34*Src.M41;
  Tmp.M32:=M31*Src.M12 + M32*Src.M22 + M33*Src.M32 + M34*Src.M42;
  Tmp.M33:=M31*Src.M13 + M32*Src.M23 + M33*Src.M33 + M34*Src.M43;
  Tmp.M34:=M31*Src.M14 + M32*Src.M24 + M33*Src.M34 + M34*Src.M44;
  Tmp.M41:=M41*Src.M11 + M42*Src.M21 + M43*Src.M31 + M44*Src.M41;
  Tmp.M42:=M41*Src.M12 + M42*Src.M22 + M43*Src.M32 + M44*Src.M42;
  Tmp.M43:=M41*Src.M13 + M42*Src.M23 + M43*Src.M33 + M44*Src.M43;
  Tmp.M44:=M41*Src.M14 + M42*Src.M24 + M43*Src.M34 + M44*Src.M44;
  Self:=Tmp;
end;

function TMatrix.Multiplied(const Src: TMatrix): TMatrix;
begin
  Result.M11:=M11*Src.M11 + M12*Src.M21 + M13*Src.M31 + M14*Src.M41;
  Result.M12:=M11*Src.M12 + M12*Src.M22 + M13*Src.M32 + M14*Src.M42;
  Result.M13:=M11*Src.M13 + M12*Src.M23 + M13*Src.M33 + M14*Src.M43;
  Result.M14:=M11*Src.M14 + M12*Src.M24 + M13*Src.M34 + M14*Src.M44;
  Result.M21:=M21*Src.M11 + M22*Src.M21 + M23*Src.M31 + M24*Src.M41;
  Result.M22:=M21*Src.M12 + M22*Src.M22 + M23*Src.M32 + M24*Src.M42;
  Result.M23:=M21*Src.M13 + M22*Src.M23 + M23*Src.M33 + M24*Src.M43;
  Result.M24:=M21*Src.M14 + M22*Src.M24 + M23*Src.M34 + M24*Src.M44;
  Result.M31:=M31*Src.M11 + M32*Src.M21 + M33*Src.M31 + M34*Src.M41;
  Result.M32:=M31*Src.M12 + M32*Src.M22 + M33*Src.M32 + M34*Src.M42;
  Result.M33:=M31*Src.M13 + M32*Src.M23 + M33*Src.M33 + M34*Src.M43;
  Result.M34:=M31*Src.M14 + M32*Src.M24 + M33*Src.M34 + M34*Src.M44;
  Result.M41:=M41*Src.M11 + M42*Src.M21 + M43*Src.M31 + M44*Src.M41;
  Result.M42:=M41*Src.M12 + M42*Src.M22 + M43*Src.M32 + M44*Src.M42;
  Result.M43:=M41*Src.M13 + M42*Src.M23 + M43*Src.M33 + M44*Src.M43;
  Result.M44:=M41*Src.M14 + M42*Src.M24 + M43*Src.M34 + M44*Src.M44;
end;

procedure TMatrix.SwapMultiply(const Src: TMatrix);
var
  Tmp: TMatrix;
begin
  Tmp.M11:=Src.M11*M11 + Src.M12*M21 + Src.M13*M31 + Src.M14*M41;
  Tmp.M12:=Src.M11*M12 + Src.M12*M22 + Src.M13*M32 + Src.M14*M42;
  Tmp.M13:=Src.M11*M13 + Src.M12*M23 + Src.M13*M33 + Src.M14*M43;
  Tmp.M14:=Src.M11*M14 + Src.M12*M24 + Src.M13*M34 + Src.M14*M44;
  Tmp.M21:=Src.M21*M11 + Src.M22*M21 + Src.M23*M31 + Src.M24*M41;
  Tmp.M22:=Src.M21*M12 + Src.M22*M22 + Src.M23*M32 + Src.M24*M42;
  Tmp.M23:=Src.M21*M13 + Src.M22*M23 + Src.M23*M33 + Src.M24*M43;
  Tmp.M24:=Src.M21*M14 + Src.M22*M24 + Src.M23*M34 + Src.M24*M44;
  Tmp.M31:=Src.M31*M11 + Src.M32*M21 + Src.M33*M31 + Src.M34*M41;
  Tmp.M32:=Src.M31*M12 + Src.M32*M22 + Src.M33*M32 + Src.M34*M42;
  Tmp.M33:=Src.M31*M13 + Src.M32*M23 + Src.M33*M33 + Src.M34*M43;
  Tmp.M34:=Src.M31*M14 + Src.M32*M24 + Src.M33*M34 + Src.M34*M44;
  Tmp.M41:=Src.M41*M11 + Src.M42*M21 + Src.M43*M31 + Src.M44*M41;
  Tmp.M42:=Src.M41*M12 + Src.M42*M22 + Src.M43*M32 + Src.M44*M42;
  Tmp.M43:=Src.M41*M13 + Src.M42*M23 + Src.M43*M33 + Src.M44*M43;
  Tmp.M44:=Src.M41*M14 + Src.M42*M24 + Src.M43*M34 + Src.M44*M44;
  Self:=Tmp;
end;

function TMatrix.SwapMultiplied(const Src: TMatrix): TMatrix;
begin
  Result:=Src;
  Result.Multiply(Self);
end;

function TMatrix.Determinant: Single;
begin
  Result:=M14*M23*M32*M41 - M13*M24*M32*M41 -
          M14*M22*M33*M41 + M12*M24*M33*M41 +
          M13*M22*M32*M41 - M12*M23*M34*M41 -
          M14*M23*M31*M42 + M13*M24*M31*M42 +
          M14*M21*M33*M42 - M11*M24*M33*M42 -
          M13*M21*M34*M42 + M11*M23*M34*M42 +
          M14*M22*M31*M43 - M12*M24*M31*M43 -
          M14*M21*M32*M43 + M11*M24*M32*M43 +
          M12*M21*M34*M43 - M11*M22*M34*M43 -
          M13*M22*M31*M44 + M12*M23*M31*M44 +
          M13*M21*M32*M44 - M11*M23*M32*M44 -
          M12*M21*M33*M44 + M11*M22*M33*M44;
end;

procedure TMatrix.Invert;
var
  R: TMatrix;
  Det: Single;
begin
  Det:=Determinant;
  if Det <> 0.0 then begin
    R.M11:=M23*M34*M42 - M24*M33*M42 + M24*M32*M43 - M22*M34*M43 - M23*M32*M44 + M22*M33*M44;
    R.M12:=M14*M33*M42 - M13*M34*M42 - M14*M32*M43 + M12*M34*M43 + M13*M32*M44 - M12*M33*M44;
    R.M13:=M13*M24*M42 - M14*M23*M42 + M14*M22*M43 - M12*M24*M43 - M13*M22*M44 + M12*M23*M44;
    R.M14:=M14*M23*M32 - M13*M24*M32 - M14*M22*M33 + M12*M24*M33 + M13*M22*M34 - M12*M23*M34;
    R.M21:=M24*M33*M41 - M23*M34*M41 - M24*M31*M43 + M21*M34*M43 + M23*M31*M44 - M21*M33*M44;
    R.M22:=M13*M34*M41 - M14*M33*M41 + M14*M31*M43 - M11*M34*M43 - M13*M31*M44 + M11*M33*M44;
    R.M23:=M14*M23*M41 - M13*M24*M41 - M14*M21*M43 + M11*M24*M43 + M13*M21*M44 - M11*M23*M44;
    R.M24:=M13*M24*M31 - M14*M23*M31 + M14*M21*M33 - M11*M24*M33 - M13*M21*M34 + M11*M23*M34;
    R.M31:=M22*M34*M41 - M24*M32*M41 + M24*M31*M42 - M21*M34*M42 - M22*M31*M44 + M21*M32*M44;
    R.M32:=M14*M32*M41 - M12*M34*M41 - M14*M31*M42 + M11*M34*M42 + M12*M31*M44 - M11*M32*M44;
    R.M33:=M12*M24*M41 - M14*M22*M41 + M14*M21*M42 - M11*M24*M42 - M12*M21*M44 + M11*M22*M44;
    R.M34:=M14*M22*M31 - M12*M24*M31 - M14*M21*M32 + M11*M24*M32 + M12*M21*M34 - M11*M22*M34;
    R.M41:=M23*M32*M41 - M22*M33*M41 - M23*M31*M42 + M21*M33*M42 + M22*M31*M43 - M21*M32*M43;
    R.M42:=M12*M33*M41 - M13*M32*M41 + M13*M31*M42 - M11*M33*M42 - M12*M31*M43 + M11*M32*M43;
    R.M43:=M13*M22*M41 - M12*M23*M41 - M13*M21*M42 + M11*M23*M42 + M12*M21*M43 - M11*M22*M43;
    R.M44:=M12*M23*M31 - M13*M22*M31 + M13*M21*M32 - M11*M23*M32 - M12*M21*M33 + M11*M22*M33;
    M11:=R.M11/Det; M12:=R.M12/Det; M13:=R.M13/Det; M14:=R.M14/Det;
    M21:=R.M21/Det; M22:=R.M22/Det; M23:=R.M23/Det; M24:=R.M24/Det;
    M31:=R.M31/Det; M32:=R.M32/Det; M33:=R.M33/Det; M34:=R.M34/Det;
    M41:=R.M41/Det; M42:=R.M42/Det; M43:=R.M43/Det; M44:=R.M44/Det;
  end;
end;

function TMatrix.Inverted: TMatrix;
var
  Det: Single;
begin
  Det:=Determinant;
  if Det <> 0.0 then begin
    Result.M11:=(M23*M34*M42 - M24*M33*M42 + M24*M32*M43 - M22*M34*M43 - M23*M32*M44 + M22*M33*M44)/Det;
    Result.M12:=(M14*M33*M42 - M13*M34*M42 - M14*M32*M43 + M12*M34*M43 + M13*M32*M44 - M12*M33*M44)/Det;
    Result.M13:=(M13*M24*M42 - M14*M23*M42 + M14*M22*M43 - M12*M24*M43 - M13*M22*M44 + M12*M23*M44)/Det;
    Result.M14:=(M14*M23*M32 - M13*M24*M32 - M14*M22*M33 + M12*M24*M33 + M13*M22*M34 - M12*M23*M34)/Det;
    Result.M21:=(M24*M33*M41 - M23*M34*M41 - M24*M31*M43 + M21*M34*M43 + M23*M31*M44 - M21*M33*M44)/Det;
    Result.M22:=(M13*M34*M41 - M14*M33*M41 + M14*M31*M43 - M11*M34*M43 - M13*M31*M44 + M11*M33*M44)/Det;
    Result.M23:=(M14*M23*M41 - M13*M24*M41 - M14*M21*M43 + M11*M24*M43 + M13*M21*M44 - M11*M23*M44)/Det;
    Result.M24:=(M13*M24*M31 - M14*M23*M31 + M14*M21*M33 - M11*M24*M33 - M13*M21*M34 + M11*M23*M34)/Det;
    Result.M31:=(M22*M34*M41 - M24*M32*M41 + M24*M31*M42 - M21*M34*M42 - M22*M31*M44 + M21*M32*M44)/Det;
    Result.M32:=(M14*M32*M41 - M12*M34*M41 - M14*M31*M42 + M11*M34*M42 + M12*M31*M44 - M11*M32*M44)/Det;
    Result.M33:=(M12*M24*M41 - M14*M22*M41 + M14*M21*M42 - M11*M24*M42 - M12*M21*M44 + M11*M22*M44)/Det;
    Result.M34:=(M14*M22*M31 - M12*M24*M31 - M14*M21*M32 + M11*M24*M32 + M12*M21*M34 - M11*M22*M34)/Det;
    Result.M41:=(M23*M32*M41 - M22*M33*M41 - M23*M31*M42 + M21*M33*M42 + M22*M31*M43 - M21*M32*M43)/Det;
    Result.M42:=(M12*M33*M41 - M13*M32*M41 + M13*M31*M42 - M11*M33*M42 - M12*M31*M43 + M11*M32*M43)/Det;
    Result.M43:=(M13*M22*M41 - M12*M23*M41 - M13*M21*M42 + M11*M23*M42 + M12*M21*M43 - M11*M22*M43)/Det;
    Result.M44:=(M12*M23*M31 - M13*M22*M31 + M13*M21*M32 - M11*M23*M32 - M12*M21*M33 + M11*M22*M33)/Det;
  end else Result:=Self;
end;

procedure TMatrix.Transpose;
begin
  Self:=Self.Transposed;
end;

function TMatrix.Transposed: TMatrix;
begin
  Result.M11:=M11; Result.M21:=M12; Result.M31:=M13; Result.M41:=M14;
  Result.M12:=M21; Result.M22:=M22; Result.M32:=M23; Result.M42:=M24;
  Result.M13:=M31; Result.M23:=M32; Result.M33:=M33; Result.M43:=M34;
  Result.M14:=M41; Result.M24:=M42; Result.M34:=M43; Result.M44:=M44;
end;

procedure TMatrix.NormalizeAxes;
var
  Tmp: TVector;
begin
  Tmp:=XAxis.Normalized;
  M11:=Tmp.X;
  M21:=Tmp.Y;
  M31:=Tmp.Z;
  Tmp:=YAxis.Normalized;
  M12:=Tmp.X;
  M22:=Tmp.Y;
  M32:=Tmp.Z;
  Tmp:=ZAxis.Normalized;
  M13:=Tmp.X;
  M23:=Tmp.Y;
  M33:=Tmp.Z;
end;

function TMatrix.XAxis: TVector;
begin
  Result.X:=M11;
  Result.Y:=M21;
  Result.Z:=M31;
end;

function TMatrix.YAxis: TVector;
begin
  Result.X:=M12;
  Result.Y:=M22;
  Result.Z:=M32;
end;

function TMatrix.ZAxis: TVector;
begin
  Result.X:=M13;
  Result.Y:=M23;
  Result.Z:=M33;
end;

function TMatrix.Translation: TVector;
begin
  Result.X:=M14;
  Result.Y:=M24;
  Result.Z:=M34;
end;

function TMatrix.Position: TVector;
begin
  Result.X:=-M14;
  Result.Y:=-M24;
  Result.Z:=-M34;
end;

procedure TMatrix.SetXAxis(const V: TVector; W: Single);
begin
  M11:=V.X;
  M21:=V.Y;
  M31:=V.Z;
  M41:=W;
end;

procedure TMatrix.SetXAxis(X, Y, Z: Single; W: Single);
begin
  M11:=X;
  M21:=Y;
  M31:=Z;
  M41:=W;
end;

procedure TMatrix.SetYAxis(const V: TVector; W: Single);
begin
  M12:=V.X;
  M22:=V.Y;
  M32:=V.Z;
  M42:=W;
end;

procedure TMatrix.SetYAxis(X, Y, Z: Single; W: Single);
begin
  M12:=X;
  M22:=Y;
  M32:=Z;
  M42:=W;
end;

procedure TMatrix.SetZAxis(const V: TVector; W: Single);
begin
  M13:=V.X;
  M23:=V.Y;
  M33:=V.Z;
  M43:=W;
end;

procedure TMatrix.SetZAxis(X, Y, Z: Single; W: Single);
begin
  M13:=X;
  M23:=Y;
  M33:=Z;
  M43:=W;
end;

procedure TMatrix.SetTranslation(const V: TVector; W: Single);
begin
  M14:=V.X;
  M24:=V.Y;
  M34:=V.Z;
  M44:=W;
end;

procedure TMatrix.SetTranslation(X, Y, Z: Single; W: Single);
begin
  M14:=X;
  M24:=Y;
  M34:=Z;
  M44:=W;
end;

procedure TMatrix.SetAxes(X, Y, Z, W: TVector; XW: Single; YW: Single;
  ZW: Single; WW: Single);
begin
  SetXAxis(X, XW);
  SetYAxis(Y, YW);
  SetZAxis(Z, ZW);
  SetTranslation(W, WW);
end;

procedure TMatrix.SetAxes(X, Y, Z: TVector; XW: Single; YW: Single; ZW: Single);
begin
  SetXAxis(X, XW);
  SetYAxis(Y, YW);
  SetZAxis(Z, ZW);
end;

procedure TMatrix.RemoveTranslation;
begin
  M14:=0.0;
  M24:=0.0;
  M34:=0.0;
  M44:=1.0;
end;

{ TPlane }
procedure TPlane.FromThreePoints(const A, B, C: TVector);
var
  ab, ac: TVector;
begin
  ab.x:=b.x - A.x;
  ab.y:=b.y - A.y;
  ab.z:=b.z - A.z;
  ac.x:=C.x - A.x;
  ac.y:=C.y - A.y;
  ac.z:=C.z - A.z;
  ab.Cross(ac, N);
  N.Normalize;
  D:=N.x*A.x + N.y*A.y + N.z*A.z;
end;

procedure TPlane.FromPointAndNormal(const P, Normal: TVector);
var
  tmp: TVector;
begin
  N:=Normal;
  tmp:=P;
  tmp.Normalize;
  D:=P.Length*N.Dot(tmp);
end;

procedure TPlane.Invert;
begin
  D:=-D;
  N.Invert;
end;

function TPlane.SignDist(const P: TVector): Single;
var
  a: TVector;
begin
  a.x:=P.x - N.x*D;
  a.y:=P.y - N.y*D;
  a.z:=P.z - N.z*D;
  Result:=a.x*N.x + a.y*N.y + a.z*N.z;
end;

function TPlane.Front(const P: TVector): Boolean;
begin
  Result:=SignDist(P) >= 0.0;
end;

procedure TPlane.Project(var P: TVector);
var
  sd: Single;
begin
  sd:=SignDist(P);
  P.x -= N.x*sd;
  P.y -= N.y*sd;
  P.z -= N.z*sd;
end;

function TPlane.Projected(const P: TVector): TVector;
var
  sd: Single;
begin
  sd:=SignDist(P);
  Result.x:=P.x - N.x*sd;
  Result.y:=P.y - N.y*sd;
  Result.z:=P.z - N.z*sd;
end;

function TPlane.ProjectedNormal(const P: TVector): TVector;
begin
  Result.x:=P.x - N.x*N.x*P.x;
  Result.y:=P.y - N.y*N.y*P.y;
  Result.z:=P.z - N.z*N.z*P.z;
end;

procedure TPlane.Flip(var P: TVector);
var
  sd: Single;
begin
  sd:=SignDist(P);
  P.x -= N.x*sd*2;
  P.y -= N.y*sd*2;
  P.z -= N.z*sd*2;
end;

function TPlane.Flipped(const P: TVector): TVector;
var
  sd: Single;
begin
  sd:=SignDist(P);
  Result.x:=P.x - N.x*sd*2;
  Result.y:=P.y - N.y*sd*2;
  Result.z:=P.z - N.z*sd*2;
end;

function TPlane.Intersect(const A, B: TVector; out P: TVector): Boolean;
var
  u, w, pp: TVector;
  si, vd, vn: Single;
begin
  u.x:=b.x - A.x;
  u.y:=b.y - A.y;
  u.z:=b.z - A.z;

  pp.x:=N.x*D;
  pp.y:=N.y*D;
  pp.z:=N.z*D;

  w.x:=A.x - pp.x;
  w.y:=A.y - pp.y;
  w.z:=A.z - pp.z;

  vd:=N.x*u.x + N.y*u.y + N.z*u.z;
  vn:=-(N.x*w.x + N.y*w.y + N.z*w.z);

  if vd=0 then Exit(False);

  si:=vn/vd;
  if (si < 0.0) or (si > 1) then Exit(False);

  P.x:=A.x + u.x*si;
  P.y:=A.y + u.y*si;
  P.z:=A.z + u.z*si;

  Result:=True;
end;

function TPlane.IntersectNEP(const A, B: TVector; out P: TVector): Boolean;
var
  u, w, pp: TVector;
  si, vd, vn: Single;
begin
  u.x:=b.x - A.x;
  u.y:=b.y - A.y;
  u.z:=b.z - A.z;

  pp.x:=N.x*D;
  pp.y:=N.y*D;
  pp.z:=N.z*D;

  w.x:=A.x - pp.x;
  w.y:=A.y - pp.y;
  w.z:=A.z - pp.z;

  vd:=N.x*u.x + N.y*u.y + N.z*u.z;
  vn:=-(N.x*w.x + N.y*w.y + N.z*w.z);

  if vd=0 then Exit(False);

  si:=vn/vd;
  if (si <= 0.0001) or (si >= 1-0.0001) then Exit(False);

  P.x:=A.x + u.x*si;
  P.y:=A.y + u.y*si;
  P.z:=A.z + u.z*si;

  Result:=True;
end;

procedure TPlane.Normalize;
var
  L: Single;
begin
  L:=N.Length;
  N.X /= L;
  N.Y /= L;
  N.Z /= L;
  D /= L;
end;

{ TRay }
procedure TRay.FromSegment(const A, B: TVector);
begin
  O:=A;
  D:=B;
  D.Sub(O);
  D.Normalize;
end;

function TRay.SphereHit(const SC: TVector; SR: Single; out IP: TVector): Boolean;
var
  b, c, dd, e, t0, t1, t, srx, sry, srz: Single;
begin
  srx:=O.x - SC.x;
  sry:=O.y - SC.y;
  srz:=O.z - SC.z;
  b:=srx*D.x + sry*D.y + srz*D.z;
  c:=(srx*srx + sry*sry + srz*srz) - SR*SR;
  dd:=b*b - c;
  if (dd > 0.0001) then begin
    e:=Sqrt(dd);
    t0:=-b-e;
    t1:=-b+e;
    if t1 < t0 then t:=t1 else t:=t0;
    if t < 0.0 then Exit(False);
    IP.x:=O.x + D.x*t;
    IP.y:=O.y + D.y*t;
    IP.z:=O.z + D.z*t;
    Result:=True;
  end else
    Result:=False;
end;

function TRay.PlaneHit(const Plane: TPlane; out IP: TVector): Boolean;
var
  W: TVector;
  T, VD, VN: Single;
begin
  VD:=Plane.N.Dot(D);
  if VD=0 then Exit(False);
  W.x:=O.x - Plane.N.x*Plane.D;
  W.y:=O.y - Plane.N.y*Plane.D;
  W.z:=O.z - Plane.N.z*Plane.D;
  VN:=-Plane.N.Dot(W);
  T:=VN/VD;
  if T < 0.0 then Exit(False);
  IP.x:=O.x + D.x*t;
  IP.y:=O.y + D.y*t;
  IP.z:=O.z + D.z*t;
  Result:=True;
end;

function TRay.TriangleHit(const A, B, C: TVector; out IP: TVector): Boolean;
var
  P: TPlane;
  AB, AC: TVector;
  uu, uv, vv, wu, wv, wx, wy, wz, dd, sv, tv: Single;
begin
  AB:=B;
  AB.Sub(A);
  AC:=C;
  AC.Sub(A);
  AB.Cross(AC, P.N);
  P.N.Normalize;
  P.D:=A.Dot(P.N);
  if not PlaneHit(P, IP) then Exit(False);
  uu:=AB.x*AB.x + AB.y*AB.y + AB.z*AB.z;
  uv:=AB.x*AC.x + AB.y*AC.y + AB.z*AC.z;
  vv:=AC.x*AC.x + AC.y*AC.y + AC.z*AC.z;
  wx:=IP.x - A.x;
  wy:=IP.y - A.y;
  wz:=IP.z - A.z;
  wu:=wx*AB.x + wy*AB.y + wz*AB.z;
  wv:=wx*AC.x + wy*AC.y + wz*AC.z;
  dd:=uv*uv - uu*vv;
  if dd=0 then Exit(False);
  sv:=(uv*wv - vv*wu)/dd;
  if (sv < 0.0) or (sv > 1.0) then Exit(False);
  tv:=(uv*wu - uu*wv)/dd;
  if (tv < 0.0) or ((sv + tv) > 1.0) then Exit(False);
  Result:=True;
end;

function TRay.TriangleHit(const A, AB, AC: TVector; const P: TPlane; UU, UV, VV, DD: Single; out ip: TVector): Boolean;
var
  wu, wv, wx, wy, wz, sv, tv: Single;
begin
  if not PlaneHit(P, ip) then Exit(False);
  wx:=ip.x - A.x;
  wy:=ip.y - A.y;
  wz:=ip.z - A.z;
  wu:=wx*AB.x + wy*AB.y + wz*AB.z;
  wv:=wx*AC.x + wy*AC.y + wz*AC.z;
  sv:=(uv*wv - vv*wu)/dd;
  if (sv < 0.0) or (sv > 1.0) then Exit(False);
  tv:=(uv*wu - uu*wv)/dd;
  if (tv < 0.0) or ((sv + tv) > 1.0) then Exit(False);
  Result:=True;
end;

function TRay.AABoxHit(const AABox: TAABox): Boolean;
var
  df: TVector;
  t1, t2, t3, t4, t5, t6, tmin, tmax: Single;
begin
  if D.x=0 then df.x:=0 else df.x:=1/D.x;
  if D.y=0 then df.y:=0 else df.y:=1/D.y;
  if D.z=0 then df.z:=0 else df.z:=1/D.z;
  t1:=(AABox.Min.x - O.x)*df.x;
  t2:=(AABox.Max.x - O.x)*df.x;
  t3:=(AABox.Min.y - O.y)*df.y;
  t4:=(AABox.Max.y - O.y)*df.y;
  t5:=(AABox.Min.z - O.z)*df.z;
  t6:=(AABox.Max.z - O.z)*df.z;
  tmin:=Max(Max(Min(t1, t2), Min(t3, t4)), Min(t5, t6));
  tmax:=Min(Min(Max(t1, t2), Max(t3, t4)), Max(t5, t6));
  if TMax < 0 then Exit(False);
  if TMin > TMax then Exit(False);
  Result:=True;
end;

function TRay.AABoxHit(const AABox: TAABox; out T: Single): Boolean;
var
  df: TVector;
  t1, t2, t3, t4, t5, t6, tmin, tmax: Single;
begin
  if D.x=0 then df.x:=0 else df.x:=1/D.x;
  if D.y=0 then df.y:=0 else df.y:=1/D.y;
  if D.z=0 then df.z:=0 else df.z:=1/D.z;
  t1:=(AABox.Min.x - O.x)*df.x;
  t2:=(AABox.Max.x - O.x)*df.x;
  t3:=(AABox.Min.y - O.y)*df.y;
  t4:=(AABox.Max.y - O.y)*df.y;
  t5:=(AABox.Min.z - O.z)*df.z;
  t6:=(AABox.Max.z - O.z)*df.z;
  tmin:=Max(Max(Min(t1, t2), Min(t3, t4)), Min(t5, t6));
  tmax:=Min(Min(Max(t1, t2), Max(t3, t4)), Max(t5, t6));
  if TMax < 0 then begin
    T:=tmax;
    Exit(False);
  end;
  if TMin > TMax then begin
    T:=tmax;
    Exit(False);
  end;
  T:=tmin;
  Result:=True;
end;

function TRay.AABoxHit(const AABox: TAABox; out IP: TVector): Boolean;
var
  t: Single;
begin
  Result:=AABoxHit(AABox, t);
  IP:=D;
  IP.Scale(t);
  IP.Add(O);
end;

{ TAABox }
function TAABox.GetExtent: TVector;
begin
  Result.X:=Max.X - Min.X;
  Result.Y:=Max.Y - Min.Y;
  Result.Z:=Max.Z - Min.Z;
end;

function TAABox.GetHeight: Single;
begin
  Result:=Abs(Max.Y - Min.Y);
end;

function TAABox.GetLength: Single;
begin
  Result:=Abs(Max.Z - Min.Z);
end;

function TAABox.GetWidth: Single;
begin
  Result:=Abs(Max.X - Min.X);
end;

procedure TAABox.SetExtent(AValue: TVector);
begin
  Max.X:=Min.X + AValue.X;
  Max.Y:=Min.Y + AValue.Y;
  Max.Z:=Min.Z + AValue.Z;
end;

procedure TAABox.SetHeight(AValue: Single);
begin
  Max.Y:=Min.Y + AValue;
end;

procedure TAABox.SetLength(AValue: Single);
begin
  Max.Z:=Min.Z + AValue;
end;

procedure TAABox.SetWidth(AValue: Single);
begin
  Max.X:=Min.X + AValue;
end;

procedure TAABox.Zero;
begin
  Min.Zero;
  Max.Zero;
end;

procedure TAABox.MakeMaxInverted;
begin
  Min:=Vector(MaxSingle, MaxSingle, MaxSingle);
  Max:=Vector(-MaxSingle, -MaxSingle, -MaxSingle);
end;

function TAABox.IsValid: Boolean;
begin
  Result:=(Min.X <= Max.X) and (Min.Y <= Max.Y) and (Min.Z <= Max.Z);
end;

procedure TAABox.Include(const Box: TAABox);
begin
  if Box.Min.X < Min.X then Min.X:=Box.Min.X;
  if Box.Min.Y < Min.Y then Min.Y:=Box.Min.Y;
  if Box.Min.Z < Min.Z then Min.Z:=Box.Min.Z;
  if Box.Max.X > Max.X then Max.X:=Box.Max.X;
  if Box.Max.Y > Max.Y then Max.Y:=Box.Max.Y;
  if Box.Max.Z > Max.Z then Max.Z:=Box.Max.Z;
end;

procedure TAABox.Include(const V: TVector);
begin
  if V.X < Min.X then Min.X:=V.X;
  if V.Y < Min.Y then Min.Y:=V.Y;
  if V.Z < Min.Z then Min.Z:=V.Z;
  if V.X > Max.X then Max.X:=V.X;
  if V.Y > Max.Y then Max.Y:=V.Y;
  if V.Z > Max.Z then Max.Z:=V.Z;
end;

procedure TAABox.MakeProper;
var
  C: Single;
begin
  if Min.X > Max.X then begin
    C:=Min.X;
    Min.X:=Max.X;
    Max.X:=C;
  end;
  if Min.Y > Max.Y then begin
    C:=Min.Y;
    Min.Y:=Max.Y;
    Max.Y:=C;
  end;
  if Min.Z > Max.Z then begin
    C:=Min.Z;
    Min.Z:=Max.Z;
    Max.Z:=C;
  end;
end;

function TAABox.Includes(const V: TVector): Boolean;
begin
  Result:=(V.X >= Min.X) and (V.Y >= Min.Y) and (V.Z >= Min.Z) and
          (V.X <= Max.X) and (V.Y <= Max.Y) and (V.Z <= Max.Z);
end;

function TAABox.Includes(const Box: TAABox): Boolean;
begin
  Result:=(Box.Min.X >= Min.X) and (Box.Min.Y >= Min.Y) and (Box.Min.Z >= Min.Z) and
          (Box.Max.X <= Max.X) and (Box.Max.Y <= Max.Y) and (Box.Max.Z <= Max.Z);
end;

function TAABox.Overlaps(const Box: TAABox): Boolean;
begin
  Result:=not ((Box.Min.X > Max.X) or
              (Box.Max.X < Min.X) or
              (Box.Min.Y > Max.Y) or
              (Box.Max.Y < Min.Y) or
              (Box.Min.Z > Max.Z) or
              (Box.Max.Z < Min.Z));
end;

procedure TAABox.Grow(V: Single);
begin
  Min.X:=Min.X - V;
  Min.Y:=Min.Y - V;
  Min.Z:=Min.Z - V;
  Max.X:=Max.X + V;
  Max.Y:=Max.Y + V;
  Max.Z:=Max.Z + V;
end;

procedure TAABox.Grow(const V: TVector);
begin
  Min.X:=Min.X - V.X;
  Min.Y:=Min.Y - V.Y;
  Min.Z:=Min.Z - V.Z;
  Max.X:=Max.X + V.X;
  Max.Y:=Max.Y + V.Y;
  Max.Z:=Max.Z + V.Z;
end;

function TAABox.Grown(V: Single): TAABox;
begin
  Result:=Self;
  Result.Grow(V);
end;

function TAABox.Grown(const V: TVector): TAABox;
begin
  Result:=Self;
  Result.Grow(V);
end;

procedure TAABox.MoveTo(const V: TVector);
var
  E: TVector;
begin
  E:=Extent;
  Min:=V;
  SetExtent(E);
end;

procedure TAABox.MoveBy(const Delta: TVector);
begin
  Min.Add(Delta);
  Max.Add(Delta);
end;

procedure TAABox.GetBoundingSphere(out Center: TVector; out Radius: Single);
var
  C, D, E, F, G, H, I, J: TVector;
begin
  Center:=Max.Added(Min).Scaled(0.5);
  GetCorners(C, D, E, F, G, H, I, J);
  Radius:=Maths.DistanceSq(Center, C);
  Radius:=Maths.Max(Radius, Maths.DistanceSq(Center, D));
  Radius:=Maths.Max(Radius, Maths.DistanceSq(Center, E));
  Radius:=Maths.Max(Radius, Maths.DistanceSq(Center, F));
  Radius:=Maths.Max(Radius, Maths.DistanceSq(Center, G));
  Radius:=Maths.Max(Radius, Maths.DistanceSq(Center, H));
  Radius:=Maths.Max(Radius, Maths.DistanceSq(Center, I));
  Radius:=Maths.Max(Radius, Maths.DistanceSq(Center, J));
  Radius:=Sqrt(Radius);
end;

procedure TAABox.GetCorners(out MinA, MinB, MinC, MinD, MaxA, MaxB, MaxC, MaxD: TVector);
begin
  MinA:=Min;
  MinB:=Vector(Max.X, Min.Y, Min.Z);
  MinC:=Vector(Min.X, Min.Y, Max.Z);
  MinD:=Vector(Max.X, Min.Y, Max.Z);
  MaxA:=Vector(Min.X, Max.Y, Min.Z);
  MaxB:=Vector(Max.X, Max.Y, Min.Z);
  MaxC:=Vector(Min.X, Max.Y, Max.Z);
  MaxD:=Max;
end;

function TAABox.DistanceSq(V: TVector): Single;
begin
  if Includes(V) then
    Result:=0
  else
    Result:=Maths.DistanceSq(V, Vector(Clamp(V.X, Min.X, Max.X),
                                       Clamp(V.Y, Min.Y, Max.Y),
                                       Clamp(V.Z, Min.Z, Max.Z)));
end;

function TAABox.Distance(V: TVector): Single;
begin
  Result:=Sqrt(DistanceSq(V));
end;

function TAABox.Center: TVector;
begin
  Result.X:=(Max.X + Min.X)*0.5;
  Result.Y:=(Max.Y + Min.Y)*0.5;
  Result.Z:=(Max.Z + Min.Z)*0.5;
end;

{ TTransform }
procedure TTransform.Reset;
begin
  Translation:=Vector(0, 0, 0);
  Rotation:=Vector(0, 0, 0);
end;

procedure TTransform.FromMatrix(AMatrix: TMatrix);
var
  Mtx: TMatrix;
begin
  // We'll ignore any scale
  AMatrix.NormalizeAxes;
  Translation:=AMatrix.Translation;
  // Calculate rotation
  Rotation.Z:=RadToDeg(AngleBetweenVectorsOnPlane(
    Vector(Sign(AMatrix.ZAxis.Z), 0, 0),
    AMatrix.XAxis,
    AMatrix.ZAxis.Normalized));
  Mtx.ZRotation(-DegToRad(Rotation.Z));
  AMatrix.Multiply(Mtx);
  Rotation.Y:=RadToDeg(AngleBetweenVectorsOnPlane(
    Vector(Sign(AMatrix.YAxis.Y), 0, 0),
    AMatrix.XAxis,
    AMatrix.YAxis.Normalized));
  Mtx.YRotation(-DegToRad(Rotation.Y));
  AMatrix.Multiply(Mtx);
  Rotation.X:=RadToDeg(AngleBetweenVectorsOnPlane(
    Vector(0, 1, 0),
    AMatrix.YAxis,
    AMatrix.XAxis.Normalized));
end;

function TTransform.ToMatrix: TMatrix;
var
  Tmp: TMatrix;
begin
  Result.Identity;
  Tmp.Translation(Translation.X, Translation.Y, Translation.Z);
  Result.Multiply(Tmp);
  Tmp.XYZRotation(Rotation.X, Rotation.Y, Rotation.Z);
  Result.Multiply(Tmp);
end;

initialization
  Origin.Zero;
  Identity.Identity;
end.

