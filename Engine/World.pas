{ Game world }
unit World;

interface

uses
  Misc, Classes, Maths, Scene, GridMap, Serial, Meshes, Textures, PieceMdl,
  InputDrv;

type
  { Forward class declarations }
  TWorld = class;
  TEntity = class;
  TEntityClass = class of TEntity;

  { Collections }
  TEntityCollection = specialize TSerializableCollection<TEntity>;

  { TEntity - Base class for entities in the world }
  TEntity = class(TSerializable)
  private
    // Property storage
    FWorld: TWorld;
    FTransform: TTransform;
    FName: string;
    FSolid: Boolean;
    procedure SetTransform(const ATransform: TTransform); inline;
    procedure SetPositionX(AValue: Single); inline;
    procedure SetPositionY(AValue: Single); inline;
    procedure SetPositionZ(AValue: Single); inline;
    procedure SetRotationX(AValue: Single); inline;
    procedure SetRotationY(AValue: Single); inline;
    procedure SetRotationZ(AValue: Single); inline;
    function GetPositionX: Single; inline;
    function GetPositionY: Single; inline;
    function GetPositionZ: Single; inline;
    function GetRotationX: Single; inline;
    function GetRotationY: Single; inline;
    function GetRotationZ: Single; inline;
    function GetScene: TScene; inline;
  protected
    // Called after the entity has been added to the world
    procedure AddedToWorld; virtual;
    // Called after the entity has been removed from the world
    procedure RemovedFromWorld; virtual;
    // Called to add any scene nodes the entity may need to the scene
    procedure AddSceneNodes; virtual;
    // Called to remove any scene nodes the entity may have
    procedure RemoveSceneNodes; virtual;
    // Called when the entity's transform has changed
    procedure TransformChanged; virtual;
    // Called every ~28ms to update the entity
    procedure Update; virtual;
    {$IFDEF EDITOR}
    // called every ~28ms when in editor and *not* playing the game
    procedure EditorUpdate; virtual;
    {$ENDIF}
    // Called every frame before rendering occurs
    procedure Frame; virtual;
    // Called once after the world is initialized and all globals are available
    // but before the game itself is initialized
    procedure Initialize; virtual;
    // Return the entity's axis aligned bounding box
    function GetAABox: TAABox; virtual;
    // Return the entity's axis aligned collision bounding box
    function GetCollisionBox: TAABox; virtual;
    // Check for a ray hit against the entity
    function RayHit(const ARay: TRay; out IP: TVector): Boolean; virtual;
    // Return data from the world and game data tables
    function GetData(AKey: string; ADefValue: string=''): string; inline;
  public
    destructor Destroy; override;
    // Perform the given command (action depends on the entity), returns true
    // if the command was performed
    function PerformCommand(ACommand: string): Boolean; virtual;
    // Schedule a motion to the given transform by the given target time with
    // the given optional command target and command
    procedure ScheduleMotion(ATransform: TTransform; ATargetTime: Integer; ATarget, ACommand: string);
    // Schedule a motion to the given entity's transform by the given target
    // time with the given optional command target and command
    procedure ScheduleMotion(AEntity: TEntity; ATargetTime: Integer; ATarget, ACommand: string); inline;
    // The entity's transform
    property Transform: TTransform read FTransform write SetTransform;
    // The entity's axis aligned bounding box
    property AABox: TAABox read GetAABox;
    // The entity's axis aligned bounding box for collision checks
    property CollisionBox: TAABox read GetCollisionBox;
    // The scene this entity can use to manage scene nodes if needed
    property Scene: TScene read GetScene;
  published
    // The world this entity is part of
    property World: TWorld read FWorld;
    // The name of the entity (usually empty)
    property Name: string read FName write FName;
    // True if the entity is solid and can be collided against
    property Solid: Boolean read FSolid write FSolid;
    // Component-wise access to transform for use with the RTTI
    property PositionX: Single read GetPositionX write SetPositionX;
    property PositionY: Single read GetPositionY write SetPositionY;
    property PositionZ: Single read GetPositionZ write SetPositionZ;
    // Rotation is in degrees
    property RotationX: Single read GetRotationX write SetRotationX;
    property RotationY: Single read GetRotationY write SetRotationY;
    property RotationZ: Single read GetRotationZ write SetRotationZ;
  end;

  { TSceneNodeEntity - Base class for entities that have a primary scene node }
  TSceneNodeEntity = class(TEntity)
  private
    // Property storage
    FSceneNode: TSceneNode;
    FHeightOffset: Single;
    procedure SetHeightOffset(AOffset: Single);
  protected
    // Called to create the scene node
    function CreateSceneNode: TSceneNode; virtual;
    // Update the scene node's local matrix from the entity's transform
    procedure UpdateSceneNodeMatrix;
  protected
    // TEntity methods
    procedure AddedToWorld; override;
    procedure RemovedFromWorld; override;
    procedure AddSceneNodes; override;
    procedure RemoveSceneNodes; override;
    procedure TransformChanged; override;
    function GetAABox: TAABox; override;
  public
    // TEntity methods
    function RayHit(const ARay: TRay; out IP: TVector): Boolean; override;
  public
    // The entity's scene node
    property SceneNode: TSceneNode read FSceneNode;
  published
    // A height offset to apply to the transform
    property HeightOffset: Single read FHeightOffset write SetHeightOffset;
  end;

  { TStaticMeshLightingType - The lighting type to use for a static mesh }
  TStaticMeshLightingType = (smltFullbright, smltConstant, smltPerVertex);

  { TStaticMeshOcclusionBehavior - Occlusion behavior for the static mesh }
  TStaticMeshOcclusionBehavior = (smobOcclusionCheck, smobIgnoreOcclusion, smobOccluder);

  { TStaticMeshEntity - Static entity that refers to a mesh }
  TStaticMeshEntity = class(TSceneNodeEntity)
  private
    // Property storage
    FMeshName: string;
    FTextureName: string;
    FLightingType: TStaticMeshLightingType;
    FTexture: TTexture;
    FDepthBias: Byte;
    FOcclusionBehavior: TStaticMeshOcclusionBehavior;
    procedure SetMeshName(AMeshName: string);
    procedure SetTextureName(ATextureName: string);
    procedure SetDepthBias(ADepthBias: Byte);
    procedure SetOcclusionBehavior(AOcclusionBehavior: TStaticMeshOcclusionBehavior);
  private
    // The mesh for this entity
    FMesh: TMesh;
    // Lighting for the mesh
    Lighting: array of Byte;
    // Update/reload the mesh
    procedure UpdateMesh;
    // Update the mesh occlusion flags
    procedure UpdateMeshOcclusion(ASceneNode: TSceneNode);
    // Update the lighting
    procedure UpdateLighting(ASceneNode: TSceneNode);
  protected
    // TSerializable methods
    procedure SerializeExtraData(AContext: TSerializationContext); override;
    procedure DeserializeExtraData(AContext: TSerializationContext); override;
  protected
    // TSceneNodeEntity methods
    function CreateSceneNode: TSceneNode; override;
  public
    constructor Create; override;
    // Set the mesh lighting values - empty array means fullbright, array of one
    // value will cause constant lighting, array of triangle*3 will be
    // per-vertex lighting
    procedure SetLighting(ALighting: array of Byte);
    // The mesh this entity is using
    property Mesh: TMesh read FMesh;
    // The texture this entity is using
    property Texture: TTexture read FTexture;
  published
    // The name of the mesh
    property MeshName: string read FMeshName write SetMeshName;
    // The name of the texture (empty to attempt same as name but with raw ext)
    property TextureName: string read FTextureName write SetTextureName;
    // Depth bias to use for rendering the mesh
    property DepthBias: Byte read FDepthBias write SetDepthBias;
    // The lighting type to use for this mesh
    property LightingType: TStaticMeshLightingType read FLightingType write FLightingType;
    // The occlusion behavior for this mesh
    property OcclusionBehavior: TStaticMeshOcclusionBehavior read FOcclusionBehavior write SetOcclusionBehavior;
  end;

  { TPieceModelEntity - Entity that can display an animated piece model }
  TPieceModelEntity = class(TSceneNodeEntity)
  private
    FPieceModel: TPieceModel;
    FModelName: string;
    FAnimationName: string;
    FDepthBias: Byte;
    FAnimIndex: Integer;
    FLastFrame: Integer;
    FPaused: Boolean;
    procedure SetAnimationName(AAnimationName: string);
    procedure SetDepthBias(ADepthBias: Byte);
    procedure SetModelName(AModelName: string);
  private
    // The collision box to report - we cache this to ensure the box remains
    // static despite animations (otherwise collision checks wont be consistent)
    CurrentCollisionBox: TAABox;
    // Animation time
    AnimTime: Integer;
    // Update/reload the model
    procedure UpdateModel;
    // Setup the animation set via the AnimationName property
    procedure SetupAnimation;
    // Update animation state, returns true if the current animation finished
    function UpdateAnimation: Boolean;
  protected
    // TEntity methods
    procedure AddSceneNodes; override;
    procedure TransformChanged; override;
    procedure Update; override;
    {$IFDEF EDITOR}
    procedure EditorUpdate; override;
    {$ENDIF}
    procedure Initialize; override;
    function GetCollisionBox: TAABox; override;
  protected
    // TSceneNodeEntity methods
    function CreateSceneNode: TSceneNode; override;
  protected
    // Called when the current animation finishes
    procedure AnimationFinished;
  public
    // Restart the animation, if it is paused just go to the first frame
    procedure RestartAnimation;
  public
    // The piece model this entity is using
    property PieceModel: TPieceModel read FPieceModel;
    // Current animation index
    property AnimIndex: Integer read FAnimIndex;
    // Last known animated frame in the index
    property LastFrame: Integer read FLastFrame;
  published
    // The name of the piece model
    property ModelName: string read FModelName write SetModelName;
    // The animation to play
    property AnimationName: string read FAnimationName write SetAnimationName;
    // The depth bias to use for rendering the model's meshes
    property DepthBias: Byte read FDepthBias write SetDepthBias;
    // Animation playback is paused
    property Paused: Boolean read FPaused write FPaused;
  end;

  { TBoxEntitySide - A side of a box entity }
  TBoxEntitySide = (tbsTop, tbsBottom, tbsLeft, tbsRight, tbsFront, tbsBack);
  { TBoxEntitySides - A set of box entity sides }
  TBoxEntitySides = set of TBoxEntitySide;

  { TBoxEntity - Entity that renders a box }
  TBoxEntity = class(TSceneNodeEntity)
  private
    FDrawSides: TBoxEntitySides;
    FTopTexture: Byte;
    FBottomTexture: Byte;
    FSideTexture: Byte;
    FDepthBias: Byte;
    FWidth: Single;
    FHeight: Single;
    FLength: Single;
    procedure SetBottomTexture(const ABottomTexture: Byte);
    procedure SetDepthBias(const ADepthBias: Byte);
    procedure SetDrawSides(const ADrawSides: TBoxEntitySides);
    procedure SetHeight(const AHeight: Single);
    procedure SetLength(const ALength: Single);
    procedure SetSideTexture(const ASideTexture: Byte);
    procedure SetTopTexture(const ATopTexture: Byte);
    procedure SetWidth(const AWidth: Single);
  protected
    // TSceneNodeEntity methods
    function CreateSceneNode: TSceneNode; override;
  public
    constructor Create; override;
  published
    // Walltexture index for the box top
    property TopTexture: Byte read FTopTexture write SetTopTexture;
    // Walltexture index for the box bottom
    property BottomTexture: Byte read FBottomTexture write SetBottomTexture;
    // Walltexture index for the box sides
    property SideTexture: Byte read FSideTexture write SetSideTexture;
    // Sides to draw
    property DrawSides: TBoxEntitySides read FDrawSides write SetDrawSides;
    // Rendering depth bias
    property DepthBias: Byte read FDepthBias write SetDepthBias;
    // Box width
    property Width: Single read FWidth write SetWidth;
    // Box height
    property Height: Single read FHeight write SetHeight;
    // Box length
    property Length: Single read FLength write SetLength;
  end;

  { TLight - Light entities }
  TLight = class(TEntity)
  private
    FLight: Single;
    FRadius: Single;
    FCastShadows: Boolean;
  public
    constructor Create; override;
  protected
    // TEntity methods
    {$IFDEF EDITOR}
    procedure Frame; override;
    {$ENDIF}
    function GetAABox: TAABox; override;
  published
    // Light intensity
    property Light: Single read FLight write FLight;
    // Light radius
    property Radius: Single read FRadius write FRadius;
    // Casts shadows
    property CastShadows: Boolean read FCastShadows write FCastShadows;
  end;

  { TDummyEntity - Dummy entity meant to be used as a reference }
  TDummyEntity = class(TEntity)
  protected
    // TEntity methods
    {$IFDEF EDITOR}
    procedure Frame; override;
    function GetAABox: TAABox; override;
    {$ENDIF}
  end;

  { TEntityMotion - Information about an active motion for an entity }
  TEntityMotion = record
    // The entity in motion
    Entity: TEntity;
    // The initial transformation
    InitialTransform: TTransform;
    // The target transformation
    TargetTransform: TTransform;
    // The initial time
    InitialTime: Integer;
    // The target time
    TargetTime: Integer;
    // A target entity to send a command to after the motion finishes
    CommandTarget: string;
    // The command to send to CommandTarget
    Command: string;
  end;

  {$IFDEF EDITOR}
  { TWorldBookmark - A bookmark in the world (used by the editor) }
  TWorldBookmark = record
    // Name for the bookmark
    Name: string;
    // Camera position
    CameraPosition: TVector;
    // Camera direction
    CameraDirection: TVector;
  end;

  { TWorldBookmarks - Container for the world bookmarks, this class is
    owned by TWorld in editor builds and is modified by the editor directly }
  TWorldBookmarks = class
  public
    // The bookmarks
    Bookmarks: array of TWorldBookmark;
  end;
  {$ENDIF}

  { TWorldRayHitType - Type of world ray hit }
  TWorldRayHitType = (rhNone, rhEntity, rhGridCell);

  { TWorldRayHitInfo - Information about a ray hit check in the world }
  TWorldRayHitInfo = object
    // Ray hit type
    HitType: TWorldRayHitType;
    // Intersection point
    IP: TVector;
    // Intersection distance (squared)
    Distance: Single;
    // Entity (for rhEntity)
    Entity: TEntity;
    // Entity to ignore (optional)
    IgnoreEntity: TEntity;
    // Ignore all entities
    IgnoreAllEntities: Boolean;
    // Grid ray hit info (for rhGridCell)
    GridHit: TGridMapRayHitInfo;
    // Reset the hit info
    procedure Reset;
  end;

  { TWorld - Game world class }
  TWorld = class(TSerializable)
  private
    // Property storage
    FEntities: TEntityCollection;
    FGridMap: TGridMap;
    FScene: TScene;
    FFogRed: Byte;
    FFogGreen: Byte;
    FFogBlue: Byte;
    FOverbright: Single;
    FTintRed: Byte;
    FTintGreen: Byte;
    FTintBlue: Byte;
    FDataTable: TStringKeyValueSet;
    FDeserializedVersion: Integer;
    FBackgroundColor: Byte;
    FFogColor: Byte;
    {$IFDEF EDITOR}
    FBookmarks: TWorldBookmarks;
    {$ENDIF}
    function GetEntities(AIndex: Integer): TEntity; inline;
    function GetEntityCount: Integer; inline;
    procedure SetScene(AScene: TScene);
    procedure SetFogRed(AFogRed: Byte);
    procedure SetFogGreen(AFogGreen: Byte);
    procedure SetFogBlue(AFogBlue: Byte);
    procedure SetTintRed(ATintRed: Byte);
    procedure SetTintGreen(ATintGreen: Byte);
    procedure SetTintBlue(ATintBlue: Byte);
    procedure SetOverbright(AOverbright: Single);
    procedure SetBackgroundColor(ABackgroundColor: Byte);
    function GetDataTable: PStringKeyValueSet; inline;
  private
    // Entities to delete later
    DeleteLaterEntities: array of TEntity;
    // Active entity motions
    EntityMotions: array of TEntityMotion;
    // The color table needs to be refreshed
    ColorTableNeedsRefresh: Boolean;
    // InitializeEntities was called for this world
    InitializeEntitiesCalled: Boolean;
    // Tinting override is enabled
    TintOverride: Boolean;
    // Tinting override is to be released once the target is reached
    TintOverrideRelease: Boolean;
    // Current and target tint override
    CurrentTintOverride, TargetTintOverride: TVector;
    // Tint override speed
    TargetTintOverrideSpeed: Single;
    // Invalidate the given entity (ensure it isn't referenced, etc)
    procedure InvalidateEntity(AEntity: TEntity);
    // Add an entity motion
    procedure AddEntityMotion(const MotionInfo: TEntityMotion);
    // Remove the given entity motion
    procedure RemoveEntityMotion(MotionIndex: Integer);
    // Update entity motions
    procedure UpdateEntityMotions;
    // Delete entities scheduled to be deleted later
    procedure DeleteScheduledEntities;
    // Update tinting override
    procedure UpdateTintOverride;
  protected
    // TSerializable methods
    procedure BeforeDeserialization; override;
    procedure SerializeExtraData(AContext: TSerializationContext); override;
    procedure DeserializeExtraData(AContext: TSerializationContext); override;
    procedure AfterRootDeserialization; override;
  public
    // Create a world
    constructor Create; override;
    // Destroy the world
    destructor Destroy; override;
    // Add an entity to the world
    procedure AddEntity(AEntity: TEntity);
    // Remove the given entity from the world
    procedure RemoveEntity(AEntity: TEntity);
    // Remove the given entity from the world and delete it
    procedure DeleteEntity(AEntity: TEntity);
    // Remove and delete all entities from the world
    procedure DeleteAllEntities;
    // Remove the given entity from the world and delete it in the next update
    procedure DeleteEntityLater(AEntity: TEntity);
    // Refresh the renderer color palette with this world's settings;
    procedure RefreshColorTable;
    // Remove any world scene nodes
    procedure RemoveSceneNodes;
    // Add world (entities, gridmap) scene nodes
    procedure AddSceneNodes;
    // Find the entity with the given name
    function FindEntity(AName: string): TEntity;
    // Find the closest entity at the given position with the given class
    function FindClosestEntity(const P: TVector; Cls: TClass; MaxDistance: Single=100000): TEntity;
    // Return the floor and ceiling heights closest to the given point
    procedure GetFloorAndCeilingHeightsAt(const V: TVector; out FloorHeight, CeilingHeight: Single);
    // Return the floor and ceiling heights for the given box's corners
    procedure GetFloorAndCeilingHeightForBoxCorners(const Box: TAABox; out FloorHeight, CeilingHeight: Single);
    // Return true if the given box can fit in the world (note: only corners
    // tested for the grid so the box shouldn't be too big)
    function CanBoxFit(const Box: TAABox): Boolean;
    // Check for a ray hit against the world
    function RayHit(const ARay: TRay; var Info: TWorldRayHitInfo): Boolean;
    // Return data for this world or, if not found, game data table
    function GetData(AKey: string; ADefValue: string=''): string;
    // Set tint override using the given speed for the transition updates
    procedure SetTintOverride(ATintR, ATintG, ATintB: Byte; Speed: Single=0.0625);
    // Unset tint override and return to the default tint
    procedure UnsetTintOverride(Speed: Single=0.0625);
    // Return the current tint values
    procedure GetTint(out TintR, TintG, TintB: Byte);
    // Call every ~28 ms to update the world
    procedure Update;
    {$IFDEF EDITOR}
    // Call every ~28 ms to update the world (editor-only)
    procedure EditorUpdate;
    {$ENDIF}
    // Call before rendering
    procedure Frame;
    // Called once to initialize the entities in the world after globals are set
    // but before the game itself is initialized
    procedure InitializeEntities;
    // Run the script entity named 'startup', if one exists - this is called
    // automatically by TGameBase.PlayMap or by the editor when regular play
    // mode is enabled
    procedure RunStartupScript;
  public
    // Scene to use for entities that need to manage scene nodes
    property Scene: TScene read FScene write SetScene;
    // World entities
    property Entities[AIndex: Integer]: TEntity read GetEntities;
    // Number of world entities
    property EntityCount: Integer read GetEntityCount;
    // World data table
    property DataTable: PStringKeyValueSet read GetDataTable;
    // Fog color index (closest palette color to the fog color)
    property FogColor: Byte read FFogColor;
    // The serialization version (valid only after deserialization)
    property DeserializedVersion: Integer read FDeserializedVersion;
    {$IFDEF EDITOR}
    // Editor bookmarks
    property Bookmarks: TWorldBookmarks read FBookmarks;
    {$ENDIF}
  published
    // The world's grid map
    property GridMap: TGridMap read FGridMap;
    // Fog color red component
    property FogRed: Byte read FFogRed write SetFogRed;
    // Fog color green component
    property FogGreen: Byte read FFogGreen write SetFogGreen;
    // Fog color blue component
    property FogBlue: Byte read FFogBlue write SetFogBlue;
    // Tint color red component
    property TintRed: Byte read FTintRed write SetTintRed;
    // Tint color green component
    property TintGreen: Byte read FTintGreen write SetTintGreen;
    // Tint color blue component
    property TintBlue: Byte read FTintBlue write SetTintBlue;
    // Overbright value
    property Overbright: Single read FOverbright write SetOverbright;
    // The color to use for the background (0=fog color)
    property BackgroundColor: Byte read FBackgroundColor write SetBackgroundColor;
  end;

implementation

uses
  SysUtils, Engine, Renderer, GameBase, Scripts, ScrEnt, DataLoad;

type
  { TBoxEntitySceneNode - Scene node for a box entity }
  TBoxEntitySceneNode = class(TSceneNode)
  private
    // The box entity that owns this node
    Box: TBoxEntity;
  protected
    // TSceneNode methods
    procedure RenderNode; override;
    function GetOwnLocalAABox: TAABox; override;
  end;

{ TBoxEntitySceneNode }
procedure TBoxEntitySceneNode.RenderNode;
var
  Tri: PRenderTri;
  MinA, MinB, MinC, MinD, MaxA, MaxB, MaxC, MaxD: TVector;

  procedure AddQuad(const A, B, C, D: TVector; Texture: TTexture);
  var
    LA, LB, LC, LD: Byte;
  begin
    LA:=Round(Box.World.GridMap.LightAt(A)*(0.45 + Clamp(TriangleNormal(A, B, C).Dot(Vector(1, 3, 2).Normalized))*0.55));
    LB:=Round(Box.World.GridMap.LightAt(B)*(0.45 + Clamp(TriangleNormal(A, B, C).Dot(Vector(1, 3, 2).Normalized))*0.55));
    LC:=Round(Box.World.GridMap.LightAt(C)*(0.45 + Clamp(TriangleNormal(A, B, C).Dot(Vector(1, 3, 2).Normalized))*0.55));
    LD:=Round(Box.World.GridMap.LightAt(D)*(0.45 + Clamp(TriangleNormal(A, B, C).Dot(Vector(1, 3, 2).Normalized))*0.55));
    Tri:=GRenderer.AllocRenderTriangle;
    Tri^.A:=A;
    Tri^.SA:=63;
    Tri^.TA:=0;
    Tri^.B:=B;
    Tri^.SB:=0;
    Tri^.TB:=0;
    Tri^.C:=C;
    Tri^.SC:=0;
    Tri^.TC:=63;
    Tri^.LA:=LA;
    Tri^.LB:=LB;
    Tri^.LC:=LC;
    Tri^.Texture:=Texture;
    Tri^.DepthBias:=Box.DepthBias;
    Tri:=GRenderer.AllocRenderTriangle;
    Tri^.A:=A;
    Tri^.SA:=63;
    Tri^.TA:=0;
    Tri^.B:=C;
    Tri^.SB:=0;
    Tri^.TB:=63;
    Tri^.C:=D;
    Tri^.SC:=63;
    Tri^.TC:=63;
    Tri^.LA:=LA;
    Tri^.LB:=LC;
    Tri^.LC:=LD;
    Tri^.Texture:=Texture;
    Tri^.DepthBias:=Box.DepthBias;
  end;

  procedure AddMultiQuad(const A, B, C, D: TVector; Texture: TTexture);
  var
    AB, BC: TVector;
    H, V, HSplits, VSplits: Integer;
  begin
    HSplits:=Trunc(Distance(A, B)/2);
    VSplits:=Trunc(Distance(B, C)/2);
    if (HSplits < 2) and (VSplits < 2) then begin
      AddQuad(A, B, C, D, Texture);
      Exit;
    end;
    if HSplits=0 then HSplits:=1;
    if VSplits=0 then VSplits:=1;
    AB:=B.Subbed(A).Scaled(1/HSplits);
    BC:=C.Subbed(B).Scaled(1/VSplits);
    for V:=0 to VSplits - 1 do
      for H:=0 to HSplits - 1 do
        AddQuad(
          A.Added(AB.Scaled(H)).Added(BC.Scaled(V)),
          A.Added(AB.Scaled(H + 1)).Added(BC.Scaled(V)),
          A.Added(AB.Scaled(H + 1)).Added(BC.Scaled(V + 1)),
          A.Added(AB.Scaled(H)).Added(BC.Scaled(V + 1)),
          Texture);
  end;

begin
  // Note: all of the below are slow for doing them every frame, in the future
  // it should be a good idea to precalculate the geometry (and perhaps create
  // a new entity base class for entities that generate geometry at map load or
  // even some separate class so that it can be used by the gridmap too)
  GetOwnLocalAABox.GetCorners(MinA, MinB, MinC, MinD, MaxA, MaxB, MaxC, MaxD);
  WorldMatrix.Transform(MinA);
  WorldMatrix.Transform(MinB);
  WorldMatrix.Transform(MinC);
  WorldMatrix.Transform(MinD);
  WorldMatrix.Transform(MaxA);
  WorldMatrix.Transform(MaxB);
  WorldMatrix.Transform(MaxC);
  WorldMatrix.Transform(MaxD);
  if (tbsBottom in Box.DrawSides) and (Box.BottomTexture <> 0) then
    AddMultiQuad(MinA, MinB, MinD, MinC, TTexture(GResourceManager.WallTextures[Box.BottomTexture]));
  if (tbsTop in Box.DrawSides) and (Box.TopTexture <> 0) then
    AddMultiQuad(MaxB, MaxA, MaxC, MaxD, TTexture(GResourceManager.WallTextures[Box.TopTexture]));
  if Box.SideTexture <> 0 then begin
    if tbsLeft in Box.DrawSides then
      AddMultiQuad(MaxC, MaxA, MinA, MinC, TTexture(GResourceManager.WallTextures[Box.SideTexture]));
    if tbsFront in Box.DrawSides then
      AddMultiQuad(MaxD, MaxC, MinC, MinD, TTexture(GResourceManager.WallTextures[Box.SideTexture]));
    if tbsRight in Box.DrawSides then
      AddMultiQuad(MaxB, MaxD, MinD, MinB, TTexture(GResourceManager.WallTextures[Box.SideTexture]));
    if tbsBack in Box.DrawSides then
      AddMultiQuad(MaxA, MaxB, MinB, MinA, TTexture(GResourceManager.WallTextures[Box.SideTexture]));
  end;
end;

function TBoxEntitySceneNode.GetOwnLocalAABox: TAABox;
begin
  Result.Min.Zero;
  Result.Max:=Result.Min;
  Result.Max.X += Box.Width;
  Result.Max.Y += Box.Height;
  Result.Max.Z += Box.Length;
end;

{ TEntity }
destructor TEntity.Destroy;
begin
  if Assigned(World) then World.RemoveEntity(Self);
  inherited Destroy;
end;

procedure TEntity.SetTransform(const ATransform: TTransform);
begin
  if ATransform=FTransform then Exit;
  FTransform:=ATransform;
  TransformChanged;
end;

procedure TEntity.SetPositionX(AValue: Single);
begin
  if AValue=FTransform.Translation.X then Exit;
  FTransform.Translation.X:=AValue;
  TransformChanged;
end;

procedure TEntity.SetPositionY(AValue: Single);
begin
  if AValue=FTransform.Translation.Y then Exit;
  FTransform.Translation.Y:=AValue;
  TransformChanged;
end;

procedure TEntity.SetPositionZ(AValue: Single);
begin
  if AValue=FTransform.Translation.Z then Exit;
  FTransform.Translation.Z:=AValue;
  TransformChanged;
end;

procedure TEntity.SetRotationX(AValue: Single);
begin
  AValue:=AValue*PI/180;
  if AValue=FTransform.Rotation.X then Exit;
  FTransform.Rotation.X:=AValue;
  TransformChanged;
end;

procedure TEntity.SetRotationY(AValue: Single);
begin
  AValue:=AValue*PI/180;
  if AValue=FTransform.Rotation.Y then Exit;
  FTransform.Rotation.Y:=AValue;
  TransformChanged;
end;

procedure TEntity.SetRotationZ(AValue: Single);
begin
  AValue:=AValue*PI/180;
  if AValue=FTransform.Rotation.Z then Exit;
  FTransform.Rotation.Z:=AValue;
  TransformChanged;
end;

function TEntity.GetPositionX: Single;
begin
  Result:=FTransform.Translation.X;
end;

function TEntity.GetPositionY: Single;
begin
  Result:=FTransform.Translation.Y;
end;

function TEntity.GetPositionZ: Single;
begin
  Result:=FTransform.Translation.Z;
end;

function TEntity.GetRotationX: Single;
begin
  Result:=FTransform.Rotation.X*180/PI;
end;

function TEntity.GetRotationY: Single;
begin
  Result:=FTransform.Rotation.Y*180/PI;
end;

function TEntity.GetRotationZ: Single;
begin
  Result:=FTransform.Rotation.Z*180/PI;
end;

function TEntity.GetScene: TScene;
begin
  Result:=World.Scene;
end;

procedure TEntity.AddedToWorld;
begin
end;

procedure TEntity.RemovedFromWorld;
begin
end;

procedure TEntity.AddSceneNodes;
begin
end;

procedure TEntity.RemoveSceneNodes;
begin
end;

procedure TEntity.TransformChanged;
begin
end;

procedure TEntity.Update;
begin
end;

{$IFDEF EDITOR}
procedure TEntity.EditorUpdate;
begin
end;
{$ENDIF}

procedure TEntity.Frame;
begin
end;

procedure TEntity.Initialize;
begin
end;

function TEntity.GetAABox: TAABox;
begin
  Result:=Maths.AABox(Transform.Translation, Transform.Translation);
end;

function TEntity.GetCollisionBox: TAABox;
begin
  Result:=AABox;
end;

function TEntity.RayHit(const ARay: TRay; out IP: TVector): Boolean;
begin
  Result:=ARay.AABoxHit(CollisionBox, IP);
end;

function TEntity.GetData(AKey: string; ADefValue: string): string;
begin
  Result:=World.GetData(AKey, ADefValue);
end;

function TEntity.PerformCommand(ACommand: string): Boolean;
var
  Script: TScript;
begin
  // If the command starts with a ! then treat it as script code
  if (Length(ACommand) > 0) and (ACommand[1]='!') then begin
    // Create temporary script engine
    Script:=TScript.Create;
    Script.SetVar('me', Name);
    Script.RunCode(Copy(ACommand, 2, Length(ACommand)));
    Script.Free;
    Exit(True);
  end;
  Result:=False;
end;

procedure TEntity.ScheduleMotion(ATransform: TTransform; ATargetTime: Integer;
  ATarget, ACommand: string);
var
  EM: TEntityMotion;
begin
  with EM do begin
    Entity:=Self;
    InitialTransform:=Transform;
    TargetTransform:=ATransform;
    InitialTime:=GGame.Milliseconds;
    TargetTime:=ATargetTime;
    CommandTarget:=ATarget;
    Command:=ACommand;
  end;
  World.AddEntityMotion(EM);
end;

procedure TEntity.ScheduleMotion(AEntity: TEntity; ATargetTime: Integer;
  ATarget, ACommand: string);
begin
  ScheduleMotion(AEntity.Transform, ATargetTime, ATarget, ACommand);
end;

procedure TSceneNodeEntity.SetHeightOffset(AOffset: Single);
begin
  if FHeightOffset=AOffset then Exit;
  FHeightOffset:=AOffset;
  UpdateSceneNodeMatrix;
end;

{ TSceneNodeEntity }
function TSceneNodeEntity.CreateSceneNode: TSceneNode;
begin
  Result:=nil;
end;

procedure TSceneNodeEntity.UpdateSceneNodeMatrix;
var
  Matrix: TMatrix;
begin
  if Assigned(FSceneNode) then begin
    Matrix:=Transform.ToMatrix;
    Matrix.M24 += HeightOffset;
    FSceneNode.LocalMatrix:=Matrix;
  end;
end;

procedure TSceneNodeEntity.AddedToWorld;
begin
  FSceneNode:=CreateSceneNode;
end;

procedure TSceneNodeEntity.RemovedFromWorld;
begin
  FreeAndNil(FSceneNode);
end;

procedure TSceneNodeEntity.AddSceneNodes;
begin
  if Assigned(FSceneNode) then begin
    if FSceneNode.Parent=nil then Scene.AddChild(FSceneNode);
    UpdateSceneNodeMatrix;
  end;
end;

procedure TSceneNodeEntity.RemoveSceneNodes;
begin
  if Assigned(FSceneNode) and (FSceneNode.Parent=Scene) then Scene.RemoveChild(FSceneNode);
end;

procedure TSceneNodeEntity.TransformChanged;
begin
  UpdateSceneNodeMatrix;
end;

function TSceneNodeEntity.GetAABox: TAABox;
begin
  if Assigned(SceneNode) then
    Result:=SceneNode.AABox
  else
    Result:=inherited GetAABox;
end;

function TSceneNodeEntity.RayHit(const ARay: TRay; out IP: TVector): Boolean;
begin
  if Assigned(SceneNode) then
    Result:=ARay.AABoxHit(SceneNode.AABox, IP)
  else
    Result:=False;
end;

{ TStaticMeshEntity }
constructor TStaticMeshEntity.Create;
begin
  inherited Create;
  FLightingType:=smltPerVertex;
  FSolid:=True;
  FDepthBias:=10;
  SetMeshName('box.jtf');
end;

procedure TStaticMeshEntity.SetMeshName(AMeshName: string);
begin
  if FMeshName=AMeshName then Exit;
  FMeshName:=AMeshName;
  UpdateMesh;
end;

procedure TStaticMeshEntity.SetTextureName(ATextureName: string);
begin
  if FTextureName=ATextureName then Exit;
  FTextureName:=ATextureName;
  UpdateMesh;
end;

procedure TStaticMeshEntity.SetDepthBias(ADepthBias: Byte);
begin
  if FDepthBias=ADepthBias then Exit;
  FDepthBias:=ADepthBias;
  if Assigned(SceneNode) then TMeshNode(SceneNode).DepthBias:=FDepthBias;
end;

procedure TStaticMeshEntity.SetOcclusionBehavior(AOcclusionBehavior: TStaticMeshOcclusionBehavior);
begin
  if FOcclusionBehavior=AOcclusionBehavior then Exit;
  FOcclusionBehavior:=AOcclusionBehavior;
  UpdateMeshOcclusion(SceneNode);
end;

procedure TStaticMeshEntity.UpdateMesh;
var
  TextureFileName: string;
begin
  if TextureName='' then
    TextureFileName:=ChangeFileExt(MeshName, '.raw')
  else
    TextureFileName:=TextureName;
  if not HasDataFile(DataDirectoryPrefix + TextureFileName) then TextureFileName:='error.raw';
  FTexture:=TTexture(GResourceManager.GetResource(TextureFileName));
  FMesh:=TMesh(GResourceManager.GetResource(MeshName));
  if Assigned(SceneNode) then begin
    TMeshNode(SceneNode).DepthBias:=FDepthBias;
    TMeshNode(SceneNode).Mesh:=FMesh;
    TMeshNode(SceneNode).TextureOverride:=FTexture;
    UpdateMeshOcclusion(SceneNode);
  end;
end;

procedure TStaticMeshEntity.UpdateMeshOcclusion(ASceneNode: TSceneNode);
begin
  if Assigned(ASceneNode) then begin
    TMeshNode(ASceneNode).Occluder:=OcclusionBehavior=smobOccluder;
    TMeshNode(ASceneNode).TestOcclusion:=OcclusionBehavior=smobOcclusionCheck;
  end;
end;

function TStaticMeshEntity.CreateSceneNode: TSceneNode;
begin
  Result:=TMeshNode.Create;
  TMeshNode(Result).Mesh:=Mesh;
  TMeshNode(Result).DepthBias:=DepthBias;
  TMeshNode(Result).TextureOverride:=FTexture;
  UpdateMeshOcclusion(Result);
  UpdateLighting(Result);
end;

procedure TStaticMeshEntity.UpdateLighting(ASceneNode: TSceneNode);
begin
  if Assigned(Mesh) and Assigned(ASceneNode) then begin
    if Length(Lighting)=1 then begin
      TMeshNode(ASceneNode).Lights:=nil;
      TMeshNode(ASceneNode).Light:=Lighting[0];
    end else if Length(Lighting)=Mesh.TriangleCount*3 then begin
      TMeshNode(ASceneNode).Lights:=@Lighting[0];
      TMeshNode(ASceneNode).Light:=255;
    end else begin
      TMeshNode(ASceneNode).Lights:=nil;
      TMeshNode(ASceneNode).Light:=255;
    end;
  end;
end;

procedure TStaticMeshEntity.SerializeExtraData(AContext: TSerializationContext);
begin
  inherited SerializeExtraData(AContext);
  // Serialize lighting
  AContext.WriteInt16(Length(Lighting));
  if Length(Lighting) > 0 then AContext.Write(Lighting[0], Length(Lighting));
end;

procedure TStaticMeshEntity.DeserializeExtraData(AContext: TSerializationContext);
begin
  inherited DeserializeExtraData(AContext);
  // Deserialize lighting
  if AContext.Version > 1 then begin
    SetLength(Lighting, AContext.ReadInt16);
    if Length(Lighting) > 0 then AContext.Read(Lighting[0], Length(Lighting));
  end;
end;

procedure TStaticMeshEntity.SetLighting(ALighting: array of Byte);
begin
  SetLength(Lighting, Length(ALighting));
  if Length(ALighting) > 0 then Move(ALighting[0], Lighting[0], Length(Lighting));
  UpdateLighting(SceneNode);
end;

{ TPieceModelEntity }
procedure TPieceModelEntity.SetDepthBias(ADepthBias: Byte);
begin
  if FDepthBias=ADepthBias then Exit;
  FDepthBias:=ADepthBias;
  if Assigned(SceneNode) then TPieceModelNode(SceneNode).Root.ApplyDepthBias(DepthBias);
end;

procedure TPieceModelEntity.SetAnimationName(AAnimationName: string);
begin
  if FAnimationName=AAnimationName then Exit;
  FAnimationName:=AAnimationName;
  if Assigned(SceneNode) then SetupAnimation;
end;

procedure TPieceModelEntity.SetModelName(AModelName: string);
begin
  if FModelName=AModelName then Exit;
  FModelName:=AModelName;
  if Assigned(World) then UpdateModel;
end;

procedure TPieceModelEntity.UpdateModel;
begin
  // Ignore if we're about to reload the same model
  if Assigned(FPieceModel) and (FPieceModel.Name=LowerCase(ModelName)) then Exit;
  // Remove existing scene node
  if Assigned(SceneNode) then begin
    RemoveSceneNodes;
    FreeAndNil(FSceneNode);
  end;
  // Load piece model
  FPieceModel:=TPieceModel(GResourceManager.GetResource(ModelName));
  // Invalidate collision box
  CurrentCollisionBox.MakeMaxInverted;
  // Create new scene node
  if Assigned(FPieceModel) then begin
    FSceneNode:=CreateSceneNode;
    AddSceneNodes;
  end;
end;

procedure TPieceModelEntity.SetupAnimation;
begin
  if Assigned(PieceModel) then begin
    // Find the animation in the model
    FAnimIndex:=PieceModel.IndexOfAnimation(AnimationName);
    if FAnimIndex <> -1 then begin
      AnimTime:=0;
      FLastFrame:=PieceModel.ApplyAnimation(AnimIndex, 0, TPieceModelNode(SceneNode), 1);
    end else begin
      AnimTime:=0;
      FLastFrame:=-1;
    end;
  end else begin
    FAnimIndex:=-1;
    FLastFrame:=-1;
    AnimTime:=0;
  end;
  // Invalidate collision box
  CurrentCollisionBox.MakeMaxInverted;
end;

function TPieceModelEntity.UpdateAnimation: Boolean;
begin
  Result:=False;
  // Ignore if the animation is paused
  if Paused then Exit;
  // Only update if there is a scene node and active animation
  if Assigned(SceneNode) and (AnimIndex <> -1) then begin
    // Advance animation time
    Inc(AnimTime, 14);
    // Apply frame
    FLastFrame:=PieceModel.ApplyAnimation(AnimIndex, AnimTime/1000, TPieceModelNode(SceneNode), 1);
    // Animation ended?
    if FLastFrame=-1 then begin
      // Reset animation
      FAnimIndex:=-1;
      AnimTime:=0;
      // Invalidate collision box
      CurrentCollisionBox.MakeMaxInverted;
      // Inform subclasses
      Result:=True;
    end;
  end;
end;

procedure TPieceModelEntity.AddSceneNodes;
begin
  inherited AddSceneNodes;
  // Setup lighting
  if Assigned(SceneNode) and Assigned(World) then begin
    TPieceModelNode(SceneNode).Root.ApplyLightLevel(World.GridMap.LightAt(Transform.Translation));
  end;
  // Invalidate collision box
  CurrentCollisionBox.MakeMaxInverted;
end;

procedure TPieceModelEntity.TransformChanged;
begin
  inherited TransformChanged;
  // Update lighting
  if Assigned(SceneNode) and Assigned(World) then begin
    TPieceModelNode(SceneNode).Root.ApplyLightLevel(World.GridMap.LightAt(Transform.Translation));
  end;
  // Invalidate collision box
  CurrentCollisionBox.MakeMaxInverted;
end;

procedure TPieceModelEntity.Update;
begin
  inherited Update;
  if UpdateAnimation then AnimationFinished;
end;

{$IFDEF EDITOR}
procedure TPieceModelEntity.EditorUpdate;
begin
  inherited EditorUpdate;
  if GUpdateEditorAnimations then UpdateAnimation;
end;
{$ENDIF}

procedure TPieceModelEntity.Initialize;
begin
  SetupAnimation;
end;

function TPieceModelEntity.GetCollisionBox: TAABox;
begin
  if not CurrentCollisionBox.IsValid then CurrentCollisionBox:=AABox;
  Result:=CurrentCollisionBox;
end;

function TPieceModelEntity.CreateSceneNode: TSceneNode;
begin
  if not Assigned(PieceModel) then UpdateModel;
  if Assigned(PieceModel) and not Assigned(SceneNode) then begin
    Result:=PieceModel.CreateSceneNode;
    TPieceModelNode(Result).Root.ApplyDepthBias(DepthBias);
  end else
    Result:=SceneNode;
end;

procedure TPieceModelEntity.AnimationFinished;
begin
end;

procedure TPieceModelEntity.RestartAnimation;
begin
  if AnimIndex <> -1 then begin
    AnimTime:=0;
    FLastFrame:=PieceModel.ApplyAnimation(AnimIndex, 0, TPieceModelNode(SceneNode), 1);
    CurrentCollisionBox.MakeMaxInverted;
  end;
end;

{ TBoxEntity }
constructor TBoxEntity.Create;
begin
  inherited Create;
  FWidth:=1;
  FHeight:=1;
  FLength:=1;
  FDepthBias:=10;
  FDrawSides:=[tbsTop, tbsBottom, tbsLeft, tbsRight, tbsFront, tbsBack];
  FTopTexture:=1;
  FBottomTexture:=1;
  FSideTexture:=1;
end;

procedure TBoxEntity.SetBottomTexture(const ABottomTexture: Byte);
begin
  if FBottomTexture=ABottomTexture then Exit;
  FBottomTexture:=ABottomTexture;
end;

procedure TBoxEntity.SetDepthBias(const ADepthBias: Byte);
begin
  if FDepthBias=ADepthBias then Exit;
  FDepthBias:=ADepthBias;
end;

procedure TBoxEntity.SetDrawSides(const ADrawSides: TBoxEntitySides);
begin
  if FDrawSides=ADrawSides then Exit;
  FDrawSides:=ADrawSides;
end;

procedure TBoxEntity.SetHeight(const AHeight: Single);
begin
  if FHeight=AHeight then Exit;
  FHeight:=AHeight;
end;

procedure TBoxEntity.SetLength(const ALength: Single);
begin
  if FLength=ALength then Exit;
  FLength:=ALength;
end;

procedure TBoxEntity.SetSideTexture(const ASideTexture: Byte);
begin
  if FSideTexture=ASideTexture then Exit;
  FSideTexture:=ASideTexture;
end;

procedure TBoxEntity.SetTopTexture(const ATopTexture: Byte);
begin
  if FTopTexture=ATopTexture then Exit;
  FTopTexture:=ATopTexture;
end;

procedure TBoxEntity.SetWidth(const AWidth: Single);
begin
  if FWidth=AWidth then Exit;
  FWidth:=AWidth;
end;

function TBoxEntity.CreateSceneNode: TSceneNode;
begin
  Result:=TBoxEntitySceneNode.Create;
  TBoxEntitySceneNode(Result).Box:=Self;
end;

{ TLight }
constructor TLight.Create;
begin
  inherited Create;
  FRadius:=10;
  FCastShadows:=True;
  FLight:=1.0;
end;

{$IFDEF EDITOR}
procedure TLight.Frame;
begin
  if GPlayMode=pmEditor then GRenderer.RenderAABox(AABox);
end;
{$ENDIF}

function TLight.GetAABox: TAABox;
begin
  Result:=inherited GetAABox;
  Result.Grow(0.2);
end;

{ TDummyEntity }
{$IFDEF EDITOR}
procedure TDummyEntity.Frame;
begin
  if GPlayMode=pmEditor then GRenderer.RenderAABox(AABox);
end;

function TDummyEntity.GetAABox: TAABox;
begin
  Result:=inherited GetAABox;
  Result.Grow(0.05);
end;
{$ENDIF}

{ TWorldRayHitInfo }
procedure TWorldRayHitInfo.Reset;
begin
  {$push}{$warn 5058 off}
  FillChar(Self, SizeOf(TWorldRayHitInfo), 0);
  {$pop}
  GridHit.Reset;
end;

{ TWorld }
constructor TWorld.Create;
begin
  inherited Create;
  FTintRed:=255;
  FTintGreen:=255;
  FTintBlue:=255;
  FOverbright:=1.0;
  // Create the entities collection
  FEntities:=TEntityCollection.Create;
  // Create the grid map
  FGridMap:=TGridMap.Create;
  // Create bookmarks
  {$IFDEF EDITOR}
  FBookmarks:=TWorldBookmarks.Create;
  {$ENDIF}
end;

destructor TWorld.Destroy;
begin
  // Delete bookmarks
  {$IFDEF EDITOR}
  FBookmarks.Free;
  {$ENDIF}
  // Remove any scene nodes
  RemoveSceneNodes;
  // Delete all entities
  DeleteScheduledEntities;
  DeleteAllEntities;
  // Destroy the grid map
  FGridMap.Free;
  // Destroy the entities collection
  FEntities.Free;
  inherited Destroy;
end;

function TWorld.GetEntities(AIndex: Integer): TEntity;
begin
  Result:=FEntities[AIndex];
end;

function TWorld.GetEntityCount: Integer;
begin
  Result:=FEntities.Count;
end;

procedure TWorld.SetScene(AScene: TScene);
begin
  if FScene=AScene then Exit;
  // Remove any existing scene nodes
  RemoveSceneNodes;
  // Update scene
  FScene:=AScene;
  // Add scene nodes to the new scene
  AddSceneNodes;
end;

procedure TWorld.SetFogRed(AFogRed: Byte);
begin
  if FFogRed=AFogRed then Exit;
  FFogRed:=AFogRed;
  {$IFDEF EDITOR}
  RefreshColorTable;
  {$ELSE}
  ColorTableNeedsRefresh:=True;
  {$ENDIF}
end;

procedure TWorld.SetFogGreen(AFogGreen: Byte);
begin
  if FFogGreen=AFogGreen then Exit;
  FFogGreen:=AFogGreen;
  {$IFDEF EDITOR}
  RefreshColorTable;
  {$ELSE}
  ColorTableNeedsRefresh:=True;
  {$ENDIF}
end;

procedure TWorld.SetFogBlue(AFogBlue: Byte);
begin
  if FFogBlue=AFogBlue then Exit;
  FFogBlue:=AFogBlue;
  {$IFDEF EDITOR}
  RefreshColorTable;
  {$ELSE}
  ColorTableNeedsRefresh:=True;
  {$ENDIF}
end;

procedure TWorld.SetTintRed(ATintRed: Byte);
begin
  if FTintRed=ATintRed then Exit;
  FTintRed:=ATintRed;
  {$IFDEF EDITOR}
  RefreshColorTable;
  {$ELSE}
  ColorTableNeedsRefresh:=True;
  {$ENDIF}
end;

procedure TWorld.SetTintGreen(ATintGreen: Byte);
begin
  if FTintGreen=ATintGreen then Exit;
  FTintGreen:=ATintGreen;
  {$IFDEF EDITOR}
  RefreshColorTable;
  {$ELSE}
  ColorTableNeedsRefresh:=True;
  {$ENDIF}
end;

procedure TWorld.SetTintBlue(ATintBlue: Byte);
begin
  if FTintBlue=ATintBlue then Exit;
  FTintBlue:=ATintBlue;
  {$IFDEF EDITOR}
  RefreshColorTable;
  {$ELSE}
  ColorTableNeedsRefresh:=True;
  {$ENDIF}
end;

procedure TWorld.SetOverbright(AOverbright: Single);
begin
  if FOverbright=AOverbright then Exit;
  FOverbright:=AOverbright;
  RefreshColorTable;
end;

procedure TWorld.SetBackgroundColor(ABackgroundColor: Byte);
begin
  if FBackgroundColor=ABackgroundColor then Exit;
  FBackgroundColor:=ABackgroundColor;
end;

function TWorld.GetDataTable: PStringKeyValueSet;
begin
  Result:=@FDataTable;
end;

procedure TWorld.InvalidateEntity(AEntity: TEntity);
var
  I: Integer;
begin
  // Check entity motions
  for I:=0 to High(EntityMotions) do
    if EntityMotions[I].Entity=AEntity then begin
      RemoveEntityMotion(I);
      Break;
    end;
end;

procedure TWorld.AddEntityMotion(const MotionInfo: TEntityMotion);
var
  I: Integer;
begin
  // Check if there is already a motion for the given entity
  for I:=0 to High(EntityMotions) - 1 do
    if EntityMotions[I].Entity=MotionInfo.Entity then begin
      EntityMotions[I]:=MotionInfo;
      Exit;
    end;
  // Add new motion
  SetLength(EntityMotions, Length(EntityMotions) + 1);
  EntityMotions[High(EntityMotions)]:=MotionInfo;
end;

procedure TWorld.RemoveEntityMotion(MotionIndex: Integer);
var
  I: Integer;
begin
  for I:=MotionIndex to High(EntityMotions) - 1 do
    EntityMotions[I]:=EntityMotions[I + 1];
  SetLength(EntityMotions, Length(EntityMotions) - 1);
end;

procedure TWorld.UpdateEntityMotions;
var
  Now, I: Integer;
  CmdTarget: TEntity;
begin
  Now:=GGame.Milliseconds;
  I:=High(EntityMotions);
  while I >= 0 do with EntityMotions[I] do begin
    // Check if the motion ended
    if Now >= TargetTime then begin
      // Apply target transform
      Entity.Transform:=TargetTransform;
      // Send command to the target entity, if specified
      if CommandTarget <> '' then begin
        CmdTarget:=FindEntity(CommandTarget);
        if Assigned(CmdTarget) then
          CmdTarget.PerformCommand(Command);
      end;
      // Delete this motion
      RemoveEntityMotion(I);
    end else begin
      // Calculate new transform
      Entity.Transform:=Interpolate(
        InitialTransform,
        TargetTransform,
        (Now - InitialTime)/(TargetTime - InitialTime)
      );
    end;
    Dec(I);
  end;
end;

procedure TWorld.DeleteScheduledEntities;
var
  I: Integer;
begin
  for I:=0 to High(DeleteLaterEntities) do
    DeleteLaterEntities[I].Free;
  DeleteLaterEntities:=nil;
end;

procedure TWorld.UpdateTintOverride;
begin
  // Update tint override if enabled
  if TintOverride then begin
    CurrentTintOverride:=Interpolate(CurrentTintOverride, TargetTintOverride, TargetTintOverrideSpeed);
    if TintOverrideRelease then begin
      if (Round(CurrentTintOverride.X*255)=TintRed) and
         (Round(CurrentTintOverride.Y*255)=TintGreen) and
         (Round(CurrentTintOverride.Z*255)=TintBlue) then
        TintOverride:=False;
    end;
  end;
end;

procedure TWorld.BeforeDeserialization;
begin
  inherited BeforeDeserialization;
  // Remove any scene nodes
  RemoveSceneNodes;
  // Delete all entities
  DeleteAllEntities;
  // Destroy the gridmap since a new one will be read in
  FreeAndNil(FGridMap);
end;

procedure TWorld.SerializeExtraData(AContext: TSerializationContext);
{$IFDEF EDITOR}
var
  I: Integer;
{$ENDIF}
begin
  inherited SerializeExtraData(AContext);
  // Serialize world data
  AContext.WriteStringKeyValueSet(FDataTable);
  // Serialize entities
  AContext.WriteObject(FEntities);
  {$IFDEF EDITOR}
  // Serialize bookmarks
  AContext.WriteInt16(Length(Bookmarks.Bookmarks));
  for I:=0 to High(Bookmarks.Bookmarks) do with Bookmarks.Bookmarks[I] do begin
    AContext.WriteAnsiString(Name);
    AContext.WriteSingle(CameraPosition.X);
    AContext.WriteSingle(CameraPosition.Y);
    AContext.WriteSingle(CameraPosition.Z);
    AContext.WriteSingle(CameraDirection.X);
    AContext.WriteSingle(CameraDirection.Y);
    AContext.WriteSingle(CameraDirection.Z);
  end;
  {$ENDIF}
end;

procedure TWorld.DeserializeExtraData(AContext: TSerializationContext);
{$IFDEF EDITOR}
var
  I: Integer;
{$ENDIF}
begin
  inherited DeserializeExtraData(AContext);
  // Save version
  FDeserializedVersion:=AContext.Version;
  // Deserialize world data
  if AContext.Version >= 4 then
    AContext.ReadStringKeyValueSet(FDataTable)
  else
    FDataTable.Clear;
  // If the root object for the serialization is a game, ensure we have
  // the proper scene instance before the entities are deserialized and
  // decide to create scene node
  if AContext.RootObject is TGameBase then
    FScene:=TGameBase(AContext.RootObject).Scene;
  // Deserialize entities
  DeleteAllEntities;
  FEntities.Free;
  FEntities:=TEntityCollection(AContext.ReadObject(TEntityCollection));
  // Deserialize bookmarks
  {$IFDEF EDITOR}
  if AContext.Version >= 5 then begin
    SetLength(Bookmarks.Bookmarks, AContext.ReadInt16);
    for I:=0 to High(Bookmarks.Bookmarks) do with Bookmarks.Bookmarks[I] do begin
      Name:=AContext.ReadAnsiString;
      CameraPosition.X:=AContext.ReadSingle;
      CameraPosition.Y:=AContext.ReadSingle;
      CameraPosition.Z:=AContext.ReadSingle;
      CameraDirection.X:=AContext.ReadSingle;
      CameraDirection.Y:=AContext.ReadSingle;
      CameraDirection.Z:=AContext.ReadSingle;
    end;
  end else SetLength(Bookmarks.Bookmarks, 0);
  {$ENDIF}
end;

procedure TWorld.AfterRootDeserialization;
var
  I: Integer;
begin
  inherited AfterRootDeserialization;
  // Update entities
  for I:=0 to FEntities.Count - 1 do begin
    FEntities[I].FWorld:=Self;
    FEntities[I].AddedToWorld;
    FEntities[I].TransformChanged;
  end;
  // At this point we should have a scene set up
  if Assigned(Scene) then begin
    AddSceneNodes;
    Scene.UpdateWorldMatrix(True);
    Scene.UpdateAABox;
  end;
  // Refresh the color table with this world's settings
  RefreshColorTable;
  // When loading older worlds, make sure the scene is set in "always"
  // visibility mode since there are no portals to use for visibility
  if DeserializedVersion < 7 then Scene.VisibilityMode:=svmAlways;
end;

procedure TWorld.AddEntity(AEntity: TEntity);
begin
  // Register the entity
  FEntities.Add(AEntity);
  AEntity.FWorld:=Self;
  // Notify the entity that it was added to the world
  AEntity.AddedToWorld;
  // If we have a scene, ask the entity to create its scene enodes
  if Assigned(Scene) then AEntity.AddSceneNodes;
end;

procedure TWorld.RemoveEntity(AEntity: TEntity);
begin
  // If we have a scene, ask the entity to remove its scene enodes
  if Assigned(Scene) then AEntity.RemoveSceneNodes;
  // Unregister the entity
  FEntities.Remove(AEntity);
  // Invalidate the entity
  InvalidateEntity(AEntity);
  // Notify the entity that it was removed
  AEntity.RemovedFromWorld;
  // Update the entity's world reference as a last step so that it can still use
  // it in the notification above
  AEntity.FWorld:=nil;
end;

procedure TWorld.DeleteEntity(AEntity: TEntity);
begin
  RemoveEntity(AEntity);
  AEntity.Free;
end;

procedure TWorld.DeleteAllEntities;
begin
  while FEntities.Count > 0 do DeleteEntity(FEntities.Last);
end;

procedure TWorld.DeleteEntityLater(AEntity: TEntity);
var
  I: Integer;
begin
  // Remove the entity from the world
  if FEntities.IndexOf(AEntity) <> -1 then RemoveEntity(AEntity);
  // Check if the entity is already in the delete later list
  for I:=High(DeleteLaterEntities) downto 0 do
    if DeleteLaterEntities[I]=AEntity then Exit;
  // Add the entity to the list
  SetLength(DeleteLaterEntities, Length(DeleteLaterEntities) + 1);
  DeleteLaterEntities[High(DeleteLaterEntities)]:=AEntity;
end;

procedure TWorld.RefreshColorTable;
begin
  GRenderer.ColorPalette.BuildLightTable(
    FogRed, FogGreen, FogBlue,
    Overbright,
    Vector(1, 1, 1));
    //Vector(TintRed/255, TintGreen/255, TintBlue/255));
  FFogColor:=GRenderer.ColorPalette.GetClosestColor(FogRed, FogGreen, FogBlue);
  ColorTableNeedsRefresh:=False;
end;

procedure TWorld.RemoveSceneNodes;
var
  I: Integer;
begin
  // Remove entity scene nodes
  for I:=0 to EntityCount - 1 do Entities[I].RemoveSceneNodes;
  // Remove gridmap scene nodes
  if Assigned(GridMap) then GridMap.RemoveSceneNodes(Scene);
end;

procedure TWorld.AddSceneNodes;
var
  I: Integer;
begin
  // Add gridmap scene nodes
  GridMap.AddSceneNodes(Scene);
  // Add entity scene nodes
  for I:=0 to EntityCount - 1 do Entities[I].AddSceneNodes;
end;

function TWorld.FindEntity(AName: string): TEntity;
var
  I: Integer;
begin
  AName:=LowerCase(AName);
  for I:=0 to EntityCount - 1 do
    if LowerCase(Entities[I].Name)=AName then Exit(Entities[I]);
  Result:=nil;
end;

function TWorld.FindClosestEntity(const P: TVector; Cls: TClass;
  MaxDistance: Single): TEntity;
var
  Dist, BestDist: Single;
  I: Integer;
begin
  BestDist:=$FFFFFFFF;
  Result:=nil;
  MaxDistance:=MaxDistance*MaxDistance;
  for I:=0 to EntityCount - 1 do
    if Entities[I].InheritsFrom(Cls) then begin
      Dist:=DistanceSq(P, Entities[I].Transform.Translation);
      if (Dist <= MaxDistance) and (Dist < BestDist) then begin
        BestDist:=Dist;
        Result:=Entities[I];
      end;
    end;
end;

procedure TWorld.GetFloorAndCeilingHeightsAt(const V: TVector; out FloorHeight,
  CeilingHeight: Single);
var
  I: Integer;
  ColBox: TAABox;
begin
  // Check gridmap
  GridMap.GetFloorAndCeilingHeightsAt(V, FloorHeight, CeilingHeight);
  // Check entities
  for I:=0 to EntityCount - 1 do with Entities[I] do begin
    // Ignore non-solid entities
    if not Solid then Continue;
    // Get collision box
    ColBox:=CollisionBox;
    // Ignore entities that could never be above or below the point
    if (ColBox.Min.X > V.X) or (ColBox.Min.Z > V.Z) or
       (ColBox.Max.X < V.X) or (ColBox.Max.Z < V.Z) then Continue;
    // If the entity's base is below the point, treat it's top as floor
    if ColBox.Min.Y < V.Y then
      FloorHeight:=Max(FloorHeight, ColBox.Max.Y + 0.01)
    // otherwise, treat its base as ceiling
    else
      CeilingHeight:=Min(CeilingHeight, ColBox.Min.Y - 0.01);
  end;
end;

procedure TWorld.GetFloorAndCeilingHeightForBoxCorners(const Box: TAABox; out
  FloorHeight, CeilingHeight: Single);
var
  FH, CH: Single;
begin
  // TODO: do not call GetFloorAndCeilingHeightsAt multiple times and instead
  // use gridmap directly
  GetFloorAndCeilingHeightsAt(Box.Min, FloorHeight, CeilingHeight);
  GetFloorAndCeilingHeightsAt(Box.Max, FH, CH);
  FloorHeight:=Max(FH, FloorHeight);
  CeilingHeight:=Min(CH, CeilingHeight);
  GetFloorAndCeilingHeightsAt(Vector(Box.Min.X, Box.Min.Y, Box.Max.Z), FH, CH);
  FloorHeight:=Max(FH, FloorHeight);
  CeilingHeight:=Min(CH, CeilingHeight);
  GetFloorAndCeilingHeightsAt(Vector(Box.Max.X, Box.Min.Y, Box.Min.Z), FH, CH);
  FloorHeight:=Max(FH, FloorHeight);
  CeilingHeight:=Min(CH, CeilingHeight);
end;

function TWorld.CanBoxFit(const Box: TAABox): Boolean;
var
  I: Integer;
begin
  // Check gridmap
  if not GridMap.CanBoxFit(Box) then Exit(False);
  // Check entities
  for I:=0 to EntityCount - 1 do
    if Entities[I].Solid and Entities[I].CollisionBox.Overlaps(Box) then
      Exit(False);
  // Box fits
  Result:=True;
end;

function TWorld.RayHit(const ARay: TRay; var Info: TWorldRayHitInfo): Boolean;
var
  I: Integer;
  IP: TVector;
  CurDist: Single;
begin
  // Initialize
  Info.Distance:=$FFFFFFFF;
  // Perform ray checks against the entities
  if not Info.IgnoreAllEntities then begin
    for I:=0 to EntityCount - 1 do begin
      if Entities[I]=Info.IgnoreEntity then Continue;
      if Entities[I].RayHit(ARay, IP) then begin
        CurDist:=DistanceSq(IP, ARay.O);
        if CurDist < Info.Distance then begin
          Info.Distance:=CurDist;
          Info.IP:=IP;
          Info.HitType:=rhEntity;
          Info.Entity:=Entities[I];
        end;
      end;
    end;
  end;
  // Perform a ray check again the gridmap
  if GridMap.RayHit(ARay, Info.GridHit) then begin
    CurDist:=DistanceSq(Info.GridHit.IP, ARay.O);
    if CurDist < Info.Distance then begin
      Info.Distance:=CurDist;
      Info.IP:=Info.GridHit.IP;
      Info.HitType:=rhGridCell;
    end;
  end;
  // Done
  Result:=Info.HitType <> rhNone;
end;

function TWorld.GetData(AKey: string; ADefValue: string): string;
var
  Index: Integer;
begin
  Index:=FDataTable.IndexOfKey(AKey);
  if Index=-1 then
    Result:=GGame.DataTable^.GetValue(AKey, ADefValue)
  else
    Result:=FDataTable.Values[Index];
end;

procedure TWorld.SetTintOverride(ATintR, ATintG, ATintB: Byte; Speed: Single);
var
  TintR, TintG, TintB: Byte;
begin
  if (ATintR=TintRed) and (ATintG=TintGreen) and (ATintB=TintBlue) and not TintOverride then
    Exit;
  GetTint(TintR, TintG, TintB);
  CurrentTintOverride:=Vector(TintR/255, TintG/255, TintB/255);
  TargetTintOverride:=Vector(ATintR/255, ATintG/255, ATintB/255);
  TintOverride:=True;
  TintOverrideRelease:=False;
  TargetTintOverrideSpeed:=Speed;
end;

procedure TWorld.UnsetTintOverride(Speed: Single);
var
  TintR, TintG, TintB: Byte;
begin
  if not TintOverride then Exit;
  GetTint(TintR, TintG, TintB);
  CurrentTintOverride:=Vector(TintR/255, TintG/255, TintB/255);
  TargetTintOverride:=Vector(TintRed/255, TintGreen/255, TintBlue/255);
  TintOverride:=True;
  TintOverrideRelease:=True;
  TargetTintOverrideSpeed:=Speed;
end;

procedure TWorld.GetTint(out TintR, TintG, TintB: Byte);
begin
  if TintOverride then begin
    TintR:=Round(CurrentTintOverride.X*255);
    TintG:=Round(CurrentTintOverride.Y*255);
    TintB:=Round(CurrentTintOverride.Z*255);
    if TintOverrideRelease then begin
      if (TintR=TintRed) and (TintG=TintGreen) and (TintB=TintBlue) then
        TintOverride:=False;
    end;
  end else begin
    TintR:=TintRed;
    TintG:=TintGreen;
    TintB:=TintBlue;
  end;
end;

procedure TWorld.Update;
var
  I: Integer;
begin
  // Delete entities scheduled to be deleted later
  DeleteScheduledEntities;
  // Update entity motions
  UpdateEntityMotions;
  // Update remaining entities
  for I:=0 to FEntities.Count - 1 do FEntities[I].Update;
  // Update tinting
  UpdateTintOverride;
  // Update color table if needed
  if ColorTableNeedsRefresh then RefreshColorTable;
end;

{$IFDEF EDITOR}
procedure TWorld.EditorUpdate;
var
  I: Integer;
begin
  // Perform editor updates
  if GPlayMode=pmEditor then begin
    UpdateTintOverride;
    for I:=0 to FEntities.Count - 1 do FEntities[I].EditorUpdate;
  end;
end;
{$ENDIF}

procedure TWorld.Frame;
var
  I: Integer;
begin
  // Setup scene background color
  if Assigned(Scene) then Scene.BackgroundColor:=BackgroundColor;
  // Inform entities
  for I:=0 to FEntities.Count - 1 do FEntities[I].Frame;
end;

procedure TWorld.InitializeEntities;
var
  I: Integer;
begin
  // Ensure this is called only once
  if InitializeEntitiesCalled then Exit;
  InitializeEntitiesCalled:=True;
  // Inform entities
  for I:=0 to FEntities.Count - 1 do FEntities[I].Initialize;
end;

procedure TWorld.RunStartupScript;
var
  StartupScript: TEntity;
begin
  {$IFNDEF PETRANOSTARTUP}
  StartupScript:=FindEntity('startup');
  if StartupScript is TScriptEntity then
    TScriptEntity(StartupScript).Run;
  {$ENDIF}
end;

initialization
  RegisterSerializableClass(TWorld);
  RegisterSerializableClass(TLight);
  RegisterSerializableClass(TDummyEntity);
  RegisterSerializableClass(TStaticMeshEntity);
  RegisterSerializableClass(TPieceModelEntity);
  RegisterSerializableClass(TBoxEntity);
  RegisterSerializableClass(TEntityCollection);
end.

