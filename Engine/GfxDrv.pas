{ Common graphics driver functionality }
unit GfxDrv;

interface

uses
  Classes, Colors, Raster;

type
  TFrameData = array [0..1] of Byte;
  PFrameData = ^TFrameData;

  { The gamma value }
  TGammaValue = 0..7;

  { TGraphicsDriver - provides the core graphics functionality }
  { This is just a base class for the environment-specific code }
  TGraphicsDriver = class
  public
    // Initialize the graphics mode
    procedure Initialize; virtual; abstract;
    // Shutdown the graphics mode
    procedure Shutdown; virtual; abstract;
    {$IFDEF PETRAHW}
    // Begins a new frame and returns true if the frame can be rendered
    function BeginFrame: Boolean; virtual; abstract;
    {$ELSE}
    // Begins a new frame and returns the graphics buffer address
    // or returns Nil if cannot draw a frame at this point
    function BeginFrame: PFrameData; virtual; abstract;
    {$ENDIF}
    // Finishes drawing the frame and flips or copies any buffers
    procedure FinishFrame; virtual; abstract;
    // Applies the given color palette
    procedure SetColorPalette(const APalette: TColorPalette);
    // Applies a part of the given color palette
    procedure SetColorPalettePart(const APalette: TColorPalette; First, Last: Integer); virtual; abstract;
    // Set the gamma to the given value
    procedure SetGamma(AGamma: TGammaValue); virtual; abstract;
    // Returns the rasterizer this driver supports
    function GetRasterizer: TRasterizer; virtual; abstract;
  end;

{$IFDEF GO32V2}
const
  HRes = 320;
  VRes = 200;
  HalfHRes = HRes div 2;
  HalfVRes = VRes div 2;
  MaxX = HRes - 1;
  MaxY = VRes - 1;
  AspectRatio = 4/3;
  BaseX = 0;
  BaseY = 0;
{$ELSE}
var
  HRes: Integer;
  VRes: Integer;
  HalfHRes: Integer;
  HalfVRes: Integer;
  MaxX: Integer;
  MaxY: Integer;
  AspectRatio: Single;
  BaseX: Integer;
  BaseY: Integer;
  {$IFDEF PETRAHW}
  ColorDepth: Integer;
  RealHRes: Integer;
  RealVRes: Integer;
  {$ENDIF}
  
procedure ConfigGraphicsResolution(AHRes, AVRes: Integer);
  
{$ENDIF}

implementation

{ Functions }
{$IFNDEF GO32V2}
procedure ConfigGraphicsResolution(AHRes, AVRes: Integer);
begin
  {$IFDEF PETRAHW}
  HRes:=320;
  VRes:=200;
  HalfHRes:=160;
  HalfVRes:=100;
  RealHRes:=320;
  RealVRes:=200;
  MaxX:=319;
  MaxY:=199;
  BaseX:=0;
  BaseY:=0;
  AspectRatio:=AHRes/AVRes;
  ColorDepth:=24;
  {$ELSE}
  HRes:=AHRes;
  VRes:=AVRes;
  HalfHRes:=AHRes div 2;
  HalfVRes:=AVRes div 2;
  MaxX:=AHRes - 1;
  MaxY:=AVRes - 1;
  // Special case for non-4:3 VGA resolution
  if ((AHRes=320) and (AVRes=200)) or
     ((AHRes=640) and (AVRes=400)) then
    AspectRatio:=4/3
  else
    AspectRatio:=AHRes/AVRes;
  BaseX:=(HRes - 320) div 2;
  BaseY:=(VRes - 200) div 2;
  {$ENDIF}
end;
{$ENDIF}

{ TGraphicsDriver }
procedure TGraphicsDriver.SetColorPalette(const APalette: TColorPalette);
begin
  SetColorPalettePart(APalette, 0, 255);
end;

initialization
  {$IFNDEF GO32V2}
  ConfigGraphicsResolution(320, 200);
  {$ENDIF}
end.
