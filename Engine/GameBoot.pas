{ Game booting/startup unit }
unit GameBoot;

interface

uses
  DataLoad, Maths;

procedure BootGame(AGameTitle, ADataDir: string);
procedure RunGame;
procedure ShutdownGame;

implementation

uses
  {$IFDEF WINDOWS}Windows,{$ENDIF}
  Math, {$IFNDEF PAS2JS}Crt,{$ENDIF} Classes, Engine, InputDrv, Fonts,
  Screens, GfxDrv, SysUtils, Settings;

{$IFNDEF PETRANOSTARTUP}
procedure ShowLogoAndTitle;
var
  F: TStream;
  LogoData: array [1..320*200] of Byte;
  I: Integer;

  procedure FillScreenWithLogoData;
  {$IFNDEF GO32V2}
  var
    X, Y: Integer;
  {$ENDIF}
  begin
    {$IFDEF PETRAHW}
    for Y:=0 to MaxY do
      for X:=0 to MaxX do
        {$R-}GRenderer.SetPixel(X, Y, LogoData[(Y*200 div VRes)*320 + (X*320 div HRes)]);{$R+}
    {$ELSE}
    {$IFDEF GO32V2}
    Move(LogoData, GRenderer.RawFrameData^, 320*200);
    {$ELSE}
    if (HRes=320) and (VRes=200) then
      Move(LogoData, GRenderer.RawFrameData^, 320*200)
    else begin
      for Y:=0 to MaxY do
        for X:=0 to MaxX do
          {$R-}GRenderer.RawFrameData^[Y*HRes + X]:=LogoData[(Y*200 div VRes)*320 + (X*320 div HRes)];{$R+}
    end;
    {$ENDIF}
    {$ENDIF}
  end;

begin
  // Load palette
  GRenderer.ColorPalette.LoadJASCPalette(DataDirectoryPrefix + 'colors.pal');
  GRenderer.GraphicsDriver.SetColorPalette(GRenderer.ColorPalette);
  // Show 'one moment' while building the color palette
  GRenderer.BeginFrame;
  {$IFDEF PETRAHW}
  GRenderer.FillRectangle(0, 0, HRes, VRes, 0);
  {$ELSE}
  FillChar(GRenderer.RawFrameData^, HRes*VRes, 0);
  {$ENDIF}
  GRenderer.DrawText(10, VRes - 20, 'One moment...', TBitFont(GResourceManager.DefaultFont), 15);
  GRenderer.FinishFrame;
  {$IFDEF PETRAHW}
  GRenderer.DrawShadowTextures;
  {$ENDIF}
  GRenderer.GraphicsDriver.FinishFrame;
  // Build the color palette
  GRenderer.ColorPalette.BuildLightTable(0, 0, 0, 1, Vector(1, 1, 1));
  // Scan parameters to see if there is a map file specified and do not
  // show the intro graphics if there is one
  for I:=1 to ParamCount do
    if LowerCase(ParamStr(I))='-map' then begin
      // Clear the screen to remove the 'One moment...' line
      GRenderer.BeginFrame;
      {$IFDEF PETRAHW}
      GRenderer.FillRectangle(0, 0, HRes, VRes, 0);
      {$ELSE}
      FillChar(GRenderer.RawFrameData^, HRes*VRes, 0);
      {$ENDIF}
      GRenderer.FinishFrame;
      {$IFDEF PETRAHW}
      GRenderer.DrawShadowTextures;
      {$ENDIF}
      GRenderer.GraphicsDriver.FinishFrame;
      Exit;
    end;
  // Logo
  F:=GetDataStream(DataDirectoryPrefix + 'logo.raw');
  if not Assigned(F) then Exit;
  try
    F.ReadBuffer(LogoData, 320*200);
  except
    F.Free;
    Exit;
  end;
  F.Free;
  GInput.ExclusiveHandler:=GScreenManager;
  while EngineRunning do begin
    GRenderer.BeginFrame;
    FillScreenWithLogoData;
    GRenderer.FinishFrame;
    {$IFDEF PETRAHW}
    GRenderer.DrawShadowTextures;
    {$ENDIF}
    GRenderer.GraphicsDriver.FinishFrame;
    if GInput.KeyState[ikLeftButton] or GInput.KeyState[ikSpace] or
       GInput.KeyState[ikEnter] or GInput.KeyState[ikEscape] then Break;
  end;
  GInput.ReleaseKeys;
  // Title
  F:=GetDataStream(DataDirectoryPrefix + 'title.raw');
  if not Assigned(F) then begin
    GInput.ExclusiveHandler:=nil;
    Exit;
  end;
  try
    F.ReadBuffer(LogoData, 320*200);
  except
    F.Free;
    GInput.ExclusiveHandler:=GScreenManager;
    Exit;
  end;
  F.Free;
  while EngineRunning do begin
    GRenderer.BeginFrame;
    FillScreenWithLogoData;
    GRenderer.FinishFrame;
    {$IFDEF PETRAHW}
    GRenderer.DrawShadowTextures;
    {$ENDIF}
    GRenderer.GraphicsDriver.FinishFrame;
    if GInput.KeyState[ikLeftButton] or GInput.KeyState[ikSpace] or
       GInput.KeyState[ikEnter] or GInput.KeyState[ikEscape] then Break;
  end;
  GInput.ExclusiveHandler:=nil;
end;
{$ENDIF}

{$IFDEF DARWIN}
procedure FixBundleCWD;
var
  I: Integer;
begin
  for I:=1 to ParamCount do if ParamStr(I)='-inbundle' then ChDir('../..');
end;
{$ENDIF}


procedure BootGame(AGameTitle, ADataDir: string);
begin
  // Disable FPU exceptions
  SetExceptionMask([exInvalidOp, exDenormalized, exZeroDivide, exOverflow, exUnderflow, exPrecision]);
  // Store title
  GGameTitle:=AGameTitle;
  // Store data directory
  DataDirectoryPrefix:=ADataDir;
  if DataDirectoryPrefix <> '/' then DataDirectoryPrefix += '/';
  // Change directory when running in Mac OS bundle
  {$IFDEF DARWIN}FixBundleCWD;{$ENDIF}
  // Initial graphics settings
  {$IFNDEF GO32V2}{$IFNDEF PETRAGCW}
  ConfigGraphicsResolution(640, 480);
  {$ENDIF}{$ENDIF}
  {$IFNDEF WINDOWS}
  // Fancy startup
  TextAttr:=7;
  ClrScr;
  TextAttr:=$4E;
  Write('':(80 - (Length(GGameTitle) + 8)) div 2, '-*- ', GGameTitle, ' -*-');
  ClrEol;
  TextAttr:=7;
  GotoXY(1, 3);
  Writeln('One moment, doing stuff...');
  {$ENDIF}
  // Initialization
  LoadSettings;
  {$IFDEF PETRAD3D}
  // Ensure the user has ran the wincfg utility so that a correct GUID is set
  // to avoid rare cases where a different device is selected by default that
  // can end up with a black screen
  if (GSettings.D3D3DDevGUID.Data1=0) and (GSettings.D3D3DDevGUID.Data2=0) and
     (GSettings.D3D3DDevGUID.Data3=0) and (GSettings.D3D3DDevGUID.Data4[0]=0) and
     (GSettings.D3D3DDevGUID.Data4[1]=0) and (GSettings.D3D3DDevGUID.Data4[2]=0) and
     (GSettings.D3D3DDevGUID.Data4[3]=0) and (GSettings.D3D3DDevGUID.Data4[4]=0) and
     (GSettings.D3D3DDevGUID.Data4[5]=0) and (GSettings.D3D3DDevGUID.Data4[6]=0) and
     (GSettings.D3D3DDevGUID.Data4[7]=0) then begin
    MessageBox(0, 'Please run WINCFG.EXE before running the Direct3D version',
      'Configuration Needed', MB_OK);
    Halt;
  end;
  {$ENDIF}
  InitializeEngine;
  {$IFDEF PETRANOSTARTUP}
  GRenderer.ColorPalette.LoadJASCPalette(DataDirectoryPrefix + 'colors.pal');
  GRenderer.ColorPalette.BuildLightTable(0, 0, 0, 1, Vector(1, 1, 1));
  {$ELSE}
  ShowLogoAndTitle;
  {$ENDIF}
  // Initialize the game
  GGame.Initialize;
end;

procedure RunGame;
begin
  // Main loop
  while EngineRunning do
    RunEngineCycle;
end;

procedure ShutdownGame;
begin
  // Shutdown
  ShutdownEngine;
  {$IFNDEF WINDOWS}
  Writeln('Thank you for playing ', GGameTitle, '!');
  {$ENDIF}
end;

end.
