{ Common settings }
unit Settings;

interface

const
  SettingsFile = 'settings.cfg';

type
  { TSettings - Contains the engine settings }
  TSettings = record
    // Disable sound
    NoSound: Boolean;
    // Mouse rotation speed
    MouseRotateSpeed: Integer;
    // Custom map to play
    CustomMap: string;
    // Max FPS (0=no cap)
    MaxFPS: Integer;
    // (DOS) SB16 Port
    SB16Port: Integer;
    // (DOS) SB16 DMA channel
    SB16DMA: Integer;
    // (DOS) SB16 IRQ
    SB16IRQ: Integer;
    // (Hardware acceleration) Enable bilinear texture filtering
    BilinearFiltering: Boolean;
    // (Hardware acceleration) Do not use indexed textures even if available
    NoIndexedTextures: Boolean;
    // (Direct3D) Color depth to use (0=auto)
    D3DColorDepth: Integer;
    // (Direct3D) GUID for the DirectDraw device to use
    D3DDevGUID: TGuid;
    // (Direct3D) GUID for the Direct3D device to use
    D3D3DDevGUID: TGuid;
  end;

var
  // Global settings
  GSettings: TSettings;

{ Reset to default settings }
procedure ResetSettings;
{ Load the settings file }
procedure LoadSettings;
{ Save the settings file }
procedure SaveSettings;

implementation

uses
  SysUtils;

{ Functions }
procedure ResetSettings;
begin
  with GSettings do begin
    NoSound:=False;
    MouseRotateSpeed:=100;
    CustomMap:='';
    MaxFPS:=500;
    SB16Port:=$220;
    SB16DMA:=5;
    SB16IRQ:=5;
    BilinearFiltering:=True;
    NoIndexedTextures:=False;
    D3DColorDepth:=0;
    FillChar(D3DDevGUID, SizeOf(D3DDevGUID), 0);
    FillChar(D3D3DDevGUID, SizeOf(D3D3DDevGUID), 0);
  end;
end;

procedure LoadSettings;
{$IFNDEF NODIRECTFILEIO}
var
  F: Text;
  I: Integer;
  Line, Key, Value: string;
{$ENDIF}
begin
  ResetSettings;
  {$IFNDEF NODIRECTFILEIO}
  Assign(F, SettingsFile);{$I-}Reset(F);{$I+}
  if IOResult <> 0 then Exit;
  with GSettings do while not Eof(F) do begin
    Readln(F, Line);
    Line:=Trim(Line);
    if (Line='') or (Line[1]=';') then Continue;
    I:=Pos('=', Line);
    if i=0 then begin
      Key:=LowerCase(Line);
      Value:='';
    end else begin
      Key:=LowerCase(Trim(Copy(Line, 1, I - 1)));
      Value:=Trim(Copy(Line, I + 1, MaxInt));
    end;
    if Key='nosound' then
      NoSound:=StrToIntDef(Value, 0) <> 0
    else if Key='mouserotatespeed' then
      MouseRotateSpeed:=StrToIntDef(Value, 100)
    else if Key='custommap' then
      CustomMap:=Value
    else if Key='maxfps' then begin
      MaxFPS:=StrToIntDef(Value, 280);
      if MaxFPS < 0 then MaxFPS:=0;
    end else if Key='sb16port' then
      SB16Port:=StrToIntDef(Value, $220)
    else if Key='sb16dma' then
      SB16DMA:=StrToIntDef(Value, 5)
    else if Key='sb16port' then
      SB16IRQ:=StrToIntDef(Value, 5)
    else if Key='bilinear' then
      BilinearFiltering:=Value <> '0'
    else if Key='noindexedtextures' then
      NoIndexedTextures:=Value <> '0'
    else if Key='d3dcolordepth' then
      D3DColorDepth:=StrToIntDef(Value, 0)
    else if Key='d3ddevguid' then begin
      try
        D3DDevGUID:=StringToGUID(Value);
      except
      end;
    end else if Key='d3d3ddevguid' then begin
      try
        D3D3DDevGUID:=StringToGUID(Value);
      except
      end;
    end;
  end;
  Close(F);
  {$ENDIF}
end;

procedure SaveSettings;
{$IFDEF NODIRECTFILEIO}
begin
end;
{$ELSE}
var
  F: Text;
begin
  Assign(F, SettingsFile);{$I-}ReWrite(F);{$I+}
  if IOResult <> 0 then Exit;
  Writeln(F, '; This is a generated file, it is recommended to use');
  Writeln(F, '; the configuration utility that comes with the game');
  Writeln(F, '; to modify it instead of editing it directly');
  Writeln(F, '');
  Writeln(F, '; Disable count (0=have sound, 1=will NOT have sound)');
  if GSettings.NoSound then Writeln(F, 'NoSound=1') else Writeln(F, 'NoSound=0');
  Writeln(F, '; Mouse rotation speed (default=100, higher means faster)');
  Writeln(F, 'MouseRotateSpeed=', GSettings.MouseRotateSpeed);
  Writeln(F, '; Custom map (empty for the default map, you can use -map mapname too)');
  Writeln(F, 'CustomMap=', GSettings.CustomMap);
  Writeln(F, '; Maximum FPS (0=no cap)');
  Writeln(F, 'MaxFPS=', GSettings.MaxFPS);
  Writeln(F, '; Sound Blaster 16 port ($220, $240, $260, $280)');
  Writeln(F, 'SB16Port=$', HexStr(GSettings.SB16Port, 3));
  Writeln(F, '; Sound Blaster 16 stereo DMA channel (5, 6 or 7)');
  Writeln(F, 'SB16DMA=', GSettings.SB16DMA);
  Writeln(F, '; Sound Blaster 16 IRQ (2, 5 or 7)');
  Writeln(F, 'SB16IRQ=', GSettings.SB16IRQ);
  Writeln(F, '; Bilinear texture filtering for hardware accelerated mode');
  if GSettings.BilinearFiltering then Writeln(F, 'Bilinear=1') else Writeln(F, 'Bilinear=0');
  Writeln(F, '; Do not use indexed textures in hardware accelerated mode');
  if GSettings.NoIndexedTextures then Writeln(F, 'NoIndexedTextures=1') else Writeln(F, 'NoIndexedTextures=0');
  Writeln(F, '; Direct3D color depth (0, 16 or 32 - 0 means auto)');
  Writeln(F, 'D3DColorDepth=', GSettings.D3DColorDepth);
  Writeln(F, '; Direct3D DirectDraw device GUID');
  Writeln(F, 'D3DDevGUID=', GUIDToString(GSettings.D3DDevGUID));
  Writeln(F, '; Direct3D Direct3D device GUID');
  Writeln(F, 'D3D3DDevGUID=', GUIDToString(GSettings.D3D3DDevGUID));
  Close(F);
end;
{$ENDIF}

initialization
  ResetSettings;
end.
