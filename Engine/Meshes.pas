// 3D mesh objects
unit Meshes;

interface

uses
  Maths, Textures, ResMan, Renderer;

type

  { TMesh - A mesh of triangles }
  TMesh = class(TResource)
  private
    // Property storage
    FTriangles: array of TRenderTri;
    FTexture: TTexture;
    FAABox: TAABox;
    function GetTriangles(AIndex: Integer): TRenderTri; inline;
    procedure SetTriangles(AIndex: Integer; const ATri: TRenderTri); inline;
    function GetTriangleCount: Integer; inline;
    procedure SetTriangleCount(ACount: Integer); inline;
  public
    // Create an undefined mesh
    constructor Create;
    // Destroy the mesh
    destructor Destroy; override;

    // Clear the mesh data
    procedure Clear;
    // Copy the mesh data from the given mesh to this one
    procedure Assign(AMesh: TMesh);
    // Add a triangle to the mesh
    procedure AddTriangle(const ATri: TRenderTri);
    // Load a mesh from the given JTF file
    procedure LoadJTFMesh(AFileName: string);
    // Update the AABox for this mesh
    procedure UpdateAABox;

    // Render the mesh with the given model matrix and constant light level
    procedure Render(const AModelMatrix: TMatrix; Light, ADepthBias: Byte; AOccluder: Boolean);
    // Render the mesh with the given model matrix and per-vertex light level
    procedure Render(const AModelMatrix: TMatrix; Light: PByte; ADepthBias: Byte; AOccluder: Boolean);

    // Mesh triangles
    property Triangles[AIndex: Integer]: TRenderTri read GetTriangles write SetTriangles;
    // Triangle counter
    property TriangleCount: Integer read GetTriangleCount write SetTriangleCount;
    // The texture to use for rendering the mesh
    property Texture: TTexture read FTexture write FTexture;
    // The axis aligned bounding box for this mesh (must call UpdateAABox to
    // ensure this is correct after modifying the mesh)
    property AABox: TAABox read FAABox;
  end;

implementation

uses
  Classes, DataLoad, Engine, SysUtils;

{ TMesh }
constructor TMesh.Create;
begin
  inherited Create;
end;

destructor TMesh.Destroy;
begin
  Clear;
  inherited Destroy;
end;

function TMesh.GetTriangles(AIndex: Integer): TRenderTri;
begin
  Result:=FTriangles[AIndex];
end;

procedure TMesh.SetTriangles(AIndex: Integer; const ATri: TRenderTri);
begin
  FTriangles[AIndex]:=ATri;
end;

function TMesh.GetTriangleCount: Integer;
begin
  Result:=Length(FTriangles);
end;

procedure TMesh.SetTriangleCount(ACount: Integer);
begin
  SetLength(FTriangles, ACount);
end;

procedure TMesh.Clear;
begin
  SetLength(FTriangles, 0);
end;

procedure TMesh.Assign(AMesh: TMesh);
var
  I: Integer;
begin
  if AMesh=Self then Exit;
  SetLength(FTriangles, Length(AMesh.FTriangles));
  Move(AMesh.FTriangles[0], FTriangles[0], Length(FTriangles)*SizeOf(TRenderTri));
  for I:=0 to High(FTriangles) do FTriangles[I].Texture:=Texture;
end;

procedure TMesh.AddTriangle(const ATri: TRenderTri);
begin
  SetLength(FTriangles, Length(FTriangles) + 1);
  FTriangles[High(FTriangles)]:=ATri;
  FTriangles[High(FTriangles)].Texture:=Texture;
end;

procedure TMesh.LoadJTFMesh(AFileName: string);
var
  F: TStream;
  I, Faces: Integer;
  NX, NY, NZ, S, T: Single;
begin
  F:=GetDataStream(AFileName);
  if not Assigned(F) then Exit;
  F.ReadBuffer(Faces, 4); // JTF magic ignored
  F.ReadBuffer(Faces, 4); // version ignored
  F.ReadBuffer(Faces, 4); // the actual number of faces
  SetLength(FTriangles, Faces);
  for I:=0 to Faces - 1 do with FTriangles[I] do begin
    Texture:=FTexture;
    F.ReadBuffer(A.X, 4);
    F.ReadBuffer(A.Y, 4);
    F.ReadBuffer(A.Z, 4);
    F.ReadBuffer(NX, 4);
    F.ReadBuffer(NY, 4);
    F.ReadBuffer(NZ, 4);
    F.ReadBuffer(S, 4);
    F.ReadBuffer(T, 4);
    LA:=Round(Clamp(NZ+NY*0.5, 0.45, 1)*255);
    SA:=Clamp(Round(S*(Texture.Width - 1)), 0, Texture.Width - 1);
    TA:=Clamp(Round(T*(Texture.Height - 1)), 0, Texture.Height - 1);
    F.ReadBuffer(B.X, 4);
    F.ReadBuffer(B.Y, 4);
    F.ReadBuffer(B.Z, 4);
    F.ReadBuffer(NX, 4);
    F.ReadBuffer(NY, 4);
    F.ReadBuffer(NZ, 4);
    F.ReadBuffer(S, 4);
    F.ReadBuffer(T, 4);
    LB:=Round(Clamp(NZ+NY*0.5, 0.45, 1)*255);
    SB:=Clamp(Round(S*(Texture.Width - 1)), 0, Texture.Width - 1);
    TB:=Clamp(Round(T*(Texture.Height - 1)), 0, Texture.Height - 1);
    F.ReadBuffer(C.X, 4);
    F.ReadBuffer(C.Y, 4);
    F.ReadBuffer(C.Z, 4);
    F.ReadBuffer(NX, 4);
    F.ReadBuffer(NY, 4);
    F.ReadBuffer(NZ, 4);
    F.ReadBuffer(S, 4);
    F.ReadBuffer(T, 4);
    LC:=Round(Clamp(NZ+NY*0.5, 0.45, 1)*255);
    SC:=Clamp(Round(S*(Texture.Width - 1)), 0, Texture.Width - 1);
    TC:=Clamp(Round(T*(Texture.Height - 1)), 0, Texture.Height - 1);
  end;
  F.Free;
  UpdateAABox;
end;

procedure TMesh.UpdateAABox;
var
  I: Integer;
begin
  if Length(FTriangles)=0 then begin
    FAABox.MakeMaxInverted;
    Exit;
  end;
  FAABox.Min:=FTriangles[0].A;
  FAABox.Max:=FAABox.Min;
  FAABox.Include(FTriangles[0].B);
  FAABox.Include(FTriangles[0].C);
  for I:=1 to High(FTriangles) do begin
    FAABox.Include(FTriangles[I].A);
    FAABox.Include(FTriangles[I].B);
    FAABox.Include(FTriangles[I].C);
  end;
end;

procedure TMesh.Render(const AModelMatrix: TMatrix; Light, ADepthBias: Byte; AOccluder: Boolean);
var
  I: Integer;
  Tri: PRenderTri;
begin
  for I:=0 to High(FTriangles) do begin
    FTriangles[I].Texture:=Texture;
    Tri:=GRenderer.AllocRenderTriangle;
    Tri^:=FTriangles[I];
    with AModelMatrix do with Tri^ do begin
      DepthBias:=ADepthBias;
      LA:=Light;
      LB:=Light;
      LC:=Light;
      Transform(A);
      Transform(B);
      Transform(C);
      {$IFNDEF PETRAHW}
      Occluder:=AOccluder;
      {$ENDIF}
    end;
  end;
end;

procedure TMesh.Render(const AModelMatrix: TMatrix; Light: PByte; ADepthBias: Byte; AOccluder: Boolean);
var
  LCtr, I: Integer;
  Tri: PRenderTri;
begin
  LCtr:=0;
  for I:=0 to High(FTriangles) do begin
    FTriangles[I].Texture:=Texture;
    Tri:=GRenderer.AllocRenderTriangle;
    Tri^:=FTriangles[I];
    with AModelMatrix do with Tri^ do begin
      DepthBias:=ADepthBias;
      LA:=Light[LCtr]; Inc(LCtr);
      LB:=Light[LCtr]; Inc(LCtr);
      LC:=Light[LCtr]; Inc(LCtr);
      Transform(A);
      Transform(B);
      Transform(C);
      {$IFNDEF PETRAHW}
      Occluder:=AOccluder;
      {$ENDIF}
    end;
  end;
end;

end.
