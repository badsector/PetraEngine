{ Script entity }
unit ScrEnt;

interface

uses
  Misc, SysUtils, Maths, Serial, World, Scripts;

type
  { TScriptEntity - An entity that holds a persistent script }
  TScriptEntity = class(TEntity)
  private
    // Property storage
    FScript: TScript;
  private
    // Timeout time (0=no timeout)
    TimeoutTime: Integer;
    // Label to run after the timeout
    TimeoutLabel: string;
    FScriptCode: string;
    // Script command handler
    function ScriptCommand(Sender: TScript; Args: TStringArray): Boolean;
  protected
    // TEntity methods
    procedure Update; override;
    {$IFDEF EDITOR}
    procedure Frame; override;
    {$ENDIF}
    function GetAABox: TAABox; override;
  public
    // TEntity methods
    function PerformCommand(ACommand: string): Boolean; override;
  public
    constructor Create; override;
    destructor Destroy; override;
    // Run script code under this script, optionally starting at the given label
    procedure RunScriptCode(Code: string; ALabel: string='');
    // Run the script's own code, optionally starting at the given label
    procedure Run(ALabel: string='');
    // The entity's script instance
    property Script: TScript read FScript;
  published
    property ScriptCode: string read FScriptCode write FScriptCode;
  end;

implementation

uses
  Engine;

{ TScriptEntity }
constructor TScriptEntity.Create;
begin
  inherited Create;
  FScript:=TScript.Create;
  FScript.OnScriptCommand:=@ScriptCommand;
end;

destructor TScriptEntity.Destroy;
begin
  FScript.Free;
  inherited Destroy;
end;

procedure TScriptEntity.RunScriptCode(Code: string; ALabel: string);
begin
  FScript.SetVar('me', Name);
  FScript.RunCode(Code, ALabel);
end;

procedure TScriptEntity.Run(ALabel: string);
begin
  RunScriptCode(ScriptCode, ALabel);
end;

function TScriptEntity.ScriptCommand(Sender: TScript; Args: TStringArray): Boolean;
begin
  // Command: timeout time label
  if Args[0]='timeout' then begin
    if Length(Args) < 3 then Exit(True);
    TimeoutTime:=GGame.Milliseconds + StrToIntDef(Args[1], 0);
    TimeoutLabel:=Args[2];
    Exit(True);
  end;
  // Unknown command
  Result:=False;
end;

procedure TScriptEntity.Update;
var
  Temp: string;
begin
  inherited Update;
  if TimeoutTime <> 0 then begin
    if GGame.Milliseconds >= TimeoutTime then begin
      TimeoutTime:=0;
      Temp:=TimeoutLabel;
      TimeoutLabel:='';
      Run(Temp);
    end;
  end;
end;

{$IFDEF EDITOR}
procedure TScriptEntity.Frame;
begin
  if GPlayMode=pmEditor then begin
    GRenderer.RenderAABox(AABox.Grown(Vector(0.2, 0, 0)));
    GRenderer.RenderAABox(AABox.Grown(Vector(0, 0.2, 0)));
  end;
end;
{$ENDIF}

function TScriptEntity.GetAABox: TAABox;
begin
  Result:=inherited GetAABox;
  Result.Grow(0.1);
end;

function TScriptEntity.PerformCommand(ACommand: string): Boolean;
begin
  if ACommand='' then Exit(True);
  // Handle ! commands before superclass so they will be executed in this
  // script's own TScript instance
  if ACommand[1]='!' then begin
    RunScriptCode(Copy(ACommand, 2, MaxInt));
    Exit(True);
  end;
  // Pass to superclass
  Result:=inherited PerformCommand(ACommand);
  // Handle own commands
  // Command: run
  if ACommand='run' then begin
    Run;
    Exit(True);
  end;
  // Command: run label
  if Copy(ACommand, 1, 4)='run ' then begin
    Run(Trim(Copy(ACommand, 4, MaxInt)));
    Exit(True);
  end;
  Result:=False;
end;

initialization
  RegisterSerializableClass(TScriptEntity);
end.
