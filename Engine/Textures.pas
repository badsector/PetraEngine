{ Textures }
unit Textures;

interface

uses
  ResMan;

type
  {$IFDEF PETRAHW}
  { TTextureFilter - Texture filtering mode (useful only for HW) }
  TTextureFilter = (tfDefault, tfNearest);
  {$ENDIF}

  { TTexture - represents a texture }
  TTexture = class(TResource)
  private
    // Property storage
    FWidth: Integer;
    FHeight: Integer;
    {$IFNDEF PETRAHW}
    FTexels: PByte;
    {$ENDIF}
    FFrame: Integer;
    FFrameCount: Integer;
    {$IFDEF PETRAHW}
    FFilter: TTextureFilter;
    {$ENDIF}
    FHasTranparency: Boolean;
    procedure SetWidth(AWidth: Integer); inline;
    procedure SetHeight(AHeight: Integer); inline;
    procedure SetTexels(X, Y: Integer; Color: Byte); inline;
    function GetTexels(X, Y: Integer): Byte; inline;
    procedure SetFrame(AValue: Integer);
    {$IFDEF PETRAHW}
    function GetHWTexture: TObject; inline;
    procedure SetFilter(AValue: TTextureFilter);
    {$ENDIF}
  private
    // All the frames in the texture
    Frames: array of Byte;
    {$IFDEF PETRAHW}
    // Hardware accelerated textures for the frames in this texture
    FHWTextures: array of TObject;
    // Next time the hardware texture is requested update it first
    NeedHWUpdate: Boolean;
    {$ENDIF}
  public
    // Create a new texture of the specified dimensions
    constructor Create(AWidth, AHeight: Integer{$IFDEF PETRAHW}; AFilter: TTextureFilter=tfDefault{$ENDIF});
    // Destroy the texture
    destructor Destroy; override;
    // Assign the texture data from the given texture
    procedure Assign(ATexture: TTexture);
    // Set the texture size
    procedure SetSize(AWidth, AHeight: Integer);
    // Clear the texture
    procedure Clear;
    // Set the texels
    procedure SetTexels(const Texels);
    // Get the texels
    procedure GetTexels(out Texels);
    // Load the texels from the given RAW file
    procedure LoadRAWFile(AFileName: string);
    // Check for transparent pixels (index color 0 is used)
    procedure CheckForTransparency;
    {$IFDEF PETRAHW}
    // Update hardware accelerated texture
    procedure UpdateHWTexture;
    {$ENDIF}
    // Texture width
    property Width: Integer read FWidth write SetWidth;
    // Texture height
    property Height: Integer read FHeight write SetHeight;
    // Texel access
    property Texels[X, Y: Integer]: Byte read GetTexels write SetTexels;
    // Current frame
    property Frame: Integer read FFrame write SetFrame;
    // Number of frames
    property FrameCount: Integer read FFrameCount;
    {$IFDEF PETRAHW}
    // Hardware accelerated texture
    property HardwareTexture: TObject read GetHWTexture;
    // Texture filtering to use
    property Filter: TTextureFilter read FFilter write SetFilter;
    {$ELSE}
    // Texel data
    property TexelData: PByte read FTexels;
    {$ENDIF}
    // The texture has transparent pixels (uses index 0)
    property HasTranparency: Boolean read FHasTranparency write FHasTranparency;
  end;

implementation

uses
  {$IFDEF PETRAHW}
  Raster, Engine,
  {$ENDIF}
  Classes, DataLoad, Settings, SysUtils;

{ TTexture }
constructor TTexture.Create(AWidth, AHeight: Integer{$IFDEF PETRAHW}; AFilter: TTextureFilter{$ENDIF});
begin
  if AWidth < 1 then AWidth:=1;
  if AHeight < 1 then AHeight:=1;
  FWidth:=AWidth;
  FHeight:=AHeight;
  FFrameCount:=1;
  FHasTranparency:=False;
  SetLength(Frames, FWidth*FHeight);
  {$IFDEF PETRAHW}
  FFilter:=AFilter;
  {$ELSE}
  FTexels:=@Frames[0];
  {$ENDIF}
end;

destructor TTexture.Destroy;
{$IFDEF PETRAHW}
var
  I: Integer;
{$ENDIF}
begin
  {$IFDEF PETRAHW}
  for I:=0 to High(FHWTextures) do FHWTextures[I].Free;
  {$ENDIF}
  inherited Destroy;
end;

procedure TTexture.SetWidth(AWidth: Integer);
begin
  SetSize(AWidth, Height);
end;

procedure TTexture.SetHeight(AHeight: Integer);
begin
  SetSize(Width, AHeight);
end;

procedure TTexture.SetTexels(X, Y: Integer; Color: Byte);
{$IFDEF PETRAHW}
var
  FTexels: PByte;
{$ENDIF}
begin
  {$IFDEF PETRAHW}
  // Make sure there is space for a single frame
  if not NeedHWUpdate then begin
    FFrameCount:=1;
    SetLength(Frames, FWidth*FHeight);
    NeedHWUpdate:=True;
  end;
  FTexels:=@Frames[0];
  {$ENDIF}
  if (X >= 0) and (Y >= 0) and (X < Width) and (Y < Height) then begin
    FTexels[Y*FWidth + X]:=Color;
    if Color=255 then FHasTranparency:=True;
  end;
end;

function TTexture.GetTexels(X, Y: Integer): Byte;
begin
  if (X >= 0) and (Y >= 0) and (X < Width) and (Y < Height) then
    {$IFDEF PETRAHW}
    Result:=Frames[Y*FWidth + X]
    {$ELSE}
    Result:=FTexels[Y*FWidth + X]
    {$ENDIF}
  else
    Result:=0;
end;

procedure TTexture.SetFrame(AValue: Integer);
begin
  AValue:=AValue mod FrameCount;
  if FFrame=AValue then Exit;
  FFrame:=AValue;
  {$IFNDEF PETRAHW}
  FTexels:=@Frames[FFrame*FWidth*FHeight];
  {$ENDIF}
end;

{$IFDEF PETRAHW}
function TTexture.GetHWTexture: TObject;
begin
  if NeedHWUpdate then UpdateHWTexture;
  Result:=FHWTextures[Frame];
end;

procedure TTexture.SetFilter(AValue: TTextureFilter);
begin
  if FFilter=AValue then Exit;
  FFilter:=AValue;
  NeedHWUpdate:=True;
end;
{$ENDIF}

procedure TTexture.Assign(ATexture: TTexture);
begin
  if Assigned(ATexture) then begin
    FWidth:=ATexture.Width;
    FHeight:=ATexture.Height;
    FFrameCount:=ATexture.FrameCount;
    FHasTranparency:=ATexture.FHasTranparency;
    SetLength(Frames, FWidth*FHeight*FFrameCount);
    Move(ATexture.Frames[0], Frames[0], Length(Frames));
  end else begin
    FWidth:=1;
    FHeight:=1;
    FFrameCount:=1;
    FHasTranparency:=False;
    SetLength(Frames, 1);
    Frames[0]:=0;
  end;
  {$IFNDEF PETRAHW}
  FTexels:=@Frames[0];
  {$ENDIF}
end;

procedure TTexture.SetSize(AWidth, AHeight: Integer);
begin
  if AWidth < 1 then AWidth:=1;
  if AHeight < 1 then AHeight:=1;
  FWidth:=AWidth;
  FHeight:=AHeight;
  FFrameCount:=1;
  SetLength(Frames, AWidth*AHeight);
  {$IFDEF PETRAHW}
  NeedHWUpdate:=True;
  {$ELSE}
  FTexels:=@Frames[0];
  {$ENDIF}
end;

procedure TTexture.Clear;
begin
  {$IFDEF PETRAHW}
  // Make sure there is space for a single frame
  if not NeedHWUpdate then begin
    FFrameCount:=1;
    SetLength(Frames, FWidth*FHeight);
    NeedHWUpdate:=True;
  end;
  FillChar(Frames[0], FWidth*FHeight, 0);
  {$ELSE}
  FillChar(FTexels^, FWidth*FHeight, 0);
  {$ENDIF}
  FHasTranparency:=False;
end;

procedure TTexture.SetTexels(const Texels);
begin
  {$IFDEF PETRAHW}
  // Make sure there is space for a single frame
  if not NeedHWUpdate then begin
    FFrameCount:=1;
    SetLength(Frames, FWidth*FHeight);
    // Set the frame data
    Move(Texels, Frames[0], FWidth*FHeight);
    NeedHWUpdate:=True;
  end;
  {$ELSE}
  // Set the frame data
  Move(Texels, FTexels^, FWidth*FHeight);
  {$ENDIF}
  // Check for transparency
  CheckForTransparency;
end;

procedure TTexture.GetTexels(out Texels);
begin
  {$push}{$warn 5058 off}
  {$IFDEF PETRAHW}
  Move(Frames[0], Texels, FWidth*FHeight);
  {$ELSE}
  Move(FTexels^, Texels, FWidth*FHeight);
  {$ENDIF}
  {$pop}
end;

procedure TTexture.LoadRAWFile(AFileName: string);
var
  F: TStream;
  I: Integer;
begin
  // Load first frame
  F:=GetDataStream(AFileName);
  if not Assigned(F) then Exit;
  {$push}{$warn 4081 off}
  F.ReadBuffer(Frames[0], FWidth*FHeight);
  {$pop}
  F.Free;
  FFrameCount:=1;
  // Load any extra frames
  for I:=1 to 255 do begin
    F:=GetDataStream(StringReplace(AFileName, '.raw', '_' + IntToStr(I) + '.raw', [rfIgnoreCase]));
    if not Assigned(F) then Break;
    SetLength(Frames, Length(Frames) + FWidth*FHeight);
    {$push}{$warn 4081 off}
    F.ReadBuffer(Frames[I*FWidth*FHeight], FWidth*FHeight);
    {$pop}
    F.Free;
    Inc(FFrameCount);
  end;
  {$IFDEF PETRAHW}
  NeedHWUpdate:=True;
  {$ELSE}
  FTexels:=@Frames[0];
  {$ENDIF}
  // Check for transparency
  CheckForTransparency;
end;

procedure TTexture.CheckForTransparency;
var
  I: Integer;
begin
  FHasTranparency:=False;
  for I:=0 to High(Frames) do
    if Frames[I]=255 then begin
      FHasTranparency:=True;
      Break;
    end;
end;

{$IFDEF PETRAHW}
procedure TTexture.UpdateHWTexture;
var
  Filtered: Boolean;
  I: Integer;
begin
  // Create the textures if needed
  if Length(FHWTextures) <> FrameCount then begin
    for I:=0 to High(FHWTextures) do FHWTextures[I].Free;
    SetLength(FHWTextures, FrameCount);
    for I:=0 to High(FHWTextures) do FHWTextures[I]:=GRenderer.Rasterizer.CreateTexture;
  end;
  if (Width > 0) and (Height > 0) and (Length(Frames) > 0) then begin
    if Filter=tfNearest then
      Filtered:=False
    else
      Filtered:=GSettings.BilinearFiltering;
    for I:=0 to FrameCount - 1 do
      TRasterTexture(FHWTextures[I]).SetData(Width, Height, Filtered, GRenderer.ColorPalette,
        Frames[I*Width*Height]);
  end;
  NeedHWUpdate:=False;
end;
{$ENDIF}

end.
