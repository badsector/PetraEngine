{ Common sound driver functionality }
unit SndDrv;

interface

uses
  Classes, Sounds;

const
  // Maximum number of sound channels
  MaxSoundChannels = 32;
  {$IFDEF PETRASDL}
  {$IFDEF PETRAGCW}
  MaxSoundBuffers = 3;
  SoundBufferSize = 4096;
  {$ELSE}
  MaxSoundBuffers = 2;
  SoundBufferSize = 2048;
  {$ENDIF}
  {$ELSE}
  {$IFDEF WINDOWS}
  MaxSoundBuffers = 4;
  SoundBufferSize = 1024;
  {$ELSE}
  {$IFDEF ANDROID}
  MaxSoundBuffers = 3;
  SoundBufferSize = 1024;
  {$ELSE}
  MaxSoundBuffers = 2;
  SoundBufferSize = 2048;
  {$ENDIF}
  {$ENDIF}
  {$ENDIF}

type
  { TSoundChannelState - The playback state of a sound channel }
  TSoundChannelState = (scsUnused, scsPlay, scsRepeat);

  { TSoundChannel - Single sound channel }
  TSoundChannel = record
    // Playback data
    Data: PByte;
    // Data length
    Length: Integer;
    // Related clip
    Clip: TSoundClip;
    // Playback offset
    Offset: Integer;
    // Channel state
    State: TSoundChannelState;
    // Left and right channel volumes
    LeftVolume, RightVolume: Byte;
  end;

  { TSoundDriver - Base class for sound playback and mixing }
  TSoundDriver = class
  private
    // Property storage
    FMaxChannels: Integer;
  protected
    // Sound channels
    Channels: array [0..MaxSoundChannels - 1] of TSoundChannel;
    // Mix buffers
    {$IFDEF GO32V2}
    Buffer: array [0..2047] of SmallInt;
    {$ELSE}
    Buffers: array [0..MaxSoundBuffers - 1, 0..SoundBufferSize - 1] of SmallInt;
    // Next buffer to mix
    NextBuffer: Integer;
    Mixed: Integer;
    {$ENDIF}
    // Set to true by the driver if sound is available
    Available: Boolean;
    // Set to true by the driver to indicate that a new mix should be made
    NeedMix: Boolean;
    // Silent counter (0=no silence)
    SilentCounter: Integer;
    {$IFDEF EDITOR}
    // Force silence for editor
    EditorSilence: Boolean;
    {$ENDIF}
    // Called to initialize the driver-specific part
    procedure InitializeDriver; virtual; abstract;
    // Called to lock the buffers
    procedure LockBuffers; virtual; abstract;
    // Called to unlock the buffers
    procedure UnlockBuffers; virtual; abstract;
    // Mix the buffer now
    procedure MixNow;
  public
    constructor Create;
    // Initialize the sound driver
    procedure Initialize;
    // Find an unused channel, returns -1 if all channels are under use
    function UnusedChannel: Integer;
    // Increase the silence counter
    procedure Silence;
    // Decrease the silence counter
    procedure Unsilence;
    // Set the clip to play for the given sound channel in repeat mode,
    // passing nil will release the channel.
    procedure SetChannelClip(AChannel: Integer; AClip: TSoundClip; AVolume: Byte);
    // Set the volume for the given channel
    procedure SetChannelVolume(AChannel: Integer; Left, Right: Byte);
    // Return the clip currently used by the given channel, returns nil if
    // the channel is not in use
    function GetChannelClip(AChannel: Integer): TSoundClip; inline;
    // Play the given sound clip once at any free channel at the given volume,
    // returns the channel the clip is playing at
    function PlayClip(AClip: TSoundClip; AVolume: Byte=200): Integer;
    // Play the given sound clip repeatedly at any free channel at the given
    // volume, returns the channel the clip is playing at
    function RepeatClip(AClip: TSoundClip; AVolume: Byte=200): Integer;
    // Stop and release any channel that is playing the given clip
    procedure StopClip(AClip: TSoundClip);
    // Update the sound driver (must be called every frame to ensure the audio
    // buffer is ready to be picked up by the interrupt)
    procedure Update; virtual;
    // Maximum channels to allow for mixing (must be between 1 and MaxSoundChannels)
    property MaxChannels: Integer read FMaxChannels write FMaxChannels;
  end;

implementation

uses
  SysUtils, Maths, Settings
  {$IFDEF EDITOR}
  , Engine
  {$ENDIF};

{ TSoundDriver }
constructor TSoundDriver.Create;
begin
  FMaxChannels:=MaxSoundChannels;
end;

procedure TSoundDriver.MixNow;
var
  L, R, I, J, Sample: Integer;
begin
  // Fill the buffer
  {$IFDEF GO32V2}
  for I:=0 to High(Buffer) div 2 do begin
  {$ELSE}
  for I:=0 to High(Buffers[0]) div 2 do begin
  {$ENDIF}
    // Calculate left and right samples
    L:=0;
    R:=0;
    for J:=0 to MaxChannels-1 do with Channels[J] do begin
      // Ignore this channel if it isn't used
      if State=scsUnused then Continue;
      // Mix channel data
      Sample:=(Integer(Data[Offset]) - $80)*255;
      L += (Integer(LeftVolume) * Sample) div 255;
      R += (Integer(RightVolume) * Sample) div 255;
      // Advance the offset
      Inc(Offset);
      if Offset >= Length then begin
        // Repeat the sample if this is a repeating channel
        if State=scsRepeat then
          Offset:=0
        else begin // ...otherwise release the channel
          State:=scsUnused;
          // If there is a clip associated, decrease its use count
          if Assigned(Clip) then Clip.UsedCount:=Clip.UsedCount - 1;
        end;
      end;
    end;
    // Set left and right samples
    {$IFDEF GO32V2}
    Buffer[I*2]:=Clamp(L, -32768, 32767);
    Buffer[I*2 + 1]:=Clamp(R, -32768, 32767);
    {$ELSE}
    Buffers[NextBuffer, I*2]:=Clamp(L, -32768, 32767);
    Buffers[NextBuffer, I*2 + 1]:=Clamp(R, -32768, 32767);
    {$ENDIF}
  end;
  NeedMix:=False;
  {$IFNDEF GO32V2}
  Inc(Mixed);
  {$ENDIF}
end;

procedure TSoundDriver.Initialize;
begin
  // Ensure the channels are in the proper range
  FMaxChannels:=Clamp(FMaxChannels, 1, MaxSoundChannels);
  // Reset silent counter
  SilentCounter:=0;
  // Fill the mix buffers with the baseline value
  {$IFDEF GO32V2}
  FillChar(Buffer, SizeOf(Buffer), 0);
  {$ELSE}
  FillChar(Buffers, SizeOf(Buffers), 0);
  {$ENDIF}
  // Initialize the driver
  Available:=False;
  if not GSettings.NoSound then InitializeDriver;
end;

function TSoundDriver.UnusedChannel: Integer;
var
  I: Integer;
begin
  for I:=0 to MaxChannels - 1 do
    if Channels[I].State=scsUnused then Exit(I);
  Result:=-1;
end;

procedure TSoundDriver.Silence;
begin
  if not Available then Exit;
  LockBuffers;
  Inc(SilentCounter);
  UnlockBuffers;
end;

procedure TSoundDriver.Unsilence;
begin
  if not Available then Exit;
  LockBuffers;
  Dec(SilentCounter);
  UnlockBuffers;
end;

procedure TSoundDriver.SetChannelClip(AChannel: Integer; AClip: TSoundClip;
  AVolume: Byte);
begin
  LockBuffers;
  // If the channel wasn't unused, decrease previous clip use cound
  if (Channels[AChannel].State <> scsUnused) and
     Assigned(Channels[AChannel].Clip) then begin
    Channels[AChannel].Clip.UsedCount:=Channels[AChannel].Clip.UsedCount - 1;
  end;
  if Assigned(AClip) then begin
    // Set up the channel
    Channels[AChannel].State:=scsRepeat;
    Channels[AChannel].Data:=AClip.Data;
    Channels[AChannel].Length:=AClip.Length;
    Channels[AChannel].Clip:=AClip;
    Channels[AChannel].LeftVolume:=AVolume;
    Channels[AChannel].RightVolume:=AVolume;
    Channels[AChannel].Offset:=0;
    // Increase clip count
    AClip.UsedCount:=AClip.UsedCount + 1;
  end else begin
    Channels[AChannel].State:=scsUnused;
  end;
  UnlockBuffers;
end;

procedure TSoundDriver.SetChannelVolume(AChannel: Integer; Left, Right: Byte);
begin
  LockBuffers;
  Channels[AChannel].LeftVolume:=Left;
  Channels[AChannel].RightVolume:=Right;
  UnlockBuffers;
end;

function TSoundDriver.GetChannelClip(AChannel: Integer): TSoundClip;
begin
  LockBuffers;
  if Channels[AChannel].State=scsUnused then
    Result:=nil
  else
    Result:=Channels[AChannel].Clip;
  UnlockBuffers;
end;

function TSoundDriver.PlayClip(AClip: TSoundClip; AVolume: Byte): Integer;
begin
  // Ignore request for nil clips
  if not Assigned(AClip) then Exit(-1);
  // Find unused channel
  Result:=UnusedChannel;
  if Result=-1 then Exit;
  // Lock the buffers
  LockBuffers;
  // Set up the channel
  Channels[Result].State:=scsPlay;
  Channels[Result].Data:=AClip.Data;
  Channels[Result].Length:=AClip.Length;
  Channels[Result].Clip:=AClip;
  Channels[Result].LeftVolume:=AVolume;
  Channels[Result].RightVolume:=AVolume;
  Channels[Result].Offset:=0;
  // Increase clip count
  AClip.UsedCount:=AClip.UsedCount + 1;
  // Unlock buffers
  UnlockBuffers;
end;

function TSoundDriver.RepeatClip(AClip: TSoundClip; AVolume: Byte): Integer;
begin
  // Ignore request for nil clips
  if not Assigned(AClip) then Exit(-1);
  // Find unused channel
  Result:=UnusedChannel;
  if Result=-1 then Exit;
  // Lock the buffers
  LockBuffers;
  // Set up the channel
  Channels[Result].State:=scsRepeat;
  Channels[Result].Data:=AClip.Data;
  Channels[Result].Length:=AClip.Length;
  Channels[Result].Clip:=AClip;
  Channels[Result].LeftVolume:=AVolume;
  Channels[Result].RightVolume:=AVolume;
  Channels[Result].Offset:=0;
  // Increase clip count
  AClip.UsedCount:=AClip.UsedCount + 1;
  // Unlock the buffers
  UnlockBuffers;
end;

procedure TSoundDriver.StopClip(AClip: TSoundClip);
var
  I: Integer;
begin
  // Ignore request for nil clips
  if not Assigned(AClip) then Exit;
  // Lock the buffers
  LockBuffers;
  // Stop channels
  for I:=0 to MaxChannels - 1 do
    if Channels[I].Clip=AClip then
      Channels[I].State:=scsUnused;
  // Reset used count for the clip
  AClip.UsedCount:=0;
  // Unlock the buffers
  UnlockBuffers;
end;

procedure TSoundDriver.Update;
begin
  // Ignore if we have no sound or no need for mix
  if not Available then Exit;
  // Lock the buffers so we can mix the active channels
  LockBuffers;
  if not NeedMix then begin
    {$IFDEF EDITOR}
    // Sound preview is disabled in the editor, ensure the buffers are clean
    // even when they are updated from the sound thread
    EditorSilence:=(GPlayMode=pmEditor) and not GUpdateEditorSounds;
    {$ENDIF}
    UnlockBuffers;
    Exit;
  end;
  {$IFDEF EDITOR}
  // If sound preview is disabled in the editor, just fill the buffer with
  // silence (note: this code will only be executed on Windows - the OpenAL
  // backend for the editor never sets NeedMix to True)
  if (GPlayMode=pmEditor) and not GUpdateEditorSounds then begin
    NeedMix:=False;
    FillChar(Buffers[NextBuffer], SizeOf(Buffers[NextBuffer]), 0);
    // Unlock the buffers
    UnlockBuffers;
    Exit;
  end;
  {$ENDIF}
  // Mix the buffer
  MixNow;
  // Unlock the buffers
  UnlockBuffers;
end;

end.
