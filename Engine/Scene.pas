{ Scene tree }
unit Scene;

interface

uses
  Maths, Cameras, Meshes, Textures, Renderer;

type

  { Forward class declarations }
  TSceneSector = class;
  TScene = class;

  { TSceneVisibilityMode - Affect how visibility is handled for the scene }
  TSceneVisibilityMode = (
    // Everything is always visible and rendered
    svmAlways,
    // Use portals between scene sectors to determine visibility
    svmPortals
  );

  { TSceneNode - A scene node }
  TSceneNode = class
  private
    // Property storage
    FScene: TScene;
    FParent: TSceneNode;
    FChildren: array of TSceneNode;
    FLocalMatrix, FWorldMatrix: TMatrix;
    FAABox: TAABox;
    FVisible: Boolean;
    FTestOcclusion: Boolean;
    function GetChildren(AIndex: Integer): TSceneNode; inline;
    function GetChildCount: Integer; inline;
  private
    // Set the scene for the given node and its children to the given value
    class procedure SetNodeScene(ANode: TSceneNode; AScene: TScene);
    // Render this node if it is near the camera
    procedure RenderNodeIfVisible;
  protected
    // Render this node
    procedure RenderNode; virtual;
    // Render this node's children
    procedure RenderChildren;
    // Return this node's own AABox in local coordinates
    function GetOwnLocalAABox: TAABox; virtual;
  public
    // Construct a scene node
    constructor Create;
    // Destructor
    destructor Destroy; override;
    // Add a child node
    procedure AddChild(ANode: TSceneNode);
    // Remove the child node at the given index
    procedure RemoveChildAt(AIndex: Integer);
    // Remove a child node
    procedure RemoveChild(ANode: TSceneNode);
    // Remove and delete a child node at the given index
    procedure DeleteChildAt(AIndex: Integer);
    // Remove and delete a child node
    procedure DeleteChild(ANode: TSceneNode);
    // Delete all child nodes
    procedure DeleteAllChildren;
    // Returns the index of the given child node
    function IndexOfChild(ANode: TSceneNode): Integer;
    // Update the world matrix of this node and optionally its children
    procedure UpdateWorldMatrix(Recursive: Boolean=True);
    // Update this node's and its children's world AABox (needs up to date
    // world matrix so you may also need to call UpdateWorldMatrix)
    procedure UpdateAABox;
  public
    // The scene this node is part of
    property Scene: TScene read FScene;
    // The node's parent
    property Parent: TSceneNode read FParent;
    // The node's children
    property Children[AIndex: Integer]: TSceneNode read GetChildren;
    // The number of children this node has
    property ChildCount: Integer read GetChildCount;
    // The node's local transformation matrix
    property LocalMatrix: TMatrix read FLocalMatrix write FLocalMatrix;
    // The world transformation matrix (might be out of date)
    property WorldMatrix: TMatrix read FWorldMatrix;
    // The world space axis aligned bounding box for this node and its
    // children (might be out of date)
    property AABox: TAABox read FAABox;
    // The node is visible
    property Visible: Boolean read FVisible write FVisible;
    // Perform an occlusion test before rendering this node (note that the node
    // may still be hidden if the sector it is in is invisible)
    property TestOcclusion: Boolean read FTestOcclusion write FTestOcclusion;
  end;

  { TMeshNode - A mesh scene node }
  TMeshNode = class(TSceneNode)
  private
    // Property storage
    FMesh: TMesh;
    FTextureOverride: TTexture;
    FLight: Byte;
    FLights: PByte;
    FDepthBias: Byte;
    FOccluder: Boolean;
  protected
    // TSceneNode methods
    procedure RenderNode; override;
    function GetOwnLocalAABox: TAABox; override;
  public
    constructor Create;
    // The mesh for this node
    property Mesh: TMesh read FMesh write FMesh;
    // Texture override for this mesh node (nil uses the default texture)
    property TextureOverride: TTexture read FTextureOverride write FTextureOverride;
    // Constant light level for rendering the node
    property Light: Byte read FLight write FLight;
    // Pointer to array for per-vertex light levels for rendering the node
    property Lights: PByte read FLights write FLights;
    // Depth bias to use for rendering the mesh
    property DepthBias: Byte read FDepthBias write FDepthBias;
    // The mesh generates occlusion data
    property Occluder: Boolean read FOccluder write FOccluder;
  end;

  { TOwnedMeshNode - A mesh scene node that owns its mesh }
  TOwnedMeshNode = class(TMeshNode)
  public
    destructor Destroy; override;
  end;

  { TScenePortal - A portal from one scene sector to another }
  TScenePortal = class
  private
    // Property storage
    FEnabled: Boolean;
    FTarget: TSceneSector;
  private
    // Portal coordinates
    A, B, C, D: TVector;
    // Portal direction
    Dir: TVector;
    // Portal centroid
    Centroid: TVector;
  public
    // If this is False, the portal will be always ignored
    property Enabled: Boolean read FEnabled write FEnabled;
    // The portal target
    property Target: TSceneSector read FTarget;
  end;

  { TSceneSector - A scene sector used to group nodes for visibility }
  TSceneSector = class
  private
    // Property storage
    FAABox: TAABox;
    FPortals: array of TScenePortal;
    FVisible: Boolean;
    function GetPortals(AIndex: Integer): TScenePortal;
    function GetPortalCount: Integer;
  private
    // Followed flag (used in updates to avoid following portals back)
    Followed: Boolean;
  public
    // Create a sector with the given dimensions
    destructor Destroy; override;
    // Remove all portals from the sector
    procedure RemovePortals;
    // Add a new portal to the sector
    function AddPortal(const A, B, C, D: TVector; Target: TSceneSector): TScenePortal;
  public
    // Sector dimensions
    property AABox: TAABox read FAABox write FAABox;
    // Portals to other sectors
    property Portals[AIndex: Integer]: TScenePortal read GetPortals;
    // Number of portals in this sector
    property PortalCount: Integer read GetPortalCount;
    // Current visibility state
    property Visible: Boolean read FVisible write FVisible;
  end;

  { TScene - the scene }
  TScene = class(TSceneNode)
  private
    // Property storage
    FCamera: TCamera;
    FSectors: array of TSceneSector;
    FBackgroundColor: Integer;
    FVisibilityMode: TSceneVisibilityMode;
    procedure SetVisibilityMode(AVisibilityMode: TSceneVisibilityMode);
  private
    // Update sector visibility state using the current renderer state
    procedure UpdateVisibility;
  public
    // Construct the scene
    constructor Create;
    // Destroy the scene
    destructor Destroy; override;
    // Render the scene
    procedure Render;
    // Add the given sector to the scene
    procedure AddSector(ASector: TSceneSector);
    // Remove the given sector from the scene
    procedure RemoveSector(ASector: TSceneSector);
    // Remove all sectors from the scene
    procedure RemoveAllSectors;
    // Return the sector at the given position
    function SectorAt(const P: TVector): TSceneSector;
    // Return the first portal (if any) near the given location
    function FindPortalNear(const P: TVector; MaxDistance: Single): TScenePortal;
    // Return true if the given box overlaps one of the visible sectors
    function IsBoxVisible(const ABox: TAABox): Boolean;
  public
    // The scene's camera
    property Camera: TCamera read FCamera;
    // The scene's visibility mode
    property VisibilityMode: TSceneVisibilityMode read FVisibilityMode write SetVisibilityMode;
    // The background color to use for rendering the scene (0=world fog color)
    property BackgroundColor: Integer read FBackgroundColor write FBackgroundColor;
  end;

implementation

uses
  Engine, GfxDrv;

{ TSceneNode }
constructor TSceneNode.Create;
begin
  inherited Create;
  FLocalMatrix.Identity;
  FWorldMatrix.Identity;
  FVisible:=True;
end;

destructor TSceneNode.Destroy;
begin
  if Assigned(Parent) then Parent.RemoveChild(Self);
  DeleteAllChildren;
  inherited Destroy;
end;

class procedure TSceneNode.SetNodeScene(ANode: TSceneNode; AScene: TScene);
var
  I: Integer;
begin
  ANode.FScene:=AScene;
  for I:=0 to High(ANode.FChildren) do SetNodeScene(ANode.FChildren[I], AScene);
end;

function TSceneNode.GetChildren(AIndex: Integer): TSceneNode;
begin
  Result:=FChildren[AIndex];
end;

function TSceneNode.GetChildCount: Integer;
begin
  Result:=Length(FChildren);
end;

procedure TSceneNode.RenderNodeIfVisible;
begin
  if Visible and ((Scene.VisibilityMode=svmAlways) or
    (GRenderer.IsBoxVisible(AABox) and Scene.IsBoxVisible(AABox) and
      ((not TestOcclusion) or GRenderer.BoxWasNotOccluded(AABox)))) then
        RenderNode;
end;

procedure TSceneNode.RenderNode;
begin
  RenderChildren;
end;

procedure TSceneNode.RenderChildren;
var
  I: Integer;
begin
  for I:=0 to ChildCount - 1 do Children[I].RenderNodeIfVisible;
end;

function TSceneNode.GetOwnLocalAABox: TAABox;
begin
  Result.Min.Zero;
  Result.Max.Zero;
end;

procedure TSceneNode.AddChild(ANode: TSceneNode);
begin
  // Remove the child from any existing parent
  if Assigned(ANode.FParent) then ANode.FParent.RemoveChild(ANode);
  // Add the child to this node
  SetLength(FChildren, Length(FChildren) + 1);
  FChildren[High(FChildren)]:=ANode;
  ANode.FParent:=Self;
  // Store the scene to the node and all its subnodes
  SetNodeScene(ANode, FScene);
end;

procedure TSceneNode.RemoveChildAt(AIndex: Integer);
var
  I: Integer;
begin
  // Ensure the index is valid
  if (AIndex < 0) or (AIndex >= ChildCount) then Exit;
  // Disconnect the child from this node
  SetNodeScene(FChildren[AIndex], nil);
  FChildren[AIndex].FParent:=nil;
  // Remove the child from the children array
  for I:=AIndex to High(FChildren) - 1 do FChildren[I]:=FChildren[I + 1];
  SetLength(FChildren, Length(FChildren) - 1);
end;

procedure TSceneNode.RemoveChild(ANode: TSceneNode);
begin
  RemoveChildAt(IndexOfChild(ANode));
end;

procedure TSceneNode.DeleteChildAt(AIndex: Integer);
var
  Child: TSceneNode;
begin
  if (AIndex < 0) or (AIndex >= ChildCount) then Exit;
  Child:=FChildren[AIndex];
  RemoveChildAt(AIndex);
  Child.Free;
end;

procedure TSceneNode.DeleteChild(ANode: TSceneNode);
begin
  DeleteChildAt(IndexOfChild(ANode));
end;

procedure TSceneNode.DeleteAllChildren;
var
  I: Integer;
begin
  for I:=0 to ChildCount - 1 do begin
    Children[I].FParent:=nil;
    SetNodeScene(Children[I], nil);
    Children[I].Free;
  end;
  SetLength(FChildren, 0);
end;

function TSceneNode.IndexOfChild(ANode: TSceneNode): Integer;
begin
  for Result:=0 to ChildCount - 1 do
    if Children[Result]=ANode then Exit;
  Result:=-1;
end;

procedure TSceneNode.UpdateWorldMatrix(Recursive: Boolean);
var
  I: Integer;
begin
  FWorldMatrix:=FLocalMatrix;
  if Assigned(Parent) then FWorldMatrix.SwapMultiply(Parent.WorldMatrix);
  if Recursive then
    for I:=0 to ChildCount - 1 do Children[I].UpdateWorldMatrix;
end;

procedure TSceneNode.UpdateAABox;
var
  I: Integer;
begin
  FAABox:=AABoxOfTransformedAABox(GetOwnLocalAABox, WorldMatrix);
  for I:=0 to ChildCount - 1 do begin
    Children[I].UpdateAABox;
    FAABox.Include(Children[I].FAABox);
  end;
end;

{ TMeshNode }
constructor TMeshNode.Create;
begin
  inherited Create;
  TestOcclusion:=True; // Test meshes for occlusion
  Occluder:=False;
  FLight:=255;
end;

procedure TMeshNode.RenderNode;
var
  OldTexture: TTexture;
begin
  if Assigned(Mesh) then begin
    if Assigned(TextureOverride) then begin
      OldTexture:=Mesh.Texture;
      Mesh.Texture:=TextureOverride;
    end;
    if Assigned(Lights) then
      Mesh.Render(WorldMatrix, Lights, DepthBias, Occluder)
    else
      Mesh.Render(WorldMatrix, Light, DepthBias, Occluder);
    if Assigned(TextureOverride) then
      Mesh.Texture:=OldTexture;
  end;
  RenderChildren;
end;

function TMeshNode.GetOwnLocalAABox: TAABox;
begin
  if Assigned(Mesh) then
    Result:=Mesh.AABox
  else
    Result:=inherited GetOwnLocalAABox;
end;

{ TOwnedMeshNode }
destructor TOwnedMeshNode.Destroy;
begin
  if Assigned(FMesh) then FMesh.Free;
  inherited Destroy;
end;

{ TSceneSector }
destructor TSceneSector.Destroy;
begin
  RemovePortals;
  inherited Destroy;
end;

function TSceneSector.GetPortals(AIndex: Integer): TScenePortal;
begin
  Result:=FPortals[AIndex];
end;

function TSceneSector.GetPortalCount: Integer;
begin
  Result:=Length(FPortals);
end;

procedure TSceneSector.RemovePortals;
var
  I: Integer;
begin
  for I:=0 to High(FPortals) do FPortals[I].Free;
  FPortals:=nil;
end;

function TSceneSector.AddPortal(const A, B, C, D: TVector; Target: TSceneSector): TScenePortal;
begin
  Result:=TScenePortal.Create;
  Result.A:=A;
  Result.B:=B;
  Result.C:=C;
  Result.D:=D;
  Result.Dir:=TriangleNormal(A, B, C).Added(TriangleNormal(A, C, D)).Normalized;
  Result.Centroid:=A.Added(B).Added(C).Added(D).Scaled(0.25);
  Result.FTarget:=Target;
  Result.Enabled:=True;
  SetLength(FPortals, Length(FPortals) + 1);
  FPortals[High(FPortals)]:=Result;
end;

{ TScene }
constructor TScene.Create;
begin
  inherited Create;
  FScene:=Self;
  FCamera:=TCamera.Create;
  FVisibilityMode:=svmPortals;
end;

destructor TScene.Destroy;
begin
  FCamera.Free;
  inherited Destroy;
end;

procedure TScene.SetVisibilityMode(AVisibilityMode: TSceneVisibilityMode);
begin
  if FVisibilityMode=AVisibilityMode then Exit;
  FVisibilityMode:=AVisibilityMode;
end;

procedure TScene.UpdateVisibility;
var
  PM, VM: TMatrix;
  I, J: Integer;
  SectorsReset: Boolean;
  CamBox: TAABox;

  // Follow the given portal to mark other sectors
  procedure FollowPortal(APortal: TScenePortal; VMinX, VMinY, VMaxX, VMaxY: Single);
  var
    PA, PB, PC, PD: TVector;
    MinX, MinY, MaxX, MaxY, MinZ: Single;
    I: Integer;
    SaveFollowed: Boolean;
    {$IFDEF EDITOR}
    PortalColor: Byte;
    {$ENDIF}
  begin
    {$IFNDEF EDITOR}
    // Ignore the portal if it is disabled
    if not APortal.Enabled then Exit;
    {$ENDIF}
    // Ignore the portal if it was already followed
    if APortal.Target.Followed then Exit;
    // If the portal is too far from the camera, ignore it
    if DistanceSq(APortal.Centroid, GRenderer.Camera.Position) > Sqr(GRenderer.Camera.FarPlane) then Exit;
    // If the portal is not facing the camera, ignore it
    if APortal.Dir.Dot(GRenderer.Camera.Direction) >= 0.5 then Exit;
    // Project the portal's geometry to the screen
    PA:=VM.Transformed(APortal.A);
    PB:=VM.Transformed(APortal.B);
    PC:=VM.Transformed(APortal.C);
    PD:=VM.Transformed(APortal.D);
    // Ignore the portal if it is behind the near plane
    if (PA.Z < -GRenderer.Camera.NearPlane) and
       (PB.Z < -GRenderer.Camera.NearPlane) and
       (PC.Z < -GRenderer.Camera.NearPlane) and
       (PD.Z < -GRenderer.Camera.NearPlane) then Exit;
    // Clamp the portal to near plane
    PA.Z:=Max(PA.Z, GRenderer.Camera.NearPlane);
    PB.Z:=Max(PB.Z, GRenderer.Camera.NearPlane);
    PC.Z:=Max(PC.Z, GRenderer.Camera.NearPlane);
    PD.Z:=Max(PD.Z, GRenderer.Camera.NearPlane);
    // Save portal MinZ for occlusion test later
    MinZ:=Min(Min(PA.Z, PB.Z), Min(PC.Z, PD.Z));
    // Transform to screen space
    PA:=PM.TransformedProj(PA);
    PB:=PM.TransformedProj(PB);
    PC:=PM.TransformedProj(PC);
    PD:=PM.TransformedProj(PD);
    // Calculate bounding box
    MinX:=Min(Min(PA.X, PB.X), Min(PC.X, PD.X));
    MinY:=Min(Min(PA.Y, PB.Y), Min(PC.Y, PD.Y));
    MaxX:=Max(Max(PA.X, PB.X), Max(PC.X, PD.X));
    MaxY:=Max(Max(PA.Y, PB.Y), Max(PC.Y, PD.Y));
    // Reject the portal if the bounding box is outside visible bounds
    if (MinX > VMaxX) or (MaxX < VMinX) or (MinY > VMaxY) or (MaxY < VMinY) then Exit;
    // Clamp the portal area to visible bounds
    MinX:=Max(MinX, VMinX);
    MinY:=Max(MinY, VMinY);
    MaxX:=Min(MaxX, VMaxX);
    MaxY:=Min(MaxY, VMaxY);
    // Reject the portal if the bounding box is too small
    if (MaxX-MinX < 1/320) or (MaxY-MinY < 1/200) then exit;
    {$IFNDEF EDITOR}
    // Check if the portal is obscured using the last frame's data
    if GRenderer.Rasterizer.WasRectOccluded(
      Trunc(MinX*(HalfHRes - 1) + HalfHRes), Trunc(MinY*(HalfVRes - 1) + HalfVRes),
      Trunc(MaxX*(HalfHRes - 1) + HalfHRes), Trunc(MaxY*(HalfVRes - 1) + HalfVRes),
      MinZ) then Exit;
    {$ENDIF}
    // Show portals
    {$IFDEF EDITOR}
    if GShowPortals then begin
      PortalColor:=13;
      if not APortal.Enabled then PortalColor:=8;
      if GRenderer.Rasterizer.WasRectOccluded(
        Round(MinX*(HalfHRes - 1) + HalfHRes), Round(MinY*(HalfVRes - 1) + HalfVRes),
        Round(MaxX*(HalfHRes - 1) + HalfHRes), Round(MaxY*(HalfVRes - 1) + HalfVRes),
        MinZ) then PortalColor:=7;;
      AddEditorRect(Round(MinX*160 + 160), Round(MinY*100 + 100),
        Round(MaxX*160 + 160), Round(MaxY*100 + 100),
        PortalColor);
      if GRenderer.Rasterizer.WasRectOccluded(
        Round(MinX*(HalfHRes - 1) + HalfHRes), Round(MinY*(HalfVRes - 1) + HalfVRes),
        Round(MaxX*(HalfHRes - 1) + HalfHRes), Round(MaxY*(HalfVRes - 1) + HalfVRes),
        MinZ) then Exit;
    end;
    // Ignore the portal if it is disabled
    if not APortal.Enabled then Exit;
    {$ENDIF}
    // Mark the target as visible and follow its portals
    APortal.Target.Visible:=True;
    SaveFollowed:=APortal.Target.Followed;
    APortal.Target.Followed:=True;
    for I:=0 to High(APortal.Target.FPortals) do
      FollowPortal(APortal.Target.FPortals[I], MinX, MinY, MaxX, MaxY);
    APortal.Target.Followed:=SaveFollowed;
  end;

begin
  {$IFDEF EDITOR}
  if GLockVisibilityUpdates then Exit;
  {$ENDIF}
  // If the mode is to always render everything, set all sectors to visible
  if VisibilityMode=svmAlways then begin
    for I:=0 to High(FSectors) do FSectors[I].Visible:=False;
    Exit;
  end;
  // Start from the renderer's camera (note that the renderer may not be using
  // the scene's own camera - e.g. in case where it has been overridden)
  CamBox.Min:=GRenderer.Camera.Position;
  CamBox.Max:=CamBox.Min;
  CamBox.Grow(GRenderer.Camera.NearPlane);
  // Calculate the projection and view matrices from the renderer's camera
  PM:=GRenderer.Camera.ProjectionMatrix;
  VM:=GRenderer.Camera.ViewMatrix;
  // Find sectors
  SectorsReset:=False;
  // Try to find sectors for the current start position
  for I:=0 to High(FSectors) do if FSectors[I].AABox.Overlaps(CamBox) then begin
    // Mark all sectors as invisible only if any sector was found
    if not SectorsReset then begin
      SectorsReset:=True;
      for J:=0 to High(FSectors) do FSectors[J].Visible:=False;
    end;
    // Follow this sector
    FSectors[I].Followed:=True;
    for J:=0 to High(FSectors[I].FPortals) do
      FollowPortal(FSectors[I].FPortals[J], -1, -1, 1, 1);
    FSectors[I].Followed:=False;
    FSectors[I].Visible:=True;
  end;
end;

procedure TScene.Render;
begin
  // Update the world matrix for all nodes
  UpdateWorldMatrix;
  // Update all the bounding boxes
  UpdateAABox;
  // Update visibility state for the scene sectors using current render state
  UpdateVisibility;
  // Render the nodes
  RenderChildren;
end;

procedure TScene.AddSector(ASector: TSceneSector);
begin
  SetLength(FSectors, Length(FSectors) + 1);
  FSectors[High(FSectors)]:=ASector;
end;

procedure TScene.RemoveSector(ASector: TSceneSector);
var
  Index, I: Integer;
begin
  Index:=-1;
  for I:=0 to High(FSectors) do
    if FSectors[I]=ASector then begin
      Index:=I;
      Break;
    end;
  if Index=-1 then Exit;
  for I:=Index to High(FSectors) - 1 do
    FSectors[I]:=FSectors[I + 1];
  SetLength(FSectors, Length(FSectors) - 1);
end;

procedure TScene.RemoveAllSectors;
begin
  FSectors:=nil;
end;

function TScene.SectorAt(const P: TVector): TSceneSector;
var
  I: Integer;
begin
  for I:=0 to High(FSectors) do
    if FSectors[I].FAABox.Includes(P) then Exit(FSectors[I]);
  Result:=nil;
end;

function TScene.FindPortalNear(const P: TVector; MaxDistance: Single): TScenePortal;
var
  I, J: Integer;
begin
  MaxDistance *= MaxDistance;
  for I:=0 to High(FSectors) do with FSectors[I] do
    for J:=0 to High(FPortals) do
      if DistanceSq(FPortals[J].Centroid, P) <= MaxDistance then Exit(FPortals[J]);
  Result:=nil;
end;

function TScene.IsBoxVisible(const ABox: TAABox): Boolean;
var
  I: Integer;
begin
  // For the "always" visibility mode, always treat boxes as visible
  if VisibilityMode=svmAlways then Exit(True);
  // Check sectors
  for I:=0 to High(FSectors) do
    if FSectors[I].Visible and ABox.Overlaps(FSectors[I].FAABox) then
      Exit(True);
  // The box is not visible
  Result:=False;
end;

end.
