{ Direct3D Rasterizer }
unit D3DRaster;

interface

uses
  Classes, SysUtils, Windows, DirectDraw, Direct3D, Maths, Colors, Raster,
  HWRaster, Textures, GfxDrv;

type
  { TD3DTexture - Direct3D-specific data for a hardware accelerated texture }
  TD3DTexture = class(TRasterTexture)
  private
    // Texture surface
    FSurface: IDirectDrawSurface7;
    FWidth, FHeight: Integer;
    PaletteOK: Boolean;
  public
    // TRasterTexture methods
    procedure SetData(AWidth, AHeight: Integer; AFiltered: Boolean; APalette: TColorPalette; const Texels); override;
  end;

  { TD3DRasterizer - Direct3D Rasterizer }
  TD3DRasterizer = class(THardwareRasterizer)
  private
    // Property storage
    FDevice: IDirect3DDevice7;
  private
    // DirectDraw object
    DDraw: IDirectDraw7;
    // Direct3D object
    D3D: IDirect3D7;
    // Primary surface and backbuffer
    Primary, Back: IDirectDrawSurface7;
    // Palette to use for textures (if available)
    TexturePalette: IDirectDrawPalette;
  protected
    // THardwareRasterizer methods
    procedure BeforeBatchesDraw; override;
    procedure AfterBatchesDraw; override;
    procedure DrawBatch(const Batch: TTriBatch); override;
  public
    // TRasterizer methods
    function CreateTexture: TRasterTexture; override;
    function BeginFrame: Boolean; override;
    procedure FinishFrame; override;
    procedure Clear(R, G, B: Single); override;
    procedure SetMatrices(Projection, View: TMatrix); override;
    procedure SetFog(FogR, FogG, FogB, FogStart, FogEnd: Single); override;
    procedure RenderQuad(X1, Y1, S1, T1, X2, Y2, S2, T2: Integer; Blend: Single; Texture: TTexture); override;
    procedure DrawHUDImage(X, Y: Integer; Texture: TTexture; SX, SY, SW, SH: Integer); override;
  public
    // Create the rasterizer and initialize Direct3D, throws exception on error
    constructor Create(AWinHandle: HWND);
    destructor Destroy; override;
    // Begin the scene
    function BeginScene: Boolean;
    // End the scene
    procedure EndScene;
    // Flip the buffers
    procedure Flip;
  public
    // The Direct3D device
    property Device: IDirect3DDevice7 read FDevice;
  end;

implementation

uses
  Engine, Renderer, Settings;

var
  // Pixel format for textures
  TextureFormat: TDDPixelFormat;
  // All-zero guid
  ZeroGuid: TGUID;

{ TD3DTexture }
procedure TD3DTexture.SetData(AWidth, AHeight: Integer; AFiltered: Boolean;
  APalette: TColorPalette; const Texels);
var
  X, Y: Integer;
  ddsd: TDDSurfaceDesc2;
  Pixels: PWORD;
  BPixels: PBYTE;
  TexelBytes: PByte;
  Pals: array [0..255] of TPaletteEntry;
begin
  if (AWidth <> FWidth) or (AHeight <> FHeight) then begin
    FSurface:=nil;
    // Recreate the texture's surface
    ZeroMemory(@ddsd, SizeOf(ddsd));
    with ddsd do begin
      dwSize:=SizeOf(ddsd);
      dwFlags:=DDSD_CAPS or DDSD_WIDTH or DDSD_HEIGHT or DDSD_PIXELFORMAT or DDSD_TEXTURESTAGE;
      dwWidth:=AWidth;
      dwHeight:=AHeight;
      ddpfPixelFormat:=TextureFormat;
      ddsCaps.dwCaps:=DDSCAPS_TEXTURE;
      ddsCaps.dwCaps2:=DDSCAPS2_TEXTUREMANAGE;
    end;
    FWidth:=AWidth;
    FHeight:=AHeight;
    if Failed(TD3DRasterizer(GRenderer.Rasterizer).DDraw.CreateSurface(ddsd, FSurface, nil)) then begin
      MessageBox(GetForegroundWindow, 'Texture creation failed', 'Direct3D Error', MB_ICONERROR or MB_OK or MB_TOPMOST);
      Halt;
    end;
    PaletteOK:=False;
  end;
  if ((TextureFormat.dwFlags and DDPF_PALETTEINDEXED8)=DDPF_PALETTEINDEXED8) and not PaletteOK then begin
    PaletteOK:=True;
    for X:=0 to 255 do with APalette do begin
      Pals[X].peRed:=Red[X];
      Pals[X].peGreen:=Green[X];
      Pals[X].peBlue:=Blue[X];
    end;
    TD3DRasterizer(GRenderer.Rasterizer).TexturePalette.SetEntries(0, 0, 256, @Pals);
    FSurface.SetPalette(TD3DRasterizer(GRenderer.Rasterizer).TexturePalette);
  end;
  ZeroMemory(@ddsd, SizeOf(ddsd));
  ddsd.dwSize:=SizeOf(ddsd);
  if Failed(FSurface.Lock(nil, ddsd, 0, 0)) then begin
    MessageBox(GetForegroundWindow, 'Texture creation failed', 'Direct3D Error', MB_ICONERROR or MB_OK or MB_TOPMOST);
    Halt;
  end;
  TexelBytes:=PByte(@Texels);
  if (TextureFormat.dwFlags and DDPF_PALETTEINDEXED8)=DDPF_PALETTEINDEXED8 then begin
    for Y:=0 to AHeight - 1 do
      Move(TexelBytes[Y*AWidth], (PByte(ddsd.lpSurface) + Y*ddsd.lPitch)^, AWidth);
  end else if TextureFormat.dwRBitMask=$7C00 then begin
    for Y:=0 to AHeight - 1 do begin
      Pixels:=PWord(PByte(ddsd.lpSurface) + Y*ddsd.lPitch);
      for X:=0 to AWidth - 1 do with APalette do begin
        Pixels[X]:=((Red[TexelBytes[Y*AWidth + X]] >> 3) << 10) or
                   ((Green[TexelBytes[Y*AWidth + X]] >> 3) << 5) or
                   (Blue[TexelBytes[Y*AWidth + X]] >> 3);
      end;
    end;
  end else begin
    for Y:=0 to AHeight - 1 do begin
      Pixels:=PWord(PByte(ddsd.lpSurface) + Y*ddsd.lPitch);
      for X:=0 to AWidth - 1 do with APalette do begin
        Pixels[X]:=((Red[TexelBytes[Y*AWidth + X]] >> 3) << 11) or
                   ((Green[TexelBytes[Y*AWidth + X]] >> 2) << 5) or
                   (Blue[TexelBytes[Y*AWidth + X]] >> 3);
      end;
    end;
  end;
  FSurface.Unlock(nil)
end;

{ TD3DRasterizer }
function DDrawEnumProc(lpGUID: PGUID; lpDriverDesc: PChar; lpDriverName: PChar; lpContext: Pointer; hm: HMonitor): BOOL; stdcall;
type
  PPGUID = ^PGUID;
var
  DDraw: IDirectDraw7;
  Caps: TDDCaps;
begin
  Result:=True;
  DDraw:=nil;
  if Succeeded(DirectDrawCreateEx(lpGUID, DDraw, IID_IDirectDraw7, nil)) then begin
    ZeroMemory(@Caps, SizeOf(Caps));
    Caps.dwSize:=SizeOf(TDDCaps);
    if Succeeded(DDraw.GetCaps(@Caps, nil)) then begin
      if (Caps.dwCaps and DDCAPS_3D)=DDCAPS_3D then begin
        PPGUID(lpContext)^:=lpGUID;
      end;
    end;
    DDraw:=nil;
  end;
end;

function D3DDevEnumProc(A, B: PChar; const Desc: TD3DDeviceDesc7; lpContext: Pointer): HRESULT; stdcall;
begin
  if (Desc.dwDevCaps and D3DDEVCAPS_HWRASTERIZATION)=D3DDEVCAPS_HWRASTERIZATION then begin
    PD3DDeviceDesc7(lpContext)^:=Desc;
  end;
  Result:=D3DENUMRET_OK;
end;

function D3DPureZBufferEnumProc(var fmt: TDDPixelFormat; lpContext: Pointer): HRESULT; stdcall;
var
  outFmt: PDDPixelFormat;
begin
  outFmt:=PDDPixelFormat(lpContext);
  if fmt.dwFlags=DDPF_ZBUFFER then begin
    if outFmt^.dwZBufferBitDepth < fmt.dwZBufferBitDepth then
      outFmt^:=fmt;
  end;
  Result:=D3DENUMRET_OK;
end;

function D3DAnyZBufferEnumProc(var fmt: TDDPixelFormat; lpContext: Pointer): HRESULT; stdcall;
var
  outFmt: PDDPixelFormat;
begin
  outFmt:=PDDPixelFormat(lpContext);
  if (fmt.dwFlags and DDPF_ZBUFFER)=DDPF_ZBUFFER then begin
    if outFmt^.dwZBufferBitDepth < fmt.dwZBufferBitDepth then
      outFmt^:=fmt;
  end;
  Result:=D3DENUMRET_OK;
end;

function D3DTexFmtEnumProc(var fmt: TDDPixelFormat; lpContent: Pointer): HRESULT; stdcall;
begin
  if fmt.dwFourCC <> 0 then Exit(D3DENUMRET_OK);
  if not GSettings.NoIndexedTextures then begin
    if ((fmt.dwFlags and DDPF_PALETTEINDEXED8)=DDPF_PALETTEINDEXED8) and
        ((fmt.dwFlags and DDPF_ALPHAPIXELS)=0) and
        ((fmt.dwFlags and DDPF_ALPHAPREMULT)=0) then begin
      TextureFormat:=Fmt;
      Exit(D3DENUMRET_OK);
    end;
  end;
  if ((fmt.dwFlags and DDPF_RGB)=DDPF_RGB) and
     ((fmt.dwFlags and DDPF_ALPHAPIXELS)=0) and
     ((fmt.dwFlags and DDPF_ALPHAPREMULT)=0) and
     (fmt.dwRGBBitCount=16) then begin
    if (TextureFormat.dwFlags and DDPF_PALETTEINDEXED8)=0 then
      TextureFormat:=Fmt;
  end;
  Result:=D3DENUMRET_OK;
end;

constructor TD3DRasterizer.Create(AWinHandle: HWND);
var
  ddsd: TDDSurfaceDesc2;
  DepthPF: TDDPixelFormat;
  Depth: IDirectDrawSurface7;
  DevGUID: TGUID;
  DDGUID: PGUID;
  D3DDesc: TD3DDeviceDesc7;
  Pals: array [0..255] of TPaletteEntry;
begin
  inherited Create;
  // Create DirectDraw 7 object
  DDGUID:=nil;
  if Failed(DirectDrawEnumerateEx(@DDrawEnumProc, @DDGUID, DDENUM_ATTACHEDSECONDARYDEVICES or DDENUM_DETACHEDSECONDARYDEVICES or DDENUM_NONDISPLAYDEVICES)) then
    raise Exception.Create('Failed to enumerate Direct Draw 7 devices');
  if not IsEqualGUID(ZeroGuid, GSettings.D3DDevGUID) then begin
    if not Failed(DirectDrawCreateEx(@GSettings.D3DDevGUID, DDraw, IID_IDirectDraw7, nil)) then begin
      DDraw:=nil;
      DDGUID:=@GSettings.D3DDevGUID;
    end;
  end;
  if Failed(DirectDrawCreateEx(DDGUID, DDraw, IID_IDirectDraw7, nil)) then
    if Failed(DirectDrawCreateEx(nil, DDraw, IID_IDirectDraw7, nil)) then
      raise Exception.Create('Failed to create Direct Draw 7 object');
  // Setup cooperative level
  if Failed(DDraw.SetCooperativeLevel(AWinHandle, DDSCL_EXCLUSIVE or DDSCL_FULLSCREEN)) then
    raise Exception.Create('Failed to set Direct Draw 7 cooperative level');
  // Attempt to change the display mode
  if GSettings.D3DColorDepth=0 then begin
    if Failed(DDraw.SetDisplayMode(640, 480, 16, 0, 0)) then
      if Failed(DDraw.SetDisplayMode(640, 480, 32, 0, 0)) then
        raise Exception.Create('Failed to set 640x480 video mode');
  end else
    if Failed(DDraw.SetDisplayMode(640, 480, GSettings.D3DColorDepth, 0, 0)) then
      raise Exception.Create('Failed to set 640x480 video mode');
  // Create primary surface
  ZeroMemory(@ddsd, SizeOf(ddsd));
  with ddsd do begin
    dwSize:=SizeOf(ddsd);
    dwFlags:=DDSD_CAPS or DDSD_BACKBUFFERCOUNT;
    ddsCaps.dwCaps:=DDSCAPS_PRIMARYSURFACE or DDSCAPS_FLIP or DDSCAPS_COMPLEX or DDSCAPS_3DDEVICE;
    dwBackBufferCount:=1;
  end;
  if Failed(DDraw.CreateSurface(ddsd, Primary, nil)) then
    raise Exception.Create('Failed to create primary Direct Draw 7 surface');
  // Obtain backbuffer
  ZeroMemory(@ddsd.ddsCaps, SizeOf(ddsd.ddsCaps));
  ddsd.ddsCaps.dwCaps:=DDSCAPS_BACKBUFFER;
  if Failed(Primary.GetAttachedSurface(ddsd.ddsCaps, Back)) then
    raise Exception.Create('Failed to obtain back buffer from the primary Direct Draw 7 surface');
  // Obtain Direct3D object interface
  if Failed(DDraw.QueryInterface(IID_IDirect3D7, D3D)) then
    raise Exception.Create('Failed to obtain Direct3D interface');
  // Enumerate devices
  ZeroMemory(@D3DDesc, SizeOf(D3DDesc));
  if Failed(D3D.EnumDevices(@D3DDevEnumProc, @D3DDesc)) then
    raise Exception.Create('Failed to enumerate Direct3D devices');
  if D3DDesc.dwDevCaps=0 then DevGUID:=IID_IDirect3DHALDevice else DevGUID:=D3DDesc.deviceGUID;
  // Create device
  FDevice:=nil;
  if not IsEqualGUID(ZeroGuid, GSettings.D3D3DDevGUID) then begin
    if not Failed(D3D.CreateDevice(GSettings.D3D3DDevGUID, Back, FDevice)) then begin
      FDevice:=nil;
      DevGUID:=GSettings.D3D3DDevGUID;
    end;
  end;
  if Failed(D3D.CreateDevice(DevGUID, Back, FDevice)) then
    if Failed(D3D.CreateDevice(IID_IDirect3DHALDevice, Back, FDevice)) then begin
      if Failed(D3D.CreateDevice(IID_IDirect3DMMXDevice, Back, FDevice)) then begin
        if Failed(D3D.CreateDevice(IID_IDirect3DRGBDevice, Back, FDevice)) then begin
          raise Exception.Create('Failed to create Direct3D device');
        end else DevGUID:=IID_IDirect3DRGBDevice;
      end else DevGUID:=IID_IDirect3DMMXDevice;
    end else DevGUID:=IID_IDirect3DHALDevice;
  FDevice:=nil;
  // Enumerate depth formats
  ZeroMemory(@DepthPF, SizeOf(DepthPF));
  if Failed(d3d.EnumZBufferFormats(DevGUID, @D3DPureZBufferEnumProc, @DepthPF)) then
    raise Exception.Create('Failed to enumerate depth formats');
  if DepthPF.dwSize=0 then begin
    if Failed(d3d.EnumZBufferFormats(DevGUID, @D3DAnyZBufferEnumProc, @DepthPF)) then
      raise Exception.Create('Failed to enumerate depth formats');
  end;
  if DepthPF.dwSize=0 then
    raise Exception.Create('Failed to find a depth format');
  // Create a depth buffer
  ZeroMemory(@ddsd, SizeOf(ddsd));
  with ddsd do begin
    dwSize:=SizeOf(ddsd);
    dwFlags:=DDSD_CAPS or DDSD_WIDTH or DDSD_HEIGHT or DDSD_PIXELFORMAT;
    dwWidth:=640;
    dwHeight:=480;
    ddpfPixelFormat:=DepthPF;
    ddsCaps.dwCaps:=DDSCAPS_ZBUFFER;
    dwBackBufferCount:=1;
  end;
  if Failed(DDraw.CreateSurface(ddsd, Depth, nil)) then
    raise Exception.Create('Failed to create a depth buffer');
  // Attach the depth buffer
  if Failed(Back.AddAttachedSurface(Depth)) then
    raise Exception.Create('Failed to attach the depth buffer');
  // Recreate device
  if Failed(D3D.CreateDevice(DevGUID, Back, FDevice)) then
    raise Exception.Create('Failed to create Direct3D device');
  // Enumerate texture formats
  ZeroMemory(@TextureFormat, SizeOf(TextureFormat));
  if Failed(FDevice.EnumTextureFormats(@D3DTexFmtEnumProc, nil)) then begin
    FDevice:=nil;
    D3D:=nil;
    raise Exception.Create('Failed to enumerate device texture formats');
  end;
  if TextureFormat.dwSize=0 then begin
    FDevice:=nil;
    D3D:=nil;
    raise Exception.Create('Failed to find a usable texture format');
  end;
  // Check if we found an indexed texture format
  TexturePalette:=nil;
  if (TextureFormat.dwFlags and DDPF_PALETTEINDEXED8)=DDPF_PALETTEINDEXED8 then begin
    // Create palette
    ZeroMemory(@Pals, SizeOf(Pals));
    if Failed(DDraw.CreatePalette(DDPCAPS_8BIT or DDPCAPS_ALLOW256, @Pals, TexturePalette, nil)) then
      TexturePalette:=nil;
    // Doublecheck
    if not Assigned(TexturePalette) then begin
      FDevice:=nil;
      D3D:=nil;
      raise Exception.Create('Failed to create texture palette');
    end;
  end;
end;

destructor TD3DRasterizer.Destroy;
begin
  FDevice:=nil;
  TexturePalette:=nil;
  D3D:=nil;
  DDraw:=nil;
  inherited Destroy;
end;

function TD3DRasterizer.BeginScene: Boolean;
var
  VP: TD3DViewport7;
begin
  if Failed(FDevice.BeginScene) then Exit(False);
  Result:=True;
  vp.dwX:=0;
  vp.dwY:=0;
  vp.dwWidth:=RealHRes;
  vp.dwHeight:=RealVRes;
  vp.dvMinZ:=0;
  vp.dvMaxZ:=1;
  FDevice.SetViewport(vp);
  FDevice.Clear(0, nil, D3DCLEAR_ZBUFFER, 0, 1.0, 0);
  FDevice.SetRenderState(D3DRENDERSTATE_LIGHTING, 0);
  FDevice.SetRenderState(D3DRENDERSTATE_CULLMODE, Ord(D3DCULL_CW));
  FDevice.SetRenderState(D3DRENDERSTATE_ZENABLE, 1);
  FDevice.SetRenderState(D3DRENDERSTATE_DITHERENABLE, 1);
  if GSettings.BilinearFiltering then begin
    FDevice.SetTextureStageState(0, D3DTSS_MAGFILTER, Ord(D3DTFG_LINEAR));
    FDevice.SetTextureStageState(0, D3DTSS_MINFILTER, Ord(D3DTFN_LINEAR));
  end else begin
    FDevice.SetTextureStageState(0, D3DTSS_MAGFILTER, Ord(D3DTFG_POINT));
    FDevice.SetTextureStageState(0, D3DTSS_MINFILTER, Ord(D3DTFN_POINT));
  end;
  FDevice.SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
  FDevice.SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);
  FDevice.SetRenderState(D3DRENDERSTATE_FOGENABLE, 1);
end;

procedure TD3DRasterizer.EndScene;
begin
  FDevice.EndScene;
end;

procedure TD3DRasterizer.Flip;
begin
  Primary.Flip(nil, DDFLIP_WAIT);
end;

procedure TD3DRasterizer.BeforeBatchesDraw;
begin
  inherited BeforeBatchesDraw;
end;

procedure TD3DRasterizer.AfterBatchesDraw;
begin
  inherited AfterBatchesDraw;
end;

procedure TD3DRasterizer.DrawBatch(const Batch: TTriBatch);
const
  FVF = D3DFVF_XYZ or D3DFVF_DIFFUSE or D3DFVF_TEX1;
begin
  with Batch do begin
    FDevice.SetTexture(0, TD3DTexture(HWTexture).FSurface);
    FDevice.DrawPrimitive(D3DPT_TRIANGLELIST, FVF, Vertices[0], Count div 6, 0);
  end;
end;

function TD3DRasterizer.CreateTexture: TRasterTexture;
var
  ddsd: TDDSurfaceDesc2;
  Tex: TD3DTexture;
begin
  // Create texture object
  Tex:=TD3DTexture.Create;
  // Create the texture's surface
  ZeroMemory(@ddsd, SizeOf(ddsd));
  with ddsd do begin
    dwSize:=SizeOf(ddsd);
    dwFlags:=DDSD_CAPS or DDSD_WIDTH or DDSD_HEIGHT or DDSD_PIXELFORMAT or DDSD_TEXTURESTAGE;
    dwWidth:=64;
    dwHeight:=64;
    ddpfPixelFormat:=TextureFormat;
    ddsCaps.dwCaps:=DDSCAPS_TEXTURE;
    ddsCaps.dwCaps2:=DDSCAPS2_TEXTUREMANAGE;
  end;
  Tex.FWidth:=64;
  Tex.FHeight:=64;
  if Failed(DDraw.CreateSurface(ddsd, Tex.FSurface, nil)) then begin
    MessageBox(GetForegroundWindow, 'Texture creation failed', 'Direct3D Error', MB_ICONERROR or MB_OK or MB_TOPMOST);
    Halt;
  end;
  Result:=Tex;
end;

function TD3DRasterizer.BeginFrame: Boolean;
begin
  Result:=inherited BeginFrame;
end;

procedure TD3DRasterizer.FinishFrame;
begin
  inherited FinishFrame;
end;

procedure TD3DRasterizer.Clear(R, G, B: Single);
begin
  FDevice.Clear(0, nil, D3DCLEAR_TARGET, RGB(Round(B*255), Round(G*255), Round(R*255)), 1.0, 0);
  DiscardBatches;
end;

procedure TD3DRasterizer.SetMatrices(Projection, View: TMatrix);
var
  M: TMatrix;
begin
  M.YRotation(3.1415);
  View.SwapMultiply(M);
  FDevice.SetTransform(D3DTRANSFORMSTATE_PROJECTION, PD3DMatrix(@Projection.M11)^);
  FDevice.SetTransform(D3DTRANSFORMSTATE_VIEW, PD3DMatrix(@View.M11)^);
  FDevice.SetTransform(D3DTRANSFORMSTATE_WORLD, PD3DMatrix(@Identity.M11)^);
end;

procedure TD3DRasterizer.SetFog(FogR, FogG, FogB, FogStart, FogEnd: Single);
var
  FogColor: DWORD;
begin
  FlushBatches;
  FogColor:=RGB(Round(FogR*255), Round(FogG*255), Round(FogB)*255);
  FDevice.SetRenderState(D3DRENDERSTATE_FOGCOLOR, FogColor);
  FDevice.SetRenderState(D3DRENDERSTATE_FOGTABLEMODE, Ord(D3DFOG_NONE));
  FDevice.SetRenderState(D3DRENDERSTATE_FOGVERTEXMODE, Ord(D3DFOG_LINEAR));
  FDevice.SetRenderState(D3DRENDERSTATE_FOGSTART, PDWORD(@FogStart)^);
  FDevice.SetRenderState(D3DRENDERSTATE_FOGEND, PDWORD(@FogEnd)^);
  FDevice.SetRenderState(D3DRENDERSTATE_RANGEFOGENABLE, 1);
end;

procedure TD3DRasterizer.RenderQuad(X1, Y1, S1, T1, X2, Y2, S2, T2: Integer;
  Blend: Single; Texture: TTexture);
const
  FVF = D3DFVF_XYZ or D3DFVF_DIFFUSE or D3DFVF_TEX1;
var
  V: array [0..4] of packed record
    P: TVector;
    RGB: DWord;
    S, T: Single;
  end;
begin
  FlushBatches;
  V[0].P:=Vector(X1/(RealHRes - 1)*2-1, (1-Y1/(RealVRes - 1))*2-1, 0);
  V[1].P:=Vector(X1/(RealHRes - 1)*2-1, (1-Y2/(RealVRes - 1))*2-1, 0);
  V[2].P:=Vector(X2/(RealHRes - 1)*2-1, (1-Y2/(RealVRes - 1))*2-1, 0);
  V[3].P:=Vector(X2/(RealHRes - 1)*2-1, (1-Y1/(RealVRes - 1))*2-1, 0);
  V[0].S:=S1/Texture.Width; V[0].T:=T1/Texture.Height;
  V[1].S:=S1/Texture.Width; V[1].T:=T2/Texture.Height;
  V[2].S:=S2/Texture.Width; V[2].T:=T2/Texture.Height;
  V[3].S:=S2/Texture.Width; V[3].T:=T1/Texture.Height;
  V[0].RGB:=$C0FFFFFF;
  V[1].RGB:=$C0FFFFFF;
  V[2].RGB:=$C0FFFFFF;
  V[3].RGB:=$C0FFFFFF;
  if GSettings.BilinearFiltering and (Texture.Filter=tfNearest) then begin
    FDevice.SetTextureStageState(0, D3DTSS_MAGFILTER, Ord(D3DTFG_POINT));
    FDevice.SetTextureStageState(0, D3DTSS_MINFILTER, Ord(D3DTFN_POINT));
  end;
  FDevice.SetTextureStageState(0, D3DTSS_ALPHAOP, Ord(D3DTOP_MODULATE));
  FDevice.SetRenderState(D3DRENDERSTATE_ZENABLE, 0);
  FDevice.SetRenderState(D3DRENDERSTATE_FOGENABLE, 0);
  FDevice.SetRenderState(D3DRENDERSTATE_ZWRITEENABLE, 0);
  FDevice.SetRenderState(D3DRENDERSTATE_ALPHABLENDENABLE, 1);
  FDevice.SetRenderState(D3DRENDERSTATE_SRCBLEND, Ord(D3DBLEND_SRCALPHA));
  FDevice.SetRenderState(D3DRENDERSTATE_DESTBLEND, Ord(D3DBLEND_INVSRCALPHA));
  FDevice.SetTransform(D3DTRANSFORMSTATE_PROJECTION, PD3DMatrix(@Identity.M11)^);
  FDevice.SetTransform(D3DTRANSFORMSTATE_VIEW, PD3DMatrix(@Identity.M11)^);
  FDevice.SetTexture(0, TD3DTexture(Texture.HardwareTexture).FSurface);
  FDevice.DrawPrimitive(D3DPT_TRIANGLEFAN, FVF, V, 4, 0);
  FDevice.SetRenderState(D3DRENDERSTATE_ALPHABLENDENABLE, 0);
  FDevice.SetRenderState(D3DRENDERSTATE_ZWRITEENABLE, 1);
  FDevice.SetRenderState(D3DRENDERSTATE_ZENABLE, 1);
  FDevice.SetRenderState(D3DRENDERSTATE_FOGENABLE, 1);
  FDevice.SetTextureStageState(0, D3DTSS_ALPHAOP, Ord(D3DTOP_SELECTARG1));
  if GSettings.BilinearFiltering and (Texture.Filter=tfNearest) then begin
    FDevice.SetTextureStageState(0, D3DTSS_MAGFILTER, Ord(D3DTFG_LINEAR));
    FDevice.SetTextureStageState(0, D3DTSS_MINFILTER, Ord(D3DTFN_LINEAR));
  end;
end;

procedure TD3DRasterizer.DrawHUDImage(X, Y: Integer; Texture: TTexture; SX, SY, SW, SH: Integer);
begin
end;

initialization
  ZeroMemory(@ZeroGuid, SizeOf(ZeroGuid));
end.

