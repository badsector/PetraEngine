{ Base class for GDI based graphics driver }
unit GDIGfx;

interface

uses
  Windows, GfxDrv, Colors, Raster
  {$IFDEF PETRAGL}
  , GL
  {$ENDIF}
  ;

type
  { TGDIGraphicsDriver - base class for GDI based graphics driver }
  TGDIGraphicsDriver = class(TGraphicsDriver)
  private
    // Target window handle
    FWindow: HWND;
    // Device context for the window
    DC: HDC;
    {$IFNDEF PETRAHW}
    // Bitmap info for the frame data
    BitmapInfo: PBITMAPINFO;
    // Actual frame data
    FrameData: PFrameData;
    // Current gamma value
    Gamma: TGammaValue;
    {$ENDIF}
    {$IFDEF PETRAGL}
    // OpenGL render context
    RC: HGLRC;
    {$ENDIF}
    // Rasterizer to use for rendering with this driver
    Rasterizer: TRasterizer;
  protected
    {$IFNDEF PETRAHW}
    // Current color palette
    Palette: TColorPalette;
    {$ENDIF}
    // Initialize the GDI-specific bits for the graphics driver
    procedure InitializeGDI(AWindow: HWND);
    // Shutdown the GDI-specific bits for the graphics driver
    procedure ShutdownGDI;
  public
    constructor Create;
    destructor Destroy; override;
    property Window: HWND read FWindow;
  public
    // TGraphicsDriver methods
    {$IFDEF PETRAHW}
    function BeginFrame: Boolean; override;
    {$ELSE}
    function BeginFrame: PFrameData; override;
    {$ENDIF}
    procedure FinishFrame; override;
    procedure SetColorPalettePart(const APalette: TColorPalette; First, Last: Integer); override;
    procedure SetGamma(AGamma: TGammaValue); override;
    function GetRasterizer: TRasterizer; override;
  end;

implementation

{$IFNDEF PETRAHW}
uses
  FBRaster;
{$ENDIF}
{$IFDEF PETRAGL}
uses
  GLRaster;
{$ENDIF}

{ TGDIGraphicsDriver }
constructor TGDIGraphicsDriver.Create;
begin
  inherited Create;
  {$IFNDEF PETRAHW}
  // Allocate frame data memory
  GetMem(FrameData, HRes*VRes);
  // Create rasterizer
  Rasterizer:=TFramebufferRasterizer.Create;
  // Create palette
  Palette:=TColorPalette.Create;
  {$ENDIF}
  {$IFDEF PETRAGL}
  Rasterizer:=TOpenGLRasterizer.Create;
  {$ENDIF}
end;

destructor TGDIGraphicsDriver.Destroy;
begin
  // Delete objects
  Rasterizer.Free;
  Rasterizer:=nil;
  {$IFNDEF PETRAHW}
  FreeMem(FrameData);
  Palette.Free;
  {$ENDIF}
  inherited Destroy;
end;

procedure TGDIGraphicsDriver.InitializeGDI(AWindow: HWND);
var
  {$IFNDEF PETRAHW}
  I: Integer;
  {$ENDIF}
  {$IFDEF PETRAGL}
  PixelFormat: Integer;
  PixelFormatDescriptor: TPixelFormatDescriptor;
  R: TRect;
  {$ENDIF}
begin
  FWindow:=AWindow;
  DC:=GetDC(Window);
  {$IFDEF PETRAGL}
  // Configure desired pixel format
  ZeroMemory(@PixelFormatDescriptor, SizeOf(PixelFormatDescriptor));
  with PixelFormatDescriptor do begin
    nSize:=SizeOf(PixelFormatDescriptor);
    nVersion:=1;
    dwFlags:=PFD_DRAW_TO_WINDOW or PFD_SUPPORT_OPENGL or PFD_DOUBLEBUFFER;
    iPixelType:=PFD_TYPE_RGBA;
    if ColorDepth=15 then begin
      cColorBits:=15;
      cDepthBits:=16;
    end else if ColorDepth=16 then begin
      cColorBits:=16;
      cDepthBits:=16;
    end else begin
      cDepthBits:=24;
    end;
  end;
  // Find a matching pixel format
  PixelFormat:=ChoosePixelFormat(DC, @PixelFormatDescriptor);
  if PixelFormat=0 then begin
    MessageBox(Window, 'Failed to find a pixel format', 'OpenGL Error', MB_ICONERROR or MB_OK);
    Halt;
  end;
  // Enable the pixel format
  if not SetPixelFormat(DC, PixelFormat, @PixelFormatDescriptor) then begin
    MessageBox(Window, 'Failed to set pixel format', 'OpenGL Error', MB_ICONERROR or MB_OK);
    Halt;
  end;
  // Create OpenGL context
  RC:=wglCreateContext(DC);
  if RC=0 then begin
    MessageBox(Window, 'Failed to create OpenGL context', 'OpenGL Error', MB_ICONERROR or MB_OK);
    Halt;
  end;
  // Configure context
  wglMakeCurrent(DC, RC);
  GetClientRect(Window, R);
  glViewport(0, 0, R.Right, R.Bottom);
  {$ENDIF}
  {$IFNDEF PETRAHW}
  // Create the bitmap info structure
  GetMem(BitmapInfo, SizeOf(TBITMAPINFOHEADER)+256*4);
  {$R-}
  for I:=0 to 255 do begin
    with BitmapInfo^.bmiColors[I] do begin
      rgbRed:=I;
      rgbGreen:=I;
      rgbBlue:=I;
      rgbReserved:=0;
    end;
  end;
  {$R+}
  with BitmapInfo^.bmiHeader do begin
    biSize:=SizeOf(BitmapInfo^.bmiHeader);
    biWidth:=HRes;
    biHeight:=1;
    biPlanes:=1;
    biBitCount:=8;
    biCompression:=BI_RGB;
    biSizeImage:=0;
    biXPelsPerMeter:=0;
    biYPelsPerMeter:=0;
    biClrUsed:=0;
    biClrImportant:=0;
  end;
  {$ENDIF}
end;

procedure TGDIGraphicsDriver.ShutdownGDI;
begin
  {$IFNDEF PETRAHW}
  FreeMem(BitmapInfo);
  {$ENDIF}
  {$IFDEF PETRAGL}
  wglDeleteContext(RC);
  RC:=0;
  {$ENDIF}
  ReleaseDC(Window, DC);
  FWindow:=0;
end;

{$IFDEF PETRAGL}
function TGDIGraphicsDriver.BeginFrame: Boolean;
begin
  Result:=True;
end;
{$ENDIF}

{$IFNDEF PETRAHW}
function TGDIGraphicsDriver.BeginFrame: PFrameData;
begin
  Result:=FrameData;
end;
{$ENDIF}

procedure TGDIGraphicsDriver.FinishFrame;
{$IFNDEF PETRAHW}
var
  Y: Integer;
  R: TRect;
{$ENDIF}
begin
  {$IFNDEF PETRAHW}
  // Obtain window size
  {$push}{$warn 5057 off}
  GetClientRect(Window, R);
  {$pop}
  // Apply palette
  RealizePalette(DC);
  // Stretch frame data to cover the window
  for Y:=0 to R.Bottom - 1 do
    StretchDIBits(DC, 0, Y, R.Right, 1, 0, 0,
      HRes, 1, PBYTE(FrameData) + (Y*VRes div R.Bottom)*HRes,
      BitmapInfo^, DIB_RGB_COLORS, SRCCOPY);
  {$ENDIF}
  {$IFDEF PETRAGL}
  // Swap OpenGL buffers
  SwapBuffers(DC);
  {$ENDIF}
end;

procedure TGDIGraphicsDriver.SetColorPalettePart(const APalette: TColorPalette; First, Last: Integer);
{$IFNDEF PETRAHW}
var
  Value: Integer;
{$ENDIF}
begin
  {$IFNDEF PETRAHW}
  while First <= Last do begin
    {$R-}
    with BitmapInfo^.bmiColors[First] do begin
      Palette.Red[First]:=APalette.Red[First];
      Palette.Green[First]:=APalette.Green[First];
      Palette.Blue[First]:=APalette.Blue[First];
      Value:=Round(Palette.Red[First]*(1.0 + Gamma/High(TGammaValue)) + Gamma);
      if Value > 255 then Value:=255;
      rgbRed:=Value;
      Value:=Round(Palette.Green[First]*(1.0 + Gamma/High(TGammaValue)) + Gamma);
      if Value > 255 then Value:=255;
      rgbGreen:=Value;
      Value:=Round(Palette.Blue[First]*(1.0 + Gamma/High(TGammaValue)) + Gamma);
      if Value > 255 then Value:=255;
      rgbBlue:=Value;
      rgbReserved:=0;
    end;
    {$R+}
    Inc(First);
  end;
  {$ENDIF}
end;

procedure TGDIGraphicsDriver.SetGamma(AGamma: TGammaValue);
begin
  {$IFNDEF PETRAHW}
  Gamma:=AGamma;
  SetColorPalette(Palette);
  {$ENDIF}
end;

function TGDIGraphicsDriver.GetRasterizer: TRasterizer;
begin
  Result:=Rasterizer;
end;

end.
