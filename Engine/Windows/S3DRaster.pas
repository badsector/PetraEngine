{ S3D Rasterizer }
unit S3DRaster;

interface

uses
  Classes, SysUtils, Windows, DirectDraw, S3DTK, Maths, Colors, Raster,
  HWRaster, Textures, GfxDrv;

type
  { TS3DTexture - S3D-specific data for a hardware accelerated texture }
  TS3DTexture = class(TRasterTexture)
  private
    // Texture surface
    FSurface: IDirectDrawSurface;
    // S3D structure
    FS3DSurf: TS3DTK_Surface;
  public
    // TRasterTexture methods
    procedure SetData(AWidth, AHeight: Integer; AFiltered: Boolean; APalette: TColorPalette; const Texels); override;
  end;

  { TS3DRasterizer - S3D Rasterizer }
  TS3DRasterizer = class(THardwareRasterizer)
  protected
    // THardwareRasterizer methods
    procedure BeforeBatchesDraw; override;
    procedure AfterBatchesDraw; override;
    procedure DrawBatch(const Batch: TTriBatch); override;
  public
    // TRasterizer methods
    function CreateTexture: TRasterTexture; override;
    function BeginFrame: Boolean; override;
    procedure FinishFrame; override;
    procedure Clear(R, G, B: Single); override;
    procedure SetMatrices(Projection, View: TMatrix); override;
    procedure SetFog(FogR, FogG, FogB, FogStart, FogEnd: Single); override;
    procedure RenderQuad(X1, Y1, S1, T1, X2, Y2, S2, T2: Integer; Blend: Single; Texture: TTexture); override;
    procedure DrawHUDImage(X, Y: Integer; Texture: TTexture; SX, SY, SW, SH: Integer); override;
  public
    // Create the rasterizer and initialize S3D, throws exception on error
    constructor Create(AWinHandle: HWND);
    destructor Destroy; override;
    // Flip the buffers
    procedure Flip;
  end;

implementation

uses
  Engine, Renderer, Settings;

var
  // S3D functions
  S3D: PS3DTK_FUNCTION_LIST;
  // S3D primary and backbuffer surfaces
  S3DSurf: array [0..1] of TS3DTK_SURFACE;
  // S3D zbuffer surface
  S3DZBuffer: TS3DTK_SURFACE;
  // Current backbuffer index
  BBIndex: Integer;
  // DirectDraw object
  DDraw: IDirectDraw;
  // Primary, back and zbuffer Direct Draw surfaces
  DDPrimary, DDBack, DDZBuffer: IDirectDrawSurface;
  // Framebuffer physical address
  FramebufferPhysical: ULONG;
  // Matrices
  ProjMatrix, ViewMatrix: TMatrix;
  // Projected vertices
  ProjVtx: array of TS3DTK_VERTEX_TEX;
  // Pointer to projected vertices
  ProjVtxPtr: array of PS3DTK_VERTEX_TEX;
  // Fog settings
  FogRange, InvFogRange: Single;

function RGB555(R, G, B: Byte): Word;
begin
  Result:=((R >> 3) << 10) or ((G >> 3) << 5) or (B >> 3);
end;

function LinearToPhysical(Linear: ULONG): ULONG;
begin
  Result:=S3DTK_LinearToPhysical(Linear and $FFFFF000) + (Linear and $FFF);
end;

procedure SetProjVtxSize(Size: Integer);
var
  I: Integer;
begin
  SetLength(ProjVtx, Size);
  SetLength(ProjVtxPtr, Size);
  for I:=0 to Size - 1 do ProjVtxPtr[I]:=@ProjVtx[I];
end;

{ TS3DTexture }
procedure TS3DTexture.SetData(AWidth, AHeight: Integer; AFiltered: Boolean;
  APalette: TColorPalette; const Texels);
var
  X, Y: Integer;
  ddsd: TDDSurfaceDesc;
  Pixels: PWORD;
  TexelBytes: PByte;
begin
  // Create DDSD
  ZeroMemory(@ddsd, SizeOf(ddsd));
  with ddsd do begin
    dwSize:=SizeOf(ddsd);
    dwFlags:=DDSD_CAPS or DDSD_WIDTH or DDSD_HEIGHT or DDSD_PIXELFORMAT;
    dwWidth:=AWidth;
    dwHeight:=AHeight;
    ddsCaps.dwCaps:=DDSCAPS_OVERLAY or DDSCAPS_VIDEOMEMORY;
    with ddpfPixelFormat do begin
      dwSize:=SizeOf(DDPIXELFORMAT);
      dwFlags:=DDPF_RGB;
      dwRGBBitCount:=16;
      dwRBitMask:=$7C00;
      dwGBitMask:=$03E0;
      dwBBitMask:=$001F;
    end;
  end;
  // Recreate the texture if needed
  if (AWidth <> FS3DSurf.sfWidth) or (AHeight <> FS3DSurf.sfHeight) then begin
    FSurface:=nil;
    // Recreate the texture's surface
    with FS3DSurf do begin
      sfWidth:=AWidth;
      sfHeight:=AHeight;
      sfFormat:=S3DTK_VIDEO or S3DTK_TEXARGB1555;
    end;
    if Failed(DDraw.CreateSurface(ddsd, FSurface, nil)) then begin
      MessageBox(GetForegroundWindow, 'Texture creation failed', 'S3D Error', MB_ICONERROR or MB_OK or MB_TOPMOST);
      Halt;
    end;
  end;
  // Set the pixels
  if Failed(FSurface.Lock(nil, ddsd, DDLOCK_SURFACEMEMORYPTR or DDLOCK_WAIT, 0)) then begin
    MessageBox(GetForegroundWindow, 'Texture lock failed', 'S3D Error', MB_ICONERROR or MB_OK or MB_TOPMOST);
    Halt;
  end;
  TexelBytes:=PByte(@Texels);
  for Y:=0 to AHeight - 1 do begin
    Pixels:=PWord(PByte(ddsd.lpSurface) + Y*ddsd.lPitch);
    for X:=0 to AWidth - 1 do with APalette do begin
      Pixels[X]:=((Red[TexelBytes[Y*AWidth + X]] >> 3) << 10) or
                 ((Green[TexelBytes[Y*AWidth + X]] >> 3) << 5) or
                 (Blue[TexelBytes[Y*AWidth + X]] >> 3);
    end;
  end;
  // Store the physical memory offset from the framebuffer
  FS3DSurf.sfOffset:=LinearToPhysical(ULONG(ddsd.lpSurface)) - FramebufferPhysical;
  FSurface.Unlock(nil);
end;

{ TS3DRasterizer }
constructor TS3DRasterizer.Create(AWinHandle: HWND);
var
  ddsd: TDDSurfaceDesc;
  ddscaps: TDDSCaps;
  S3DLibInit: TS3DTK_LIB_INIT;
  S3DRendererInit: TS3DTK_RENDERER_INITSTRUCT;
  FramebufferLinear: PByte;
  R: TS3DTK_RECTAREA;
begin
  inherited Create;
  // Create DirectDraw object
  if Failed(DirectDrawCreate(nil, DDraw, nil)) then
    raise Exception.Create('Failed to create Direct Draw object');
  // Init S3D
  ZeroMemory(@S3DLibInit, SizeOf(S3DLibInit));
  S3DLibInit.libFlags:=S3DTK_INITPIO;
  if S3DTK_InitLib(ULONG(@S3DLibInit)) <> S3DTK_OK then begin
    DDraw:=nil;
    raise Exception.Create('Failed to initialize S3D library');
  end;
  ZeroMemory(@S3DRendererInit, SizeOf(S3DRendererInit));
  S3DRendererInit.initFlags:=S3DTK_FORMAT_FLOAT or
                             S3DTK_VERIFY_UVRANGE or
                             S3DTK_VERIFY_XYRANGE;
  if S3DTK_CreateRenderer(ULONG(@S3DRendererInit), @S3D) <> S3DTK_OK then begin
    S3DTK_ExitLib;
    DDraw:=nil;
    raise Exception.Create('Failed to initialize S3D renderer');
  end;
  // Setup cooperative level
  if Failed(DDraw.SetCooperativeLevel(AWinHandle, DDSCL_EXCLUSIVE or DDSCL_FULLSCREEN)) then begin
    DDraw:=nil;
    raise Exception.Create('Failed to set Direct Draw cooperative level');
  end;
  // Attempt to change the display mode
  if Failed(DDraw.SetDisplayMode(640, 480, 16)) then begin
    DDraw:=nil;
    raise Exception.Create('Failed to set 640x480x16bpp video mode');
  end;
  // Get framebuffer address
  FramebufferLinear:=nil;
  S3D^.S3DTK_GetState(S3D, S3DTK_VIDEOMEMORYADDRESS, ULONG(@FramebufferLinear));
  FramebufferPhysical:=LinearToPhysical(ULONG(FramebufferLinear));
  // Create primary surface
  ZeroMemory(@ddsd, SizeOf(ddsd));
  with ddsd do begin
    dwSize:=SizeOf(ddsd);
    dwFlags:=DDSD_CAPS or DDSD_BACKBUFFERCOUNT;
    ddsCaps.dwCaps:=DDSCAPS_PRIMARYSURFACE or DDSCAPS_FLIP or DDSCAPS_COMPLEX;
    dwBackBufferCount:=1;
  end;
  if Failed(DDraw.CreateSurface(ddsd, DDPrimary, nil)) then begin
    DDraw.RestoreDisplayMode;
    S3DTK_ExitLib;
    DDraw:=nil;
    raise Exception.Create('Failed to create primary Direct Draw surface');
  end;
  // Obtain backbuffer
  ZeroMemory(@ddscaps, SizeOf(ddscaps));
  ddscaps.dwCaps:=DDSCAPS_BACKBUFFER;
  if Failed(DDPrimary.GetAttachedSurface(ddscaps, DDBack)) then begin
    DDPrimary:=nil;
    DDraw.RestoreDisplayMode;
    S3DTK_ExitLib;
    DDraw:=nil;
    raise Exception.Create('Failed to obtain back buffer from the primary Direct Draw surface');
  end;
  // Prepare S3D surface data
  ZeroMemory(@ddsd, SizeOf(ddsd));
  ddsd.dwSize:=SizeOf(ddsd);
  if Failed(DDPrimary.Lock(nil, ddsd, DDLOCK_SURFACEMEMORYPTR or DDLOCK_WAIT, 0)) then begin
    DDPrimary:=nil;
    DDraw.RestoreDisplayMode;
    S3DTK_ExitLib;
    DDraw:=nil;
    raise Exception.Create('Failed to lock the primary buffer');
  end;
  ZeroMemory(@S3DSurf, SizeOf(S3DSurf));
  with S3DSurf[0] do begin
    sfOffset:=LinearToPhysical(ULONG(ddsd.lpSurface)) - FramebufferPhysical;
    sfWidth:=ddsd.dwWidth;
    sfHeight:=ddsd.dwHeight;
    sfFormat:=S3DTK_VIDEORGB15;
  end;
  DDPrimary.Unlock(nil);
  if Failed(DDBack.Lock(nil, ddsd, DDLOCK_SURFACEMEMORYPTR or DDLOCK_WAIT, 0)) then begin
    DDPrimary:=nil;
    DDraw.RestoreDisplayMode;
    S3DTK_ExitLib;
    DDraw:=nil;
    raise Exception.Create('Failed to lock the back buffer');
  end;
  S3DSurf[1]:=S3DSurf[0];
  S3DSurf[1].sfOffset:=LinearToPhysical(ULONG(ddsd.lpSurface)) - FramebufferPhysical;
  DDBack.Unlock(nil);
  // "Next" backbuffer index
  BBIndex:=1;
  // Create a zbuffer
  ZeroMemory(@ddsd, SizeOf(ddsd));
  with ddsd do begin
    dwSize:=SizeOf(ddsd);
    dwFlags:=DDSD_CAPS or DDSD_WIDTH or DDSD_HEIGHT;
    dwWidth:=S3DSurf[0].sfWidth;
    dwHeight:=S3DSurf[0].sfHeight;
    ddsCaps.dwCaps:=DDSCAPS_OFFSCREENPLAIN or DDSCAPS_VIDEOMEMORY;
  end;
  if Failed(DDraw.CreateSurface(ddsd, DDZBuffer, nil)) then begin
    DDPrimary:=nil;
    DDraw.RestoreDisplayMode;
    S3DTK_ExitLib;
    DDraw:=nil;
    raise Exception.Create('Failed to create a zbuffer');
  end;
  // Prepare S3D zbuffer surface data
  if Failed(DDZBuffer.Lock(nil, ddsd, DDLOCK_SURFACEMEMORYPTR or DDLOCK_WAIT, 0)) then begin
    DDZBuffer:=nil;
    DDPrimary:=nil;
    DDraw.RestoreDisplayMode;
    S3DTK_ExitLib;
    DDraw:=nil;
    raise Exception.Create('Failed to lock the zbuffer');
  end;
  ZeroMemory(@S3DZBuffer, SizeOf(S3DZBuffer));
  with S3DZBuffer do begin
    sfOffset:=LinearToPhysical(ULONG(ddsd.lpSurface)) - FramebufferPhysical;
    sfWidth:=S3DSurf[0].sfWidth;
    sfHeight:=S3DSurf[0].sfHeight;
    sfFormat:=S3DTK_Z16;
  end;
  DDZBuffer.Unlock(nil);
  S3D^.S3DTK_SetState(S3D, S3DTK_ZBUFFERSURFACE, ULONG(@S3DZBuffer));
  // Clear the buffers
  with R do begin
    Left:=0;
    Top:=0;
    Right:=S3DSurf[0].sfWidth;
    Bottom:=S3DSurf[0].sfHeight;
  end;
  S3D^.S3DTK_RectFill(S3D, @S3DSurf[0], @R, 0);
  S3D^.S3DTK_RectFill(S3D, @S3DSurf[1], @R, 0);
end;

destructor TS3DRasterizer.Destroy;
begin
  DDZBuffer:=nil;
  DDPrimary:=nil;
  S3DTK_ExitLib;
  if Assigned(DDraw) then begin
    DDraw.RestoreDisplayMode;
    DDraw:=nil;
  end;
  inherited Destroy;
end;

procedure TS3DRasterizer.Flip;
begin
  DDPrimary.Flip(nil, DDFLIP_WAIT);
  BBIndex:=1 - BBIndex;
end;

procedure TS3DRasterizer.BeforeBatchesDraw;
begin
  inherited BeforeBatchesDraw;
end;

procedure TS3DRasterizer.AfterBatchesDraw;
begin
  inherited AfterBatchesDraw;
end;

procedure TS3DRasterizer.DrawBatch(const Batch: TTriBatch);
var
  PVC: Integer = 0;
  Base: Integer = 0;
  I, Clips: Integer;
  Tris: Integer;
  A, B, C: TVector;
  L: Single;
begin
  with Batch do begin
    // Project the triangles in the batch
    Tris:=Count div 24;
    if Length(ProjVtx) < Tris*3 then SetProjVtxSize((Tris + 256)*3);
    for I:=0 to Tris - 1 do begin
      // Transform the triangle and fill the PVC array
      ProjVtx[PVC].U:=Vertices[Base]*64; Inc(Base);
      ProjVtx[PVC].V:=Vertices[Base]*64; Inc(Base);
      L:=Vertices[Base]; Inc(Base, 3);
      A.X:=Vertices[Base]; Inc(Base);
      A.Y:=Vertices[Base]; Inc(Base);
      A.Z:=Vertices[Base]; Inc(Base);
      ViewMatrix.Transform(A);
      ProjVtx[PVC].R:=Round(L*255*(1 - Clamp(A.Z - FogRange, 0, FogRange)*InvFogRange));
      ProjVtx[PVC].G:=ProjVtx[PVC].R;
      ProjVtx[PVC].B:=ProjVtx[PVC].R;
      ProjVtx[PVC].A:=255;
      ProjVtx[PVC].W:=A.Z;
      Inc(PVC);
      ProjVtx[PVC].U:=Vertices[Base]*64; Inc(Base);
      ProjVtx[PVC].V:=Vertices[Base]*64; Inc(Base);
      L:=Vertices[Base]; Inc(Base, 3);
      B.X:=Vertices[Base]; Inc(Base);
      B.Y:=Vertices[Base]; Inc(Base);
      B.Z:=Vertices[Base]; Inc(Base);
      ViewMatrix.Transform(B);
      ProjVtx[PVC].R:=Round(L*255*(1 - Clamp(B.Z - FogRange, 0, FogRange)*InvFogRange));
      ProjVtx[PVC].G:=ProjVtx[PVC].R;
      ProjVtx[PVC].B:=ProjVtx[PVC].R;
      ProjVtx[PVC].A:=255;
      ProjVtx[PVC].W:=B.Z;
      Inc(PVC);
      ProjVtx[PVC].U:=Vertices[Base]*64; Inc(Base);
      ProjVtx[PVC].V:=Vertices[Base]*64; Inc(Base);
      L:=Vertices[Base]; Inc(Base, 3);
      C.X:=Vertices[Base]; Inc(Base);
      C.Y:=Vertices[Base]; Inc(Base);
      C.Z:=Vertices[Base]; Inc(Base);
      ViewMatrix.Transform(C);
      ProjVtx[PVC].R:=Round(L*255*(1 - Clamp(C.Z - FogRange, 0, FogRange)*InvFogRange));
      ProjVtx[PVC].G:=ProjVtx[PVC].R;
      ProjVtx[PVC].B:=ProjVtx[PVC].R;
      ProjVtx[PVC].A:=255;
      ProjVtx[PVC].W:=C.Z;
      Inc(PVC);
      // Check if the triangle is in front of the near clipping plane
      // HACK: use the "real" clipping plane
      Clips:=0;
      if A.Z < 0.5 then begin Inc(Clips); A.Z:=0.5; ProjVtx[PVC - 3].W:=0.5; end;
      if B.Z < 0.5 then begin Inc(Clips); B.Z:=0.5; ProjVtx[PVC - 2].W:=0.5; end;
      if C.Z < 0.5 then begin Inc(Clips); C.Z:=0.5; ProjVtx[PVC - 1].W:=0.5; end;
      // If the entire triangle is behind the clipping plane, ignore it
      if Clips=3 then begin
        Dec(PVC, 3);
        Continue;
      end;
      // Transform the triangle to screen space
      ProjMatrix.TransformProj(A);
      ProjMatrix.TransformProj(B);
      ProjMatrix.TransformProj(C);
      // Ignore backfacing triangles
      if (C.X-A.X)*(C.Y-B.Y) - (C.X-B.X)*(C.Y-A.Y) > 0 then begin
        Dec(PVC, 3);
        Continue;
      end;
      // Ignore triangles completely outside the screen
      {if ((A.X < -1) and (B.X < -1) and (C.X < -1)) or
         ((A.Y < -1) and (B.Y < -1) and (C.Y < -1)) or
         ((A.X > 1) and (B.X > 1) and (C.X > 1)) or
         ((A.Y > 1) and (B.Y > 1) and (C.Y > 1)) then begin}
      if (A.X < -1) or (B.X < -1) or (C.X < -1) or
         (A.X > 1) or (B.X > 1) or (C.X > 1) or
         (A.Y < -1) or (A.Y < -1) or (C.Y < -1) or
         (A.Y > 1) or (B.Y > 1) or (C.Y > 1) then begin
        Dec(PVC, 3);
        Continue;
      end;
      // Store the screen space vertices
      ProjVtx[PVC - 3].X:=(S3DSurf[0].sfWidth*0.5 + A.X*S3DSurf[0].sfWidth*0.5);
      ProjVtx[PVC - 3].Y:=(S3DSurf[0].sfHeight*0.5 + A.Y*S3DSurf[0].sfHeight*0.5);
      ProjVtx[PVC - 3].Z:=A.Z*512;
      ProjVtx[PVC - 2].X:=(S3DSurf[0].sfWidth*0.5 + B.X*S3DSurf[0].sfWidth*0.5);
      ProjVtx[PVC - 2].Y:=(S3DSurf[0].sfHeight*0.5 + B.Y*S3DSurf[0].sfHeight*0.5);
      ProjVtx[PVC - 2].Z:=B.Z*512;
      ProjVtx[PVC - 1].X:=(S3DSurf[0].sfWidth*0.5 + C.X*S3DSurf[0].sfWidth*0.5);
      ProjVtx[PVC - 1].Y:=(S3DSurf[0].sfHeight*0.5 + C.Y*S3DSurf[0].sfHeight*0.5);
      ProjVtx[PVC - 1].Z:=C.Z*512;
    end;
    // Any remaining vertices?
    if PVC=0 then Exit;
    // Setup texture
    S3D^.S3DTK_SetState(S3D, S3DTK_TEXTUREACTIVE, ULONG(@TS3DTexture(HWTexture).FS3DSurf));
    // Draw the triangles
    S3D^.S3DTK_TriangleSet(S3D, PULONG(@ProjVtxPtr[0]), PVC, S3DTK_TRILIST);
  end;
end;

function TS3DRasterizer.CreateTexture: TRasterTexture;
var
  ddsd: TDDSurfaceDesc;
  Tex: TS3DTexture;
begin
  // Create texture object
  Tex:=TS3DTexture.Create;
  // Create the texture's DirectDraw surface
  ZeroMemory(@ddsd, SizeOf(ddsd));
  with ddsd do begin
    dwSize:=SizeOf(ddsd);
    dwFlags:=DDSD_CAPS or DDSD_WIDTH or DDSD_HEIGHT or DDSD_PIXELFORMAT;
    dwWidth:=64;
    dwHeight:=64;
    ddsCaps.dwCaps:=DDSCAPS_OVERLAY or DDSCAPS_VIDEOMEMORY;
    with ddpfPixelFormat do begin
      dwSize:=SizeOf(DDPIXELFORMAT);
      dwFlags:=DDPF_RGB;
      dwRGBBitCount:=16;
      dwRBitMask:=$7C00;
      dwGBitMask:=$03E0;
      dwBBitMask:=$001F;
    end;
  end;
  if Failed(DDraw.CreateSurface(ddsd, Tex.FSurface, nil)) then begin
    DDZBuffer:=nil;
    DDPrimary:=nil;
    DDraw.RestoreDisplayMode;
    S3DTK_ExitLib;
    DDraw:=nil;
    MessageBox(GetForegroundWindow, 'Texture creation failed', 'S3D Error', MB_ICONERROR or MB_OK or MB_TOPMOST);
    Halt;
  end;
  // Setup S3D surface
  ZeroMemory(@Tex.FS3DSurf, SizeOf(Tex.FS3DSurf));
  with Tex.FS3DSurf do begin
    sfOffset:=FramebufferPhysical; // Dummy, will be properly set in SetData
    sfWidth:=64;
    sfHeight:=64;
    sfFormat:=S3DTK_VIDEO or S3DTK_TEXARGB1555;
  end;
  Result:=Tex;
end;

function TS3DRasterizer.BeginFrame: Boolean;
var
  R: TS3DTK_RECTAREA;
begin
  Result:=inherited BeginFrame;
  if not Result then Exit;
  // Wait for previous frame to finish
  while DDPrimary.GetFlipStatus(DDGFS_ISFLIPDONE)=DDERR_WASSTILLDRAWING do;
  // Clear zbuffer
  with R do begin
    Left:=0;
    Top:=0;
    Right:=S3DZBuffer.sfWidth;
    Bottom:=S3DZBuffer.sfHeight;
  end;
  S3D^.S3DTK_RectFill(S3D, @S3DZBuffer, @R, 0);
  // Setup initial frame state
  S3D^.S3DTK_SetState(S3D, S3DTK_DRAWSURFACE, ULONG(@S3DSurf[BBIndex]));
  S3D^.S3DTK_SetState(S3D, S3DTK_ZBUFFERENABLE, S3DTK_ON);
  S3D^.S3DTK_SetState(S3D, S3DTK_RENDERINGTYPE, S3DTK_LITTEXTUREPERSPECT);
  S3D^.S3DTK_SetState(S3D, S3DTK_ZBUFFERCOMPAREMODE, S3DTK_ZSRCGEZFB);
  S3D^.S3DTK_SetState(S3D, S3DTK_TEXFILTERINGMODE, S3DTK_TEX4TPP);
  S3D^.S3DTK_SetState(S3D, S3DTK_TEXBLENDINGMODE, S3DTK_TEXMODULATE);
  S3D^.S3DTK_SetState(S3D, S3DTK_ALPHABLENDING, S3DTK_ALPHAOFF);
  S3D^.S3DTK_SetState(S3D, S3DTK_FOGCOLOR, S3DTK_FOGOFF);
  S3D^.S3DTK_SetState(S3D, S3DTK_CLIPPING_AREA, ULONG(@R));
end;

procedure TS3DRasterizer.FinishFrame;
begin
  inherited FinishFrame;
end;

procedure TS3DRasterizer.Clear(R, G, B: Single);
var
  DR: TS3DTK_RECTAREA;
begin
  with DR do begin
    Left:=0;
    Top:=0;
    Right:=S3DSurf[0].sfWidth;
    Bottom:=S3DSurf[0].sfHeight;
  end;
  S3D^.S3DTK_RectFill(S3D, @S3DSurf[BBIndex], @DR,
    RGB555(Round(R*255.0), Round(G*255.0), Round(B*255.0)));
  DiscardBatches;
end;

procedure TS3DRasterizer.SetMatrices(Projection, View: TMatrix);
begin
  ProjMatrix:=Projection;
  ViewMatrix:=View;
end;

procedure TS3DRasterizer.SetFog(FogR, FogG, FogB, FogStart, FogEnd: Single);
begin
  FlushBatches;
  FogEnd:=FogEnd;
  FogRange:=FogEnd - FogStart;
  InvFogRange:=1/FogRange;
end;

procedure TS3DRasterizer.RenderQuad(X1, Y1, S1, T1, X2, Y2, S2, T2: Integer;
  Blend: Single; Texture: TTexture);
var
  V: array [0..3] of TS3DTK_VERTEX_TEX;
  VP: array [0..5] of PS3DTK_VERTEX_TEX;
begin
  FlushBatches;
  // Setup vertices
  V[0].X:=X1;
  V[0].Y:=Y1;
  V[0].Z:=0.5;
  V[0].W:=0.5;
  V[0].U:=S1;
  V[0].V:=T1;
  V[0].B:=255;
  V[0].G:=255;
  V[0].R:=255;
  V[0].A:=192;
  V[1].X:=X1;
  V[1].Y:=Y2;
  V[1].Z:=0.5;
  V[1].W:=0.5;
  V[1].U:=S1;
  V[1].V:=T2;
  V[1].B:=255;
  V[1].G:=255;
  V[1].R:=255;
  V[1].A:=192;
  V[2].X:=X2;
  V[2].Y:=Y2;
  V[2].Z:=0.5;
  V[2].W:=0.5;
  V[2].U:=S2;
  V[2].V:=T2;
  V[2].B:=255;
  V[2].G:=255;
  V[2].R:=255;
  V[2].A:=192;
  V[3].X:=X2;
  V[3].Y:=Y1;
  V[3].Z:=0.5;
  V[3].W:=0.5;
  V[3].U:=S2;
  V[3].V:=T1;
  V[3].B:=255;
  V[3].G:=255;
  V[3].R:=255;
  V[3].A:=192;
  // Setup state
  S3D^.S3DTK_SetState(S3D, S3DTK_ZBUFFERENABLE, S3DTK_OFF);
  S3D^.S3DTK_SetState(S3D, S3DTK_ZBUFFERUPDATEENABLE, S3DTK_OFF);
  S3D^.S3DTK_SetState(S3D, S3DTK_RENDERINGTYPE, S3DTK_LITTEXTURE);
  S3D^.S3DTK_SetState(S3D, S3DTK_TEXFILTERINGMODE, S3DTK_TEX1TPP);
  S3D^.S3DTK_SetState(S3D, S3DTK_ALPHABLENDING, S3DTK_ALPHASOURCE);
  S3D^.S3DTK_SetState(S3D, S3DTK_TEXTUREACTIVE, ULONG(@TS3DTexture(Texture.HardwareTexture).FS3DSurf));
  // Draw the triangles
  VP[0]:=@V[0];
  VP[1]:=@V[1];
  VP[2]:=@V[2];
  VP[3]:=@V[0];
  VP[4]:=@V[2];
  VP[5]:=@V[3];
  S3D^.S3DTK_TriangleSet(S3D, PULONG(@VP[0]), 6, S3DTK_TRILIST);
  // Restore normal state
  S3D^.S3DTK_SetState(S3D, S3DTK_ZBUFFERENABLE, S3DTK_ON);
  S3D^.S3DTK_SetState(S3D, S3DTK_ZBUFFERUPDATEENABLE, S3DTK_ON);
  S3D^.S3DTK_SetState(S3D, S3DTK_RENDERINGTYPE, S3DTK_LITTEXTUREPERSPECT);
  S3D^.S3DTK_SetState(S3D, S3DTK_TEXFILTERINGMODE, S3DTK_TEX4TPP);
  S3D^.S3DTK_SetState(S3D, S3DTK_ALPHABLENDING, S3DTK_ALPHAOFF);
end;

procedure TS3DRasterizer.DrawHUDImage(X, Y: Integer; Texture: TTexture; SX, SY, SW, SH: Integer);
begin
end;

end.
