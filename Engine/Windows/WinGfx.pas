{ Class for standalone Win32 graphics }
unit WinGfx;

interface

uses
  Classes, SysUtils, Windows, WinInput, GfxDrv, Colors,
  {$DEFINE GDIBASE}
  {$IFDEF PETRADDRAW}
  Raster, FBRaster, DirectDraw
  {$UNDEF GDIBASE}
  {$ENDIF}
  {$IFDEF PETRAD3D}
  Raster, D3DRaster, DirectDraw, Direct3D
  {$UNDEF GDIBASE}
  {$ENDIF}
  {$IFDEF PETRAS3D}
  Raster, S3DRaster, DirectDraw
  {$UNDEF GDIBASE}
  {$ENDIF}
  {$IFDEF GDIBASE}
  GDIGfx
  {$ENDIF};

type
  { TWinGraphicsDriver - Windows graphics driver }
  TWinGraphicsDriver = class({$IFDEF GDIBASE}TGDIGraphicsDriver{$ELSE}TGraphicsDriver{$ENDIF})
  private
    // Window handle
    WinHandle: HWND;
    // Windowed mode
    Windowed: Boolean;
    {$IFDEF PETRADDRAW}
    Palette: TColorPalette;
    Gamma: TGammaValue;
    DDraw: IDirectDraw7;
    Primary: IDirectDrawSurface7;
    DDPal: IDirectDrawPalette;
    Rasterizer: TRasterizer;
    Pixels: array [0..199] of array [0..319] of Byte;
    {$ENDIF}
    {$IFDEF PETRAD3D}
    Rasterizer: TD3DRasterizer;
    {$ENDIF}
    {$IFDEF PETRAS3D}
    Rasterizer: TS3DRasterizer;
    {$ENDIF}
  protected
  public
    // Check for messages
    procedure CheckMessages;
  public
    // TGraphicsDriver methods
    procedure Initialize; override;
    procedure Shutdown; override;
    {$IFDEF PETRAHW}
    function BeginFrame: Boolean; override;
    {$ELSE}
    function BeginFrame: PFrameData; override;
    {$ENDIF}
    {$IFDEF PETRADDRAW}
    procedure FinishFrame; override;
    procedure SetColorPalettePart(const APalette: TColorPalette; First, Last: Integer); override;
    procedure SetGamma(AGamma: TGammaValue); override;
    function GetRasterizer: TRasterizer; override;
    {$ENDIF}
    {$IFDEF PETRAD3D}
    procedure FinishFrame; override;
    procedure SetColorPalettePart(const APalette: TColorPalette; First, Last: Integer); override;
    procedure SetGamma(AGamma: TGammaValue); override;
    function GetRasterizer: TRasterizer; override;
    {$ENDIF}
    {$IFDEF PETRAS3D}
    procedure FinishFrame; override;
    procedure SetColorPalettePart(const APalette: TColorPalette; First, Last: Integer); override;
    procedure SetGamma(AGamma: TGammaValue); override;
    function GetRasterizer: TRasterizer; override;
    {$ENDIF}
  end;

implementation

uses
  Engine;

function WindowProc(Wnd: HWND; Msg: UINT; WParam: WPARAM; LParam: LPARAM): LRESULT; stdcall;
var
  R: TRect;
  P: TPoint;
  CX, CY: Integer;
begin
  case Msg of
    WM_CLOSE: begin
      PostQuitMessage(0);
      Exit(0);
    end;
    WM_SYSKEYDOWN,
    WM_KEYDOWN: if Assigned(GInput) then TWinInputDriver(GInput).NotifyKey(WParam, True);
    WM_SYSKEYUP,
    WM_KEYUP: if Assigned(GInput) then TWinInputDriver(GInput).NotifyKey(WParam, False);
    WM_LBUTTONDOWN: if Assigned(GInput) then TWinInputDriver(GInput).NotifyKey(10000, True);
    WM_LBUTTONUP: if Assigned(GInput) then TWinInputDriver(GInput).NotifyKey(10000, False);
    WM_MBUTTONDOWN: if Assigned(GInput) then TWinInputDriver(GInput).NotifyKey(10001, True);
    WM_MBUTTONUP: if Assigned(GInput) then TWinInputDriver(GInput).NotifyKey(10001, False);
    WM_RBUTTONDOWN: if Assigned(GInput) then TWinInputDriver(GInput).NotifyKey(10002, True);
    WM_RBUTTONUP: if Assigned(GInput) then TWinInputDriver(GInput).NotifyKey(10002, False);
    WM_MOUSEWHEEL: if Assigned(GInput) then begin
      if WParam > 0 then begin
        TWinInputDriver(GInput).NotifyKey(10003, True);
        TWinInputDriver(GInput).NotifyKey(10003, False);
      end else if WParam < 0 then begin
        TWinInputDriver(GInput).NotifyKey(10004, True);
        TWinInputDriver(GInput).NotifyKey(10004, False);
      end;
    end;
    WM_MOUSEMOVE: if GetActiveWindow=Wnd then begin
      GetCursorPos(@P);
      GetWindowRect(Wnd, @R);
      if (R.Left=0) and (R.Top=0) then begin
        CX:=HRes div 2;
        CY:=VRes div 2;
      end else begin
        CX:=R.Left + (R.Right - R.Left) div 2;
        CY:=R.Top + (R.Bottom - R.Top) div 2;
      end;
      if (CX <> P.X) or (CY <> P.Y) then begin
        if Assigned(GInput) then
          TWinInputDriver(GInput).NotifyMouse(P.X - CX, P.Y - CY);
        SetCursorPos(CX, CY);
      end;
    end;
    WM_SYSCOMMAND: if (WParam=SC_MOUSEMENU) or (WParam=SC_KEYMENU) then Exit(0);
    WM_PAINT: ValidateRect(Wnd, nil);
  end;
  Result:=DefWindowProc(Wnd, Msg, WParam, LParam);
end;

{ TWinGraphicsDriver }
procedure TWinGraphicsDriver.CheckMessages;
{$push}{$WARN 5036 off}
var
  Msg: TMsg;
begin
  while PeekMessage(@Msg, 0, 0, 0, PM_REMOVE) do begin
    if Msg.Message=WM_QUIT then begin
      if Assigned(GInput) then TWinInputDriver(GInput).NotifyGotQuit;
    end;
    TranslateMessage(@Msg);
    DispatchMessage(@Msg);
  end;
end;
{$pop}

procedure TWinGraphicsDriver.Initialize;
var
  WC: TWndClass;
  R, DR: TRect;
  I, DX, DY: Integer;
  {$IFDEF PETRADDRAW}
  ddsd: TDDSurfaceDesc2;
  PalColors: array [0..255] of TPALETTEENTRY;
  {$ENDIF}
begin
  // Check if we're running in windowed mode
  for I:=1 to ParamCount do if LowerCase(ParamStr(I))='-window' then
    Windowed:=True;
  // Prepare window class
  ZeroMemory(@WC, SizeOf(WC));
  {$IFDEF PETRAGL}
  WC.style:=CS_OWNDC;
  {$ENDIF}
  WC.lpfnWndProc:=@WindowProc;
  WC.hInstance:=HInstance;
  WC.hIcon:=LoadIcon(0, IDI_APPLICATION);
  WC.hCursor:=LoadCursor(0, IDC_ARROW);
  WC.lpszClassName:='CSCLASS';
  RegisterClass(@WC);
  R.Left:=0;
  R.Top:=0;
  R.Right:=640;
  R.Bottom:=480;
  {$IFDEF GDIBASE}
  GetWindowRect(GetDesktopWindow, R);
  {$ENDIF}
  if Windowed then begin
    DR:=R;
    R.Left:=0;
    R.Top:=0;
    R.Right:=HRes;
    R.Bottom:=VRes;
    AdjustWindowRect(@R, WS_CAPTION or WS_BORDER or WS_OVERLAPPED or
      WS_SYSMENU or WS_MINIMIZEBOX, False);
    DX:=R.Right - R.Left;
    DY:=R.Bottom - R.Top;
    R.Left:=((DR.Right - DR.Left) - DX) div 2;
    R.Right:=R.Left + DX;
    R.Top:=((DR.Bottom - DR.Top) - DY) div 2;
    R.Bottom:=R.Top + DY;
    if R.Left < 0 then R.Left:=0;
    if R.Top < 0 then R.Top:=0;
    WinHandle:=CreateWindowEx(
      0,
      'CSCLASS', 'CS',
      WS_CAPTION or WS_BORDER or WS_OVERLAPPED or WS_SYSMENU or WS_MINIMIZEBOX,
      R.Left, R.Top, R.Right - R.Left, R.Bottom - R.Top,
      0, 0, HInstance, nil);
  end else begin
    WinHandle:=CreateWindowEx(
      0,
      'CSCLASS', 'CS',
      WS_POPUP or WS_SYSMENU,
      0, 0, R.Right - R.Left, R.Bottom - R.Top,
      0, 0, HInstance, nil);
  end;
  ShowWindow(WinHandle, SW_SHOW);
  ShowCursor(False);
  {$IFDEF PETRADDRAW}
  // Create DirectDraw 7 object
  if Failed(DirectDrawCreateEx(nil, DDraw, IID_IDirectDraw7, nil)) then begin
    MessageBox(WinHandle, 'Failed to create Direct Draw 7 object', 'Error', MB_ICONERROR or MB_OK or MB_TOPMOST);
    Halt;
  end;
  // Setup cooperative level
  if Failed(DDraw.SetCooperativeLevel(WinHandle, DDSCL_EXCLUSIVE or DDSCL_FULLSCREEN or DDSCL_ALLOWMODEX)) then begin
    MessageBox(WinHandle, 'Failed to create Direct Draw 7 object', 'Error', MB_ICONERROR or MB_OK or MB_TOPMOST);
    Halt;
  end;
  // Attempt to change the display mode
  if Failed(DDraw.SetDisplayMode(320, 200, 8, 0, DDSDM_STANDARDVGAMODE)) then begin
    if Failed(DDraw.SetDisplayMode(320, 200, 8, 0, 0)) then begin
      MessageBox(WinHandle, 'Failed to set 320x200x8 mode', 'Error', MB_ICONERROR or MB_OK or MB_TOPMOST);
      Halt;
    end;
  end;
  // Create primary surface
  ZeroMemory(@ddsd, SizeOf(ddsd));
  with ddsd do begin
    dwSize:=SizeOf(ddsd);
    dwFlags:=DDSD_CAPS;
    ddsCaps.dwCaps:=DDSCAPS_PRIMARYSURFACE;
  end;
  if Failed(DDraw.CreateSurface(ddsd, Primary, nil)) then begin
    MessageBox(WinHandle, 'Failed to create primary Direct Draw 7 surface', 'Error', MB_ICONERROR or MB_OK or MB_TOPMOST);
    Halt;
  end;
  // Create palette
  for I:=0 to 255 do with PalColors[I] do begin
    peRed:=I;
    peGreen:=I;
    peBlue:=I;
    peFlags:=0;
  end;
  if Failed(DDraw.CreatePalette(DDPCAPS_8BIT, @PalColors[0], DDPal, nil)) then begin
    MessageBox(WinHandle, 'Failed to create primary Direct Draw 7 surface palette', 'Error', MB_ICONERROR or MB_OK or MB_TOPMOST);
    Halt;
  end;
  if Failed(Primary.SetPalette(DDPal)) then begin
    MessageBox(WinHandle, 'Failed to setup primary Direct Draw 7 surface palette', 'Error', MB_ICONERROR or MB_OK or MB_TOPMOST);
    Halt;
  end;
  // Create color palette
  Palette:=TColorPalette.Create;
  // Create the software rasterizer
  Rasterizer:=TFramebufferRasterizer.Create;
  {$ENDIF}
  {$IFDEF PETRAHW}
  GetClientRect(WinHandle, R);
  AspectRatio:=R.Right/R.Bottom;
  RealHRes:=R.Right;
  RealVRes:=R.Bottom;
  {$ENDIF}
  {$IFDEF PETRAD3D}
  try
    Rasterizer:=TD3DRasterizer.Create(WinHandle);
  except
    MessageBox(WinHandle, PChar(Exception(ExceptObject).Message), 'Direct3D Initialization Error', MB_ICONERROR or MB_OK or MB_TOPMOST);
    Halt;
  end;
  {$ENDIF}
  {$IFDEF PETRAS3D}
  try
    Rasterizer:=TS3DRasterizer.Create(WinHandle);
  except
    MessageBox(WinHandle, PChar(Exception(ExceptObject).Message), 'S3D Initialization Error', MB_ICONERROR or MB_OK or MB_TOPMOST);
    Halt;
  end;
  {$ENDIF}
  {$IFDEF GDIBASE}
  InitializeGDI(WinHandle);
  {$ENDIF}
end;

procedure TWinGraphicsDriver.Shutdown;
begin
  {$IFDEF PETRAD3D}
  //TODO: figure out why this is crashing inside FPC's automatic refcounter
  //Rasterizer.Free;
  //Rasterizer:=nil;
  {$ENDIF}
  {$IFDEF PETRADDRAW}
  Palette.Free;
  Palette:=nil;
  Rasterizer.Free;
  Rasterizer:=nil;
  DDPal:=nil;
  Primary:=nil;
  DDraw:=nil;
  {$ENDIF}
  {$IFDEF GDIBASE}
  ShutdownGDI;
  {$ENDIF}
  ShowCursor(True);
  DestroyWindow(WinHandle);
  WinHandle:=0;
end;

{$IFDEF PETRAHW}
function TWinGraphicsDriver.BeginFrame: Boolean;
{$ELSE}
function TWinGraphicsDriver.BeginFrame: PFrameData;
{$ENDIF}
{$IFDEF PETRADDRAW}
var
  desc: TDDSurfaceDesc2;
  R: HRESULT;
{$ENDIF}
begin
  CheckMessages;
  {$IFDEF GDIBASE}
  InvalidateRect(WinHandle, nil, FALSE);
  Result:=inherited BeginFrame;
  {$ELSE}
  {$IFDEF PETRAHW}
  {$IFDEF PETRAD3D}
  Result:=Rasterizer.BeginScene;
  {$ELSE}
  Result:=True;
  {$ENDIF}
  {$ELSE}
  Result:=@Pixels[0, 0];
  {$ENDIF}
  {$ENDIF}
end;

{$IFDEF PETRADDRAW}
procedure TWinGraphicsDriver.FinishFrame;
var
  R: HRESULT;
  desc: TDDSurfaceDesc2;
  Y: Integer;
begin
  desc.dwSize:=SizeOf(desc);
  R:=Primary.Lock(nil, desc, DDLOCK_WAIT, 0);
  if Failed(R) then begin
    if R=DDERR_SURFACELOST then DDraw.RestoreAllSurfaces;
    Exit;
  end;
  if desc.lPitch=HRes then
    Move(Pixels, desc.lpSurface^, SizeOf(Pixels))
  else
    for Y:=0 to VRes - 1 do
      Move(Pixels[Y], (PByte(desc.lpSurface) + Y*desc.lPitch)^, HRes);
  Primary.Unlock(nil);
end;

procedure TWinGraphicsDriver.SetColorPalettePart(const APalette: TColorPalette;
  First, Last: Integer);
var
  E: TPALETTEENTRY;
  Value: Integer;
begin
  while First <= Last do begin
    Palette.Red[First]:=APalette.Red[First];
    Palette.Green[First]:=APalette.Green[First];
    Palette.Blue[First]:=APalette.Blue[First];
    Value:=Round(Palette.Red[First]*(1.0 + Gamma/High(TGammaValue)) + Gamma);
    if Value > 255 then Value:=255;
    E.peRed:=Value;
    Value:=Round(Palette.Green[First]*(1.0 + Gamma/High(TGammaValue)) + Gamma);
    if Value > 255 then Value:=255;
    E.peGreen:=Value;
    Value:=Round(Palette.Blue[First]*(1.0 + Gamma/High(TGammaValue)) + Gamma);
    if Value > 255 then Value:=255;
    E.peBlue:=Value;
    DDPal.SetEntries(0, First, 1, @E);
    Inc(First);
  end;
end;

procedure TWinGraphicsDriver.SetGamma(AGamma: TGammaValue);
begin
  Gamma:=AGamma;
  SetColorPalette(Palette);
end;

function TWinGraphicsDriver.GetRasterizer: TRasterizer;
begin
  Result:=Rasterizer;
end;
{$ENDIF}

{$IFDEF PETRAD3D}
procedure TWinGraphicsDriver.FinishFrame;
begin
  Rasterizer.EndScene;
  Rasterizer.Flip;
end;

procedure TWinGraphicsDriver.SetColorPalettePart(const APalette: TColorPalette;
  First, Last: Integer);
begin
end;

procedure TWinGraphicsDriver.SetGamma(AGamma: TGammaValue);
begin
end;

function TWinGraphicsDriver.GetRasterizer: TRasterizer;
begin
  Result:=Rasterizer;
end;
{$ENDIF}

{$IFDEF PETRAS3D}
procedure TWinGraphicsDriver.FinishFrame;
begin
  Rasterizer.Flip;
end;

procedure TWinGraphicsDriver.SetColorPalettePart(const APalette: TColorPalette;
  First, Last: Integer);
begin
end;

procedure TWinGraphicsDriver.SetGamma(AGamma: TGammaValue);
begin
end;

function TWinGraphicsDriver.GetRasterizer: TRasterizer;
begin
  Result:=Rasterizer;
end;
{$ENDIF}

end.
