{ Windows sound driver }
unit WinSnd;

interface

uses
  Windows, MMSystem, Classes, SndDrv;

type
  { TWinSoundDriver - Windows sound driver }
  TWinSoundDriver = class(TSoundDriver)
  private
    // Called by the callback to fill the next samples
    procedure OnCallback;
  protected
    // TSoundDriver methods
    procedure InitializeDriver; override;
    procedure LockBuffers; override;
    procedure UnlockBuffers; override;
  public
    // TSoundDriver methods
    procedure Update; override;
  public
    destructor Destroy; override;
  end;

implementation

uses
  Engine;

var
  Header: array [0..MaxSoundBuffers - 1] of TWAVEHDR;
  Handle: THandle;
  Crit: TCRITICAL_SECTION;
  Exiting: Boolean;
  ThreadID: DWORD;
  ThreadHandle: THandle;

{ Local Functions }
procedure SoundCallback(wo: THandle; msg: UINT; inst, p1, p2: DWORD_PTR); stdcall;
begin
  if Msg=WOM_DONE then TWinSoundDriver(GSound).OnCallback;
end;

function SoundThreadProc(Parameter: Pointer): DWORD; stdcall;
var
  Msg: TMSG;
begin
  while GetMessage(@Msg, 0, 0, 0) do begin
    case Msg.Message of
      MM_WOM_DONE: begin
        EnterCriticalSection(Crit);
        if not Exiting then TWinSoundDriver(Parameter).OnCallback;
        LeaveCriticalSection(Crit);
      end;
    end;
  end;
  Result:=0;
  // Notify the main thread that this thread is done
  EnterCriticalSection(Crit);
  Exiting:=False;
  LeaveCriticalSection(Crit);
end;

{ TWinSoundDriver }
destructor TWinSoundDriver.Destroy;
var
  I: Integer;
begin
  if Available then begin
    waveOutPause(Handle);
    EnterCriticalSection(Crit);
    Exiting:=True;
    LeaveCriticalSection(Crit);
    PostThreadMessage(ThreadID, WM_QUIT, 0, 0);
    while True do begin
      Sleep(1);
      EnterCriticalSection(Crit);
      if not Exiting then begin
        LeaveCriticalSection(Crit);
        Break;
      end;
      LeaveCriticalSection(Crit);
    end;
    for I:=High(Header) downto 0 do
      waveOutUnprepareHeader(Handle, @Header[I], SizeOf(Header[0]));
    waveOutClose(Handle);
    DeleteCriticalSection(Crit);
  end;
  Handle:=0;
  timeEndPeriod(1);
  inherited Destroy;
end;

procedure TWinSoundDriver.OnCallback;
begin
  if SilentCounter <> 0 then
    FillChar(Buffers[NextBuffer], SizeOf(Buffers[0]), 0);
  if Mixed=0 then MixNow;
  waveOutWrite(Handle, @Header[NextBuffer], SizeOf(Header[0]));
  NextBuffer:=(NextBuffer + 1) mod MaxSoundBuffers;
  NeedMix:=True;
  while Mixed < Length(Buffers) - 1 do MixNow;
  Dec(Mixed);
end;

procedure TWinSoundDriver.InitializeDriver;
var
  Fmt: TWAVEFORMATEX;
  I: Integer;
begin
  timeBeginPeriod(1);
  // Reset exiting flag
  Exiting:=False;
  // Create critical section for the audio thread
  InitializeCriticalSection(Crit);
  // Initialize format header
  FillChar(Fmt, SizeOf(Fmt), 0);
  with Fmt do begin
    wFormatTag:=WAVE_FORMAT_PCM;
    nChannels:=2;
    nSamplesPerSec:=22050;
    wBitsPerSample:=16;
    nBlockAlign:=(nChannels*wBitsPerSample) div 8;
    nAvgBytesPerSec:=nBlockAlign*nSamplesPerSec;
  end;
  // Create audio thread
  ThreadHandle:=CreateThread(nil, 0, @SoundThreadProc, Self, 0, ThreadID);
  if ThreadHandle=0 then begin
    DeleteCriticalSection(Crit);
    Exit;
  end;
  SetThreadPriority(ThreadHandle, THREAD_PRIORITY_HIGHEST);
  // Wait until the audio thread has a message queue
  while not PostThreadMessage(ThreadID, WM_USER, 0, 0) do Sleep(1);
  // Open wave device
  Handle:=0;
  if waveOutOpen(@Handle, WAVE_MAPPER, @Fmt, DWORD_PTR(ThreadID), 0, CALLBACK_THREAD) <> MMSYSERR_NOERROR then begin
    PostThreadMessage(ThreadID, WM_QUIT, 0, 0);
    CloseHandle(ThreadHandle);
    ThreadHandle:=0;
    Handle:=0;
    Exit;
  end;
  // Stop audio for now
  waveOutPause(Handle);
  // Prepare header
  FillChar(Header, SizeOf(Header), 0);
  for I:=0 to High(Buffers) do begin
    Header[I].dwBufferLength:=SizeOf(Buffers[I]);
    Header[I].lpData:=@Buffers[I, 0];
    waveOutPrepareHeader(Handle, @Header[I], SizeOf(Header[0]));
    if I < Length(Buffers) - 1 then
      waveOutWrite(Handle, @Header[I], SizeOf(Header[0]));
  end;
  NextBuffer:=0;
  // Set the available flag, we can use audio
  Available:=True;
  // Start playback
  waveOutRestart(Handle);
end;

procedure TWinSoundDriver.LockBuffers;
begin
  EnterCriticalSection(Crit);
end;

procedure TWinSoundDriver.UnlockBuffers;
begin
  LeaveCriticalSection(Crit);
end;

procedure TWinSoundDriver.Update;
begin
  // Do nothing
end;

end.

