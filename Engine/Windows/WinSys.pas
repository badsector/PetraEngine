{ Windows system driver }
unit WinSys;

interface

uses
  SysDrv;

type
  { TWinSystemDriver - windows-specific system driver }
  TWinSystemDriver = class(TSystemDriver)
  private
    Base, Freq: UInt64;
  protected
    function GetMilliseconds: Integer; override;
  public
    constructor Create;
  end;

implementation

uses
  Windows;

{ TWinSystemDriver }
constructor TWinSystemDriver.Create;
var
  LIFreq: TLargeInteger;
  LIBase: TLargeInteger;
begin
  inherited Create;
  QueryPerformanceFrequency(LIFreq);
  QueryPerformanceCounter(LIBase);
  Freq:=LIFreq;
  Base:=LIBase;
end;

function TWinSystemDriver.GetMilliseconds: Integer;
var
  LICounter: TLargeInteger;
begin
  QueryPerformanceCounter(LICounter);
  Result:=(LICounter - Base)*1000 div Freq;
end;

end.
