{ Windows input driver }
unit WinInput;

interface

uses
  Windows, MMSystem, InputDrv;

type

  { TWinInputDriver }

  TWinInputDriver = class(TInputDriver)
  private
    // Got a quit request
    GotQuit: Boolean;
    // Stored joystick state for all known joysticks
    JoyState: array of TJOYINFOEX;
    // Plugged joystick state
    JoyPlugged: array of Boolean;
    // Joystick caps
    JoyCaps: array of TJOYCAPS;
    // Fake joystick keys
    FakeJoyKeys: array [TInputKey] of Boolean;
    // Check for any joystick events
    procedure CheckJoystickEvents;
  public
    // Called by TWinGraphicsDriver for key events
    procedure NotifyKey(VKey: DWORD; Down: Boolean);
    // Called by TWinGraphicsDriver for mouse events
    procedure NotifyMouse(X, Y: Integer);
    // Called by TWinGraphicsDriver when WM_QUIT arrives
    procedure NotifyGotQuit;
  public
    constructor Create;
    // TInputDriver methods
    function IsLive: Boolean; override;
  end;

implementation

uses
  Maths;

{ Utilities }

// Returns the input key for the given virtual key code
function GetKeyFor(VKey: DWORD): TInputKey;
begin
  case VKey of
    27: Result:=ikEscape;
    112: Result:=ikF1;
    113: Result:=ikF2;
    114: Result:=ikF3;
    115: Result:=ikF4;
    116: Result:=ikF5;
    117: Result:=ikF6;
    118: Result:=ikF7;
    119: Result:=ikF8;
    120: Result:=ikF9;
    121: Result:=ikF10;
    122: Result:=ikF11;
    123: Result:=ikF12;
    145: Result:=ikScrollLock;
    19: Result:=ikPause;
    192: Result:=ikBackQuote;
    48: Result:=ik0;
    49: Result:=ik1;
    50: Result:=ik2;
    51: Result:=ik3;
    52: Result:=ik4;
    53: Result:=ik5;
    54: Result:=ik6;
    55: Result:=ik7;
    56: Result:=ik8;
    57: Result:=ik9;
    189: Result:=ikMinus;
    187: Result:=ikEquals;
    8: Result:=ikBackspace;
    45: Result:=ikInsert;
    36: Result:=ikHome;
    33: Result:=ikPageUp;
    34: Result:=ikPageDown;
    9: Result:=ikTab;
    65: Result:=ikA;
    66: Result:=ikB;
    67: Result:=ikC;
    68: Result:=ikD;
    69: Result:=ikE;
    70: Result:=ikF;
    71: Result:=ikG;
    72: Result:=ikH;
    73: Result:=ikI;
    74: Result:=ikJ;
    75: Result:=ikK;
    76: Result:=ikL;
    77: Result:=ikM;
    78: Result:=ikN;
    79: Result:=ikO;
    80: Result:=ikP;
    81: Result:=ikQ;
    82: Result:=ikR;
    83: Result:=ikS;
    84: Result:=ikT;
    85: Result:=ikU;
    86: Result:=ikV;
    87: Result:=ikW;
    88: Result:=ikX;
    89: Result:=ikY;
    90: Result:=ikZ;
    219: Result:=ikLeftBracket;
    221: Result:=ikRightBracket;
    220: Result:=ikBackSlash;
    13: Result:=ikEnter;
    20: Result:=ikCapsLock;
    186: Result:=ikSemiColon;
    222: Result:=ikQuote;
    16: Result:=ikShift;
    188: Result:=ikComma;
    190: Result:=ikPeriod;
    191: Result:=ikSlash;
    17: Result:=ikControl;
    18: Result:=ikAlt;
    91: Result:=ikLeftMeta;
    92: Result:=ikRightMeta;
    32: Result:=ikSpace;
    46: Result:=ikDelete;
    35: Result:=ikEnd;
    37: Result:=ikLeft;
    39: Result:=ikRight;
    38: Result:=ikUp;
    40: Result:=ikDown;
    141: Result:=ikNumLock;
    111: Result:=ikDivide;
    106: Result:=ikMultiply;
    109: Result:=ikSubtract;
    107: Result:=ikAdd;
    96: Result:=ikKeypad0;
    97: Result:=ikKeypad1;
    98: Result:=ikKeypad2;
    99: Result:=ikKeypad3;
    100: Result:=ikKeypad4;
    101: Result:=ikKeypad5;
    102: Result:=ikKeypad6;
    103: Result:=ikKeypad7;
    104: Result:=ikKeypad8;
    105: Result:=ikKeypad9;
    110: Result:=ikKeypadPoint;
    10000: Result:=ikLeftButton;
    10001: Result:=ikMiddleButton;
    10002: Result:=ikRightButton;
    10003: Result:=ikWheelUp;
    10004: Result:=ikWheelDown;
    else Result:=ikUnknown;
  end;
end;

{ TWinInputDriver }
procedure TWinInputDriver.CheckJoystickEvents;
var
  Info: TJOYINFOEX;
  I: Integer;
  Motion: TVector;

  // Ensure the given fake joystick key is at the given state
  procedure EnsureFakeKey(AKey: TInputKey; State: Boolean);
  begin
    if FakeJoyKeys[AKey] <> State then begin
      FakeJoyKeys[AKey]:=State;
      NotifyHandlersForKey(AKey, State);
    end;
  end;

begin
  for I:=0 to High(JoyState) do if JoyPlugged[I] then begin
    // Prepare structure for querying this joystick
    Info:=JoyState[I];
    Info.dwSize:=SizeOf(Info);
    Info.dwFlags:=JOY_RETURNX or JOY_RETURNY or JOY_RETURNZ or JOY_RETURNR or
      JOY_RETURNU or JOY_RETURNV or JOY_RETURNBUTTONS;
    // Query the current state
    if joyGetPosEx(I, @Info)=JOYERR_NOERROR then begin
      // Check X/Y axes
      if (Info.wXpos <> JoyState[I].wXpos) or (Info.wYpos <> JoyState[I].wYpos) then begin
        // Create a motion vector from the joystick position (left stick on
        // xbox controller)
        Motion:=Vector( ((Info.wXpos/JoyCaps[I].wXmax)-0.5)*2.0,
                       -((Info.wYpos/JoyCaps[I].wYmax)-0.5)*2.0, 0);
        // If the vector isn't long enough, stop the motion
        if Motion.Length < 0.09 then begin
          EnsureFakeKey(ikLeft, False);
          EnsureFakeKey(ikRight, False);
          EnsureFakeKey(ikUp, False);
          EnsureFakeKey(ikDown, False);
          EnsureFakeKey(ikShift, False);
        end else begin
          // Handle backwards motion
          if Motion.Y < -0.1 then begin
            // Down key should only be sent if the joystick has moved enough to
            // avoid accidental backsteps
            EnsureFakeKey(ikDown, Motion.Y < -0.7);
            // Other keys should be off
            EnsureFakeKey(ikUp, False);
            EnsureFakeKey(ikShift, False);
          end else begin // Handle forward motion
            EnsureFakeKey(ikUp, Motion.Y > 0.4);
            EnsureFakeKey(ikShift, Motion.Y < 0.85);
          end;
          // Handle rotation
          EnsureFakeKey(ikLeft, Motion.X < -0.4);
          EnsureFakeKey(ikRight, Motion.X > 0.4);
        end;
      end;
      // Map button 1 (A on xbox controller) to control
      if (Info.wButtons and 1) <> (JoyState[I].wButtons and 1) then begin
        NotifyHandlersForKey(ikControl, (Info.wButtons and 1) <> 0);
      end;
      // Map button 2 (B on xbox controller) to alt
      if (Info.wButtons and 2) <> (JoyState[I].wButtons and 2) then begin
        NotifyHandlersForKey(ikAlt, (Info.wButtons and 2) <> 0);
      end;
      // Map button 3 (X on xbox controller) to enter
      if (Info.wButtons and 4) <> (JoyState[I].wButtons and 4) then begin
        NotifyHandlersForKey(ikEnter, (Info.wButtons and 4) <> 0);
      end;
      // Map button 4 (Y on xbox controller) to end
      if (Info.wButtons and 8) <> (JoyState[I].wButtons and 8) then begin
        NotifyHandlersForKey(ikEnd, (Info.wButtons and 8) <> 0);
      end;
      // Map button 7 (Select on xbox controller) to escape
      if (Info.wButtons and 64) <> (JoyState[I].wButtons and 64) then begin
        NotifyHandlersForKey(ikEscape, (Info.wButtons and 64) <> 0);
      end;
      // Map button 8 (Start on xbox controller) to space
      if (Info.wButtons and 128) <> (JoyState[I].wButtons and 128) then begin
        NotifyHandlersForKey(ikSpace, (Info.wButtons and 128) <> 0);
      end;
      // Check POV angle (d-pad on xbox controller)
      if Info.dwPOV <> JoyState[I].dwPOV then with Info do begin
        EnsureFakeKey(ikUp, (dwPOV <= 8900) or (dwPOV >= 28000) and (dwPOV <= 35900));
        EnsureFakeKey(ikLeft, (dwPOV >= 19000) and (dwPOV <= 32000));
        EnsureFakeKey(ikDown, (dwPOV >= 10000) and (dwPOV <= 26000));
        EnsureFakeKey(ikRight, (dwPOV >= 3000) and (dwPOV <= 17000));
      end;
      // Store new state
      JoyState[I]:=Info;
    end;
  end;
end;

procedure TWinInputDriver.NotifyKey(VKey: DWORD; Down: Boolean);
var
  FakeKey, InputKey: TInputKey;
begin
  InputKey:=GetKeyFor(VKey);
  if not (InputKey in [ikNone, ikUnknown]) then begin
    // Reset any fake keys
    for FakeKey:=Low(TInputKey) to High(TInputKey) do
      if FakeJoyKeys[FakeKey] then begin
        FakeJoyKeys[FakeKey]:=False;
        NotifyHandlersForKey(FakeKey, False);
      end;
    // Notify for the key pressed/released now
    NotifyHandlersForKey(InputKey, Down);
  end;
end;

procedure TWinInputDriver.NotifyMouse(X, Y: Integer);
begin
  NotifyHandlersForMouse(X, Y);
end;

procedure TWinInputDriver.NotifyGotQuit;
begin
  GotQuit:=True;
end;

constructor TWinInputDriver.Create;
var
  Info: TJOYINFO;
  I: Integer;
begin
  inherited Create;
  // Initialize joystick info
  SetLength(JoyState, joyGetNumDevs);
  SetLength(JoyPlugged, Length(JoyState));
  SetLength(JoyCaps, Length(JoyState));
  for I:=0 to High(JoyState) do begin
    JoyPlugged[I]:=joyGetPos(I, @Info)=JOYERR_NOERROR;
    if JoyPlugged[I] then joyGetDevCaps(I, @JoyCaps[I], SizeOf(TJOYCAPS));
  end;
end;

function TWinInputDriver.IsLive: Boolean;
begin
  if GotQuit then Exit(False);
  // Check for any joystick events
  CheckJoystickEvents;
  Result:=True;
end;

end.
