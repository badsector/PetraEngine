{ Object serialization/deserialization }
unit Serial;

{$mode objfpc}{$H+}{$M+}
{$WARN 5026 off : Value parameter "$1" is assigned but never used}

interface

uses
  Misc, Classes, SysUtils;

type
  { Forward class declarations }
  TSerializable = class;

  { Exception for serialization errors }
  ESerializationError = class(Exception);

  { Callback to be called for root object deserialization updates - Progress
    will be a value between 0 and 1 based on the current progress }
  TRootDeserializationUpdate = procedure(Progress: Single) of object;

  { TSerializationContext - Contains data during the serialization across
    all objects that are serialized and provides convenient methods for
    writing to and reading from the serialization stream }
  TSerializationContext = class
  private
    // Property storage
    FStream: TStream;
    FVersion: Cardinal;
    function GetRootObject: TSerializable;
  private
    // Objects already written
    HandledObjects: array of TSerializable;
    // Strings already written
    HandledStrings: array of string;
    // Known object count, if any
    KnownObjectCount: Cardinal;
  public
    // Create a serialization context
    constructor Create(AStream: TStream);
    // Write a value of the specified size
    procedure Write(const Value; Size: SizeInt);
    // Write a 8bit unsigned integer
    procedure WriteUInt8(Value: UInt8); inline;
    // Write a 16bit unsigned integer
    procedure WriteUInt16(Value: UInt16); inline;
    // Write a 32bit unsigned integer
    procedure WriteUInt32(Value: UInt32); inline;
    // Write a 64bit unsigned integer
    procedure WriteUInt64(Value: UInt64); inline;
    // Write a 8bit signed integer
    procedure WriteInt8(Value: Int8); inline;
    // Write a 16bit signed integer
    procedure WriteInt16(Value: Int16); inline;
    // Write a 32bit signed integer
    procedure WriteInt32(Value: Int32); inline;
    // Write a 64bit signed integer
    procedure WriteInt64(Value: Int64); inline;
    // Write a 32bit floating point number
    procedure WriteSingle(Value: Single); inline;
    // Write a boolean
    procedure WriteBoolean(Value: Boolean); inline;
    // Write an ansi string
    procedure WriteAnsiString(Value: AnsiString); inline;
    // Write a string key/value set
    procedure WriteStringKeyValueSet(const Value: TStringKeyValueSet);
    // Write the given object
    procedure WriteObject(Value: TSerializable);
    // Read a value of the specified size
    procedure Read(out Value; Size: SizeInt);
    // Read a 8bit unsigned integer
    function ReadUInt8: UInt8; inline;
    // Read a 16bit unsigned integer
    function ReadUInt16: UInt16; inline;
    // Read a 32bit unsigned integer
    function ReadUInt32: UInt32; inline;
    // Read a 64bit unsigned integer
    function ReadUInt64: UInt64; inline;
    // Read a 8bit signed integer
    function ReadInt8: Int8; inline;
    // Read a 16bit signed integer
    function ReadInt16: Int16; inline;
    // Read a 32bit signed integer
    function ReadInt32: Int32; inline;
    // Read a 64bit signed integer
    function ReadInt64: Int64; inline;
    // Read a 32bit floating point number
    function ReadSingle: Single; inline;
    // Read a boolean
    function ReadBoolean: Boolean; inline;
    // Read an ansi string
    function ReadAnsiString: AnsiString; inline;
    // Read a string key/value set
    procedure ReadStringKeyValueSet(out Value: TStringKeyValueSet);
    // Read an object
    function ReadObject: TSerializable;
    // Read an object of the given class (raises exception if it doesn't match)
    function ReadObject(AClass: TClass): TSerializable;
    // The serialization stream
    property Stream: TStream read FStream;
    // Serialized data version
    property Version: Cardinal read FVersion;
    // Root object for the de/serialization
    property RootObject: TSerializable read GetRootObject;
  end;

  { TSerializable - Base class for serializable objects.  Note that this
    descends from TPersistent only so that TSerializable subclasses can be used
    with RegisterClass/GetClass and Lazarus' property editor. }
  TSerializable = class(TPersistent)
  private
    // Write the object's own data to the stream
    procedure WriteToStream(AContext: TSerializationContext);
    // Read the object's own data from the stream
    procedure ReadFromStream(AContext: TSerializationContext);
  protected
    // Called before serializing the object
    procedure BeforeSerialization; virtual;
    // Called after serializing the object
    procedure AfterSerialization; virtual;
    // Called before deserializing the object
    procedure BeforeDeserialization; virtual;
    // Called after deserializing the object
    procedure AfterDeserialization; virtual;
    // Called after completely deserializing the root object
    procedure AfterRootDeserialization; virtual;
    // Called when an unknown property was found (the property data must be
    // either read in and converted or skipped).  Must return false in case of
    // failure (the property cannot be loaded)
    function DeserializeUnknownProperty(APropName: string; AContext: TSerializationContext): Boolean; virtual;
    // Called to serialize any extra data not accessible through RTTI
    procedure SerializeExtraData(AContext: TSerializationContext); virtual;
    // Called to deserialize any extra data not accessible through RTTI
    procedure DeserializeExtraData(AContext: TSerializationContext); virtual;
  public
    // Virtual constructor
    constructor Create; virtual;
    // Serialize this object to the given stream
    procedure SerializeToStream(AStream: TStream);
    // Deserialize this object from the given stream
    procedure DeserializeFromStream(AStream: TStream);
    // Returns true if this object can be serialized
    function CanSerialize: Boolean; virtual;
    // Create a deep clone of this object
    function CreateDeepClone: TSerializable;
  end;

  { TSerializableCollection - A generic collection of TSerializable objects
    that can be used in a published property for automatic de/serialization }
  generic TSerializableCollection<T> = class(TSerializable)
  private
    // Objects in the collection (length can be greater than count)
    FObjects: array of T;
    // Number of objects in the collection
    FCount: Integer;
    function GetObjects(AIndex: Integer): T; inline;
    procedure SetCount(ASize: Integer);
    function GetFirst: T; inline;
    function GetLast: T; inline;
  protected
    // TSerializable methods
    procedure SerializeExtraData(AContext: TSerializationContext); override;
    procedure DeserializeExtraData(AContext: TSerializationContext); override;
  public
    destructor Destroy; override;
    // Clear the collection
    procedure Clear;
    // Delete all objects and clear the collection
    procedure DeleteAllAndClear;
    // Delete all objects, clear the collection and free this instance
    procedure DeleteAllAndFree;
    // Add the given object to the collection
    procedure Add(Obj: T);
    // Insert the given object in the collection at the given index
    procedure Insert(Index: Integer; Obj: T);
    // Remove the given object from the collection
    procedure Remove(Obj: T);
    // Remove the object at the given index from the collection
    procedure RemoveAt(Index: Integer);
    // Remove and delete the given object from the collection
    procedure Delete(Obj: T);
    // Remove and delete the object at the given index from the collection
    procedure DeleteAt(Index: Integer);
    // Swap the objects at the given indices
    procedure Swap(Index1, Index2: Integer);
    // Return the index of the given object or -1 if the object was unknow
    function IndexOf(Obj: T): Integer;
    // Return true if the collection is empty
    function IsEmpty: Boolean; inline;
    // Return true if the collection has objects (is not empty)
    function HasObjects: Boolean; inline;
    // The objects in the collection
    property Objects[AIndex: Integer]: T read GetObjects; default;
    // The size of the collection
    property Count: Integer read FCount write SetCount;
    // The first object in the collection
    property First: T read GetFirst;
    // The last object in the collection
    property Last: T read GetLast;
  end;

  { Class of serializable }
  TSerializableClass = class of TSerializable;

var
  // Used to indicate the current serialization version
  SerializationVersion: Cardinal = 9;
  // Known TSerializable classes
  SerializableClasses: array of TSerializableClass;
  // Root deserialization update callback
  GRootDeserializationUpdate: TRootDeserializationUpdate;

// Register a TSerializable class so that it becomes part of SerializableClasses
procedure RegisterSerializableClass(Cls: TSerializableClass);
// Find a TSerializable class by the given name
function FindSerializableClass(AName: string): TClass;

implementation

uses
  TypInfo;

{ Functions }
procedure RegisterSerializableClass(Cls: TSerializableClass);
begin
  RegisterClass(Cls);
  SetLength(SerializableClasses, Length(SerializableClasses) + 1);
  SerializableClasses[High(SerializableClasses)]:=Cls;
end;

function FindSerializableClass(AName: string): TClass;
var
  Alias: string;
  I: Integer;
begin
  Result:=GetClass(AName);
  if Assigned(Result) then Exit;
  // FPC 2.x generic alias
  I:=Pos('<', AName);
  if I <> 0 then begin
    Alias:=Copy(AName, 1, I - 1);
    I:=Pos('>', AName);
    while I > 1 do begin
      if AName[I] in ['.', '<'] then Break;
      Dec(I);
    end;
    Alias += '$' + Copy(AName, I + 1, MaxInt);
    Alias:=Copy(Alias, 1, Length(Alias) - 1);
    Exit(GetClass(Alias));
  end;
  Result:=nil;
end;

{ TSerializationContext }
constructor TSerializationContext.Create(AStream: TStream);
begin
  FStream:=AStream;
end;

function TSerializationContext.GetRootObject: TSerializable;
begin
  Result:=HandledObjects[0];
end;

procedure TSerializationContext.Write(const Value; Size: SizeInt);
begin
  Stream.WriteBuffer(Value, Size);
end;

procedure TSerializationContext.WriteUInt8(Value: UInt8);
begin
  Stream.WriteBuffer(Value, SizeOf(Value));
end;

procedure TSerializationContext.WriteUInt16(Value: UInt16);
begin
  Stream.WriteBuffer(Value, SizeOf(Value));
end;

procedure TSerializationContext.WriteUInt32(Value: UInt32);
begin
  Stream.WriteBuffer(Value, SizeOf(Value));
end;

procedure TSerializationContext.WriteUInt64(Value: UInt64);
begin
  Stream.WriteBuffer(Value, SizeOf(Value));
end;

procedure TSerializationContext.WriteInt8(Value: Int8);
begin
  Stream.WriteBuffer(Value, SizeOf(Value));
end;

procedure TSerializationContext.WriteInt16(Value: Int16);
begin
  Stream.WriteBuffer(Value, SizeOf(Value));
end;

procedure TSerializationContext.WriteInt32(Value: Int32);
begin
  Stream.WriteBuffer(Value, SizeOf(Value));
end;

procedure TSerializationContext.WriteInt64(Value: Int64);
begin
  Stream.WriteBuffer(Value, SizeOf(Value));
end;

procedure TSerializationContext.WriteSingle(Value: Single);
begin
  Stream.WriteBuffer(Value, SizeOf(Value));
end;

procedure TSerializationContext.WriteBoolean(Value: Boolean);
begin
  Stream.WriteBuffer(Value, SizeOf(Value));
end;

procedure TSerializationContext.WriteAnsiString(Value: AnsiString);
var
  I: Integer;
begin
  // Write 0 if this is an empty string
  if Length(Value)=0 then begin
    WriteInt16(0);
    Exit;
  end;
  // Check if there is already a string with the value
  for I:=High(HandledStrings) downto 0 do
    if HandledStrings[I]=Value then begin
      // Write the string index
      WriteUInt16(I + 1);
      Exit;
    end;
  // Write the new string's index
  if Length(HandledStrings)=65534 then
    raise ESerializationError.Create('Too many unique strings');
  WriteUInt16(Length(HandledStrings) + 1);
  // Write new string
  WriteInt32(Length(Value));
  if Length(Value) > 0 then Write(Value[1], Length(Value));
  // Store the string to the handled strings array
  SetLength(HandledStrings, Length(HandledStrings) + 1);
  HandledStrings[High(HandledStrings)]:=Value;
end;

procedure TSerializationContext.WriteStringKeyValueSet(const Value: TStringKeyValueSet);
var
  I: Integer;
begin
  // Write the number of entries in the set
  WriteInt32(Length(Value.Keys));
  // Write the strings
  for I:=0 to High(Value.Keys) do begin
    WriteAnsiString(Value.Keys[I]);
    WriteAnsiString(Value.Values[I]);
  end;
end;

procedure TSerializationContext.WriteObject(Value: TSerializable);
var
  I: Integer;
begin
  // Write 0 if this is nil or if the object is not serializable
  if not (Assigned(Value) and Value.CanSerialize) then begin
    WriteUInt16(0);
    Exit;
  end;
  // Check if the object has already been written
  for I:=High(HandledObjects) downto 0 do
    if Value=HandledObjects[I] then begin
      // Write the object index
      WriteUInt16(I + 1);
      Exit;
    end;
  // Write the object's new index
  WriteUInt16(Length(HandledObjects) + 1);
  // Store the object to the handled objects array
  SetLength(HandledObjects, Length(HandledObjects) + 1);
  HandledObjects[High(HandledObjects)]:=Value;
  // Write the object itself
  Value.WriteToStream(Self);
end;

procedure TSerializationContext.Read(out Value; Size: SizeInt);
begin
  {$push}{$warn 5058 off}
  Stream.Read(Value, Size);
  {$pop}
end;

function TSerializationContext.ReadUInt8: UInt8;
begin
  {$push}{$warn 5060 off}
  Stream.ReadBuffer(Result, SizeOf(Result));
  {$pop}
end;

function TSerializationContext.ReadUInt16: UInt16;
begin
  {$push}{$warn 5060 off}
  Stream.ReadBuffer(Result, SizeOf(Result));
  {$pop}
end;

function TSerializationContext.ReadUInt32: UInt32;
begin
  {$push}{$warn 5060 off}
  Stream.ReadBuffer(Result, SizeOf(Result));
  {$pop}
end;

function TSerializationContext.ReadUInt64: UInt64;
begin
  {$push}{$warn 5060 off}
  Stream.ReadBuffer(Result, SizeOf(Result));
  {$pop}
end;

function TSerializationContext.ReadInt8: Int8;
begin
  {$push}{$warn 5060 off}
  Stream.ReadBuffer(Result, SizeOf(Result));
  {$pop}
end;

function TSerializationContext.ReadInt16: Int16;
begin
  {$push}{$warn 5060 off}
  Stream.ReadBuffer(Result, SizeOf(Result));
  {$pop}
end;

function TSerializationContext.ReadInt32: Int32;
begin
  {$push}{$warn 5060 off}
  Stream.ReadBuffer(Result, SizeOf(Result));
  {$pop}
end;

function TSerializationContext.ReadInt64: Int64;
begin
  {$push}{$warn 5060 off}
  Stream.ReadBuffer(Result, SizeOf(Result));
  {$pop}
end;

function TSerializationContext.ReadSingle: Single;
begin
  {$push}{$warn 5060 off}
  Stream.ReadBuffer(Result, SizeOf(Result));
  {$pop}
end;

function TSerializationContext.ReadBoolean: Boolean;
begin
  {$push}{$warn 5060 off}
  Stream.ReadBuffer(Result, SizeOf(Result));
  {$pop}
end;

function TSerializationContext.ReadAnsiString: AnsiString;
var
  Index: Integer;
begin
  // Read the string's index
  Index:=ReadUInt16;
  // 0 means empty string
  if Index=0 then Exit('');
  // Adjust and check the index range
  Dec(Index);
  if Index > Length(HandledStrings) then
    raise ESerializationError.Create('Invalid string index ' + IntToStr(Index) + ' in serialized data');
  // If this is a known string that has already been loaded then return it
  if Index < Length(HandledStrings) then Exit(HandledStrings[Index]);
  // Read the new string
  SetLength(Result, ReadInt32);
  if Length(Result) > 0 then Stream.ReadBuffer(Result[1], Length(Result));
  // Store the new string in the handled strings array for future use
  SetLength(HandledStrings, Length(HandledStrings) + 1);
  HandledStrings[High(HandledStrings)]:=Result;
end;

procedure TSerializationContext.ReadStringKeyValueSet(out Value: TStringKeyValueSet);
var
  Count, I: Integer;
begin
  // Read the number of entries in the set
  Count:=ReadInt32;
  SetLength(Value.Keys, Count);
  SetLength(Value.Values, Count);
  // Read the entries
  for I:=0 to Count - 1 do begin
    Value.Keys[I]:=LowerCase(ReadAnsiString);
    Value.Values[I]:=ReadAnsiString;
  end;
end;

function TSerializationContext.ReadObject: TSerializable;
var
  Index: Integer;
  ObjClass: TClass;
  ClsName: string;
begin
  Result:=nil;
  // Read the object's index
  Index:=ReadUInt16;
  // If the index is 0 this is a nil object
  if Index=0 then Exit(nil);
  // Adjust and check the index range
  Dec(Index);
  if Index > Length(HandledObjects) then
    raise ESerializationError.Create('Invalid object index ' + IntToStr(Index) + ' in serialized data');
  // If this is a known object that has already been loaded then return it
  if Index < Length(HandledObjects) then Exit(HandledObjects[Index]);
  // Find new object's class
  ClsName:=ReadAnsiString;
  ObjClass:=FindSerializableClass(ClsName);
  if not Assigned(ObjClass) then
    raise ESerializationError.Create('Unknown class ' + ClsName);
  if not ObjClass.InheritsFrom(TSerializable) then
    raise ESerializationError.Create('Invalid class ' + ClsName);
  // Construct and read in the new object
  Result:=TSerializable(ObjClass.NewInstance);
  Result.Create;
  // Store the object in the handled objects array (since it might be referenced
  // by itself or by any other object that it causes to be loaded)
  SetLength(HandledObjects, Length(HandledObjects) + 1);
  HandledObjects[High(HandledObjects)]:=Result;
  // Read the object from the stream
  try
    Result.ReadFromStream(Self);
  except
    Result.Free;
    raise;
  end;
end;

function TSerializationContext.ReadObject(AClass: TClass): TSerializable;
begin
  Result:=ReadObject();
  if Assigned(Result) and not (Result is AClass) then begin
    Result.Free;
    raise ESerializationError.Create('Invalid object class, a ' + AClass.ClassName + ' was expected');
  end;
end;

{ TSerializable }
constructor TSerializable.Create;
begin
end;

procedure TSerializable.WriteToStream(AContext: TSerializationContext);
var
  PropList: PPropList;
  PropCount, I: Integer;

  // Write a single property
  procedure WriteProperty(const Prop: TPropInfo);
  var
    Obj: TObject;
  begin
    // Write property name
    AContext.WriteAnsiString(Prop.Name);
    // Write property value
    case Prop.PropType^.Kind of
      tkSet, tkEnumeration, tkInteger: case GetTypeData(Prop.PropType)^.OrdType of
        otSByte: AContext.WriteInt8(GetOrdProp(Self, @Prop));
        otUByte: AContext.WriteUInt8(UInt8(GetOrdProp(Self, @Prop)));
        otSWord: AContext.WriteInt16(GetOrdProp(Self, @Prop));
        otUWord: AContext.WriteUInt16(UInt16(GetOrdProp(Self, @Prop)));
        otSLong: AContext.WriteInt32(GetOrdProp(Self, @Prop));
        otULong: AContext.WriteUInt32(UInt32(GetOrdProp(Self, @Prop)));
        else
          raise ESerializationError.Create('Type for property ' + Prop.Name + ' of class ' + ClassName + ' is not supported');
      end;
      tkInt64: AContext.WriteInt64(GetOrdProp(Self, @Prop));
      tkQWord: AContext.WriteUInt64(UInt64(GetOrdProp(Self, @Prop)));
      tkChar, tkBool: AContext.WriteUInt8(UInt8(GetOrdProp(Self, @Prop)));
      tkFloat: AContext.WriteSingle(GetFloatProp(Self, @Prop));
      tkSString, tkAString: AContext.WriteAnsiString(GetStrProp(Self, @Prop));
      tkClass: begin
        Obj:=GetObjectProp(Self, @Prop);
        if Assigned(Obj) and not (Obj is TSerializable) then
          raise ESerializationError.Create(Obj.ClassName + ' used for property ' + Prop.Name + ' in ' + ClassName + ' is not a TSerializable descendant');
        AContext.WriteObject(TSerializable(Obj));
      end;
      else
        raise ESerializationError.Create('Type for property ' + Prop.Name + ' of class ' + ClassName + ' is not supported');
    end;
  end;

begin
  // Notify subclasses this object is about to be serialized
  BeforeSerialization;
  // Write the object class' name
  AContext.WriteAnsiString(ClassName);
  // Obtain property information
  PropCount:=GetPropList(ClassInfo, PropList);
  try
    // Write property count
    AContext.WriteUInt16(PropCount);
    // Write all properties
    for I:=0 to PropCount - 1 do WriteProperty(PropList^[I]^);
    // Perform any extra data serialization
    SerializeExtraData(AContext);
  finally
    FreeMem(PropList);
  end;
  // Notify subclasses this object has been serialized
  AfterSerialization;
end;

procedure TSerializable.ReadFromStream(AContext: TSerializationContext);
var
  PropCount, I: Integer;

  // Read a single property
  procedure ReadProperty;
  var
    PropName: string;
    Prop: PPropInfo;
    FakeProp: TPropInfo;
  begin
    // Read property name
    PropName:=AContext.ReadAnsiString;
    // Find the property
    Prop:=GetPropInfo(Self, PropName);
    if not Assigned(Prop) then begin
      if not DeserializeUnknownProperty(PropName, AContext) then
        raise ESerializationError.Create('Property ' + PropName + ' in class ' + ClassName + ' not found');
      Exit;
    end;
    // Handle a property without a write clause
    if ((Prop^.PropProcs shr 2) and 3)=ptConst then begin
      // If there is a read clause that uses a getter directly then write to it
      if (Prop^.PropProcs and 3)=ptField then begin
        FakeProp:=Prop^;
        FakeProp.PropProcs:=ptField shl 2;
        FakeProp.SetProc:=FakeProp.GetProc;
        Prop:=@FakeProp;
      end else
        raise ESerializationError.Create('Cannot write to property ' + PropName + ' of class ' + ClassName + ' - no write clause nor pure field read clause');
    end;
    // Read and store the property value
    case Prop^.PropType^.Kind of
      tkSet, tkEnumeration, tkInteger: case GetTypeData(Prop^.PropType)^.OrdType of
        otSByte: SetOrdProp(Self, Prop, AContext.ReadInt8);
        otUByte: SetOrdProp(Self, Prop, AContext.ReadUInt8);
        otSWord: SetOrdProp(Self, Prop, AContext.ReadInt16);
        otUWord: SetOrdProp(Self, Prop, AContext.ReadUInt16);
        otSLong: SetOrdProp(Self, Prop, AContext.ReadInt32);
        otULong: SetOrdProp(Self, Prop, AContext.ReadUInt32);
        else
          raise ESerializationError.Create('Type for property ' + Prop^.Name + ' of class ' + ClassName + ' is not supported');
      end;
      tkInt64: SetOrdProp(Self, Prop, AContext.ReadInt64);
      tkQWord: SetOrdProp(Self, Prop, Int64(AContext.ReadUInt64));
      tkChar, tkBool: SetOrdProp(Self, Prop, AContext.ReadUInt8);
      tkFloat: SetFloatProp(Self, Prop, AContext.ReadSingle);
      tkSString, tkAString: SetStrProp(Self, Prop, AContext.ReadAnsiString);
      tkClass: SetObjectProp(Self, Prop, AContext.ReadObject);
      else
        raise ESerializationError.Create('Type for property ' + Prop^.Name + ' of class ' + ClassName + ' is not supported');
    end;
  end;

begin
  // Notify any potential callback about the current state of deserialization
  if Assigned(GRootDeserializationUpdate) then
    GRootDeserializationUpdate(
      (Length(AContext.HandledObjects) - 1)/AContext.KnownObjectCount);

  // Notify subclasses this object is about to be deserialized
  BeforeDeserialization;

  // Note: in SerializeObject the name would be stored here, but it is assumed
  // that the name was read in before the object was created so we skip ahead
  // to reading in properties

  // Obtain property count
  PropCount:=AContext.ReadUInt16;
  // Read all properties
  for I:=0 to PropCount - 1 do ReadProperty;
  // Perform any extra data deserialization
  DeserializeExtraData(AContext);
  // Notify subclasses this object has been deserialized
  AfterDeserialization;
end;

procedure TSerializable.BeforeSerialization;
begin
end;

procedure TSerializable.AfterSerialization;
begin
end;

procedure TSerializable.BeforeDeserialization;
begin
end;

procedure TSerializable.AfterDeserialization;
begin
end;

procedure TSerializable.AfterRootDeserialization;
begin
end;

function TSerializable.DeserializeUnknownProperty(APropName: string;
  AContext: TSerializationContext): Boolean;
begin
  Result:=False;
end;

procedure TSerializable.SerializeExtraData(AContext: TSerializationContext);
{$push}{$warn 5024 off}
begin
end;
{$pop}

procedure TSerializable.DeserializeExtraData(AContext: TSerializationContext);
{$push}{$warn 5024 off}
begin
end;
{$pop}

procedure TSerializable.SerializeToStream(AStream: TStream);
var
  Ctx: TSerializationContext;
  ObjectCountPos: Cardinal;
  StoreCurPos: Int64;
begin
  // Begin the serialization
  if CanSerialize then begin
    try
      Ctx:=TSerializationContext.Create(AStream);
      // Write version
      Ctx.FVersion:=SerializationVersion;
      Ctx.WriteUInt32(SerializationVersion);
      // Reserve space for writing number of serialized objects
      ObjectCountPos:=AStream.Position;
      Ctx.WriteUInt32(0);
      // Set initial object
      SetLength(Ctx.HandledObjects, 1);
      Ctx.HandledObjects[0]:=Self;
      // Write the object
      WriteToStream(Ctx);
      // Store the number of serialized objects
      StoreCurPos:=AStream.Position;
      AStream.Position:=ObjectCountPos;
      Ctx.WriteUInt32(Length(Ctx.HandledObjects));
      AStream.Position:=StoreCurPos;
    finally
      Ctx.Free;
    end;
  end;
end;

procedure TSerializable.DeserializeFromStream(AStream: TStream);
var
  Ctx: TSerializationContext;
  ClsName: String;
  I: Integer;
begin
  // Begin the serialization
  if CanSerialize then begin
    try
      Ctx:=TSerializationContext.Create(AStream);
      // Read version
      Ctx.FVersion:=Ctx.ReadUInt32;
      // Read number of known serialized objects
      if Ctx.FVersion >= 8 then
        Ctx.KnownObjectCount:=Ctx.ReadUInt32
      else
        Ctx.KnownObjectCount:=MaxInt;
      // Read stored class name
      ClsName:=Ctx.ReadAnsiString;
      if ClsName <> ClassName then
        raise ESerializationError.Create('Invalid root object class name: ' + ClsName + ' instead of ' + ClassName);
      // Set initial object
      SetLength(Ctx.HandledObjects, 1);
      Ctx.HandledObjects[0]:=Self;
      // Read the object
      ReadFromStream(Ctx);
      // Notify all loaded objects they were completely deserialized
      for I:=0 to High(Ctx.HandledObjects) do
        Ctx.HandledObjects[I].AfterRootDeserialization;
      // Notify the final progress update
      if Assigned(GRootDeserializationUpdate) then
        GRootDeserializationUpdate(1.0);
    finally
      Ctx.Free;
    end;
  end;
end;

function TSerializable.CanSerialize: Boolean;
begin
  Result:=True;
end;

function TSerializable.CreateDeepClone: TSerializable;
var
  S: TMemoryStream;
begin
  try
    S:=TMemoryStream.Create;
    SerializeToStream(S);
    S.Position:=0;
    Result:=TSerializable(ClassType.NewInstance);
    Result.Create;
    Result.DeserializeFromStream(S);
  finally
    S.Free;
  end;
end;


{ TSerializableCollection }
destructor TSerializableCollection.Destroy;
begin
  Clear;
  inherited Destroy;
end;

function TSerializableCollection.GetObjects(AIndex: Integer): T;
begin
  Result:=FObjects[AIndex];
end;

procedure TSerializableCollection.SetCount(ASize: Integer);
begin
  if FCount=ASize then Exit;
  // Increase the capacity if needed
  if ASize > Length(FObjects) then SetLength(FObjects, ASize);
  // Set any new object slots to nil
  while ASize > FCount do begin
    FObjects[FCount]:=nil;
    Inc(ASize);
  end;
end;

function TSerializableCollection.GetFirst: T;
begin
  Result:=FObjects[0];
end;

function TSerializableCollection.GetLast: T;
begin
  Result:=FObjects[FCount - 1];
end;

procedure TSerializableCollection.SerializeExtraData(AContext: TSerializationContext);
var
  I, SerCount: Integer;
begin
  inherited SerializeExtraData(AContext);
  // Cound and write the number of serializable objects in the collection
  SerCount:=0;
  for I:=0 to FCount - 1 do if FObjects[I].CanSerialize then Inc(SerCount);
  AContext.WriteInt32(SerCount);
  // Write each object in the collection
  for I:=0 to FCount - 1 do if FObjects[I].CanSerialize then
    AContext.WriteObject(FObjects[I]);
end;

procedure TSerializableCollection.DeserializeExtraData(AContext: TSerializationContext);
var
  I: Integer;
begin
  inherited DeserializeExtraData(AContext);
  // Clear any existing objects
  Clear;
  // Setup object storage
  FCount:=AContext.ReadInt32;
  SetLength(FObjects, FCount);
  // Read each object
  for I:=0 to FCount - 1 do FObjects[I]:=T(AContext.ReadObject);
end;

procedure TSerializableCollection.Clear;
begin
  FCount:=0;
end;

procedure TSerializableCollection.DeleteAllAndClear;
begin
  while FCount > 0 do begin
    Dec(FCount);
    FObjects[FCount].Free;
  end;
end;

procedure TSerializableCollection.DeleteAllAndFree;
begin
  DeleteAllAndClear;
  Free;
end;

procedure TSerializableCollection.Add(Obj: T);
begin
  // Increase capacity if needed
  if FCount=Length(FObjects) then SetLength(FObjects, FCount + FCount div 2 + 1);
  // Store the object
  FObjects[FCount]:=Obj;
  Inc(FCount);
end;

procedure TSerializableCollection.Insert(Index: Integer; Obj: T);
var
  I: Integer;
begin
  // If the index is at the last item just add the object at the end
  if Index=FCount then begin
    Add(Obj);
    Exit;
  end;
  // Increase capacity if needed
  if FCount=Length(FObjects) then SetLength(FObjects, FCount + FCount div 2 + 1);
  // Move all objects to the right
  for I:=FCount downto Index + 1 do FObjects[I]:=FObjects[I - 1];
  // Store the object
  FObjects[Index]:=Obj;
  Inc(FCount);
end;

procedure TSerializableCollection.Remove(Obj: T);
var
  Index: Integer;
begin
  Index:=IndexOf(Obj);
  if Index <> -1 then RemoveAt(Index);
end;

procedure TSerializableCollection.RemoveAt(Index: Integer);
var
  I: Integer;
begin
  // Check index valitity
  if (Index < 0) or (Index >= FCount) then Exit;
  // Move the items to the left
  Dec(FCount);
  for I:=Index to FCount - 1 do FObjects[I]:=FObjects[I + 1];
end;

procedure TSerializableCollection.Delete(Obj: T);
begin
  Remove(Obj);
  Obj.Free;
end;

procedure TSerializableCollection.DeleteAt(Index: Integer);
begin
  // Check index valitity
  if (Index < 0) or (Index >= FCount) then Exit;
  // Delete the object
  FObjects[Index].Free;
  // Remove it too
  DeleteAt(Index);
end;

procedure TSerializableCollection.Swap(Index1, Index2: Integer);
var
  Tmp: T;
begin
  Tmp:=FObjects[Index1];
  FObjects[Index1]:=FObjects[Index2];
  FObjects[Index2]:=Tmp;
end;

function TSerializableCollection.IndexOf(Obj: T): Integer;
var
  I: Integer;
begin
  for I:=0 to High(FObjects) do
    if FObjects[I]=Obj then Exit(I);
  Result:=-1;
end;

function TSerializableCollection.IsEmpty: Boolean;
begin
  Result:=FCount=0;
end;

function TSerializableCollection.HasObjects: Boolean;
begin
  Result:=FCount > 0;
end;

end.

