{ Renderer }
unit Renderer;

interface

uses
  GfxDrv, Raster, Maths, Colors, Textures, Cameras, Fonts;

type
  { Pointers }
  PRenderTri = ^TRenderTri;

  { TRenderTri - information about a single triangle to be rendered }
  TRenderTri = record
    // Triangle positions
    A, B, C: TVector;
    // Texture coordinates
    SA, TA, SB, TB, SC, TC: Byte;
    // Light levels
    LA, LB, LC: Byte;
    // Depth bias
    DepthBias: Byte;
    // Texture
    Texture: TTexture;
    {$IFNDEF PETRAHW}
    // Sorting Z (used by the renderer)
    BaseZ, TriZ: Single;
    // True if this triangle is from an occluder
    Occluder: Boolean;
    {$ENDIF}
  end;

  { TRenderer - renderer class }
  TRenderer = class
  private
    // Property storage
    FGraphicsDriver: TGraphicsDriver;
    FRasterizer: TRasterizer;
    FDefaultCamera: TCamera;
    FCamera: TCamera;
    FFrameTriangles: Integer;
    FColorPalette: TColorPalette;
    procedure SetCamera(ACamera: TCamera); inline;
  private
    // Triangles to render
    RenderTri: array of {$IFDEF PETRAHW}TRenderTri{$ELSE}PRenderTri{$ENDIF};
    // Number of triangles to render for this frame
    RenderTris: Integer;
    // A (most likely) white texture used to render lines
    WhiteTexture: TTexture;
    // Current (or last) projection and view matrices
    ProjMatrix, ViewMatrix: TMatrix;
    {$IFDEF PETRAHW}
    // Shadow textures for direct pixel access (used for the UI)
    ShadowTexture1: TTexture;
    ShadowTexture2: TTexture;
    // If true, the shadow textures were written to and need to be drawn
    WrittenToShadowTextures: Boolean;
    {$ELSE}
    // Current frame data
    FrameData: PFrameData;
    {$ENDIF}
  public
    // Camera direction vectors, updated in BeginFrame
    CameraLeft, CameraRight: TVector;
    // Camera left/right/top/bottom/near/far planes, updated in BeginFrame
    CameraPlanes: array [0..5] of TPlane;
    // Construct a new renderer using the given graphics driver
    constructor Create(AGraphicsDriver: TGraphicsDriver);
    // Destructor
    destructor Destroy; override;
    // Start a new frame, returns false if this is not possible at the moment
    function BeginFrame: Boolean;
    // Finish the frame
    procedure FinishFrame;

    // Allocate a render triangle and return a pointer to it
    function AllocRenderTriangle: PRenderTri;
    // Flush the render triangles so far (done automatically when needed
    // this only makes sense to call directly when also using Rasterizer)
    procedure FlushRenderTriangles;

    // Render a line using triangles
    procedure RenderTriLine(const VA, VB: TVector);
    // Render an axis aligned box using triangles
    procedure RenderAABox(const AABox: TAABox);

    // Set a pixel at the given coordinates and color
    procedure SetPixel(X, Y: Integer; AColor: Byte);
    // Draw a rectangle at the given coordinates using the given color
    procedure DrawRectangle(X1, Y1, X2, Y2: Integer; AColor: Byte);
    // Fill a rectangle at the given coordinates using the given color
    procedure FillRectangle(X1, Y1, X2, Y2: Integer; AColor: Byte);
    // Shade a rectangle at the given coordinates
    procedure ShadeRectangle(X1, Y1, X2, Y2: Integer);
    // Draw part of the given texture on screen for HUD display using index 255
    // as a transparent color.  The part is specified by the SX,SY top left
    // corner in the texture and its size is SW by SH.
    procedure DrawHUDImage(X, Y: Integer; Texture: TTexture; SX, SY, SW, SH: Integer); inline;
    // Draw the given text using the given font and color
    procedure DrawText(X, Y: Integer; AText: string; AFont: TBitFont; AColor: Byte);
    // Draw the given text using the given font, color and shadow
    procedure DrawText(X, Y: Integer; AText: string; AFont: TBitFont; AColor, AShadow: Byte);
    // Return the size of the given text for the given font
    procedure TextSize(AText: string; AFont: TBitFont; out AWidth, AHeight: Integer);

    {$IFDEF PETRAHW}
    // Draw the shadow textures if needed
    procedure DrawShadowTextures;
    {$ENDIF}

    // Check if the given box is visible with the last frame camera settings
    function IsBoxVisible(const ABox: TAABox): Boolean;
    // Check if the given box would not be occluded during the previous frame
    function BoxWasNotOccluded(const ABox: TAABox): Boolean;

  public
    // The graphics driver used for rendering
    property GraphicsDriver: TGraphicsDriver read FGraphicsDriver;
    // The rasterizer used for rendering
    property Rasterizer: TRasterizer read FRasterizer;
    // The default camera
    property DefaultCamera: TCamera read FDefaultCamera;
    // The active camera (if set to nil it will use the default camera)
    property Camera: TCamera read FCamera write SetCamera;
    // Number of triangles drawn so far in the frame
    property FrameTriangles: Integer read FFrameTriangles;
    // Current color palette
    property ColorPalette: TColorPalette read FColorPalette;
    {$IFNDEF PETRAHW}
    // Raw frame data
    property RawFrameData: PFrameData read FrameData;
    {$ENDIF}
  end;

{$IFDEF SPEEDSTATS}
var
  FlushPre, FlushPost, DrawPre, DrawPost, SortPre, SortPost, TransPre, TransPost: Integer;
{$ENDIF}
{$IFDEF EDITOR}
type
  TEditorQuad = record
    A, B, C, D: TVector;
    Texture: TTexture;
  end;

var
  GEditorQuads: array of TEditorQuad;
  GEditorRects: array of record X1, Y1, X2, Y2, C: Integer; end;
procedure AddEditorQuad(const A, B, C, D: TVector; ATexture: TTexture);
procedure AddEditorRect(X1, Y1, X2, Y2: Integer; C: Byte=13);
{$ENDIF}

implementation

uses
  Engine;

{$IFDEF EDITOR}
procedure AddEditorQuad(const A, B, C, D: TVector; ATexture: TTexture);
var
  EQ: TEditorQuad;
begin
  EQ.A:=A;
  EQ.B:=B;
  EQ.C:=C;
  EQ.D:=D;
  EQ.Texture:=ATexture;
  SetLength(GEditorQuads, Length(GEditorQuads) + 1);
  GEditorQuads[High(GEditorQuads)]:=EQ;
end;

procedure AddEditorRect(X1, Y1, X2, Y2: Integer; C: Byte);
begin
  SetLength(GEditorRects, Length(GEditorRects) + 1);
  GEditorRects[High(GEditorRects)].X1:=X1;
  GEditorRects[High(GEditorRects)].Y1:=Y1;
  GEditorRects[High(GEditorRects)].X2:=X2;
  GEditorRects[High(GEditorRects)].Y2:=Y2;
  GEditorRects[High(GEditorRects)].C:=C;
end;
{$ENDIF}

{ TRenderer }
constructor TRenderer.Create(AGraphicsDriver: TGraphicsDriver);
var
  I: Integer;
begin
  // Store the graphics driver and rasterizer
  FGraphicsDriver:=AGraphicsDriver;
  FRasterizer:=GraphicsDriver.GetRasterizer;
  // Create the default camera
  FDefaultCamera:=TCamera.Create;
  FCamera:=FDefaultCamera;
  // Create the default color palette
  FColorPalette:=TColorPalette.Create;
  for I:=0 to 255 do with FColorPalette do begin
    Red[I]:=I;
    Green[I]:=I;
    Blue[I]:=I;
  end;
  // Prepare the render triangle storage
  SetLength(RenderTri, 1024);
  {$IFNDEF PETRAHW}
  for I:=0 to High(RenderTri) do RenderTri[I]:=New(PRenderTri);
  {$ELSE}
  // Create shadow textures
  ShadowTexture1:=TTexture.Create(256, 256, tfNearest);
  ShadowTexture2:=TTexture.Create(256, 256, tfNearest);
  {$ENDIF}
end;

destructor TRenderer.Destroy;
{$IFNDEF PETRAHW}
var
  I: Integer;
{$ENDIF}
begin
  {$IFNDEF PETRAHW}
  for I:=0 to High(RenderTri) do Dispose(RenderTri[I]);
  {$ENDIF}
  FDefaultCamera.Free;
  FColorPalette.Free;
  WhiteTexture.Free;
  {$IFDEF PETRAHW}
  ShadowTexture1.Free;
  ShadowTexture2.Free;
  {$ENDIF}
  inherited Destroy;
end;

procedure TRenderer.SetCamera(ACamera: TCamera);
begin
  if Assigned(ACamera) then FCamera:=ACamera else FCamera:=FDefaultCamera;
end;

function TRenderer.BeginFrame: Boolean;
var
  Mtx: TMatrix;
begin
  Result:=False;
  // Prepare editor drawing
  {$IFDEF EDITOR}
  if GShowPortals or GShowOcclusionTests then GEditorRects:=nil;
  {$ENDIF}
  // Prepare camera info
  CameraLeft:=Camera.Direction;
  CameraRight:=Camera.Direction;
  Mtx.Rotation(0, 1, 0, 1.5708);
  Mtx.TransformWithoutTranslation(CameraLeft);
  Mtx.Rotation(0, 1, 0, -1.5708);
  Mtx.TransformWithoutTranslation(CameraRight);
  Camera.CalcPlanes(CameraPlanes[0], CameraPlanes[1], CameraPlanes[2],
    CameraPlanes[3], CameraPlanes[4], CameraPlanes[5]);
  // Prepare the graphics driver
  {$IFDEF PETRAHW}
  if not GraphicsDriver.BeginFrame then Exit;
  {$ELSE}
  FrameData:=GraphicsDriver.BeginFrame;
  if not Assigned(FrameData) then Exit;
  {$ENDIF}
  // Prepare the rasterizer
  {$IFDEF PETRAHW}
  if not Rasterizer.BeginFrame then begin
  {$ELSE}
  if not Rasterizer.BeginFrame(ColorPalette, FrameData) then begin
  {$ENDIF}
    GraphicsDriver.FinishFrame;
    Exit;
  end;
  // Reset the render triangle count
  RenderTris:=0;
  // Reset the triangle counter
  FFrameTriangles:=0;
  {$IFDEF PETRAHW}
  // Prepare shadow textures
  WrittenToShadowTextures:=False;
  {$ENDIF}
  // Done
  Result:=True;
end;

procedure TRenderer.FinishFrame;
var
  I: Integer;
begin
  // Flush any render triangles that may be sent
  FlushRenderTriangles;
  // Draw editor rects
  {$IFDEF EDITOR}
  for I:=0 to High(GEditorRects) do with GEditorRects[I] do
    DrawRectangle(X1, Y1, X2, Y2, C);
  {$ENDIF}
  // Finish the frame for the rasterizer
  Rasterizer.FinishFrame;
end;

function TRenderer.AllocRenderTriangle: PRenderTri;
{$IFNDEF PETRAHW}
var
  I: Integer;
{$ENDIF}
begin
  // Increase storage if necessary
  if RenderTris=Length(RenderTri) then begin
    {$IFNDEF PETRAHW}I:=Length(RenderTri);{$ENDIF}
    SetLength(RenderTri, Length(RenderTri) + 256);
    {$IFNDEF PETRAHW}
    while I < Length(RenderTri) do begin
      RenderTri[I]:=New(PRenderTri);
      Inc(I);
    end;
    {$ENDIF}
  end;
  {$IFDEF PETRAHW}
  Result:=@RenderTri[RenderTris];
  {$ELSE}
  Result:=RenderTri[RenderTris];
  Result^.Occluder:=False;
  {$ENDIF}
  Inc(RenderTris);
end;

procedure TRenderer.FlushRenderTriangles;
var
  TmpRI: PRenderTri;
  TintR, TintG, TintB: Byte;
{$IFNDEF PETRAHW}
  RA, RB, RC: TRasterVertex;
  PAZ, PBZ, PCZ: Single;
  I, J: Integer;
  FogRange, FogRangeInv: Single;
{$ENDIF}

  {$IFNDEF PETRAHW}
  procedure QuicksortTris(Lo, Hi: Integer);
    function Partition(Lo, Hi: Integer): Integer;
    var
      I, J: Integer;
      Tmp: PRenderTri;
      Pivot: Single;
    begin
      Pivot:=RenderTri[(Hi + Lo) div 2]^.TriZ;
      I:=Lo - 1;
      J:=Hi + 1;
      while True do begin
        repeat
          Inc(I);
        until RenderTri[I]^.TriZ >= Pivot;
        repeat
          Dec(J);
        until RenderTri[J]^.TriZ <= Pivot;
        if I >= J then Exit(J);
        Tmp:=RenderTri[I];
        RenderTri[I]:=RenderTri[J];
        RenderTri[J]:=Tmp;
      end;
    end;
  var
    P: Integer;
  begin
    if Lo < Hi then begin
      P:=Partition(Lo, Hi);
      QuicksortTris(Lo, P);
      QuicksortTris(P + 1, Hi);
    end;
  end;
  {$ENDIF}

begin
  {$IFDEF EDITOR}
  for I:=0 to High(GEditorQuads) do with GEditorQuads[I] do begin
    TmpRI:=AllocRenderTriangle;
    TmpRI^.A:=A;
    TmpRI^.B:=B;
    TmpRI^.C:=C;
    TmpRI^.LA:=255;
    TmpRI^.LB:=255;
    TmpRI^.LC:=255;
    TmpRI^.SA:=0;
    TmpRI^.TA:=0;
    TmpRI^.SB:=0;
    TmpRI^.TB:=63;
    TmpRI^.SC:=63;
    TmpRI^.TC:=63;
    TmpRI^.DepthBias:=32;
    TmpRI^.Texture:=Texture;
    TmpRI:=AllocRenderTriangle;
    TmpRI^.A:=A;
    TmpRI^.B:=C;
    TmpRI^.C:=D;
    TmpRI^.LA:=255;
    TmpRI^.LB:=255;
    TmpRI^.LC:=255;
    TmpRI^.SA:=0;
    TmpRI^.TA:=0;
    TmpRI^.SB:=63;
    TmpRI^.TB:=63;
    TmpRI^.SC:=63;
    TmpRI^.TC:=0;
    TmpRI^.DepthBias:=32;
    TmpRI^.Texture:=Texture;
  end;
  {$ENDIF}
  // Ignore the request if there are no triangles to draw
  if RenderTris=0 then Exit;
  {$IFDEF SPEEDSTATS}FlushPre:=GSystem.Milliseconds;{$ENDIF}
  // Configure tinting
  GGame.World.GetTint(TintR, TintG, TintB);
  Rasterizer.SetTint(TintR, TintG, TintB);
  {$IFDEF PETRAHW}
  // Render the triangles
  Rasterizer.SetMatrices(Camera.ProjectionMatrix, Camera.ViewMatrix);
  if Assigned(GGame) and Assigned(GGame.World) then
    // NOTE: for the hardware accelerated version we use a closer starting fog
    // position to have more pronounced fog tint for world since the software
    // rendering version (for which the worlds were designed for) are affected
    // much more (and are darker) than the truecolor HW version
    Rasterizer.SetFog(GGame.World.FogRed/255, GGame.World.FogGreen/255, GGame.World.FogBlue/255,
      Camera.NearPlane, Camera.FarPlane)
  else
    Rasterizer.SetFog(0, 0, 0, Camera.NearPlane + (Camera.FarPlane - Camera.NearPlane)/2, Camera.FarPlane);
  Rasterizer.RenderTriangles(RenderTris, RenderTri[0]);
  {$ELSE}
  // Calculate the projection and view matrices from the active camera
  ProjMatrix:=Camera.ProjectionMatrix;
  ViewMatrix:=Camera.ViewMatrix;
  // Calculate fog range
  FogRange:=Camera.NearPlane + (Camera.FarPlane - Camera.NearPlane)/2;
  FogRangeInv:=1/FogRange;
  // Transform the triangles and collect the triangle indices to draw
  I:=0;
  {$IFDEF SPEEDSTATS}TransPre:=GSystem.Milliseconds;{$ENDIF}
  while I < RenderTris do with RenderTri[I]^ do begin
    // Transform from world space to camera space
    ViewMatrix.Transform(A);
    ViewMatrix.Transform(B);
    ViewMatrix.Transform(C);
    // Clamp the vertices to near plane and count how many vertices are behind
    J:=0;
    if A.Z < Camera.NearPlane then begin A.Z:=Camera.NearPlane; Inc(J); end;
    if B.Z < Camera.NearPlane then begin B.Z:=Camera.NearPlane; Inc(J); end;
    if C.Z < Camera.NearPlane then begin C.Z:=Camera.NearPlane; Inc(J); end;
    // If the entire triangle is behind the near plane, ignore it
    if J=3 then begin
      TmpRI:=RenderTri[I];
      RenderTri[I]:=RenderTri[RenderTris - 1];
      RenderTri[RenderTris - 1]:=TmpRI;
      Dec(RenderTris);
      Continue;
    end;
    // Save the Z values before projection
    PAZ:=A.Z;
    PBZ:=B.Z;
    PCZ:=C.Z;
    // Transform from camera space to screen space
    ProjMatrix.TransformProj(A);
    ProjMatrix.TransformProj(B);
    ProjMatrix.TransformProj(C);
    // Ignore backfacing triangles
    if (C.X-A.X)*(C.Y-B.Y) - (C.X-B.X)*(C.Y-A.Y) > 0 then begin
      TmpRI:=RenderTri[I];
      RenderTri[I]:=RenderTri[RenderTris - 1];
      RenderTri[RenderTris - 1]:=TmpRI;
      Dec(RenderTris);
      Continue;
    end;
    // Ignore triangles completely outside the screen
    if ((A.X < -1) and (B.X < -1) and (C.X < -1)) or
       ((A.Y < -1) and (B.Y < -1) and (C.Y < -1)) or
       ((A.X > 1) and (B.X > 1) and (C.X > 1)) or
       ((A.Y > 1) and (B.Y > 1) and (C.Y > 1)) then begin
      TmpRI:=RenderTri[I];
      RenderTri[I]:=RenderTri[RenderTris - 1];
      RenderTri[RenderTris - 1]:=TmpRI;
      Dec(RenderTris);
      Continue;
    end;
    BaseZ:=(PAZ + PBZ + PCZ)*0.33;
    // Set TriZ to the value to use for sorting
    if PAZ > 5 then
      TriZ:=A.Z+B.Z+C.Z+DepthBias*0.001
    else
      TriZ:=A.Z+B.Z+C.Z+DepthBias*0.01;
    // Use the Z values before projection for fog
    LA:=Round(LA*(1-Clamp(PAZ - FogRange, 0, FogRange)*FogRangeInv));
    LB:=Round(LB*(1-Clamp(PBZ - FogRange, 0, FogRange)*FogRangeInv));
    LC:=Round(LC*(1-Clamp(PCZ - FogRange, 0, FogRange)*FogRangeInv));
    // This triangle will be drawn
    Inc(I);
  end;
  {$IFDEF SPEEDSTATS}TransPost:=GSystem.Milliseconds;{$ENDIF}
  // Sort the triangles
  {$IFDEF SPEEDSTATS}SortPre:=GSystem.Milliseconds;{$ENDIF}
  QuicksortTris(0, RenderTris - 1);
  {$IFDEF SPEEDSTATS}SortPost:=GSystem.Milliseconds;{$ENDIF}
  // Draw the triangles
  {$IFDEF SPEEDSTATS}DrawPre:=GSystem.Milliseconds;{$ENDIF}
  for I:=0 to RenderTris - 1 do with RenderTri[I]^ do begin
    RA.X:=Trunc((HalfHRes + A.X*HalfHRes)*4096);
    RA.Y:=Trunc(HalfVRes + A.Y*HalfVRes);
    RA.S:=SA;
    RA.T:=TA;
    RA.L:=LA;
    RB.X:=Trunc((HalfHRes + B.X*HalfHRes)*4096);
    RB.Y:=Trunc(HalfVRes + B.Y*HalfVRes);
    RB.S:=SB;
    RB.T:=TB;
    RB.L:=LB;
    RC.X:=Trunc((HalfHRes + C.X*HalfHRes)*4096);
    RC.Y:=Trunc(HalfVRes + C.Y*HalfVRes);
    RC.S:=SC;
    RC.T:=TC;
    RC.L:=LC;
    Rasterizer.SetTexture(Texture);
    Rasterizer.DrawTriangle(@RA, @RB, @RC, Occluder, BaseZ);
  end;
  {$IFDEF SPEEDSTATS}DrawPost:=GSystem.Milliseconds;{$ENDIF}
  {$ENDIF}
  // Update the frame triangle count
  Inc(FFrameTriangles, RenderTris);
  // Reset render triangle count
  RenderTris:=0;
  {$IFDEF SPEEDSTATS}FlushPost:=GSystem.Milliseconds;{$ENDIF}
end;

procedure TRenderer.RenderTriLine(const VA, VB: TVector);
var
  Tri: PRenderTri;
  OfsX, OfsY: TVector;
  Mtx: TMatrix;
begin
  {$IFDEF EDITOR}
  if GDisableTriLines then Exit;
  {$ENDIF}
  if not Assigned(WhiteTexture) then begin
    WhiteTexture:=TTexture.Create(1, 1);
    WhiteTexture.Texels[0, 0]:=255;
  end;
  Mtx:=Camera.ViewMatrix.Inverted;
  OfsX:=Mtx.XAxis.Scaled(0.03);
  OfsY:=Mtx.YAxis.Scaled(-0.03);
  Tri:=AllocRenderTriangle;
  with Tri^ do begin
    A:=VA;
    B:=VB;
    C:=VA.Added(OfsX).Added(OfsY);
    SA:=0;
    TA:=0;
    SB:=0;
    TB:=0;
    SC:=0;
    TC:=0;
    LA:=255;
    LB:=255;
    LC:=255;
    DepthBias:=20;
    Texture:=WhiteTexture;
  end;
  Tri:=AllocRenderTriangle;
  with Tri^ do begin
    A:=VB;
    B:=VB.Added(OfsX);
    C:=VA.Added(OfsX).Added(OfsY);
    SA:=0;
    TA:=0;
    SB:=0;
    TB:=0;
    SC:=0;
    TC:=0;
    LA:=255;
    LB:=255;
    LC:=255;
    DepthBias:=20;
    Texture:=WhiteTexture;
  end;
end;

procedure TRenderer.RenderAABox(const AABox: TAABox);
var
  MinA, MinB, MinC, MinD, MaxA, MaxB, MaxC, MaxD: TVector;
begin
  AABox.GetCorners(MinA, MinB, MinC, MinD, MaxA, MaxB, MaxC, MaxD);
  RenderTriLine(MinA, MinB);
  RenderTriLine(MinC, MinD);
  RenderTriLine(MinA, MinC);
  RenderTriLine(MinB, MinD);
  RenderTriLine(MaxA, MaxB);
  RenderTriLine(MaxC, MaxD);
  RenderTriLine(MaxA, MaxC);
  RenderTriLine(MaxB, MaxD);
  RenderTriLine(MinA, MaxA);
  RenderTriLine(MinC, MaxC);
  RenderTriLine(MinB, MaxB);
  RenderTriLine(MinD, MaxD);
end;

procedure TRenderer.SetPixel(X, Y: Integer; AColor: Byte);
begin
  {$IFDEF PETRAHW}
  if (X >= 0) and (Y >= 0) and (X < 320) and (Y < 200) then begin
    // First texture
    if X < 256 then
      ShadowTexture1.Texels[X, Y]:=AColor
    else // Second texture
      ShadowTexture2.Texels[X - 256, Y]:=AColor;
    // Shadow textures were written to
    WrittenToShadowTextures:=True;
  end;
  {$ELSE}
  if (X >= 0) and (Y >= 0) and (X < HRes) and (Y < VRes) then
    PByte(FrameData)[Y*HRes + X]:=AColor;
  {$ENDIF}
end;

procedure TRenderer.DrawRectangle(X1, Y1, X2, Y2: Integer; AColor: Byte);
var
  I: Integer;
begin
  // Ensure X1 < X2
  if X1 > X2 then begin
    I:=X1;
    X1:=X2;
    X2:=I;
  end;
  // Ensure Y1 < Y2
  if Y1 > Y2 then begin
    I:=Y1;
    Y1:=Y2;
    Y2:=I;
  end;
  // Ignore if the coordinates are outside the screen
  if (X1 < 0) and (Y1 < 0) and (X2 >= HRes) and (Y2 >= VRes) then Exit;
  if (X2 < 0) or (Y2 < 0) or (X1 >= HRes) or (Y1 >= VRes) then Exit;
  // Draw upper line
  if (Y1 >= 0) and (Y1 < VRes) then for I:=X1 to X2 do SetPixel(I, Y1, AColor);
  // Draw lower line
  if (Y2 >= 0) and (Y2 < VRes) then for I:=X1 to X2 do SetPixel(I, Y2, AColor);
  // Draw left line
  if (X1 >= 0) and (X1 < HRes) then for I:=Y1 + 1 to Y2 - 1 do SetPixel(X1, I, AColor);
  // Draw right line
  if (X2 >= 0) and (X2 < HRes) then for I:=Y1 + 1 to Y2 - 1 do SetPixel(X2, I, AColor);
end;

procedure TRenderer.FillRectangle(X1, Y1, X2, Y2: Integer; AColor: Byte);
var
  I: Integer;
  {$IFDEF PETRAHW}
  J: Integer;
  {$ENDIF}
begin
  // Ensure X1 < X2
  if X1 > X2 then begin
    I:=X1;
    X1:=X2;
    X2:=I;
  end;
  // Ensure Y1 < Y2
  if Y1 > Y2 then begin
    I:=Y1;
    Y1:=Y2;
    Y2:=I;
  end;
  // Ignore if the coordinates are outside the screen
  if (X2 < 0) or (Y2 < 0) or (X1 >= HRes) or (Y1 >= VRes) then Exit;
  // Clamp to screen area
  if X1 < 0 then X1:=0;
  if Y1 < 0 then Y1:=0;
  if X2 >= HRes then X2:=HRes - 1;
  if Y2 >= VRes then Y2:=VRes - 1;
  // Fill the area
  {$IFDEF PETRAHW}
  for J:=Y1 to Y2 do
    for I:=X1 to X2 do
      SetPixel(I, J, AColor);
  {$ELSE}
  for I:=Y1 to Y2 do
    FillChar(PByte(FrameData)[I*HRes + X1], X2 - X1 + 1, AColor);
  {$ENDIF}
end;

procedure TRenderer.ShadeRectangle(X1, Y1, X2, Y2: Integer);
var
  I: Integer;
  {$IFNDEF PETRAHW}
  J: Integer;
  {$ENDIF}
begin
  // Ensure X1 < X2
  if X1 > X2 then begin
    I:=X1;
    X1:=X2;
    X2:=I;
  end;
  // Ensure Y1 < Y2
  if Y1 > Y2 then begin
    I:=Y1;
    Y1:=Y2;
    Y2:=I;
  end;
  // Ignore if the coordinates are outside the screen
  if (X2 < 0) or (Y2 < 0) or (X1 >= HRes) or (Y1 >= VRes) then Exit;
  // Clamp to screen area
  if X1 < 0 then X1:=0;
  if Y1 < 0 then Y1:=0;
  if X2 >= HRes then X2:=HRes - 1;
  if Y2 >= VRes then Y2:=VRes - 1;
  // Shade the area
  {$IFNDEF PETRAHW}
  for I:=Y1 to Y2 do
    for J:=X1 to X2 do
      PByte(FrameData)[I*HRes + J]:=ColorPalette.LightTable[130, PByte(FrameData)[I*HRes + J]];
  {$ENDIF}
end;

procedure TRenderer.DrawHUDImage(X, Y: Integer; Texture: TTexture; SX, SY, SW, SH: Integer);
begin
  Rasterizer.DrawHUDImage(X, Y, Texture, SX, SY, SW, SH);
end;

procedure TRenderer.DrawText(X, Y: Integer; AText: string; AFont: TBitFont;
  AColor: Byte);
var
  BS: PByte; // Font bitstream
  I: Integer;

  // Draw the given character at the given coordinates
  procedure DrawChar(Ch: Char; X, Y: Integer);
  var
    PX, PY, BC: Integer;
  begin
    with AFont.Chars[Byte(Ch)] do begin
      BC:=Bit;
      for PY:=0 to AFont.Height - 1 do
        for PX:=0 to Width - 1 do begin
          if (BS[BC shr 3] and (1 shl (BC and 7))) <> 0 then
            SetPixel(X + PX, Y + PY, AColor);
          Inc(BC);
        end;
    end;
  end;

begin
  BS:=AFont.BitStream;
  for I:=1 to Length(AText) do begin
    DrawChar(AText[I], X, Y);
    Inc(X, AFont.Chars[Byte(AText[I])].Width);
  end;
end;

procedure TRenderer.DrawText(X, Y: Integer; AText: string; AFont: TBitFont;
  AColor, AShadow: Byte);
begin
  DrawText(X + 1, Y + 1, AText, AFont, AShadow);
  DrawText(X, Y, AText, AFont, AColor);
end;

procedure TRenderer.TextSize(AText: string; AFont: TBitFont; out AWidth,
  AHeight: Integer);
var
  I: Integer;
begin
  AWidth:=0;
  for I:=1 to Length(AText) do
    AWidth += AFont.Chars[Ord(AText[I])].Width;
  AHeight:=AFont.Height;
end;

function TRenderer.IsBoxVisible(const ABox: TAABox): Boolean;
var
  I: Integer;
  A, B, C, D, E, F, G, H: TVector;
begin
  // Ignore the box if it is too far
  if ABox.DistanceSq(Camera.Position) > Sqr(Camera.FarPlane) then
    Exit(False);
  // Check if the box is inside the view frustum
  ABox.GetCorners(A, B, C, D, E, F, G, H);
  for I:=0 to 3 do begin
    if CameraPlanes[I].Front(A) then Continue;
    if CameraPlanes[I].Front(B) then Continue;
    if CameraPlanes[I].Front(C) then Continue;
    if CameraPlanes[I].Front(D) then Continue;
    if CameraPlanes[I].Front(E) then Continue;
    if CameraPlanes[I].Front(F) then Continue;
    if CameraPlanes[I].Front(G) then Continue;
    if CameraPlanes[I].Front(H) then Continue;
    Exit(False);
  end;
  // The box is visible, at least as far as the camera is concerned
  Result:=True;
end;

function TRenderer.BoxWasNotOccluded(const ABox: TAABox): Boolean;
var
  A, B, C, D, E, F, G, H: TVector;
  XMin, YMin, XMax, YMax: Integer;
  V, Z: Single;
begin
  {$IFDEF PETRAHW}
  Result:=True;
  {$ELSE}
  ABox.GetCorners(A, B, C, D, E, F, G, H);
  ViewMatrix.Transform(A);
  Z:=A.Z;
  ProjMatrix.TransformProj(A);
  ViewMatrix.Transform(B);
  if B.Z < Z then Z:=B.Z;
  ProjMatrix.TransformProj(B);
  ViewMatrix.Transform(C);
  if C.Z < Z then Z:=C.Z;
  ProjMatrix.TransformProj(C);
  ViewMatrix.Transform(D);
  if D.Z < Z then Z:=D.Z;
  ProjMatrix.TransformProj(D);
  ViewMatrix.Transform(E);
  if E.Z < Z then Z:=E.Z;
  ProjMatrix.TransformProj(E);
  ViewMatrix.Transform(F);
  if F.Z < Z then Z:=F.Z;
  ProjMatrix.TransformProj(F);
  ViewMatrix.Transform(G);
  if G.Z < Z then Z:=G.Z;
  ProjMatrix.TransformProj(G);
  ViewMatrix.Transform(H);
  if H.Z < Z then Z:=H.Z;
  if Z < Camera.NearPlane then Exit(True);
  ProjMatrix.TransformProj(H);
  V:=A.X;
  if B.X < V then V:=B.X;
  if C.X < V then V:=C.X;
  if D.X < V then V:=D.X;
  if E.X < V then V:=E.X;
  if F.X < V then V:=F.X;
  if G.X < V then V:=G.X;
  if H.X < V then V:=H.X;
  if V < -1 then V:=-1 else if V > 1 then Exit(False);
  XMin:=Trunc(HalfHRes + (HalfHRes - 1)*V);
  V:=A.Y;
  if B.Y < V then V:=B.Y;
  if C.Y < V then V:=C.Y;
  if D.Y < V then V:=D.Y;
  if E.Y < V then V:=E.Y;
  if F.Y < V then V:=F.Y;
  if G.Y < V then V:=G.Y;
  if H.Y < V then V:=H.Y;
  if V < -1 then V:=-1 else if V > 1 then Exit(False);
  YMin:=Trunc(HalfVRes + (HalfVRes - 1)*V);
  V:=A.X;
  if B.X > V then V:=B.X;
  if C.X > V then V:=C.X;
  if D.X > V then V:=D.X;
  if E.X > V then V:=E.X;
  if F.X > V then V:=F.X;
  if G.X > V then V:=G.X;
  if H.X > V then V:=H.X;
  if V > 1 then V:=1 else if V < -1 then Exit(False);
  XMax:=Trunc(HalfHRes + (HalfHRes - 1)*V);
  V:=A.Y;
  if B.Y > V then V:=B.Y;
  if C.Y > V then V:=C.Y;
  if D.Y > V then V:=D.Y;
  if E.Y > V then V:=E.Y;
  if F.Y > V then V:=F.Y;
  if G.Y > V then V:=G.Y;
  if H.Y > V then V:=H.Y;
  if V > 1 then V:=1 else if V < -1 then Exit(False);
  YMax:=Trunc(HalfVRes + (HalfVRes - 1)*V);
  if Rasterizer.WasRectOccluded(XMin, YMin, XMax, YMax, Z - 30*0.01) then begin
    {$IFDEF EDITOR}
    if GShowOcclusionTests then AddEditorRect(XMin, YMin, XMax, YMax, 2);
    {$ENDIF}
    Exit(False);
  end{$IFDEF EDITOR} else if GShowOcclusionTests then
    AddEditorRect(XMin, YMin, XMax, YMax, 3){$ENDIF};
  Result:=True;
  {$ENDIF}
end;


{$IFDEF PETRAHW}
procedure TRenderer.DrawShadowTextures;
var
  Blend: Single;
begin
  if not WrittenToShadowTextures then Exit;
  // HACK: Use blend factor depending on if the current screen renders the
  // 3D scene or not
  if GScreenManager.FullScreen then Blend:=1 else Blend:=0.75;
  // Draw and clear left texture
  ShadowTexture1.UpdateHWTexture;
  Rasterizer.RenderQuad(0, 0, 0, 0, Round(RealHRes*0.8), RealVRes, 255, 200, Blend, ShadowTexture1);
  ShadowTexture1.Clear;
  // Draw and clear right texture
  ShadowTexture2.UpdateHWTexture;
  Rasterizer.RenderQuad(Round(RealHRes*0.8), 0, 0, 0, RealHRes, RealVRes, 64, 200, Blend, ShadowTexture2);
  ShadowTexture2.Clear;
end;
{$ENDIF}

end.
