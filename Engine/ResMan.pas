{ Resource manager }
unit ResMan;

interface

uses
  Classes, SysUtils, FGL;

type

  { TResource - Base class for resources }
  TResource = class
  private
    // Property storage
    FName: string;
  public
    // The name of the resource
    property Name: string read FName;
  end;

  { TResourceList - List of resources }
  TResourceList = specialize TFPGList<TResource>;

  { TResourceManager - Resource manager }
  TResourceManager = class
  private
    // Property storage
    FLoadedResources: TResourceList;
    FErrorTexture: TResource;
    FBoxMesh: TResource;
    FDefaultFont: TResource;
    function GetLoadedResources(AIndex: Integer): TResource; inline;
    function GetLoadedResourceCount: Integer; inline;
    function GetWallTextures(AIndex: Integer): TResource;
  private
    // Loaded wall textures
    FWallTextures: array [0..255] of TResource;
    // Animated textures
    FAnimatedTextures: TResourceList;
    // Texture animation tick (causes a frame flip every 10 updates)
    FTexAnimTick: Integer;
  public
    constructor Create;
    destructor Destroy; override;
    // Update the resources (animate textures, etc)
    procedure Update;
    // Return (and potentially, load) the resource with the given name
    function GetResource(AName: string): TResource;
    // Unload the given resource
    procedure UnloadResource(AResource: TResource);
    // Unload all resources
    procedure UnloadAllResources;
    // Access loaded resources
    property LoadedResources[AIndex: Integer]: TResource read GetLoadedResources;
    // The number of loaded resources
    property LoadedResourceCount: Integer read GetLoadedResourceCount;
    // Error texture resource
    property ErrorTexture: TResource read FErrorTexture;
    // Box mesh
    property BoxMesh: TResource read FBoxMesh;
    // Default font
    property DefaultFont: TResource read FDefaultFont;
    // Wall textures
    property WallTextures[AIndex: Integer]: TResource read GetWallTextures;
  end;

implementation

uses
  DataLoad, Textures, Meshes, PieceMdl, Fonts, Sounds;

{ TResourceManager }
constructor TResourceManager.Create;
begin
  FLoadedResources:=TResourceList.Create;
  FAnimatedTextures:=TResourceList.Create;
  FErrorTexture:=GetResource('error.raw');
  FBoxMesh:=GetResource('box.jtf');
  FDefaultFont:=GetResource('bold.bff');
end;

destructor TResourceManager.Destroy;
begin
  UnloadAllResources;
  FAnimatedTextures.Free;
  FLoadedResources.Free;
  inherited Destroy;
end;

procedure TResourceManager.Update;
var
  I: Integer;
begin
  FTexAnimTick:=(FTexAnimTick + 1) mod 10;
  if FTexAnimTick=0 then begin
    for I:=0 to FAnimatedTextures.Count - 1 do
      with TTexture(FAnimatedTextures[I]) do begin
        Frame:=(Frame + 1) mod FrameCount;
      end;
  end;
end;

function TResourceManager.GetResource(AName: string): TResource;
var
  I: Integer;
  Texture: TTexture;
  Mesh: TMesh;
  Model: TPieceModel;
  Ext: String;
  Font: TBitFont;
  Sound: TSoundClip;
  LowerName: string;
begin
  // Check if the resource has been loaded
  LowerName:=LowerCase(AName);
  for I:=FLoadedResources.Count - 1 downto 0 do
    if FLoadedResources[I].Name=LowerName then Exit(FLoadedResources[I]);
  // Try to load a texture
  Ext:=ExtractFileExt(AName);
  if Ext='.raw' then begin
    if not HasDataFile(DataDirectoryPrefix + AName) then Exit(ErrorTexture);
    Texture:=TTexture.Create(64, 64);
    Texture.FName:=LowerName;
    Texture.LoadRAWFile(DataDirectoryPrefix + AName);
    FLoadedResources.Add(Texture);
    // If this is an animated texture, add it to the animated textures list
    // so that its active frame will be updated
    if Texture.FrameCount > 1 then FAnimatedTextures.Add(Texture);
    Exit(Texture);
  end;
  // Try to load a static mesh
  if Ext='.jtf' then begin
    if not HasDataFile(DataDirectoryPrefix + AName) then Exit(BoxMesh);
    Mesh:=TMesh.Create;
    Mesh.FName:=LowerName;
    Mesh.Texture:=TTexture(ErrorTexture);
    Mesh.LoadJTFMesh(DataDirectoryPrefix + AName);
    FLoadedResources.Add(Mesh);
    Exit(Mesh);
  end;
  // Try to load a piece model
  if Ext='.pmf' then begin
    Model:=TPieceModel.Create;
    Model.FName:=LowerName;
    Model.LoadPMFModel(DataDirectoryPrefix + AName, DataDirectoryPrefix + ChangeFileExt(AName, '.raw'), 64, 64);
    FLoadedResources.Add(Model);
    Exit(Model);
  end;
  // Try to load a font
  if Ext='.bff' then begin
    Font:=TBitFont.Create;
    Font.FName:=LowerName;
    Font.LoadBitFontFile(DataDirectoryPrefix + AName);
    FLoadedResources.Add(Font);
    Exit(Font);
  end;
  // Try to load a sound clip
  if Ext='.wav' then begin
    Sound:=TSoundClip.Create;
    Sound.FName:=LowerName;
    Sound.LoadWAVFile(DataDirectoryPrefix + AName);
    FLoadedResources.Add(Sound);
    Exit(Sound);
  end;
  // Failed
  Result:=nil;
end;

procedure TResourceManager.UnloadResource(AResource: TResource);
begin
  FAnimatedTextures.Remove(AResource);
  FLoadedResources.Remove(AResource);
end;

procedure TResourceManager.UnloadAllResources;
var
  I: Integer;
begin
  for I:=0 to FLoadedResources.Count - 1 do FLoadedResources[I].Free;
  FAnimatedTextures.Clear;
  FLoadedResources.Clear;
end;

function TResourceManager.GetLoadedResourceCount: Integer;
begin
  Result:=FLoadedResources.Count;
end;

function TResourceManager.GetWallTextures(AIndex: Integer): TResource;
begin
  if not Assigned(FWallTextures[AIndex]) then
    FWallTextures[AIndex]:=GetResource('Walls/' + IntToStr(AIndex) + '.raw');
  Result:=FWallTextures[AIndex];
end;

function TResourceManager.GetLoadedResources(AIndex: Integer): TResource;
begin
  Result:=FLoadedResources[AIndex];
end;

end.

