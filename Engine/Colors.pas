{ Colors and palettes }
unit Colors;
{$WARN 5057 off : Local variable "$1" does not seem to be initialized}
interface

// Disable palette caching
{ $DEFINE NOPALETTECACHE}

// Some platforms can only read an existing cache from the Data directory
{$IFDEF ANDROID}
{$DEFINE READONLYCACHE}
{$ENDIF}
{$IFDEF PETRAGCW}
{$DEFINE READONLYCACHE}
{$ENDIF}

uses
  Maths;

type
  { TColorPalette - represents a 8bit color palette }
  TColorPalette = class
    // RGB values for each index of the palette
    Red, Green, Blue: array [0..255] of Byte;
    // Light table (Light, Index)
    LightTable: array [0..255, 0..255] of Byte;
    // Load the color palette from the given JASC PAL file
    procedure LoadJASCPalette(FileName: string);
    // Return the closest color to the given RGB values
    function GetClosestColor(R, G, B: Byte): Byte;
    // Build the light table from the current values
    procedure BuildLightTable(FogR, FogG, FogB: Byte; Overbright: Single; const Tint: TVector);
    // Build a fullbright light table
    procedure BuildFullbrightLightTable;
  end;

implementation

uses
  SysUtils, Classes, DataLoad, Misc;

{ TColorPalette }
procedure TColorPalette.LoadJASCPalette(FileName: string);
var
  F: TStream;
  H, V, C: string;
  I, R, G, B: Byte;
begin
  F:=GetDataStream(FileName);
  if not Assigned(F) then Exit;
  H:=ReadLineFromStream(F);
  V:=ReadLineFromStream(F);
  C:=ReadLineFromStream(F);
  if (H <> 'JASC-PAL') or (V <> '0100') or (C <> '256') then begin
    F.Free;
    Exit;
  end;
  for I:=0 to 255 do begin
    H:=Trim(ReadLineFromStream(F));
    R:=StrToIntDef(Copy(H, 1, Pos(' ', H) - 1), 0);
    H:=Trim(Copy(H, Pos(' ', H), Length(H)));
    G:=StrToIntDef(Copy(H, 1, Pos(' ', H) - 1), 0);
    H:=Trim(Copy(H, Pos(' ', H), Length(H)));
    B:=StrToIntDef(H, 0);
    Red[I]:=R;
    Green[I]:=G;
    Blue[I]:=B;
  end;
  F.Free;
end;

function TColorPalette.GetClosestColor(R, G, B: Byte): Byte;
var
  D: Integer = MaxInt;
  I, ID: Integer;
begin
  Result:=0;
  for I:=16 to 255 do begin
    ID:=(R - Red[I])*(R - Red[I]) +
        (G - Green[I])*(G - Green[I]) +
        (B - Blue[I])*(B - Blue[I]);
    if ID < D then begin
      Result:=I;
      D:=ID;
    end;
  end;
end;

procedure TColorPalette.BuildLightTable(FogR, FogG, FogB: Byte;
  Overbright: Single; const Tint: TVector);
{$IFNDEF NOPALETTECACHE}
const
  CacheFileName = 'palcache.dat';
{$ENDIF}
var
  L, I: Integer;
  FogColor: TVector;
{$IFNDEF NOPALETTECACHE}
  // Check if the requested values can be found in the cache
  function CheckCache: Boolean;
  var
    F: TStream = nil;
    StoredRGB: array [0..2] of Byte;
    StoredOverbright: Single;
    StoredTable: array [0..255, 0..255] of Byte;
    StoredTint: TVector;
  begin
    // Check if there is a cache available
    Result:=False;
    {$IFDEF READONLYCACHE}
    if not HasDataFile(CacheFileName) then Exit;
    {$ELSE}
    if not FileExists(CacheFileName) then Exit;
    {$ENDIF}
    try
      {$IFDEF READONLYCACHE}
      F:=GetDataStream(CacheFileName);
      {$ELSE}
      F:=TFileStream.Create(CacheFileName, fmOpenRead);
      {$ENDIF}
      while F.Position < F.Size do begin
        StoredOverbright:=0;
        StoredRGB[0]:=0;
        StoredRGB[1]:=0;
        StoredRGB[2]:=0;
        F.ReadBuffer(StoredRGB, 3);
        F.ReadBuffer(StoredOverbright, 4);
        F.ReadBuffer(StoredTint, SizeOf(TVector));
        F.ReadBuffer(StoredTable, 65536);
        if (StoredOverbright=Overbright) and
           (StoredRGB[0]=FogR) and (StoredRGB[1]=FogG) and (StoredRGB[2]=FogB) and
           (StoredTint=Tint) then begin
          LightTable:=StoredTable;
          Result:=True;
          Break;
        end;
      end;
    except
    end;
    F.Free;
  end;

  {$IFNDEF READONLYCACHE}
  // Add the current light table to the cache
  procedure SaveToCache;
  var
    F: TFileStream = nil;
  begin
    try
      if FileExists(CacheFileName) then begin
        F:=TFileStream.Create(CacheFileName, fmOpenWrite);
        F.Position:=F.Size;
      end else
        F:=TFileStream.Create(CacheFileName, fmCreate);
      F.WriteBuffer(FogR, 1);
      F.WriteBuffer(FogG, 1);
      F.WriteBuffer(FogB, 1);
      F.WriteBuffer(Overbright, 4);
      F.WriteBuffer(Tint, SizeOf(TVector));
      F.WriteBuffer(LightTable, 65536);
    finally
      F.Free;
    end;
  end;
  {$ENDIF}
{$ENDIF}

begin
  {$IFNDEF NOPALETTECACHE}
  // Check the cache
  if CheckCache then Exit;
  {$ENDIF}
  // Build the light table
  FogColor:=Vector(FogR/255, FogG/255, FogB/255);
  for L:=0 to 255 do
    for I:=16 to 255 do
      LightTable[L, I]:=GetClosestColor(
        Clamp(Trunc(Round(Overbright*Tint.X*((FogColor.X*(1-L/255) + (Red[I]/255)*(L/255))*255))), 0, 255),
        Clamp(Trunc(Round(Overbright*Tint.Y*((FogColor.Y*(1-L/255) + (Green[I]/255)*(L/255))*255))), 0, 255),
        Clamp(Trunc(Round(Overbright*Tint.Z*((FogColor.Z*(1-L/255) + (Blue[I]/255)*(L/255))*255))), 0, 255));
  // Build the always bright values
  for L:=0 to 255 do
    for I:=0 to 15 do
      LightTable[L, I]:=I;
  {$IFNDEF NOPALETTECACHE}
  {$IFNDEF READONLYCACHE}
  // Save the light table to the cache
  SaveToCache;
  {$ENDIF}
  {$ENDIF}
end;

procedure TColorPalette.BuildFullbrightLightTable;
var
  L, I: Integer;
begin
  for L:=0 to 255 do
    for I:=0 to 255 do
      LightTable[L, I]:=I;
end;

end.
