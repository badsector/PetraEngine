{ A simple scripting engine for self-contained scripts }
unit Scripts;

interface

uses
  Misc, World, Classes, SysUtils;

type
  { Forward class declaratios }
  TScript = class;

  { TScriptCommand - Callback for commands that the engine didn't handle }
  TScriptCommand = function(Sender: TScript; Args: TStringArray): Boolean of object;

  { TScript - Provides a simple scripting engine }
  TScript = class
  private
    // Property storage
    FOnScriptCommand: TScriptCommand;
    function GetVarCount: Integer;
    function GetVarNames(AIndex: Integer): string;
    function GetVarValues(AIndex: Integer): string;
    procedure SetVarValues(AIndex: Integer; AValue: string);
  private
    // Variables
    Variables: TStringKeyValueSet;
    // Lines of code
    Lines: TStringArray;
    // Current line index
    Line: Integer;
    // Run a line of code
    procedure RunLine(ALine: string);
  public
    // Run the given code from the given label (empty=start)
    procedure RunCode(ACode: string; ALabel: string='');
    // Return the index of the variable with the given name or -1
    function VarIndex(AName: string): Integer; inline;
    // Set a variable with the given name to the given value
    procedure SetVar(AName, AValue: string); inline;
    // Get the variable with the given name or return the default value
    function GetVar(AName: string; ADefValue: string=''): string; inline;
    // Number of variables
    property VarCount: Integer read GetVarCount;
    // Variable names
    property VarNames[AIndex: Integer]: string read GetVarNames;
    // Variable values
    property VarValues[AIndex: Integer]: string read GetVarValues write SetVarValues;
    // Called for unknown commands
    property OnScriptCommand: TScriptCommand read FOnScriptCommand write FOnScriptCommand;
  end;

implementation

uses
  Game, Engine, ScrEnt, Sounds;

{ TScript }

procedure TScript.RunLine(ALine: string);
var
  Args: TStringArray;
  Head, I, Volume, Time: Integer;
  Clip: TSoundClip;
  CmdEntity, Command: string;
  SrcEntity, TgtEntity: TEntity;
begin
  // Ignore empty lines
  ALine:=Trim(ALine);
  if ALine='' then Exit;
  // Ignore comment and target lines
  if ALine[1] in ['#', ':'] then Exit;
  // Split the line into arguments
  SetLength(Args, 1);
  Args[0]:='';
  Head:=1;
  // Skip spaces
  while (Head <= Length(ALine)) and (Ord(ALine[Head]) <= 32) do Inc(Head);
  while Head <= Length(ALine) do begin
    // Stop if we've reached the end
    if Head > Length(ALine) then Break;
    // Handle quotes for arguments with spaces
    if ALine[Head]='''' then begin
      Inc(Head);
      while Head <= Length(ALine) do begin
        if ALine[Head]='''' then begin
          if (Head < Length(ALine)) and (ALine[Head + 1]='''') then begin
            Inc(Head, 2);
            Args[High(Args)] += ''''
          end else begin
            Inc(Head);
            Break;
          end;
        end else begin
          Args[High(Args)] += ALine[Head];
          Inc(Head);
        end;
      end;
    end
    // New argument
    else if ALine[Head]=' ' then begin
      // Skip spaces
      while (Head <= Length(ALine)) and (Ord(ALine[Head]) <= 32) do Inc(Head);
      SetLength(Args, Length(Args) + 1);
      Args[High(Args)]:='';
    end
    // Argument character
    else begin
      Args[High(Args)] += ALine[Head];
      Inc(Head);
    end;
  end;
  // Convert the first argument to lowercase
  Args[0]:=LowerCase(Args[0]);
  // Replace any argument that begins with $ with the value of a variable
  for I:=1 to High(Args) do
    if (Length(Args[I]) > 0) and (Args[I][1]='$') then
      Args[I]:=GetVar(Copy(Args[I], 2, MaxInt));
  // Command: end
  if Args[0]='end' then begin
    Line:=Length(Lines);
    Exit;
  end;
  // Command: goto target
  if Args[0]='goto' then begin
    if Length(Args) < 2 then Exit;
    Args[1]:=':' + LowerCase(Args[1]);
    for I:=0 to High(Lines) do
      if LowerCase(Lines[I])=Args[1] then begin
        Line:=I;
        Exit;
      end;
    Exit;
  end;
  // Command: gotoif value target
  if Args[0]='gotoif' then begin
    if Length(Args) < 3 then Exit;
    if Args[1]='' then Exit;
    if StrToFloatDef(Args[1], 0)=0.0 then Exit;
    Args[2]:=':' + LowerCase(Args[2]);
    for I:=0 to High(Lines) do
      if LowerCase(Lines[I])=Args[2] then begin
        Line:=I;
        Exit;
      end;
    Exit;
  end;
  // Command: set var value
  if Args[0]='set' then begin
    if Length(Args) < 2 then Exit;
    if Length(Args)=2 then
      SetVar(Args[1], '')
    else
      SetVar(Args[1], Args[2]);
    Exit;
  end;
  // Command: add var value
  if Args[0]='add' then begin
    if Length(Args) < 3 then Exit;
    SetVar(Args[1], FloatToStr(StrToFloatDef(GetVar(Args[1]), 0) + StrToFloatDef(Args[2], 0)));
    Exit;
  end;
  // Command: sub var value
  if Args[0]='sub' then begin
    if Length(Args) < 3 then Exit;
    SetVar(Args[1], FloatToStr(StrToFloatDef(GetVar(Args[1]), 0) - StrToFloatDef(Args[2], 0)));
    Exit;
  end;
  // Command: mul var value
  if Args[0]='mul' then begin
    if Length(Args) < 3 then Exit;
    SetVar(Args[1], FloatToStr(StrToFloatDef(GetVar(Args[1]), 0) * StrToFloatDef(Args[2], 0)));
    Exit;
  end;
  // Command: div var value
  if Args[0]='div' then begin
    if Length(Args) < 3 then Exit;
    SetVar(Args[1], FloatToStr(StrToFloatDef(GetVar(Args[1]), 0) / StrToFloatDef(Args[2], 0)));
    Exit;
  end;
  // Command: round var
  if Args[0]='round' then begin
    if Length(Args) < 2 then Exit;
    SetVar(Args[1], IntToStr(Round(StrToFloatDef(GetVar(Args[1]), 0))));
    Exit;
  end;
  // Command: trunc var
  if Args[0]='trunc' then begin
    if Length(Args) < 2 then Exit;
    SetVar(Args[1], IntToStr(Trunc(StrToFloatDef(GetVar(Args[1]), 0))));
    Exit;
  end;
  // Command: greater var value1 value2
  if Args[0]='greater' then begin
    if Length(Args) < 4 then Exit;
    if StrToFloatDef(Args[2], 0) > StrToFloatDef(Args[3], 0) then
      SetVar(Args[1], '1')
    else
      SetVar(Args[1], '0');
    Exit;
  end;
  // Command: lesser var value1 value2
  if Args[0]='lesser' then begin
    if Length(Args) < 4 then Exit;
    if StrToFloatDef(Args[2], 0) < StrToFloatDef(Args[3], 0) then
      SetVar(Args[1], '1')
    else
      SetVar(Args[1], '0');
    Exit;
  end;
  // Command: greatereq var value1 value2
  if Args[0]='greatereq' then begin
    if Length(Args) < 4 then Exit;
    if StrToFloatDef(Args[2], 0) >= StrToFloatDef(Args[3], 0) then
      SetVar(Args[1], '1')
    else
      SetVar(Args[1], '0');
    Exit;
  end;
  // Command: lessereq var value1 value2
  if Args[0]='lessereq' then begin
    if Length(Args) < 4 then Exit;
    if StrToFloatDef(Args[2], 0) <= StrToFloatDef(Args[3], 0) then
      SetVar(Args[1], '1')
    else
      SetVar(Args[1], '0');
    Exit;
  end;
  // Command: equal var value1 value2
  if Args[0]='equal' then begin
    if Length(Args) < 4 then Exit;
    if Args[2]=Args[3] then
      SetVar(Args[1], '1')
    else
      SetVar(Args[1], '0');
    Exit;
  end;
  // Command: inequal var value1 value2
  if Args[0]='inequal' then begin
    if Length(Args) < 4 then Exit;
    if Args[2] <> Args[3] then
      SetVar(Args[1], '1')
    else
      SetVar(Args[1], '0');
    Exit;
  end;
  // Command: play clipfile [volume]
  if Args[0]='play' then begin
    if Length(Args) < 2 then Exit;
    Clip:=TSoundClip(GResourceManager.GetResource(Args[1]));
    Volume:=200;
    if Length(Args) > 2 then Volume:=StrToIntDef(Args[2], Volume);
    GSound.PlayClip(Clip, Volume);
    Exit;
  end;
  // Command: transform sourceentity targetentity time [commandentity command]
  if Args[0]='transform' then begin
    if Length(Args) < 3 then Exit;
    SrcEntity:=GGame.World.FindEntity(Args[1]);
    TgtEntity:=GGame.World.FindEntity(Args[2]);
    if Length(Args) > 3 then
      Time:=StrToIntDef(Args[3], 0)
    else
      Time:=0;
    if Length(Args) >= 6 then begin
      CmdEntity:=Args[4];
      Command:=Args[5];
    end else begin
      CmdEntity:='';
      Command:='';
    end;
    if Time <= 0 then begin
      SrcEntity.Transform:=TgtEntity.Transform;
      if CmdEntity <> '' then begin
        TgtEntity:=GGame.World.FindEntity(CmdEntity);
        if Assigned(TgtEntity) then TgtEntity.PerformCommand(Command);
      end;
    end else begin
      SrcEntity.ScheduleMotion(TgtEntity, GGame.Milliseconds + Time, CmdEntity, Command);
    end;
    Exit;
  end;
  // Unknown command, notify any optional handler
  if Assigned(FOnScriptCommand) then
    if FOnScriptCommand(Self, Args) then Exit;
  // Handled didn't handle the command, notify the game as a last resort
  GGame.RunScriptCommand(Self, Args);
end;

function TScript.GetVarCount: Integer;
begin
  Result:=Length(Variables.Keys);
end;

function TScript.GetVarNames(AIndex: Integer): string;
begin
  Result:=Variables.Keys[AIndex];
end;

function TScript.GetVarValues(AIndex: Integer): string;
begin
  Result:=Variables.Values[AIndex];
end;

procedure TScript.SetVarValues(AIndex: Integer; AValue: string);
begin
  Variables.Values[AIndex]:=AValue;
end;

procedure TScript.RunCode(ACode: string; ALabel: string);
var
  I: Integer;
begin
  // Split the code into lines
  SetLength(Lines, 1);
  Lines[0]:='';
  for I:=1 to Length(ACode) do
    if ACode[I]=#10 then
      SetLength(Lines, Length(Lines) + 1)
    else
      Lines[High(Lines)] += ACode[I];
  // Trim the lines
  for I:=0 to High(Lines) do Lines[I]:=Trim(Lines[I]);
  // Find starting position
  if ALabel <> '' then begin
    ALabel:=':' + LowerCase(ALabel);
    Line:=-1;
    for I:=0 to High(Lines) do
      if ALabel=LowerCase(Lines[I]) then begin
        Line:=I;
        Break;
      end;
    // If the label was not found, do not run anything
    if Line=-1 then Exit;
  end else
    Line:=0;
  // Run each line
  while Line < Length(Lines) do begin
    RunLine(Lines[Line]);
    Inc(Line);
  end;
end;

function TScript.VarIndex(AName: string): Integer;
begin
  Result:=Variables.IndexOfKey(AName);
end;

procedure TScript.SetVar(AName, AValue: string);
begin
  Variables.SetValue(AName, AValue);
end;

function TScript.GetVar(AName: string; ADefValue: string): string;
begin
  Result:=Variables.GetValue(AName, ADefValue);
end;

end.

