{ UI game screens with controls and such }
unit UIScrns;

interface

uses
  Classes, FGL, Screens, InputDrv;

type
  { Forward class declarations }
  TUIScreen = class;

  { TUIScreenBackgroundStyle - Background styles for UI screen }
  TUIScreenBackgroundStyle = (
    uisbTransparent,          // Transparent (ie. no) background
    uisbShade                 // Shade the 3D scene
  );
  { TUIControlFlag - A single TUIControl flag }
  TUIControlFlag = (
    uicfFocused,              // The control is focused
    uicfVisible               // The control is visible
  );
  { TUIControlFlags - Set of TUIControl flags }
  TUIControlFlags = set of TUIControlFlag;

  { TUIControl - Base class for user interface controls }
  TUIControl = class
  private
    // Property storage
    FScreen: TUIScreen;
    FLeft: Integer;
    FTop: Integer;
    FFlags: TUIControlFlags;
    FWidth: Integer;
    FHeight: Integer;
  protected
    // Send a command to the screen
    procedure SendCommand(ACommand: string);
  public
    constructor Create;
    destructor Destroy; override;
    // Draw the control
    procedure Draw; virtual; abstract;
    // Return true if the control can be focused
    function Focusable: Boolean; virtual;
    // Handle a key down event
    function KeyDown(Key: TInputKey): Boolean; virtual;
    // Handle a key up event
    function KeyUp(Key: TInputKey): Boolean; virtual;
    // Calculate the size of the control
    procedure CalculateSize; virtual;
    // The TUIScreen that owns this control
    property Screen: TUIScreen read FScreen;
    // Distance from the left side of the screen
    property Left: Integer read FLeft write FLeft;
    // Distance from the top side of the screen
    property Top: Integer read FTop write FTop;
    // The control's width
    property Width: Integer read FWidth write FWidth;
    // The control's height
    property Height: Integer read FHeight write FHeight;
    // Control flags
    property Flags: TUIControlFlags read FFlags write FFlags;
  end;

  { TUIControlList - A list of TUIControls }
  TUIControlList = specialize TFPGList<TUIControl>;

  { TUILabel - A simple text label }
  TUILabel = class(TUIControl)
  private
    FText: string;
  public
    // TUIControl methods
    procedure Draw; override;
    procedure CalculateSize; override;
  public
    constructor Create(ALeft, ATop: Integer; AText: string);
    property Text: string read FText write FText;
  end;

  { TUICommand - Base class for focusable controls that perform commands }
  TUICommand = class(TUIControl)
  private
    FCommand: string;
  public
    // TUIControl methods
    function Focusable: Boolean; override;
    function KeyDown(Key: TInputKey): Boolean; override;
  public
    constructor Create(ACommand: string);
    // Activate the control
    procedure Activate; virtual;
    // The command to send when the control is activated
    property Command: string read FCommand write FCommand;
  end;

  { TUIList - A list of items }
  TUIList = class(TUICommand)
  private
    // Property storage
    FItems: TStringList;
    FItemIndex: Integer;
    procedure SetItemIndex(AItemIndex: Integer);
    function GetItems(AIndex: Integer): string;
    procedure SetItems(AIndex: Integer; AValue: string);
    function GetItemCount: Integer;
  public
    // TUIControl methods
    procedure Draw; override;
    function KeyDown(Key: TInputKey): Boolean; override;
  public
    constructor Create(ALeft, ATop, AWidth, AHeight: Integer;
      AItems: array of string; ACommand: string);
    destructor Destroy; override;
    // Clear the list
    procedure Clear;
    // Add an item to the list
    procedure AddItem(AItem: string);
    // Remove an item from the list
    procedure RemoveItem(AItem: string);
    // Return the index of the given item
    function IndexOfItem(AItem: string): Integer;
    // The currently selected item
    property ItemIndex: Integer read FItemIndex write SetItemIndex;
    // The items in the list
    property Items[AIndex: Integer]: string read GetItems write SetItems;
    // The number of items in the list
    property ItemCount: Integer read GetItemCount;
  end;

  { TUITextButton - A button represented by a line of text }
  TUITextButton = class(TUICommand)
  private
    FText: string;
  public
    // TUIControl methods
    procedure Draw; override;
    procedure CalculateSize; override;
  public
    constructor Create(ALeft, ATop: Integer; AText, ACommand: string);
    // The button's text
    property Text: string read FText write FText;
  end;

  { TUIScreen - Base class for screens that mainly contain UI controls }
  TUIScreen = class(TScreen)
  private
    // Property storage
    FControls: TUIControlList;
    FFocusedControl: TUIControl;
    FBackgroundStyle: TUIScreenBackgroundStyle;
    procedure SetFocusedControl(AFocusedControl: TUIControl);
    function GetControls(AIndex: Integer): TUIControl; inline;
    function GetControlCount: Integer; inline;
  protected
    // TScreen methods
    procedure Draw; override;
    procedure Entered; override;
    procedure Exited; override;
  protected
    // Called to perform a command
    procedure PerformCommand(Sender: TUIControl; ACommand: string); virtual;
    // Called to cancel the screen
    procedure Cancel; virtual;
  public
    // TScreen methods
    function KeyDown(Key: TInputKey): Boolean; override;
    function KeyUp(Key: TInputKey): Boolean; override;
  public
    constructor Create;
    destructor Destroy; override;
    // Add a control to the screen
    procedure AddControl(AControl: TUIControl);
    // Remove (but not delete) the given control from the screen
    procedure RemoveControl(AControl: TUIControl);
    // Remove and delete the given control from the screen
    procedure DeleteControl(AControl: TUIControl);
    // Delete all controls in the screen
    procedure DeleteAllControls;
    // Return the control at the given coordinats
    function ControlAt(X, Y: Integer): TUIControl;
    // Focus the next control
    procedure FocusNext;
    // Focus the control nearest to the given point in the given area
    procedure FocusAt(X, Y, X1, Y1, X2, Y2: Integer);
    // The currently focused control
    property FocusedControl: TUIControl read FFocusedControl write SetFocusedControl;
    // The controls in the screen
    property Controls[AIndex: Integer]: TUIControl read GetControls;
    // The number of controls in the screen
    property ControlCount: Integer read GetControlCount;
    // The screen's background style
    property BackgroundStyle: TUIScreenBackgroundStyle read FBackgroundStyle write FBackgroundStyle;
  end;


implementation

uses
  SysUtils, Maths, GfxDrv, Engine, Fonts;

{ TUIControl }
constructor TUIControl.Create;
begin
  FFlags:=[uicfVisible];
  FWidth:=1;
  FHeight:=1;
end;

destructor TUIControl.Destroy;
begin
  if Assigned(Screen) then Screen.RemoveControl(Self);
  inherited Destroy;
end;

procedure TUIControl.SendCommand(ACommand: string);
begin
  if Assigned(Screen) then Screen.PerformCommand(Self, ACommand);
end;

function TUIControl.Focusable: Boolean;
begin
  Result:=False;
end;

function TUIControl.KeyDown(Key: TInputKey): Boolean;
begin
  Result:=False;
end;

function TUIControl.KeyUp(Key: TInputKey): Boolean;
begin
  Result:=False;
end;

procedure TUIControl.CalculateSize;
begin
end;

{ TUILabel }
constructor TUILabel.Create(ALeft, ATop: Integer; AText: string);
begin
  inherited Create;
  Text:=AText;
  Left:=ALeft;
  Top:=ATop;
  CalculateSize;
end;

procedure TUILabel.Draw;
begin
  GRenderer.DrawText(Left, Top, Text, TBitFont(GResourceManager.DefaultFont), 15, 0);
end;

procedure TUILabel.CalculateSize;
begin
  GRenderer.TextSize(Text, TBitFont(GResourceManager.DefaultFont), FWidth, FHeight);
end;

{ TUICommand }
constructor TUICommand.Create(ACommand: string);
begin
  inherited Create;
  FCommand:=ACommand;
end;

function TUICommand.Focusable: Boolean;
begin
  Result:=True;
end;

function TUICommand.KeyDown(Key: TInputKey): Boolean;
begin
  if inherited KeyDown(Key) then Exit(True);
  if Key in [ikEnter, ikLeftButton] then begin
    Activate;
    Exit(True);
  end;
  Result:=False;
end;

procedure TUICommand.Activate;
begin
  SendCommand(Command);
end;

{ TUIList }
constructor TUIList.Create(ALeft, ATop, AWidth, AHeight: Integer;
  AItems: array of string; ACommand: string);
var
  I: Integer;
begin
  inherited Create(ACommand);
  Left:=ALeft;
  Top:=ATop;
  Width:=AWidth;
  Height:=AHeight;
  FItems:=TStringList.Create;
  FItemIndex:=-1;
  for I:=0 to High(AItems) do AddItem(AItems[I]);
end;

destructor TUIList.Destroy;
begin
  FreeAndNil(FItems);
  inherited Destroy;
end;

procedure TUIList.SetItemIndex(AItemIndex: Integer);
begin
  AItemIndex:=Clamp(AItemIndex, 0, FItems.Count - 1);
  if FItemIndex=AItemIndex then Exit;
  FItemIndex:=AItemIndex;
  SendCommand('_selected');
end;

function TUIList.GetItemCount: Integer;
begin
  Result:=FItems.Count;
end;

function TUIList.GetItems(AIndex: Integer): string;
begin
  Result:=FItems[AIndex];
end;

procedure TUIList.SetItems(AIndex: Integer; AValue: string);
begin
  FItems[AIndex]:=AValue;
end;

procedure TUIList.Draw;
var
  Y, I: Integer;
begin
  // Draw an outline for the list
  GRenderer.DrawRectangle(Left + 1, Top + 1, Left + Width, Top + Height, 0);
  GRenderer.DrawRectangle(Left, Top, Left + Width - 1, Top + Height - 1, 15);
  // Draw the items
  Y:=Top + 2;
  for I:=0 to ItemCount - 1 do begin
    if (Y >= Top + 2) and (Y < Top + Height - TBitFont(GResourceManager.DefaultFont).Height) then begin
      if I=ItemIndex then begin
        if uicfFocused in Flags then begin
          GRenderer.FillRectangle(Left + 1, Y - 1, Left + Width - 2, Y + TBitFont(GResourceManager.DefaultFont).Height, 15);
          GRenderer.DrawText(Left + 2, Y, Items[I], TBitFont(GResourceManager.DefaultFont), 0);
        end else begin
          GRenderer.DrawRectangle(Left + 1, Y - 1, Left + Width - 2, Y + TBitFont(GResourceManager.DefaultFont).Height, 15);
          GRenderer.DrawText(Left + 2, Y, Items[I], TBitFont(GResourceManager.DefaultFont), 15, 0);
        end;
      end else
        GRenderer.DrawText(Left + 2, Y, Items[I], TBitFont(GResourceManager.DefaultFont), 15, 0);
    end;
    if Y >= Top + Height - TBitFont(GResourceManager.DefaultFont).Height then Break;
    Inc(Y, TBitFont(GResourceManager.DefaultFont).Height);
  end;
end;

function TUIList.KeyDown(Key: TInputKey): Boolean;
begin
  if inherited KeyDown(Key) then Exit(True);
  Result:=True;
  case Key of
    ikUp, ikW, ikWheelUp: if ItemIndex > 0 then ItemIndex:=ItemIndex - 1 else Exit(False);
    ikDown, ikS, ikWheelDown: if ItemIndex < ItemCount - 1 then ItemIndex:=ItemIndex + 1 else Exit(False);
    else Result:=False;
  end;
end;

procedure TUIList.Clear;
begin
  FItems.Clear;
  FItemIndex:=-1;
end;

procedure TUIList.AddItem(AItem: string);
begin
  FItems.Add(AItem);
end;

procedure TUIList.RemoveItem(AItem: string);
var
  Index: Integer;
begin
  Index:=FItems.IndexOf(AItem);
  if Index <> - 1 then FItems.Delete(Index);
end;

function TUIList.IndexOfItem(AItem: string): Integer;
begin
  Result:=FItems.IndexOf(AItem);
end;

{ TUITextButton }
constructor TUITextButton.Create(ALeft, ATop: Integer; AText, ACommand: string);
begin
  inherited Create(ACommand);
  Left:=ALeft;
  Top:=ATop;
  FText:=AText;
  CalculateSize;
end;

procedure TUITextButton.Draw;
begin
  if uicfFocused in Flags then begin
    GRenderer.DrawRectangle(Left + 1, Top + 1, Left + Width, Top + Height, 0);
    GRenderer.FillRectangle(Left, Top, Left + Width - 1, Top + Height - 1, 15);
    GRenderer.DrawText(Left + 2, Top + 2, Text, TBitFont(GResourceManager.DefaultFont), 0);
  end else begin
    GRenderer.DrawRectangle(Left + 1, Top + 1, Left + Width, Top + Height, 0);
    GRenderer.DrawRectangle(Left, Top, Left + Width - 1, Top + Height - 1, 15);
    GRenderer.DrawText(Left + 2, Top + 2, Text, TBitFont(GResourceManager.DefaultFont), 15, 0);
  end;
end;

procedure TUITextButton.CalculateSize;
begin
  GRenderer.TextSize(Text, TBitFont(GResourceManager.DefaultFont), FWidth, FHeight);
  FWidth += 4;
  FHeight += 4;
end;

{ TUIScreen }
constructor TUIScreen.Create;
begin
  inherited Create;
  FControls:=TUIControlList.Create;
  FBackgroundStyle:=uisbTransparent;
end;

destructor TUIScreen.Destroy;
begin
  DeleteAllControls;
  FControls.Free;
  inherited Destroy;
end;

function TUIScreen.GetControlCount: Integer;
begin
  Result:=FControls.Count;
end;

function TUIScreen.GetControls(AIndex: Integer): TUIControl;
begin
  Result:=FControls[AIndex];
end;

procedure TUIScreen.SetFocusedControl(AFocusedControl: TUIControl);
begin
  if FFocusedControl=AFocusedControl then Exit;
  if Assigned(FocusedControl) then FocusedControl.Flags:=FocusedControl.Flags - [uicfFocused];
  FFocusedControl:=AFocusedControl;
  if Assigned(FocusedControl) then FocusedControl.Flags:=FocusedControl.Flags + [uicfFocused];
end;

procedure TUIScreen.Draw;
var
  I: Integer;
begin
  // Draw background
  case BackgroundStyle of
    uisbShade: GRenderer.ShadeRectangle(0, 0, HRes, VRes);
  end;
  // Draw controls
  for I:=0 to ControlCount - 1 do
    if uicfVisible in Controls[I].Flags then
      Controls[I].Draw;
end;

procedure TUIScreen.Entered;
begin
  // Make the screen manager the exclusive input handler to avoid other handlers
  // getting key events
  GInput.ExclusiveHandler:=GScreenManager;
end;

procedure TUIScreen.Exited;
begin
  // Release the screen manager from being the exclusive handler
  GInput.ExclusiveHandler:=nil;
  inherited Exited;
end;

procedure TUIScreen.PerformCommand(Sender: TUIControl; ACommand: string);
begin
end;

procedure TUIScreen.Cancel;
begin
end;

function TUIScreen.KeyDown(Key: TInputKey): Boolean;
begin
  // Pass the event key to the focused control, if any
  if Assigned(FocusedControl) then
    if FocusedControl.KeyDown(Key) then Exit(True);
  // Handle the key
  case Key of
   // Move to the focusable control at the right side
   ikRight, ikD:
     if Assigned(FocusedControl) then
       FocusAt(FocusedControl.Left + FocusedControl.Width, FocusedControl.Top,
         FocusedControl.Left + FocusedControl.Width, 0, HRes, VRes)
     else
       FocusNext;
   // Move to the focusable control at the left side
   ikLeft, ikA:
     if Assigned(FocusedControl) then
       FocusAt(FocusedControl.Left - 1, FocusedControl.Top,
         0, 0, FocusedControl.Left - 1, VRes)
     else
       FocusNext;
   // Move to the focusable control at the top side
   ikUp, ikW, ikWheelUp:
     if Assigned(FocusedControl) then
       FocusAt(FocusedControl.Left, FocusedControl.Top - 1,
         0, 0, HRes, FocusedControl.Top - 1)
     else
       FocusNext;
   // Move to the focusable control at the bottom side
   ikDown, ikS, ikWheelDown:
     if Assigned(FocusedControl) then
       FocusAt(FocusedControl.Left, FocusedControl.Top + FocusedControl.Height,
         0, FocusedControl.Top + FocusedControl.Height, HRes, VRes)
     else
       FocusNext;
   // Move to the next focusable control
   ikTab: FocusNext;
   // Cancel the screen
   ikEscape, ikRightButton: Cancel;
  end;
  Result:=False;
end;

function TUIScreen.KeyUp(Key: TInputKey): Boolean;
begin
  // Pass the event key to the focused control, if any
  if Assigned(FocusedControl) then
    Result:=FocusedControl.KeyUp(Key)
  else
    Result:=False;
end;

procedure TUIScreen.AddControl(AControl: TUIControl);
begin
  // Remove the control from its previous screen, if any
  if Assigned(AControl.Screen) then AControl.Screen.RemoveControl(AControl);
  // Add the control
  FControls.Add(AControl);
  AControl.FScreen:=Self;
  // Focus the control if there isn't any other focused
  if not Assigned(FocusedControl) then
    FocusNext;
end;

procedure TUIScreen.RemoveControl(AControl: TUIControl);
begin
  if FocusedControl=AControl then FocusedControl:=nil;
  FControls.Remove(AControl);
  AControl.FScreen:=nil;
end;

procedure TUIScreen.DeleteControl(AControl: TUIControl);
begin
  RemoveControl(AControl);
  AControl.Free;
end;

procedure TUIScreen.DeleteAllControls;
var
  I: Integer;
begin
  FocusedControl:=nil;
  for I:=0 to ControlCount - 1 do begin
    Controls[I].FScreen:=nil;
    Controls[I].Free;
  end;
  FControls.Clear;
end;

function TUIScreen.ControlAt(X, Y: Integer): TUIControl;
var
  I: Integer;
begin
  for I:=0 to ControlCount - 1 do
    with Controls[I] do begin
      if not (uicfVisible in Flags) then Continue;
      if (X >= Left) and (Y >= Top) and (X < Left + Width) and (Y < Top + Height) then
        Exit(Controls[I]);
    end;
  Result:=nil;
end;

procedure TUIScreen.FocusNext;
var
  I, Index: Integer;
begin
  if ControlCount=0 then Exit;
  Index:=FControls.IndexOf(FocusedControl);
  for I:=1 to ControlCount do begin
    Index:=(Index + 1) mod ControlCount;
    if Controls[Index].Focusable then begin
      FocusedControl:=Controls[Index];
      Break;
    end;
  end;
end;

procedure TUIScreen.FocusAt(X, Y, X1, Y1, X2, Y2: Integer);
var
  I, Index: Integer;
  Control: TUIControl = nil;
  Dist, Best: Single;
begin
  if ControlCount=0 then Exit;
  Index:=FControls.IndexOf(FocusedControl);
  Best:=$FFFFFFFF;
  for I:=1 to ControlCount do begin
    Index:=(Index + 1) mod ControlCount;
    if (Controls[Index] <> FocusedControl) and
       Controls[Index].Focusable and
       (Controls[Index].Left >= X1) and
       (Controls[Index].Left <= X2) and
       (Controls[Index].Top >= Y1) and
       (Controls[Index].Top <= Y2) then begin
      Dist:=DistanceSq(Vector(X, Y, 0), Vector(Controls[Index].Left, Controls[Index].Top, 0));
      if Dist < Best then begin
        Best:=Dist;
        Control:=Controls[Index];
      end;
    end;
  end;
  if Assigned(Control) then FocusedControl:=Control;
end;

end.

