{ Game screens }
unit Screens;

interface

uses
  Misc, InputDrv;

type
  { TScreen - Base class for a game screen }
  TScreen = class
  protected
    // Called to draw the screen
    procedure Draw; virtual;
    // Called to update the screen
    procedure Update; virtual;
    // Called when the screen is entered
    procedure Entered; virtual;
    // Called when the screen is exited
    procedure Exited; virtual;
  public
    // TInputHandler methods
    function KeyDown(Key: TInputKey): Boolean; virtual;
    function KeyUp(Key: TInputKey): Boolean; virtual;
    function Mouse(X, Y: Integer): Boolean; virtual;
  public
    // Returns true if this screen pauses the game
    function Pause: Boolean; virtual;
    // Returns true if this screen covers the entire viewport and thus
    // it is not necessary to render the scene
    function FullScreen: Boolean; virtual;
  end;

  { TScreenManager - The screen manager }
  TScreenManager = class(TInputHandler)
  private
    // Property storage
    FScreen: TScreen;
    procedure SetScreen(AValue: TScreen);
  public
    // TInputHandler methods
    function KeyDown(Key: TInputKey): Boolean;
    function KeyUp(Key: TInputKey): Boolean;
    function Mouse(X, Y: Integer): Boolean;
  public
    // Draw the current screen
    procedure Draw;
    // Update the current screen
    procedure Update;
    // Returns true if the current screen pauses the game
    function Pause: Boolean;
    // Returns true if the current screen covers the entire viewport and thus
    // it is not necessary to render the scene
    function FullScreen: Boolean;
    // Current screen
    property Screen: TScreen read FScreen write SetScreen;
  end;

implementation

{ TScreen }
procedure TScreen.Draw;
begin
end;

procedure TScreen.Update;
begin
end;

procedure TScreen.Entered;
begin
end;

procedure TScreen.Exited;
begin
end;

function TScreen.KeyDown(Key: TInputKey): Boolean;
begin
  Result:=False;
end;

function TScreen.KeyUp(Key: TInputKey): Boolean;
begin
  Result:=False;
end;

function TScreen.Mouse(X, Y: Integer): Boolean;
begin
  Result:=False;
end;

function TScreen.Pause: Boolean;
begin
  Result:=True;
end;

function TScreen.FullScreen: Boolean;
begin
  Result:=False;
end;

{ TScreenManager }
procedure TScreenManager.SetScreen(AValue: TScreen);
var
  OldScreen: TScreen;
begin
  if FScreen=AValue then Exit;
  OldScreen:=FScreen;
  FScreen:=AValue;
  // Calling Exited may cause the screen to change
  if Assigned(OldScreen) then OldScreen.Exited;
  // If FScreen is still the same (ie. Exited above didn't change it) inform it
  if (AValue=FScreen) and Assigned(FScreen) then FScreen.Entered;
end;

function TScreenManager.KeyDown(Key: TInputKey): Boolean;
begin
  if Assigned(Screen) then Result:=Screen.KeyDown(Key) else Result:=False;
end;

function TScreenManager.KeyUp(Key: TInputKey): Boolean;
begin
  if Assigned(Screen) then Result:=Screen.KeyUp(Key) else Result:=False;
end;

function TScreenManager.Mouse(X, Y: Integer): Boolean;
begin
  if Assigned(Screen) then Result:=Screen.Mouse(X, Y) else Result:=False;
end;

procedure TScreenManager.Draw;
begin
  if Assigned(Screen) then Screen.Draw;
end;

procedure TScreenManager.Update;
begin
  if Assigned(Screen) then Screen.Update;
end;

function TScreenManager.Pause: Boolean;
begin
  if Assigned(Screen) then Result:=Screen.Pause else Result:=False;
end;

function TScreenManager.FullScreen: Boolean;
begin
  if Assigned(Screen) then Result:=Screen.FullScreen else Result:=False;
end;

end.
