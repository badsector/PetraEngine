{ Cameras }
unit Cameras;

interface

uses
  Maths, Serial;

type
  { TCamera - describes a camera }
  TCamera = class(TSerializable)
  private
    // Property storage
    FPosition: TVector;
    FDirection: TVector;
    FFOV: Single;
    FAspect: Single;
    FNearPlane: Single;
    FFarPlane: Single;
    function GetProjectionMatrix: TMatrix; inline;
    function GetViewMatrix: TMatrix; inline;
    procedure SetPositionX(APositionX: Single); inline;
    procedure SetPositionY(APositionY: Single); inline;
    procedure SetPositionZ(APositionZ: Single); inline;
    procedure SetDirectionX(ADirectionX: Single); inline;
    procedure SetDirectionY(ADirectionY: Single); inline;
    procedure SetDirectionZ(ADirectionZ: Single); inline;
    function GetPositionX: Single; inline;
    function GetPositionY: Single; inline;
    function GetPositionZ: Single; inline;
    function GetDirectionX: Single; inline;
    function GetDirectionY: Single; inline;
    function GetDirectionZ: Single; inline;
  protected
    // TSerializable methods
    procedure BeforeSerialization; override;
    procedure AfterDeserialization; override;
  public
    // Create a default camera at the origin looking forward
    constructor Create; override;
    // Reset the camera settings and position
    procedure Reset;
    // Assign the given camera to this camera
    procedure Assign(ACamera: TCamera); reintroduce;
    // Calculate a ray from the camera towards the direction specified by
    // the given screen space coordinates
    function CalcRay(X, Y: Single): TRay;
    // Calculate frustum planes
    procedure CalcPlanes(out Left, Right, Top, Bottom, NearP, FarP: TPlane);
    // The camera's position in 3D space
    property Position: TVector read FPosition write FPosition;
    // The camera's direction
    property Direction: TVector read FDirection write FDirection;
    // The camera's aspect ratio
    property Aspect: Single read FAspect write FAspect;
    // The projection matrix to use for this camera
    property ProjectionMatrix: TMatrix read GetProjectionMatrix;
    // The view matrix to use for this camera
    property ViewMatrix: TMatrix read GetViewMatrix;
  published
    // Component-wise access to the position
    property PositionX: Single read GetPositionX write SetPositionX;
    property PositionY: Single read GetPositionY write SetPositionY;
    property PositionZ: Single read GetPositionZ write SetPositionZ;
    // Component-wise access to the direction
    property DirectionX: Single read GetDirectionX write SetDirectionX;
    property DirectionY: Single read GetDirectionY write SetDirectionY;
    property DirectionZ: Single read GetDirectionZ write SetDirectionZ;
    // The camera's vertical field of view
    property FOV: Single read FFOV write FFOV;
    // The distance to the near plane from the camera's origin
    property NearPlane: Single read FNearPlane write FNearPlane;
    // The distance to the far plane from the camera's origin
    property FarPlane: Single read FFarPlane write FFarPlane;
  end;

implementation

uses
  GfxDrv, CamEnt;

{ TCamera }
constructor TCamera.Create;
begin
  Reset;
end;

function TCamera.GetProjectionMatrix: TMatrix;
begin
  Result.Perspective(FOV, Aspect, NearPlane, FarPlane);
end;

function TCamera.GetViewMatrix: TMatrix;
begin
  Result.LookAt(Position, Position.Subbed(Direction), Vector(0, 1, 0));
end;

procedure TCamera.SetPositionX(APositionX: Single); 
begin
  FPosition.X:=APositionX;
end;

procedure TCamera.SetPositionY(APositionY: Single); 
begin
  FPosition.Y:=APositionY;
end;

procedure TCamera.SetPositionZ(APositionZ: Single); 
begin
  FPosition.Z:=APositionZ;
end;

procedure TCamera.SetDirectionX(ADirectionX: Single); 
begin
  FDirection.X:=ADirectionX;
end;

procedure TCamera.SetDirectionY(ADirectionY: Single); 
begin
  FDirection.Y:=ADirectionY;
end;

procedure TCamera.SetDirectionZ(ADirectionZ: Single); 
begin
  FDirection.Z:=ADirectionZ;
end;

function TCamera.GetPositionX: Single;
begin
  Result:=FPosition.X;
end;

function TCamera.GetPositionY: Single;
begin
  Result:=FPosition.Y;
end;

function TCamera.GetPositionZ: Single;
begin
  Result:=FPosition.Z;
end;

function TCamera.GetDirectionX: Single;
begin
  Result:=FDirection.X;
end;

function TCamera.GetDirectionY: Single;
begin
  Result:=FDirection.Y;
end;

function TCamera.GetDirectionZ: Single;
begin
  Result:=FDirection.Z;
end;

procedure TCamera.BeforeSerialization;
begin
  // Ensure direction is normalized
  FDirection.Normalize;
end;

procedure TCamera.AfterDeserialization;
begin
  // Ensure direction is normalized
  FDirection.Normalize;
end;

procedure TCamera.Reset;
begin
  FPosition:=Origin;
  FDirection:=Vector(0, 0, -1);
  FFOV:=60*Pi/180;
  FAspect:=AspectRatio;
  FNearPlane:=0.5;
  FFarPlane:=25.0;
end;

procedure TCamera.Assign(ACamera: TCamera);
begin
  FPosition:=ACamera.Position;
  FDirection:=ACamera.Direction;
  FFOV:=ACamera.FOV;
  FAspect:=ACamera.Aspect;
  FNearPlane:=ACamera.NearPlane;
  FFarPlane:=ACamera.FarPlane;
end;

function TCamera.CalcRay(X, Y: Single): TRay;
var
  Mtx: TMatrix;
begin
  Mtx:=GetProjectionMatrix;
  Mtx.Multiply(GetViewMatrix);
  Mtx.Invert;
  Result.O:=Position;
  Result.D:=Maths.Direction(Result.O, Mtx.TransformedProj(Vector(X, Y, FarPlane)));
end;

procedure TCamera.CalcPlanes(out Left, Right, Top, Bottom, NearP, FarP: TPlane);
var
  Mtx: TMatrix;
begin
  Mtx:=GetProjectionMatrix;
  Mtx.Multiply(GetViewMatrix);
  Left.N.X:=-(Mtx.M41 + Mtx.M11);
  Left.N.Y:=-(Mtx.M42 + Mtx.M12);
  Left.N.Z:=-(Mtx.M43 + Mtx.M13);
  Left.D:=    Mtx.M44 + Mtx.M14;
  Left.Normalize;
  Right.N.X:=-(Mtx.M41 - Mtx.M11);
  Right.N.Y:=-(Mtx.M42 - Mtx.M12);
  Right.N.Z:=-(Mtx.M43 - Mtx.M13);
  Right.D:=    Mtx.M44 - Mtx.M14;
  Right.Normalize;
  Bottom.N.X:=-(Mtx.M41 + Mtx.M21);
  Bottom.N.Y:=-(Mtx.M42 + Mtx.M22);
  Bottom.N.Z:=-(Mtx.M43 + Mtx.M23);
  Bottom.D:=    Mtx.M44 + Mtx.M24;
  Bottom.Normalize;
  Top.N.X:=-(Mtx.M41 - Mtx.M21);
  Top.N.Y:=-(Mtx.M42 - Mtx.M22);
  Top.N.Z:=-(Mtx.M43 - Mtx.M23);
  Top.D:=    Mtx.M44 - Mtx.M24;
  Top.Normalize;
  NearP.N.X:=-(Mtx.M41 + Mtx.M31);
  NearP.N.Y:=-(Mtx.M42 + Mtx.M32);
  NearP.N.Z:=-(Mtx.M43 + Mtx.M33);
  NearP.D:=    Mtx.M44 + Mtx.M34;
  NearP.Normalize;
  FarP.N.X:=-(Mtx.M41 - Mtx.M31);
  FarP.N.Y:=-(Mtx.M42 - Mtx.M32);
  FarP.N.Z:=-(Mtx.M43 - Mtx.M33);
  FarP.D:=    Mtx.M44 - Mtx.M34;
  FarP.Normalize;
end;

initialization
  RegisterSerializableClass(TCamera);
end.

