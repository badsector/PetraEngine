{ Miscellaneous definitions }
unit Misc;

interface

uses
  Classes;

type
  { Pointers }
  PStringKeyValueSet = ^TStringKeyValueSet;

  { Types with sizes }
  UInt8 = Byte;
  UInt16 = Word;
  UInt32 = Cardinal;
  Int8 = ShortInt;
  Int16 = SmallInt;
  Int32 = LongInt;
  
  { TStringArray - String array }
  TStringArray = array of string;

  { TStringKeyValueSet - A set of key/value pairs }
  TStringKeyValueSet = object
    // Keys and values in the set
    Keys, Values: TStringArray;
    // Clear the set
    procedure Clear;
    // Set the value for a key
    procedure SetValue(AKey, AValue: string);
    // Retrieve the value of a keyu
    function GetValue(AKey: string; ADefValue: string=''): string;
    // Remove the given key
    procedure RemoveKey(AKey: string);
    // Remove the key at the given index
    procedure RemoveKeyAt(AIndex: Integer);
    // Return the index of the given key or -1 if the key is not in the set
    function IndexOfKey(AKey: string): Integer;
    // Return the index of the given value or -1 if no key has the given value
    function IndexOfValue(AValue: string): Integer; inline;
  end;


  { TMemoryReadStream - A stream that can be used to read from already
    allocated memory }
  TMemoryReadStream = class(TCustomMemoryStream)
  public
    constructor Create(APointer: Pointer; ASize: PtrInt);
  end;

// Read a line from stream
function ReadLineFromStream(AStream: TStream): string;

implementation

{ Functions }
function ReadLineFromStream(AStream: TStream): string;
var
  B: Byte;
begin
  Result:='';
  while True do begin
    try
      B:=AStream.ReadByte;
    except
      Exit;
    end;
    if B=10 then Exit;
    if B <> 13 then Result += Char(B);
  end;
end;

{ TStringKeyValueSet }
procedure TStringKeyValueSet.Clear;
begin
  Keys:=nil;
  Values:=nil;
end;

procedure TStringKeyValueSet.SetValue(AKey, AValue: string);
var
  Index: Integer;
begin
  Index:=IndexOfKey(AKey);
  if Index=-1 then begin
    Index:=Length(Keys);
    SetLength(Keys, Index + 1);
    SetLength(Values, Length(Keys));
    Keys[Index]:=LowerCase(AKey);
  end;
  Values[Index]:=AValue;
end;

function TStringKeyValueSet.GetValue(AKey: string; ADefValue: string): string;
var
  Index: Integer;
begin
  Index:=IndexOfKey(AKey);
  if Index <> -1 then Exit(Values[Index]) else Exit(ADefValue);
end;

procedure TStringKeyValueSet.RemoveKey(AKey: string);
var
  Index: Integer;
begin
  Index:=IndexOfKey(AKey);
  if Index=-1 then Exit;
  RemoveKeyAt(Index);
end;

procedure TStringKeyValueSet.RemoveKeyAt(AIndex: Integer);
var
  I: Integer;
begin
  for I:=AIndex to High(Keys) - 1 do begin
    Keys[I]:=Keys[I + 1];
    Values[I]:=Values[I + 1];
  end;
  SetLength(Keys, Length(Keys) - 1);
  SetLength(Values, Length(Keys));
end;

function TStringKeyValueSet.IndexOfKey(AKey: string): Integer;
var
  I: Integer;
begin
  AKey:=LowerCase(AKey);
  for I:=0 to High(Keys) do if Keys[I]=AKey then Exit(I);
  Result:=-1;
end;

function TStringKeyValueSet.IndexOfValue(AValue: string): Integer;
var
  I: Integer;
begin
  for I:=0 to High(Values) do if Values[I]=AValue then Exit(I);
  Result:=-1;
end;

{ TMemoryReadStream }
constructor TMemoryReadStream.Create(APointer: Pointer; ASize: PtrInt);
begin
  inherited Create;
  SetPointer(APointer, ASize);
end;

end.

