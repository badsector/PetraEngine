// Hardware rasterizer base class
unit HWRaster;

interface

uses
  Maths, Colors, Raster, Textures, GfxDrv;

type

  PTriBatch = ^TTriBatch;
  TTriBatch = record
    // The hardware texture for this batch
    HWTexture: TRasterTexture;
    // Vertex data
    Vertices: array of Single;
    // Number of vertices in this batch
    Count: Integer;
  end;

  { THardwareRasterizer - Base class for rasterizers that use a 3D hardware
    acceleration API (OpenGL, Direct3D, etc) }
  THardwareRasterizer = class(TRasterizer)
  private
    // Triangle batches
    TriBatches: array of TTriBatch;
    // Number of triangle batches until the last call to FluchBatches
    TriBatchCount: Integer;
    // Returns the triangle batch for this texture
    function GetTriBatch(ATexture: TRasterTexture): PTriBatch;
  protected
    // Discard any allocated triangle batches
    procedure DiscardBatches;
    // Flush and draw allocated triangle batches
    procedure FlushBatches;
    // Called before the batches are drawn
    procedure BeforeBatchesDraw; virtual;
    // Called after the batches have been drawn;
    procedure AfterBatchesDraw; virtual;
    // Called to draw the given batch
    procedure DrawBatch(const Batch: TTriBatch); virtual; abstract;
  public
    // TRasterizer methods
    function BeginFrame: Boolean; override;
    procedure FinishFrame; override;
    procedure RenderTriangles(Count: Integer; const RenderTris); override;
    function WasRectOccluded(AX1, AY1, AX2, AY2: Integer; AZ: Single): Boolean; override;
  end;

implementation

uses
  Renderer;

{ THardwareRasterizer }

function THardwareRasterizer.GetTriBatch(ATexture: TRasterTexture): PTriBatch;
var
  I: Integer;
begin
  // Check if the texture already has a triangle batch associated
  if ATexture.TriBatch > 0 then Exit(@TriBatches[ATexture.TriBatch - 1]);
  // Try to find an existing batch for the given texture
  for I:=0 to TriBatchCount - 1 do
    if TriBatches[I].HWTexture=ATexture then begin
      ATexture.TriBatch:=I + 1;
      Exit(@TriBatches[I]);
    end;
  // Increase the allocation if needed for a new batch
  if Length(TriBatches)=TriBatchCount then
    SetLength(TriBatches, Length(TriBatches) + 256);
  // Reserve a new batch
  Result:=@TriBatches[TriBatchCount];
  Result^.HWTexture:=ATexture;
  Result^.Count:=0;
  Inc(TriBatchCount);
  ATexture.TriBatch:=TriBatchCount;
end;

procedure THardwareRasterizer.DiscardBatches;
begin
  TriBatchCount:=0;
end;

procedure THardwareRasterizer.FlushBatches;
var
  I: Integer;
begin
  // Ignore if no batches
  if TriBatchCount=0 then Exit;
  // Draw the batches
  BeforeBatchesDraw;
  for I:=0 to TriBatchCount - 1 do begin
    DrawBatch(TriBatches[I]);
    TriBatches[I].HWTexture.TriBatch:=0;
  end;
  AfterBatchesDraw;
  // Reset batches
  TriBatchCount:=0;
end;

procedure THardwareRasterizer.BeforeBatchesDraw;
begin
end;

procedure THardwareRasterizer.AfterBatchesDraw;
begin
end;

function THardwareRasterizer.BeginFrame: Boolean;
begin
  DiscardBatches;
  Result:=True;
end;

procedure THardwareRasterizer.FinishFrame;
begin
  FlushBatches;
end;

procedure THardwareRasterizer.RenderTriangles(Count: Integer; const RenderTris);
type
  PRenderTri = ^TRenderTri;
var
  I: Integer;
  Batch: PTriBatch;
  DWORDVal: DWORD;
  FloatVal: Single absolute DWORDVal;
begin
  for I:=0 to Count - 1 do with PRenderTri(@RenderTris)[I] do begin
    // Find batch for the texture
    Batch:=GetTriBatch(TRasterTexture(Texture.HardwareTexture));
    with Batch^ do begin
      {$IFDEF PETRAD3D}
      // Increase batch vertex allocation if needed
      if Count=Length(Vertices) then
        SetLength(Vertices, Length(Vertices) + 18*64);
      // Store the triangle
      Vertices[Count]:=A.X; Inc(Count);
      Vertices[Count]:=A.Y; Inc(Count);
      Vertices[Count]:=A.Z; Inc(Count);
      DWORDVal:=(((DWORD(Trunc((LB/255*TintR/255)*255)) or ((DWORD(Trunc((LB/255*TintG/255)*255))) shl 8)) or ((DWORD(Trunc((LB/255*TintB/255)*255))) shl 16));
      Vertices[Count]:=FloatVal; Inc(Count);
      Vertices[Count]:=SA/Texture.Width; Inc(Count);
      Vertices[Count]:=TA/Texture.Height; Inc(Count);
      Vertices[Count]:=B.X; Inc(Count);
      Vertices[Count]:=B.Y; Inc(Count);
      Vertices[Count]:=B.Z; Inc(Count);
      DWORDVal:=(((DWORD(Trunc((LB/255*TintR/255)*255)) or ((DWORD(Trunc((LB/255*TintG/255)*255))) shl 8)) or ((DWORD(Trunc((LB/255*TintB/255)*255))) shl 16));
      Vertices[Count]:=FloatVal; Inc(Count);
      Vertices[Count]:=SB/Texture.Width; Inc(Count);
      Vertices[Count]:=TB/Texture.Height; Inc(Count);
      Vertices[Count]:=C.X; Inc(Count);
      Vertices[Count]:=C.Y; Inc(Count);
      Vertices[Count]:=C.Z; Inc(Count);
      DWORDVal:=(((DWORD(Trunc((LC/255*TintR/255)*255)) or ((DWORD(Trunc((LC/255*TintG/255)*255))) shl 8)) or ((DWORD(Trunc((LC/255*TintB/255)*255))) shl 16));
      Vertices[Count]:=FloatVal; Inc(Count);
      Vertices[Count]:=SC/Texture.Width; Inc(Count);
      Vertices[Count]:=TC/Texture.Height; Inc(Count);
      {$ELSE}
      // Increase batch vertex allocation if needed
      if Count=Length(Vertices) then
        SetLength(Vertices, Length(Vertices) + {$IFDEF ANDROID}27{$ELSE}24{$ENDIF}*64);
      // Store the triangle
      Vertices[Count]:=SA/Texture.Width; Inc(Count);
      Vertices[Count]:=TA/Texture.Height; Inc(Count);
      Vertices[Count]:=(LA/255)*(TintR/255); Inc(Count);
      Vertices[Count]:=(LA/255)*(TintG/255); Inc(Count);
      Vertices[Count]:=(LA/255)*(TintB/255); Inc(Count);
      {$IFDEF ANDROID}
      Vertices[Count]:=1.0; Inc(Count);
      {$ENDIF}
      Vertices[Count]:=A.X; Inc(Count);
      Vertices[Count]:=A.Y; Inc(Count);
      Vertices[Count]:=A.Z; Inc(Count);
      Vertices[Count]:=SB/Texture.Width; Inc(Count);
      Vertices[Count]:=TB/Texture.Height; Inc(Count);
      Vertices[Count]:=(LB/255)*(TintR/255); Inc(Count);
      Vertices[Count]:=(LB/255)*(TintG/255); Inc(Count);
      Vertices[Count]:=(LB/255)*(TintB/255); Inc(Count);
      {$IFDEF ANDROID}
      Vertices[Count]:=1.0; Inc(Count);
      {$ENDIF}
      Vertices[Count]:=B.X; Inc(Count);
      Vertices[Count]:=B.Y; Inc(Count);
      Vertices[Count]:=B.Z; Inc(Count);
      Vertices[Count]:=SC/Texture.Width; Inc(Count);
      Vertices[Count]:=TC/Texture.Height; Inc(Count);
      Vertices[Count]:=(LC/255)*(TintR/255); Inc(Count);
      Vertices[Count]:=(LC/255)*(TintG/255); Inc(Count);
      Vertices[Count]:=(LC/255)*(TintB/255); Inc(Count);
      {$IFDEF ANDROID}
      Vertices[Count]:=1.0; Inc(Count);
      {$ENDIF}
      Vertices[Count]:=C.X; Inc(Count);
      Vertices[Count]:=C.Y; Inc(Count);
      Vertices[Count]:=C.Z; Inc(Count);
      {$ENDIF}
    end;
  end;
end;

function THardwareRasterizer.WasRectOccluded(AX1, AY1, AX2, AY2: Integer; AZ: Single): Boolean;
begin
  Result:=False;
end;

end.
