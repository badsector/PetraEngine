{ Provides a method to get data as streams }
unit DataLoad;
interface

uses
  SysUtils, Classes;

var
  // The data directory prefix
  DataDirectoryPrefix: string;

// Return a stream for the given data file, returns nil if the file is not found
function GetDataStream(AFileName: string): TStream;
// Returns true if the given data file exists
function HasDataFile(AFileName: string): Boolean;

implementation

{$IFDEF ANDROID}
uses
  AndProcs;

type

  { TAssetMemoryStream - Just a read only memory stream }
  TAssetMemoryStream = class(TCustomMemoryStream)
  public
    constructor Create(APointer: Pointer; ASize: PtrInt);
    destructor Destroy; override;
  end;

{ TAssetMemoryStream }
constructor TAssetMemoryStream.Create(APointer: Pointer; ASize: PtrInt);
begin
  inherited Create;
  SetPointer(APointer, ASize);
end;

destructor TAssetMemoryStream.Destroy;
begin
  Procs.FreeMem(Memory);
  inherited Destroy;
end;
{$ELSE}

{$IFDEF EMBEDPAK}
uses
  DataPak, Misc;
{$ENDIF}

type
  { TPakEntry - Single entry in the pak file }
  TPakEntry = record
    Name: string;
    Offset, Size: Cardinal;
  end;

  { TPakStream - Read-only stream for accessing entries in the pak file }
  TPakStream = class(TStream)
  private
    // The pak entry
    Entry: TPakEntry;
    // Current position
    Pos: Int64;
  protected
    // TStream methods
    function GetSize: Int64; override;
    function GetPosition: Int64; override;
  public
    // TStream methods
    function Read(var Buffer; Count: LongInt): LongInt; override;
    function Seek(const Offset: Int64; Origin: TSeekOrigin): Int64; override;
  end;

var
  PakEntries: array of TPakEntry;
  SkipPak: Boolean;
  Pak: TStream;
{$ENDIF}

{ Local Functions }
{$IFNDEF ANDROID}
procedure InitializePak;
var
  I: Integer;
  Magic: array [0..3] of Char;
  Count, DirPos: Cardinal;
begin
  Pak:=nil;
  PakEntries:=nil;
  if SkipPak then Exit;
  for I:=1 to ParamCount do if UpperCase(ParamStr(I))='-NOPAK' then begin
    SkipPak:=True;
    Exit;
  end;
  {$IFNDEF EMBEDPAK}
  if not FileExists('data.pak') then begin
    SkipPak:=True;
    Exit;
  end;
  try
    Pak:=TFileStream.Create('data.pak', fmOpenRead);
    Pak.ReadBuffer(Magic, 4);
    DirPos:=Pak.ReadDWord;
  except
    Pak.Free;
    Pak:=nil;
    SkipPak:=True;
    Exit;
  end;
  if Magic <> 'PEPF' then begin
    Pak.Free;
    Pak:=nil;
    SkipPak:=True;
    Exit;
  end;
  {$ELSE}
  Pak:=TMemoryReadStream.Create(@DataPakBytes, Length(DataPakBytes));
  Pak.ReadBuffer(Magic, 4);
  DirPos:=Pak.ReadDWord;
  {$ENDIF}
  Pak.Position:=DirPos;
  Count:=Pak.ReadDWord;
  SetLength(PakEntries, Count);
  for I:=0 to Count - 1 do begin
    PakEntries[I].Name:=Pak.ReadAnsiString;
    PakEntries[I].Offset:=Pak.ReadDWord;
    PakEntries[I].Size:=Pak.ReadDWord;
  end;
end;

procedure ShutdownPak;
begin
  Pak.Free;
  Pak:=nil;
end;
{$ENDIF}

{ Functions }
function GetDataStream(AFileName: string): TStream;
var
{$IFDEF ANDROID}
  Data: Pointer;
  Size: Int32;
{$ELSE}
  I: Integer;
{$ENDIF}
begin
  {$IFDEF ANDROID}
  Data:=Procs.GetAssetData(PChar(AFileName), @Size);
  if not Assigned(Data) then Exit(nil);
  Result:=TAssetMemoryStream.Create(Data, Size);
  {$ELSE}
  if Assigned(Pak) then begin
    for I:=0 to High(PakEntries) do
      if AFileName=PakEntries[I].Name then begin
        Result:=TPakStream.Create;
        TPakStream(Result).Entry:=PakEntries[I];
        Exit;
      end;
  end;
  try
    Result:=TFileStream.Create(AFileName, fmOpenRead);
  except
    Result:=nil;
  end;
  {$ENDIF}
end;

function HasDataFile(AFileName: string): Boolean;
{$IFNDEF ANDROID}
var
  I: Integer;
{$ENDIF}
begin
  {$IFDEF ANDROID}
  Result:=Procs.HasDataFile(PChar(AFileName)) <> 0;
  {$ELSE}
  if Assigned(Pak) then begin
    for I:=0 to High(PakEntries) do
      if AFileName=PakEntries[I].Name then Exit(True);
  end;
  Result:=FileExists(AFileName);
  {$ENDIF}
end;

{ TPakStream }
function TPakStream.GetSize: Int64;
begin
  Result:=Entry.Size;
end;

function TPakStream.GetPosition: Int64;
begin
  Result:=Pos;
end;

function TPakStream.Read(var Buffer; Count: LongInt): LongInt;
begin
  if Pos + Count > Entry.Size then Count:=Entry.Size - Pos;
  Result:=Count;
  if Count > 0 then begin
    Pak.Position:=Entry.Offset + Pos;
    Result:=Pak.Read(Buffer, Count);
    Pos += Result;
  end else Result:=Count;
end;

function TPakStream.Seek(const Offset: Int64; Origin: TSeekOrigin): Int64;
begin
  case Word(Origin) of
    soFromBeginning: Pos:=Offset;
    soFromEnd: Pos:=Entry.Size + Offset;
    soFromCurrent: Pos += Offset;
  end;
  Result:=Pos;
end;

initialization
  DataDirectoryPrefix:='Data/';
  InitializePak;
finalization
  ShutdownPak;
end.
