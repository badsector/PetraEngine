{ DOS Sound Driver for Sound Blaster 16 }
unit DOSSnd;
{$ASMMODE ATT}

interface

uses
  SndDrv;

type
  { TDOSSoundDriver - DOS Sound Driver }
  TDOSSoundDriver = class(TSoundDriver)
  private
    // Called by the interrupt handler
    procedure OnInterrupt;
  protected
    // TSoundDriver methods
    procedure InitializeDriver; override;
    procedure LockBuffers; override;
    procedure UnlockBuffers; override;
  public
    destructor Destroy; override;
  end;

// Set SB16 settings
procedure ConfigureSB16(APortBase, ADMAChannel, AIRQ: Integer);

implementation

uses
  Engine, Crt, GO32;

var
  // Base for the SB port
  PortBase: Integer = $220;
  // DMA channel to use
  DMAChannel: Integer = 5;
  // SB IRQ
  IRQ: Integer = 5;
  // Interrupt number for the IRQ
  InterruptNumber: Integer;
  // Real mode segment for the data buffer used for DMA transfers
  BufferSegment: Integer;
  // Buffer selector
  BufferSelector: Integer;
  // Current half part to write to
  HalfPart: Integer;
  // Pascal-side interrupt handler pointer
  ProcPtr: Pointer;
  // Interrupt handler addresses
  NewIntHandler, OldIntHandler: TSegInfo;
  // Used by the asm part to refer to the data segment
  BackupDS: Word; external name '___v2prt0_ds_alias';

{ Functions }
// Interrupt handler, asm side
procedure IntHandler; assembler;
asm
  cli
  pushl %ds
  pushl %es
  pushl %fs
  pushl %gs
  pushal
  movw %cs:BackupDS, %ax
  movw %ax, %ds
  movw %ax, %es
  movw dosmemselector, %ax
  movw %ax, %fs
  call *ProcPtr
  popal
  popl %gs
  popl %fs
  popl %es
  popl %ds
  sti
  ljmp %cs:OldIntHandler
end;
procedure IntDummy; begin end; // used to obtain IntHandler size
// Interrupt handler, Pascal side
procedure InterruptHandler;
begin
  // Pass to the driver
  TDOSSoundDriver(GSound).OnInterrupt;
  // Acknowledge interrupt
  InPortB(PortBase + $F);
  OutPortB($20, $20);
end;
procedure InterruptHandlerDummy; begin end; // used to obtain InterruptHandler size

// Reset the DSP
function ResetDSP: Boolean;
var
  I: Integer;
begin
  OutPortB(PortBase + $6, 1);
  Delay(300);
  OutPortB(PortBase + $6, 0);
  for I:=1 to 100 do begin
    if (InPortB(PortBase + $E) and 128)=128 then Break;
    Delay(10);
  end;
  if I=100 then Exit(False);
  for I:=1 to 100 do begin
    if InPortB(PortBase + $A)=$AA then Break;
    Delay(10);
  end;
  Result:=I < 100;
end;
// Write a byte to DSP
procedure WriteToDSP(B: Byte); inline;
begin
  // Wait for the write-buffer status bit 7 to be cleared
  while True do
    if (InPortB(PortBase + $C) and 128)=0 then Break;
  // Write the byte
  OutPortB(PortBase + $C, B);
end;

procedure ConfigureSB16(APortBase, ADMAChannel, AIRQ: Integer);
begin
  PortBase:=APortBase;
  DMAChannel:=ADMAChannel;
  IRQ:=AIRQ;
end;

{ TDOSSoundDriver }
destructor TDOSSoundDriver.Destroy;
begin
  // Shut down
  if Available then begin
    // Stop SB16 playback
    WriteToDSP($D5);
    // Remove interrupt handler
    set_pm_interrupt(InterruptNumber, OldIntHandler);
    unlock_data(SilentCounter, SizeOf(SilentCounter));
    unlock_data(PortBase, SizeOf(PortBase));
    unlock_data(Buffer, SizeOf(Buffer));
    unlock_data(DOSMemSelector, SizeOf(DOSMemSelector));
    unlock_data(ProcPtr, SizeOf(ProcPtr));
    unlock_data(NeedMix, SizeOf(NeedMix));
    unlock_data(GSound, SizeOf(GSound));
    unlock_data(Self, SizeOf(Self));
    unlock_code(@InterruptHandler, Cardinal(@InterruptHandlerDummy) - Cardinal(@InterruptHandler));
    unlock_code(@IntHandler, Cardinal(@IntDummy) - Cardinal(@IntHandler));
  end;
  // Release DOS memory
  if BufferSelector <> 0 then begin
    global_dos_free(BufferSelector);
    BufferSelector:=0;
  end;
  inherited Destroy;
end;

procedure TDOSSoundDriver.OnInterrupt;
begin
  if NeedMix then MixNow;
  if SilentCounter=0 then
    DosMemPut(BufferSegment, SizeOf(Buffer)*HalfPart, Buffer, 4096)
  else
    DosMemFillChar(BufferSegment, SizeOf(Buffer)*HalfPart, 4096, #0);
  HalfPart:=1 - HalfPart;
  NeedMix:=True;
end;

procedure TDOSSoundDriver.InitializeDriver;
var
  R: LongInt;
  DPMIInfo: TDPMIVersionInfo;
  Allocations: array of LongInt = nil;
  I, Linear: Integer;
begin
  Available:=False;
  // Obtain DPMI info so that we'll get the PIC base
  get_dpmi_version(DPMIInfo);
  InterruptNumber:=DPMIInfo.master_pic + IRQ;
  // Allocate DOS memory for DMA transfers - we allocate in a loop that keeps
  // on adding allocations continuously until one doesn't cross a 64K page
  while True do begin
    R:=global_dos_alloc(8192);
    if R=0 then begin
      for I:=High(Allocations) downto 0 do global_dos_free(Allocations[I]);
      Exit;
    end;
    Linear:=Integer(Word(R shr 16)) shl 4;
    if (Linear and $FFFF0000)=((Linear + 8191) and $FFFF0000) then Break;
    SetLength(Allocations, Length(Allocations) + 1);
    Allocations[High(Allocations)]:=Word(R);
  end;
  for I:=High(Allocations) downto 0 do global_dos_free(Allocations[I]);
  BufferSegment:=Word(R shr 16);
  BufferSelector:=Word(R);
  // Align the buffer
  if ((BufferSegment shl 4) and $FFFF)=0 then
    BufferSegment:=(((BufferSegment shl 4) and $FFFF0000) + $FFFF) shr 4;
  // Zero out the buffer
  DosMemFillChar(BufferSegment, 0, 8192, #0);
  // Reset the DSP
  if not ResetDSP then begin
    global_dos_free(BufferSelector);
    Exit;
  end;
  // Install interrupt handler
  ProcPtr:=@InterruptHandler;
  lock_data(Self, SizeOf(Self));
  lock_data(GSound, SizeOf(GSound));
  lock_data(NeedMix, SizeOf(NeedMix));
  lock_data(ProcPtr, SizeOf(ProcPtr));
  lock_data(DOSMemSelector, SizeOf(DOSMemSelector));
  lock_data(Buffer, SizeOf(Buffer));
  lock_data(PortBase, SizeOf(PortBase));
  lock_data(SilentCounter, SizeOf(SilentCounter));
  lock_code(@InterruptHandler, Cardinal(@InterruptHandlerDummy) - Cardinal(@InterruptHandler));
  lock_code(@IntHandler, Cardinal(@IntDummy) - Cardinal(@IntHandler));
  NewIntHandler.Offset:=@IntHandler;
  NewIntHandler.Segment:=get_cs;
  OutPortB($21, InPortB($21) and ($FF - (1 shl IRQ)));
  get_pm_interrupt(InterruptNumber, OldIntHandler);
  set_pm_interrupt(InterruptNumber, NewIntHandler);
  // Disable SB DMA transfers
  OutPortB($D4, 4 + (DMAChannel mod 4));
  // Reset DMA flip-flop
  OutPortB($D8, 11 { whatever });
  // Configure transfer mode
  OutPortB($D6, $58 + (DMAChannel mod 4));
  // Set buffer address
  OutPortB($C0 + (DMAChannel - 4)*4, ((BufferSegment*8) mod 65536) and $FF);
  OutPortB($C0 + (DMAChannel - 4)*4, ((BufferSegment*8) mod 65536) shr 8);
  // Set buffer size
  OutPortB($C0 + (DMAChannel - 4)*4 + 2, 255); // 4096-1 low byte
  OutPortB($C0 + (DMAChannel - 4)*4 + 2, 15);  // 4096-1 high byte
  // Set DMA page
  case DMAChannel of
    5: OutPortB($8B, (BufferSegment shl 4) shr 16);
    6: OutPortB($89, (BufferSegment shl 4) shr 16);
    7: OutPortB($8A, (BufferSegment shl 4) shr 16);
  end;
  // Enable SB DMA transfers for DMA
  OutPortB($D4, DMAChannel - 4);
  // Set 22050Hz playback rate
  WriteToDSP($41);
  WriteToDSP($56);
  WriteToDSP($22);
  // Enable playback
  WriteToDSP($B6);
  WriteToDSP($30);
  WriteToDSP(255);
  WriteToDSP(7);
  // Setup DMA block size (half size)
  WriteToDSP($48);
  WriteToDSP(255);
  WriteToDSP(7);
  //
  WriteToDSP($D6);
  WriteToDSP($D1);
  Available:=True;
end;

procedure TDOSSoundDriver.LockBuffers;
begin
  asm
    cli
  end;
end;

procedure TDOSSoundDriver.UnlockBuffers;
begin
  asm
    sti
  end;
end;

end.

