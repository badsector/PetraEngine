// DOS graphics driver
unit DOSGfx;
interface

uses
  GfxDrv, Colors, Raster, FBRaster;

type

  { TDOSGraphicsDriver }

  TDOSGraphicsDriver = class(TGraphicsDriver)
  private
    // Backbuffer
    FBackBuffer: PFrameData;
    // Last set palette
    Palette: TColorPalette;
    // Current gamma
    Gamma: TGammaValue;
    // Software rasterizer
    Rasterizer: TFramebufferRasterizer;
  public
    // TGraphicsDriver methods
    procedure Initialize; override;
    procedure Shutdown; override;
    function BeginFrame: PFrameData; override;
    procedure FinishFrame; override;
    procedure SetColorPalettePart(const APalette: TColorPalette; First, Last: Integer); override;
    procedure SetGamma(AGamma: TGammaValue); override;
    function GetRasterizer: TRasterizer; override;
  end;

implementation

uses
  GO32;

{ TDOSGraphicsDriver }
procedure TDOSGraphicsDriver.Initialize;
var
  R: TRealRegs;
begin
  Palette:=TColorPalette.Create;
  FillChar(R, SizeOf(R), 0);
  R.AX:=$13;
  RealIntr($10, R);
  FBackBuffer:=GetMem(HRes*VRes);
  Rasterizer:=TFramebufferRasterizer.Create;
  Gamma:=0;
end;

procedure TDOSGraphicsDriver.Shutdown;
var
  R: TRealRegs;
begin
  Rasterizer.Free;
  Rasterizer:=nil;
  FreeMem(FBackBuffer);
  FBackBuffer:=nil;
  Palette.Free;
  FillChar(R, SizeOf(R), 0);
  R.AX:=$3;
  RealIntr($10, R);
end;

function TDOSGraphicsDriver.BeginFrame: PFrameData;
begin
  Result:=FBackBuffer;
end;

procedure TDOSGraphicsDriver.FinishFrame;
begin
  seg_move(get_ds, LongInt(FBackBuffer), DOSMemSelector, $A0000, 64000);
end;

procedure TDOSGraphicsDriver.SetColorPalettePart(const APalette: TColorPalette; First, Last: Integer);
var
  Value: Integer;
begin
  outportb($3C8, First);
  while First <= Last do begin
    Palette.Red[First]:=APalette.Red[First];
    Palette.Green[First]:=APalette.Green[First];
    Palette.Blue[First]:=APalette.Blue[First];
    Value:=Round(Palette.Red[First]*(1.0 + Gamma/High(TGammaValue)) + Gamma);
    if Value > 255 then Value:=255;
    outportb($3C9, Value shr 2);
    Value:=Round(Palette.Green[First]*(1.0 + Gamma/High(TGammaValue)) + Gamma);
    if Value > 255 then Value:=255;
    outportb($3C9, Value shr 2);
    Value:=Round(Palette.Blue[First]*(1.0 + Gamma/High(TGammaValue)) + Gamma);
    if Value > 255 then Value:=255;
    outportb($3C9, Value shr 2);
    Inc(First);
  end;
end;

procedure TDOSGraphicsDriver.SetGamma(AGamma: TGammaValue);
begin
  Gamma:=AGamma;
  SetColorPalette(Palette);
end;

function TDOSGraphicsDriver.GetRasterizer: TRasterizer;
begin
  Result:=Rasterizer;
end;

end.
