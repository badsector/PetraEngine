// DOS input driver
unit DOSInput;
{$ASMMODE ATT}

interface

uses
  InputDrv;

type

  TDOSInputDriver = class(TInputDriver)
  private
    HasMouse: Boolean;
    Left, Middle, Right: Boolean;
  public
    // Constructor blah blah
    constructor Create;
    // Destructor blah blah
    destructor Destroy; override;
  public
    // TInputDriver methods
    function IsLive: Boolean; override;
  end;

implementation

uses
  Crt, GO32;

type
  TKBEvent = record
    Key: TInputKey;
    Down: Boolean;
  end;

var
  ProcPtr: Pointer;
  NewInt9Handler, OldInt9Handler: TSegInfo;
  backupDS : Word; external name '___v2prt0_ds_alias';
  KBEvent: array [0..255] of TKBEvent;
  KBEventCopy: array [0..255] of TKBEvent;
  KBEvents: Integer;

{ Functions }

procedure Int9Handler; assembler;
asm
  cli
  pushl %ds
  pushl %es
  pushl %fs
  pushl %gs
  pushal
  movw %cs:backupDS, %ax
  movw %ax, %ds
  movw %ax, %es
  movw dosmemselector, %ax
  movw %ax, %fs
  call *ProcPtr
  popal
  popl %gs
  popl %fs
  popl %es
  popl %ds
  sti
  iret
end;
procedure Int9Dummy; begin end; // used to obtain KeyboardHandler size

var
  Buffer: Byte = 0;
procedure KeyboardHandler;
var
  B, Code: Byte;
  Ev: TKBEvent;
begin
  // Read keyboard buffer
  B:=InPortB($60);
  // Extended key follows, store it for the next call
  if B in [$E0..$E2] then
    Buffer:=B
  else begin
    // Check key state
    Ev.Down:=(B and $80)=0;
    // Extract code
    Code:=B and $7F;
    // Resolve code
    if Buffer=0 then begin
      case Code of
        1: Ev.Key:=ikEscape;
        59..68: Ev.Key:=TInputKey(Integer(ikF1) + (Code - 59));
        70: Ev.Key:=ikScrollLock;
        41: Ev.Key:=ikBackQuote;
        2..10: Ev.Key:=TInputKey(Integer(ik1) + (Code - 2));
        11: Ev.Key:=ik0;
        12: Ev.Key:=ikMinus;
        13: Ev.Key:=ikEquals;
        14: Ev.Key:=ikBackspace;
        82: Ev.Key:=ikInsert;
        71: Ev.Key:=ikHome;
        73: Ev.Key:=ikPageUp;
        81: Ev.Key:=ikPageDown;
        15: Ev.Key:=ikTab;
        30: Ev.Key:=ikA;
        48: Ev.Key:=ikB;
        46: Ev.Key:=ikC;
        32: Ev.Key:=ikD;
        18: Ev.Key:=ikE;
        33: Ev.Key:=ikF;
        34: Ev.Key:=ikG;
        35: Ev.Key:=ikH;
        23: Ev.Key:=ikI;
        36: Ev.Key:=ikJ;
        37: Ev.Key:=ikK;
        38: Ev.Key:=ikL;
        50: Ev.Key:=ikM;
        49: Ev.Key:=ikN;
        24: Ev.Key:=ikO;
        25: Ev.Key:=ikP;
        16: Ev.Key:=ikQ;
        19: Ev.Key:=ikR;
        31: Ev.Key:=ikS;
        20: Ev.Key:=ikT;
        22: Ev.Key:=ikU;
        47: Ev.Key:=ikV;
        17: Ev.Key:=ikW;
        45: Ev.Key:=ikX;
        21: Ev.Key:=ikY;
        44: Ev.Key:=ikZ;
        26: Ev.Key:=ikLeftBracket;
        27: Ev.Key:=ikRightBracket;
        43: Ev.Key:=ikBackSlash;
        28: Ev.Key:=ikEnter;
        58: Ev.Key:=ikCapsLock;
        39: Ev.Key:=ikSemiColon;
        40: Ev.Key:=ikQuote;
        42, 54: if Buffer=0 then Ev.Key:=ikShift;
        51: Ev.Key:=ikComma;
        52: Ev.Key:=ikPeriod;
        53: Ev.Key:=ikSlash;
        29: Ev.Key:=ikControl;
        56: Ev.Key:=ikAlt;
        57: Ev.Key:=ikSpace;
        83: Ev.Key:=ikDelete;
        79: Ev.Key:=ikEnd;
        75: Ev.Key:=ikLeft;
        77: Ev.Key:=ikRight;
        72: Ev.Key:=ikUp;
        80: Ev.Key:=ikDown;
        69: Ev.Key:=ikNumLock;
        else Ev.Key:=ikUnknown;
      end;
    end else if Buffer=$E0 then begin
      case Code of
        82: Ev.Key:=ikInsert;
        71: Ev.Key:=ikHome;
        73: Ev.Key:=ikPageUp;
        81: Ev.Key:=ikPageDown;
        28: Ev.Key:=ikEnter;
        53: Ev.Key:=ikSlash;
        29: Ev.Key:=ikControl;
        56: Ev.Key:=ikAlt;
        83: Ev.Key:=ikDelete;
        79: Ev.Key:=ikEnd;
        75: Ev.Key:=ikLeft;
        77: Ev.Key:=ikRight;
        72: Ev.Key:=ikUp;
        80: Ev.Key:=ikDown;
        else Ev.Key:=ikUnknown;
      end;
      Buffer:=0;
    end else begin
      Ev.Key:=ikUnknown;
      Buffer:=0;
    end;
    // Add key event to the events buffer
    if (Ev.Key <> ikUnknown) and (KBEvents < 255) then begin
      KBEvent[KBEvents]:=Ev;
      Inc(KBEvents);
    end;
  end;
  // Acknowledge data retrieval
  B:=InPortB($61);
  OutPortB($61, B or $80);
  OutPortB($61, B);
  // Acknowledge hardware interrupt
  OutPortB($20, $20);
end;
procedure KeyboardHandlerDummy; begin end; // used to obtain KeyboardHandler size

procedure InstallKeyboardHandler;
begin
  ProcPtr:=@KeyboardHandler;
  lock_data(ProcPtr, SizeOf(ProcPtr));
  lock_data(DOSMemSelector, SizeOf(DOSMemSelector));
  lock_data(KBEvent, SizeOf(KBEvent));
  lock_data(KBEvents, SizeOf(KBEvents));
  lock_data(Buffer, 1);
  lock_code(@KeyboardHandler, Cardinal(@KeyboardHandlerDummy) - Cardinal(@KeyboardHandler));
  lock_code(@Int9Handler, Cardinal(@Int9Dummy) - Cardinal(@Int9Handler));
  NewInt9Handler.Offset:=@Int9Handler;
  NewInt9Handler.Segment:=get_cs;
  get_pm_interrupt($9, OldInt9Handler);
  set_pm_interrupt($9, NewInt9Handler);
end;

procedure UninstallKeyboardHandler;
begin
  set_pm_interrupt($9, OldInt9Handler);
  unlock_data(Buffer, 1);
  unlock_data(KBEvents, SizeOf(KBEvents));
  unlock_data(KBEvent, SizeOf(KBEvent));
  unlock_data(DOSMemSelector, SizeOf(DOSMemSelector));
  unlock_data(ProcPtr, SizeOf(ProcPtr));
  unlock_code(@KeyboardHandler, Cardinal(@KeyboardHandlerDummy) - Cardinal(@KeyboardHandler));
  unlock_code(@Int9Handler, Cardinal(@Int9Dummy) - Cardinal(@Int9Handler));
end;

{ TDOSInputDriver }
constructor TDOSInputDriver.Create;
var
  R: TRealRegs;
begin
  // Install the keyboard handler
  InstallKeyboardHandler;
  // Reset and check for mouse
  FillChar(R, SizeOf(R), 0);
  RealIntr($33, R);
  HasMouse:=R.AX=$FFFF;
  // Init mouse
  if HasMouse then begin
    // Hide the mouse
    FillChar(R, SizeOf(R), 0);
    R.AX:=2;
    RealIntr($33, R);
    // (practically) Disable speed doubling
    FillChar(R, SizeOf(R), 0);
    R.AX:=$13;
    R.DX:=$FFFF;
    RealIntr($33, R);
  end;
  inherited Create;
end;

destructor TDOSInputDriver.Destroy;
var
  R: TRealRegs;
begin
  // Reset mouse
  if HasMouse then begin
    FillChar(R, SizeOf(R), 0);
    RealIntr($33, R);
  end;
  // Uninstal keyboard handler
  UninstallKeyboardHandler;
  inherited Destroy;
end;

function TDOSInputDriver.IsLive: Boolean;
var
  R: TRealRegs;
  NewLeft, NewMiddle, NewRight: Boolean;
  DX: SmallInt absolute R.CX;
  DY: SmallInt absolute R.DX;
  I, Count: Integer;
begin
  // Check for mouse events
  if HasMouse then begin
    // Get current mouse state
    FillChar(R, SizeOf(R), 0);
    R.AX:=3;
    RealIntr($33, R);
    NewLeft:=(R.BX and 1)=1;
    NewMiddle:=(R.BX and 4)=4;
    NewRight:=(R.BX and 2)=2;
    if NewLeft <> Left then begin
      NotifyHandlersForKey(ikLeftButton, NewLeft);
      Left:=NewLeft;
    end;
    if NewMiddle <> Middle then begin
      NotifyHandlersForKey(ikMiddleButton, NewMiddle);
      Middle:=NewMiddle;
    end;
    if NewRight <> Right then begin
      NotifyHandlersForKey(ikRightButton, NewRight);
      Right:=NewRight;
    end;
    // Get mickey counters
    FillChar(R, SizeOf(R), 0);
    R.AX:=$B;
    RealIntr($33, R);
    if (DX <> 0) or (DY <> 0) then
      NotifyHandlersForMouse(DX, DY);
  end;
  // Disable interrupts so that we can access the keyboard event buffer
  asm cli end;
  // Copy buffered events to a buffer outside of interrupts
  if KBEvents > 0 then begin
    Move(KBEvent[0], KBEventCopy[0], SizeOf(TKBEvent)*KBEvents);
    Count:=KBEvents;
    KBEvents:=0;
  end else Count:=0;
  // Restore interrupts
  asm sti end;
  // Inform the handlers
  for I:=0 to Count - 1 do NotifyHandlersForKey(KBEventCopy[I].Key, KBEventCopy[I].Down);
  Result:=True;
end;

end.
