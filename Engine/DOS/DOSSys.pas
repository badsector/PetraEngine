{ DOS system driver }
unit DOSSys;

interface

uses
  SysDrv;

type
  { TDOSSystemDriver - DOS specific system driver }
  TDOSSystemDriver = class(TSystemDriver)
  private
    Base, Freq: UInt64;
    function GetRDTSC: UInt64;
  protected
    function GetMilliseconds: Integer; override;
  public
    constructor Create;
  end;

implementation

uses
  GO32;

{ Functions }
function GetRTCSeconds: Integer;
var
  R: TRealRegs;
begin
  FillChar(R, SizeOf(R), 0);
  R.AH:=2;
  RealIntr($1A, R);
  Result:=Round((R.DX or (R.CX shl 16))/18.206);
end;

{ TDOSSystemDriver }
constructor TDOSSystemDriver.Create;
var
  Pre: Integer;
begin
  inherited Create;
  Pre:=GetRTCSeconds;
  while Pre=GetRTCSeconds do;
  Base:=GetRDTSC;
  Pre:=GetRTCSeconds;
  while Pre=GetRTCSeconds do;
  Pre:=GetRTCSeconds;
  while Pre=GetRTCSeconds do;
  Pre:=GetRTCSeconds;
  while Pre=GetRTCSeconds do;
  Freq:=(GetRDTSC - Base) div 3000;
end;

function TDOSSystemDriver.GetRDTSC: UInt64;
type
  TLoHi32 = packed record
    Lo, Hi: Cardinal;
  end;
var
  Parts: TLoHi32;
  Full: UInt64 absolute Parts;
begin
  {$ASMMODE Intel}
  asm
    rdtsc
    mov Parts.Lo, eax
    mov parts.Hi, edx
  end;
  Result:=Full;
end;

function TDOSSystemDriver.GetMilliseconds: Integer;
begin
  Result:=(GetRDTSC - Base) div Freq;
end;

end.
