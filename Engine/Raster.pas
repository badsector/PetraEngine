// Base class for the rasterizer
unit Raster;

interface

uses
  Maths, Colors, Textures;

type
  PRasterVertex = ^TRasterVertex;

  {$IFDEF PETRAHW}
  { TRasterTexture - base class for hardware accelerated textures }
  TRasterTexture = class
  public
    // Triangle batch index
    TriBatch: Integer;
  public
    // Set the texture data
    procedure SetData(AWidth, AHeight: Integer; AFiltered: Boolean; APalette: TColorPalette; const Texels); virtual; abstract;
  end;
  {$ENDIF}

  { TRasterVertex - single vertex used by the rasterizer }
  TRasterVertex = object
    // The vertex position (X, Y must be in screen coordinates)
    X, Y: Integer;
    // Texture coordinates
    S, T: Byte;
    // Light level
    L: Byte;
  end;

  { TRasterizer - base class for the rasterizer }
  TRasterizer = class
  protected
    // Tinting color
    TintR, TintG, TintB: Byte;
  public
    constructor Create;
    {$IFDEF PETRAHW}
    // CReate a new hardware accelerated texture
    function CreateTexture: TRasterTexture; virtual; abstract;
    // Start a new frame, returns false if impossible
    function BeginFrame: Boolean; virtual; abstract;
    {$ELSE}
    // Start a new frame on the given frame data with the given color palette,
    // returns false if impossible
    function BeginFrame(APalette: TColorPalette; AFrameData: Pointer): Boolean; virtual; abstract;
    {$ENDIF}
    // Finish the current frame
    procedure FinishFrame; virtual; abstract;
    {$IFDEF PETRAHW}
    // Clear the current frame with the given color
    procedure Clear(R, G, B: Single); virtual; abstract;
    // Setup matrices
    procedure SetMatrices(Projection, View: TMatrix); virtual; abstract;
    // Setup fog
    procedure SetFog(FogR, FogG, FogB, FogStart, FogEnd: Single); virtual; abstract;
    // Render the given triangles (this is an array of TRenderTri types as
    // defined in the Renderer unit)
    procedure RenderTriangles(Count: Integer; const RenderTris); virtual; abstract;
    // Render the given quad in screen space using the given texture and blend factor
    procedure RenderQuad(X1, Y1, S1, T1, X2, Y2, S2, T2: Integer; Blend: Single; Texture: TTexture); virtual; abstract;
    {$ELSE}
    // Clear the current frame with the given color
    procedure Clear(Color: Byte); virtual; abstract;
    {$ENDIF}
    // Setup tinting
    procedure SetTint(ATintR, ATintG, ATintB: Byte);
    {$IFNDEF PETRAHW}
    // Draw a single pixel at the given position
    procedure SetPixel(X, Y: Integer; Color: Byte); virtual; abstract;
    // Set the pixel area at the given rectangle
    procedure SetPixels(X1, Y1, X2, Y2: Integer; var Data); virtual; abstract;
    // Grab the pixel area at the given rectangle
    procedure GetPixels(X1, Y1, X2, Y2: Integer; var Data); virtual; abstract;
    // Set the texture source
    procedure SetTexture(const Texture: TTexture); virtual; abstract;
    // Draw a triangle using the current texture. TriZ is the sorting reference
    procedure DrawTriangle(A, B, C: PRasterVertex; Occluder: Boolean; TriZ: Single); virtual; abstract;
    {$ENDIF}
    // Draw part of the given texture on screen for HUD display using index 255
    // as a transparent color.  The part is specified by the SX,SY top left
    // corner in the texture and its size is SW by SH.
    procedure DrawHUDImage(X, Y: Integer; Texture: TTexture; SX, SY, SW, SH: Integer); virtual; abstract;
    // Check if the given screen-space rectangle at given Z would be occluded
    // in the last frame
    function WasRectOccluded(AX1, AY1, AX2, AY2: Integer; AZ: Single): Boolean; virtual; abstract;
  end;

implementation

{ TRasterizer }

constructor TRasterizer.Create;
begin
  TintR:=255;
  TintG:=255;
  TintB:=255;
end;

procedure TRasterizer.SetTint(ATintR, ATintG, ATintB: Byte);
begin
  TintR:=ATintR;
  TintG:=ATintG;
  TintB:=ATintB;
end;

end.
