{ Sound data }
unit Sounds;

interface

uses
  Misc, ResMan;

type
  { TSoundClip - A single sound clip }
  TSoundClip = class(TResource)
  private
    // Property storage
    FData: PByte;
    FLength: Integer;
    FUsedCount: Integer;
  public
    destructor Destroy; override;
    // Load a sound clip from the given WAV file
    procedure LoadWAVFile(AFileName: string);
    // Clip data
    property Data: PByte read FData;
    // Clip length
    property Length: Integer read FLength;
    // Number of channels that currently use the clip
    property UsedCount: Integer read FUsedCount write FUsedCount;
  end;

implementation

uses
  Classes, DataLoad;

destructor TSoundClip.Destroy;
begin
  if Assigned(FData) then FreeMem(FData);
  inherited Destroy;
end;

{ TSoundClip }
procedure TSoundClip.LoadWAVFile(AFileName: string);
var
  F: TStream;
  RIFF, WAVE, FMT, DATA_: array [0..3] of AnsiChar;
  W: UInt16;
  L: UInt32;
begin
  // Release any previous data
  if Assigned(FData) then FreeMem(FData);
  FData:=nil;
  FLength:=0;
  // Open the file
  F:=GetDataStream(AFileName);
  if not Assigned(F) then Exit;
  // Load header
  RIFF:='    ';
  WAVE:='    ';
  FMT:='    ';
  DATA_:='    ';
  L:=0;
  W:=0;
  F.Read(RIFF, 4);
  F.Read(L, 4);
  F.Read(WAVE, 4);
  F.Read(FMT, 4);
  if (RIFF <> 'RIFF') or (WAVE <> 'WAVE') or (FMT <> 'fmt ') then begin
    F.Free;
    Exit;
  end;
  // Format data size
  F.Read(L, 4);
  if L <> 16 then begin
    F.Free;
    Exit;
  end;
  // Type (1=PCM)
  F.Read(W, 2);
  if W <> 1 then begin
    F.Free;
    Exit;
  end;
  // Number of channels (1=Mono)
  F.Read(W, 2);
  if W <> 1 then begin
    F.Free;
    Exit;
  end;
  // Sample rate
  F.Read(L, 4);
  if L <> 22050 then begin
    F.Free;
    Exit;
  end;
  // Skip these two
  F.Read(L, 4);
  F.Read(W, 2);
  // Bits per sample
  F.Read(W, 2);
  if W <> 8 then begin
    F.Free;
    Exit;
  end;
  // Data chunk
  F.Read(DATA_, 4);
  if DATA_ <> 'data' then begin
    F.Free;
    Exit;
  end;
  // Number of samples (actually, size of the data chunk)
  F.Read(L, 4);
  FLength:=L;
  // Read the data
  FData:=GetMem(L);
  F.Read(FData^, L);
  F.Free;
end;

end.
