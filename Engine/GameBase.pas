{ Engine side for the game }
unit GameBase;

interface

uses
  Misc, SysUtils, Serial, World, Scene, Cameras, GridMap, Scripts;

type
  { TGameBase - Base class for the game }
  TGameBase = class(TSerializable)
  private
    // Property storage
    FWorld: TWorld;
    FScene: TScene;
    FInitialized: Boolean;
    FPlayer: TEntity;
    FCameraOverride: TCamera;
    FDataTable: TStringKeyValueSet;
    FMilliseconds: Integer;
    function GetDataTable: PStringKeyValueSet; inline;
  private
    // If this is not an empty string, this map will be played at the next
    // update. This is set via 'map' to avoid referencing freed objects after
    // a map switch
    PlayMapInNextUpdate: string;
    // Frame counter for benchmark mode
    FrameCounter: Integer;
    // Map load milliseconds marker
    MapLoadMilliseconds: Single;
    // Last known grid the camera was in
    LastCameraGrid: TGrid;
    // Last applied tint speed
    LastTintSpeed: Single;
    // Called on object deserialization updates during map load
    procedure MapLoadDeserializationUpdate(Progress: Single);
    // Check the camera position and update any state if needed
    procedure CheckCamera;
  protected
    // TSerializable methods
    procedure BeforeDeserialization; override;
    function DeserializeUnknownProperty(APropName: string; AContext: TSerializationContext): Boolean; override;
    procedure SerializeExtraData(AContext: TSerializationContext); override;
    procedure DeserializeExtraData(AContext: TSerializationContext); override;
  protected
    // Called to inititialize a world after loading it
    procedure InitWorld; virtual;
    // Called to initialize the game
    procedure InitGame; virtual;
    // Called to shutdown the game
    procedure ShutdownGame; virtual;
  public
    constructor Create; override;
    destructor Destroy; override;
    // Initialize the game
    procedure Initialize;
    // Shutdown the game
    procedure Shutdown;
    // Play the given map
    procedure PlayMap(AMapName: string);
    // Called every update cycle
    procedure Update; virtual;
    {$IFDEF EDITOR}
    // Called to initialize the world for playing in the editor
    procedure InitWorldForPIE;
    // Called every update cycle in the editor
    procedure EditorUpdate; virtual;
    {$ENDIF}
    // Called every frame before rendering happens
    procedure Frame; virtual;
    // Called every frame after rendering happens but before any UI
    procedure PostFrame; virtual;
    // Run a global game script command, return false if not handled. This is
    // actually called by TScript.RunLine for commands that the script engine
    // itself nor the associated object/entity could handle as a last resort
    function RunScriptCommand(Script: TScript; Args: TStringArray): Boolean; virtual;
    // Save the current world to the given file
    procedure SaveWorld(AFileName: string);
    // Load a world stored in the given file, pass empty string for empty world
    procedure LoadWorld(AFileName: string);
    // Provide the benchmark results (called right before the engine exits when
    // in benchmark mode)
    procedure GetBenchmarkResults(out FrameCount, FPS: Integer);
    // True if the game was initialized
    property Initialized: Boolean read FInitialized;
    // This game's scene
    property Scene: TScene read FScene;
    // The entity that represents the player
    property Player: TEntity read FPlayer write FPlayer;
    // Camera override
    property CameraOverride: TCamera read FCameraOverride write FCameraOverride;
    // Game-wide data table
    property DataTable: PStringKeyValueSet read GetDataTable;
    // Game time in milliseconds, it is updated via Update
    property Milliseconds: Integer read FMilliseconds;
  published
    // This game's world
    property World: TWorld read FWorld;
  end;
  
implementation

uses
  GfxDrv, Classes, DataLoad, Engine, CamEnt;

constructor TGameBase.Create;
begin
  inherited Create;
  // Create the scene
  FScene:=TScene.Create;
  // Create the world
  FWorld:=TWorld.Create;
  FWorld.Scene:=Scene;
end;

destructor TGameBase.Destroy;
begin
  // Shutdown the game
  Shutdown;
  // Free the world and scene
  FWorld.Free;
  FScene.Free;
  inherited Destroy;
end;

procedure TGameBase.Initialize;
begin
  if Initialized then Exit;
  // Initialize game time
  FMilliseconds:=0;
  // Reset state
  LastCameraGrid:=nil;
  // Initialize the game
  InitGame;
  // Set flag
  FInitialized:=True;
end;

procedure TGameBase.Shutdown;
begin
  if not Initialized then Exit;
  // Shutdown the game
  ShutdownGame;
  // Recreate an empty world and scene
  FWorld.Free;
  FScene.Free;
  FScene:=TScene.Create;
  FWorld:=TWorld.Create;
  FWorld.Scene:=Scene;
  // Set flag
  FInitialized:=False;
end;

procedure TGameBase.PlayMap(AMapName: string);
begin
  // Check user maps first
  if HasDataFile('User/' + AMapName + '.map') then
    // Load the usermap world
    LoadWorld('User/' + AMapName + '.map')
  // Check base map next
  else if HasDataFile(DataDirectoryPrefix + AMapName + '.map') then
    // Load the base map world
    LoadWorld(DataDirectoryPrefix + AMapName + '.map')
  // Map not found, load "empty" world
  else
    LoadWorld('');
  // Run the startup script
  if not GBenchmark then World.RunStartupScript;
  // Save milliseconds
  MapLoadMilliseconds:=GSystem.Milliseconds;
end;

function TGameBase.GetDataTable: PStringKeyValueSet;
begin
  Result:=@FDataTable;
end;

procedure TGameBase.MapLoadDeserializationUpdate(Progress: Single);
const
  LastUpdateMS: Integer = 0;
begin
  // Ignore if we updated too recently to avoid loading times getting vsync'ed
  if GSystem.Milliseconds - LastUpdateMS < 5 then Exit;
  LastUpdateMS:=GSystem.Milliseconds;
  // Draw a progress bar at the bottom right side with the progress
  GRenderer.BeginFrame;
  GRenderer.FillRectangle(HalfHRes - 101, VRes - 15, HalfHRes + 101, VRes - 5, 0);
  GRenderer.DrawRectangle(HalfHRes - 101, VRes - 15, HalfHRes + 101, VRes - 5, 15);
  GRenderer.FillRectangle(HalfHRes - 100, VRes - 14, HalfHRes - 100 + Round(Progress*200), VRes - 6, 15);
  GRenderer.FinishFrame;
  {$IFDEF PETRAHW}
  GRenderer.DrawShadowTextures;
  {$ENDIF}
  GRenderer.GraphicsDriver.FinishFrame;
end;

procedure TGameBase.CheckCamera;
var
  CameraGrid: TGrid = nil;
begin
  if Assigned(World) and Assigned(GRenderer.Camera) then begin
    if Assigned(CameraOverride) then
      CameraGrid:=World.GridMap.GridAt(CameraOverride.Position)
    else
      CameraGrid:=World.GridMap.GridAt(GRenderer.Camera.Position)
  end;
  if CameraGrid <> LastCameraGrid then begin
    if Assigned(CameraGrid) and CameraGrid.TintOverride then begin
      LastTintSpeed:=CameraGrid.TintOverrideSpeed;
      World.SetTintOverride(CameraGrid.TintOverrideRed, CameraGrid.TintOverrideGreen, CameraGrid.TintOverrideBlue, LastTintSpeed);
    end else
      World.UnsetTintOverride(LastTintSpeed);
    LastCameraGrid:=CameraGrid;
  end;
end;

procedure TGameBase.BeforeDeserialization;
begin
  inherited BeforeDeserialization;
  // Destroy the world since a stored one will be loaded in
  FreeAndNil(FWorld);
end;

function TGameBase.DeserializeUnknownProperty(APropName: string;
  AContext: TSerializationContext): Boolean;
begin
  // Handle removed Player property (used to be published)
  if APropName='Player' then begin
    // Read the object... and ignore it
    AContext.ReadObject;
    Exit(True);
  end;
  // Handle CameraOverride that was mistakenly put in published section
  if APropName='CameraOverride' then begin
    // Read the object... and ignore it
    AContext.ReadObject;
    Exit(True);
  end;
  Result:=False;
end;

procedure TGameBase.SerializeExtraData(AContext: TSerializationContext);
begin
  inherited SerializeExtraData(AContext);
  // Serialize game-wide data
  AContext.WriteStringKeyValueSet(FDataTable);
end;

procedure TGameBase.DeserializeExtraData(AContext: TSerializationContext);
begin
  inherited DeserializeExtraData(AContext);
  // Deserialize game-wide data
  if AContext.Version >= 4 then
    AContext.ReadStringKeyValueSet(FDataTable)
  else
    FDataTable.Clear;
end;

procedure TGameBase.InitWorld;
begin
end;

procedure TGameBase.InitGame;
begin
end;

procedure TGameBase.ShutdownGame;
begin
end;

procedure TGameBase.Update;
var
  Temp: string;
begin
  // Change map if requested
  if PlayMapInNextUpdate <> '' then begin
    Temp:=PlayMapInNextUpdate;
    PlayMapInNextUpdate:='';
    PlayMap(Temp);
    Exit;
  end;
  // Update game time
  Inc(FMilliseconds, 14);
  // Update the camera
  CheckCamera;
  // Update the world if we are in play mode
  if GPlayMode <> pmEditor then World.Update;
end;

{$IFDEF EDITOR}
procedure TGameBase.InitWorldForPIE;
begin
  // Initialize the entities
  World.InitializeEntities;
  // Initialize game
  InitWorld;
end;

procedure TGameBase.EditorUpdate;
begin
  // Update the camera
  CheckCamera;
  // Update world
  World.EditorUpdate;
end;
{$ENDIF}

procedure TGameBase.Frame;
begin
  // If we're in play mode set the camera to use the scene's current camera
  // (otherwise the editor camera will already be set by the editor before
  // this method is called)
  if GPlayMode=pmPlaying then begin
    // Check if we have a camera override
    if Assigned(CameraOverride) then
      GRenderer.Camera:=CameraOverride
    else
      // Set the scene camera
      GRenderer.Camera:=Scene.Camera;
  end;
  // Inform the world
  World.Frame;
  // Render the scene
  Scene.Render;
  // Increase frame counter for benchmark
  if GBenchmark then Inc(FrameCounter);
end;

procedure TGameBase.PostFrame;
begin
end;

function TGameBase.RunScriptCommand(Script: TScript; Args: TStringArray): Boolean;
var
  Entity: TEntity;
  Grid: TGrid;
begin
  // Command: map mapfile
  if Args[0]='map' then begin
    if Length(Args) < 2 then Exit(True);
    PlayMapInNextUpdate:=Args[1];
    Exit(True);
  end;
  // Command: wset name value
  if Args[0]='wset' then begin
    if Length(Args) < 3 then Exit(True);
    World.DataTable^.SetValue(Args[1], Args[2]);
    Exit(True);
  end;
  // Command: gset name value
  if Args[0]='gset' then begin
    if Length(Args) < 3 then Exit(True);
    FDataTable.SetValue(Args[1], Args[2]);
    Exit(True);
  end;
  // Command: wget var name
  if Args[0]='wget' then begin
    if Length(Args) < 3 then Exit(True);
    Script.SetVar(Args[1], World.DataTable^.GetValue(Args[2]));
    Exit(True);
  end;
  // Command: gget var name
  if Args[0]='gget' then begin
    if Length(Args) < 3 then Exit(True);
    Script.SetVar(Args[1], FDataTable.GetValue(Args[2]));
    Exit(True);
  end;
  // Command: dget var name
  if Args[0]='dget' then begin
    if Length(Args) < 3 then Exit(True);
    Script.SetVar(Args[1], World.GetData(Args[2]));
    Exit(True);
  end;
  // Command: send target command
  if Args[0]='send' then begin
    if Length(Args) < 3 then Exit(True);
    Entity:=World.FindEntity(Args[1]);
    if Assigned(Entity) then Entity.PerformCommand(Args[2]);
    Exit(True);
  end;
  // Command: gridlight grid lightscale
  if Args[0]='gridlight' then begin
    if Length(Args) < 3 then Exit(True);
    Grid:=World.GridMap.FindGrid(Args[1]);
    if Assigned(Grid) then
      Grid.LightScale:=StrToFloatDef(Args[2], 1);
    Exit(True);
  end;
  // Command: camera [cameraname]
  if Args[0]='camera' then begin
    if Length(Args) > 1 then Entity:=World.FindEntity(Args[1]) else Entity:=nil;
    if Entity is TCameraEntity then
      CameraOverride:=TCameraEntity(Entity).Camera
    else
      CameraOverride:=nil;
    Exit(True);
  end;
  Result:=False;
end;

procedure TGameBase.SaveWorld(AFileName: string);
var
  F: TFileStream;
  ID: array [0..3] of Char;
begin
  ID:='GWDF';
  F:=TFileStream.Create(AFileName, fmCreate);
  F.Write(ID, 4);
  World.SerializeToStream(F);
  F.Free;
end;

procedure TGameBase.LoadWorld(AFileName: string);
var
  ID: array [0..3] of Char;
  F: TStream = nil;
  NewWorld: TWorld = nil;
begin
  // Silence audio
  GSound.Silence;
  // Destroy the current world
  FWorld.Free;
  FWorld:=nil;
  // Load the new world if specified
  if AFileName <> '' then begin
    // Set to be notified for deserialization updates
    GRootDeserializationUpdate:=@MapLoadDeserializationUpdate;
    try
      // Open the world file
      F:=GetDataStream(AFileName);
      if not Assigned(F) then raise Exception.Create('Error'); // fancy goto
      // Read and check the ID
      ID:='    ';
      F.Read(ID, 4);
      if ID <> 'GWDF' then raise Exception.Create('Error'); // another fancy goto
      // Create and read the new world
      NewWorld:=TWorld.Create;
      FWorld:=NewWorld;
      NewWorld.Scene:=Scene;
      NewWorld.DeserializeFromStream(F);
    except // Failed
      FreeAndNil(NewWorld);
      NewWorld:=nil;
    end;
    // Close the file
    F.Free;
    // Dont bother with updates anymore
    GRootDeserializationUpdate:=nil;
  end;
  // Init the new world if it was loaded properly
  if Assigned(NewWorld) then begin
    NewWorld.InitializeEntities;
    InitWorld;
  end else begin
    // Create an empty world in case of failure
    FWorld:=TWorld.Create;
    Player:=nil;
  end;
  // Unsilence audio
  GSound.Unsilence;
end;

procedure TGameBase.GetBenchmarkResults(out FrameCount, FPS: Integer);
begin
  FrameCount:=FrameCounter;
  FPS:=Round(FrameCounter/((GSystem.Milliseconds - MapLoadMilliseconds)/1000.0));
end;

end.
