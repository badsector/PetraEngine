{ SDL 1.2 graphics driver }
unit SDLGfx;

interface

uses
  SDL, Raster, {$IFDEF PETRAGL}GLRaster{$ELSE}FBRaster{$ENDIF}, Colors, GfxDrv;
  
type
  { TSDLGraphicsDriver - graphics driver for SDL 1.2 }
  TSDLGraphicsDriver = class(TGraphicsDriver)
  private
    // Rasterizer
    {$IFDEF PETRAGL}
    GLRaster: TOpenGLRasterizer;
    {$ELSE}
    FBRaster: TFramebufferRasterizer;
    {$ENDIF}
    // Screen surface
    Screen: PSDL_Surface;
    // Current palette
    Palette: array [0..255] of TSDL_Color;
    // Current gamma
    Gamma: TGammaValue;
    {$IFNDEF PETRAGL}
    // The palette was changed since the last frame and needs update
    PaletteNeedsUpdate: Boolean;
    // Back buffer for double pixels (if length=0 then no dp used)
    DoubleBack: array of Byte;
    {$ENDIF}
  public
    procedure Initialize; override;
    procedure Shutdown; override;
    {$IFDEF PETRAGL}
    function BeginFrame: Boolean; override;
    {$ELSE}
    function BeginFrame: PFrameData; override;
    {$ENDIF}
    procedure FinishFrame; override;
    procedure SetColorPalettePart(const APalette: TColorPalette; First, Last: Integer); override;
    procedure SetGamma(AGamma: TGammaValue); override;
    function GetRasterizer: TRasterizer; override;
  end;

implementation

uses
  SysUtils, Engine;
  
{$IFDEF UNIX}{$IFNDEF PETRAGCW}
function setenv(a, b: PChar; c: Integer): Integer; cdecl; external 'c' name 'setenv';
{$ENDIF}{$ENDIF}

{ TSDLGraphicsDriver }
procedure TSDLGraphicsDriver.Initialize;
var
  {$IFNDEF PETRAGL}
  Width: Integer;
  Height: Integer;
  XRes, YRes: Integer;
  {$ENDIF}
  I: Integer;
  Flag: DWORD = {$IFDEF PETRAGCW}SDL_FULLSCREEN{$ELSE}0{$ENDIF};
  NoGrab: Boolean = False;
begin
  // Default size
  {$IFDEF PETRAGL}
  RealHRes:=800;
  RealVRes:=600;
  {$ELSE}
  XRes:=320;
  YRes:=240;
  Width:=XRes{$IFNDEF PETRAGCW}*3{$ENDIF};
  Height:=YRes{$IFNDEF PETRAGCW}*3{$ENDIF};
  {$ENDIF}
  // Create framebuffer rasterizer
  {$IFDEF PETRAGL}
  GLRaster:=TOpenGLRasterizer.Create;
  {$ELSE}
  FBRaster:=TFramebufferRasterizer.Create;
  {$ENDIF}
  // Create default palette
  for I:=0 to 255 do begin
    Palette[I].r:=I;
    Palette[I].g:=I;
    Palette[I].b:=I;
  end;
  // Figure out settings from command-line parameters
  for I:=1 to ParamCount do
    if UpperCase(ParamStr(I))='-FULLSCREEN' then Flag:=SDL_FULLSCREEN else
    if UpperCase(ParamStr(I))='-NOGRAB' then NoGrab:=True else
    {$IFDEF PETRAGL}
    if UpperCase(Copy(ParamStr(I), 1, 2))='-W' then RealHRes:=StrToIntDef(Copy(ParamStr(I), 3, MaxInt), RealHRes) else
    if UpperCase(Copy(ParamStr(I), 1, 2))='-H' then RealVRes:=StrToIntDef(Copy(ParamStr(I), 3, MaxInt), RealVRes) else
    {$ELSE}
    if UpperCase(Copy(ParamStr(I), 1, 2))='-W' then Width:=StrToIntDef(Copy(ParamStr(I), 3, MaxInt), HRes) else
    if UpperCase(Copy(ParamStr(I), 1, 2))='-H' then Height:=StrToIntDef(Copy(ParamStr(I), 3, MaxInt), VRes) else
    if UpperCase(Copy(ParamStr(I), 1, 2))='-X' then XRes:=StrToIntDef(Copy(ParamStr(I), 3, MaxInt), HRes) else
    if UpperCase(Copy(ParamStr(I), 1, 2))='-Y' then YRes:=StrToIntDef(Copy(ParamStr(I), 3, MaxInt), VRes);
    {$ENDIF}
  {$IFNDEF PETRAGL}
  // Ensure the palette will be updated next frame
  PaletteNeedsUpdate:=True;
  // Adjust size
  if Width < XRes then Width:=XRes;
  if Height < YRes then Height:=YRes;
  // Configure internal resolution if requested
  if (XRes >= 320) and (YRes >= 200) then
    ConfigGraphicsResolution(XRes, YRes);
  {$ENDIF}
  // Initialize SDL
  {$IFDEF UNIX}{$IFNDEF PETRAGCW}
  setenv('SDL_VIDEO_CENTERED', '1', 1);
  {$ENDIF}{$ENDIF}
  {$IFDEF PETRAGL}
  SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
  Screen:=SDL_SetVideoMode(RealHRes, RealVRes, 32, SDL_OPENGL or Flag);
  AspectRatio:=RealHRes/RealVRes;
  {$ELSE}
  Flag:=({$IFDEF PETRAGCW}SDL_HWSURFACE or SDL_HWPALETTE{$ELSE}SDL_SWSURFACE{$ENDIF}) or Flag;
  if (Width=HRes) and (Height=VRes) then begin
    Screen:=SDL_SetVideoMode(HRes, VRes, 8, Flag);
    DoubleBack:=nil;
  end else begin
    Screen:=SDL_SetVideoMode(Width, Height, 8, Flag);
    SetLength(DoubleBack, HRes*VRes);
  end;
  {$ENDIF}
  // Hide mouse and grab input
  if not NoGrab then begin
    SDL_WM_GrabInput(SDL_GRAB_ON);
    SDL_ShowCursor(SDL_DISABLE);
  end;
  // Set caption
  SDL_WM_SetCaption(PChar(GGameTitle), PChar(GGameTitle));
end;

procedure TSDLGraphicsDriver.Shutdown;
begin
  {$IFDEF PETRAGL}
  FreeAndNil(GLRaster);
  {$ELSE}
  FreeAndNil(FBRaster);
  {$ENDIF}
end;

{$IFDEF PETRAGL}
function TSDLGraphicsDriver.BeginFrame: Boolean;
begin
  Result:=True;
end;
{$ELSE}
function TSDLGraphicsDriver.BeginFrame: PFrameData;
begin
  if Length(DoubleBack)=0 then begin
    SDL_LockSurface(Screen);
    Result:=PFrameData(Screen^.Pixels);
  end else
    Result:=PFrameData(@DoubleBack[0]);
end;
{$ENDIF}

procedure TSDLGraphicsDriver.FinishFrame;
{$IFDEF PETRAGL}
begin
  SDL_GL_SwapBuffers;
{$ELSE}
var
  X, Y, XD, YD, XR, YR: Integer;
  ThePal: array [0..255] of TSDL_Color;
  Src, Tgt: PByte;
begin
  // Stretch and scale the back buffer to screen
  if Length(DoubleBack) <> 0 then begin
    SDL_LockSurface(Screen);
    XD:=Trunc(HRes/Screen^.W*256);
    YD:=Trunc(VRes/Screen^.H*256);
    YR:=0;
    for Y:=0 to Screen^.H - 1 do begin
      XR:=0;
      Src:=@DoubleBack[(YR shr 8)*HRes];
      Tgt:=PByte(Screen^.pixels) + (Screen^.pitch*Y);
      for X:=0 to Screen^.W - 1 do begin
        Tgt^:=Src[XR shr 8];
        Inc(Tgt);
        Inc(XR, XD);
      end;
      Inc(YR, YD);
    end;
    SDL_UnlockSurface(Screen);
  end;
  // Update the palette if needed
  if PaletteNeedsUpdate then begin
    if Gamma > 0 then begin
      for X:=0 to 255 do with ThePal[X] do begin
        Y:=Round(Palette[X].r*(1.0 + Gamma/High(TGammaValue)) + Gamma);
        if Y > 255 then Y:=255;
        r:=Y;
        Y:=Round(Palette[X].g*(1.0 + Gamma/High(TGammaValue)) + Gamma);
        if Y > 255 then Y:=255;
        g:=Y;
        Y:=Round(Palette[X].b*(1.0 + Gamma/High(TGammaValue)) + Gamma);
        if Y > 255 then Y:=255;
        b:=Y;
      end;
      SDL_SetPalette(Screen, SDL_LOGPAL or SDL_PHYSPAL, ThePal, 0, 255);
    end else
      SDL_SetPalette(Screen, SDL_LOGPAL or SDL_PHYSPAL, Palette, 0, 255);
    PaletteNeedsUpdate:=False;
  end;
  // Flip the buffers
  SDL_Flip(Screen);
{$ENDIF}
end;

procedure TSDLGraphicsDriver.SetColorPalettePart(const APalette: TColorPalette; First, Last: Integer);
var
  I: Integer;
begin
  for I:=First to Last do
    if (Palette[i].r <> APalette.Red[I]) or
       (Palette[i].g <> APalette.Green[I]) or
       (Palette[i].b <> APalette.Blue[I]) then begin
      Palette[I].r:=APalette.Red[I];
      Palette[I].g:=APalette.Green[I];
      Palette[I].b:=APalette.Blue[I];
      {$IFNDEF PETRAGL}PaletteNeedsUpdate:=True;{$ENDIF}
    end;
end;

procedure TSDLGraphicsDriver.SetGamma(AGamma: TGammaValue);
begin
  Gamma:=AGamma;
  {$IFNDEF PETRAGL}PaletteNeedsUpdate:=True;{$ENDIF}
end;

function TSDLGraphicsDriver.GetRasterizer: TRasterizer;
begin
  {$IFDEF PETRAGL}
  Result:=GLRaster;
  {$ELSE}
  Result:=FBRaster;
  {$ENDIF}
end;

end.
