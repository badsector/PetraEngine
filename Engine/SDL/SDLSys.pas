{ SDL 1.2 system driver }
unit SDLSys;

interface

uses
  SysDrv;

type
  { TSDLSystemDriver - system driver for SDL 1.2 }
  TSDLSystemDriver = class(TSystemDriver)
  protected
    // TSystemDriver methods
    function GetMilliseconds: Integer; override;
  public
    // Create the driver
    constructor Create;
    // Destroy the driver
    destructor Destroy; override;
  end;

implementation

uses
  SDL;

{ TSDLSystemDriver }
constructor TSDLSystemDriver.Create;
begin
  SDL_Init(SDL_INIT_VIDEO or SDL_INIT_AUDIO or SDL_INIT_NOPARACHUTE);
end;

destructor TSDLSystemDriver.Destroy; 
begin
  SDL_Quit();
end;

function TSDLSystemDriver.GetMilliseconds: Integer;
begin
  Result:=Integer(SDL_GetTicks);
end;

end.
