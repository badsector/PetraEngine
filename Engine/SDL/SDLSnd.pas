{ SDL Sound Driver }
unit SDLSnd;

interface

uses
  SndDrv, SDL;

type
  { TSDLSoundDriver - SDL Sound Driver }
  TSDLSoundDriver = class(TSoundDriver)
  private
    // Called by SDL callback to fill audio data
    procedure OnCallback(Stream: PUInt8; Len: Integer);
  protected
    // TSoundDriver methods
    procedure InitializeDriver; override;
    procedure LockBuffers; override;
    procedure UnlockBuffers; override;
  public
    destructor Destroy; override;
  end;

implementation

uses
  Engine;
  
{ Functions }
procedure AudioCallback(UserData: Pointer; Stream: PUInt8; Len: Integer); cdecl;
begin
  TSDLSoundDriver(UserData).OnCallback(Stream, Len);
end;

{ TSDLSoundDriver }
destructor TSDLSoundDriver.Destroy;
begin
  SDL_PauseAudio(1);
  inherited Destroy;
end;

procedure TSDLSoundDriver.OnCallback(Stream: PUInt8; Len: Integer);
begin
  if SilentCounter <> 0 then
    FillChar(Buffers[NextBuffer], Len, 0);
  if Mixed=0 then MixNow;
  Move(Buffers[NextBuffer], Stream^, Len);
  NeedMix:=True;
  Dec(Mixed);
end;

procedure TSDLSoundDriver.InitializeDriver;
var
  Spec: TSDL_AudioSpec;
begin
  Available:=False;
  FillChar(Spec, SizeOf(Spec), 0);
  Spec.Freq:=22050;
  Spec.Format:=AUDIO_S16;
  Spec.Channels:=2;
  Spec.Silence:=0;
  // TODO: Figure out why this needs to be different
  // between Windows and Linux
  {$IFNDEF PETRAGCW}
  Spec.Samples:=SoundBufferSize{$IFNDEF LINUX} div 2{$ENDIF};
  {$ELSE}
  Spec.Samples:=SoundBufferSize div 2;
  {$ENDIF}
  Spec.Size:=0;
  Spec.Callback:=@AudioCallback;
  Spec.UserData:=Self;
  if SDL_OpenAudio(@Spec, nil)=0 then begin
    Available:=True;
    SDL_PauseAudio(0);
  end;
end;

procedure TSDLSoundDriver.LockBuffers;
begin
  SDL_LockAudio();
end;

procedure TSDLSoundDriver.UnlockBuffers;
begin
  SDL_UnlockAudio();
end;

end.
