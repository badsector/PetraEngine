{ SDL 1.2 input driver }
unit SDLInput;

interface

uses
  InputDrv;
  
type
  { TSDLInputDriver - input driver for SDL 1.2 }
  TSDLInputDriver = class(TInputDriver)
  private
    // Should quit
    Quit: Boolean;
  public
    // TInputDriver methods
    function IsLive: Boolean; override;
  end;

implementation

uses
  SDL;
  
{ Functions }
function TranslateKey(SK: Integer): TInputKey;
begin
  case SK of
    SDLK_ESCAPE: Exit(ikEscape);
    SDLK_F1: Exit(ikF1);
    SDLK_F2: Exit(ikF2);
    SDLK_F3: Exit(ikF3);
    SDLK_F4: Exit(ikF4);
    SDLK_F5: Exit(ikF5);
    SDLK_F6: Exit(ikF6);
    SDLK_F7: Exit(ikF7);
    SDLK_F8: Exit(ikF8);
    SDLK_F9: Exit(ikF9);
    SDLK_F10: Exit(ikF10);
    SDLK_F11: Exit(ikF11);
    SDLK_F12: Exit(ikF12);
    SDLK_SCROLLOCK: Exit(ikScrollLock);
    SDLK_BACKQUOTE: Exit(ikBackQuote);
    SDLK_0: Exit(ik0);
    SDLK_1: Exit(ik1);
    SDLK_2: Exit(ik2);
    SDLK_3: Exit(ik3);
    SDLK_4: Exit(ik4);
    SDLK_5: Exit(ik5);
    SDLK_6: Exit(ik6);
    SDLK_7: Exit(ik7);
    SDLK_8: Exit(ik8);
    SDLK_9: Exit(ik9);
    SDLK_MINUS: Exit(ikMinus);
    SDLK_EQUALS: Exit(ikEquals);
    SDLK_BACKSPACE: Exit(ikBackspace);
    SDLK_INSERT: Exit(ikInsert);
    SDLK_HOME: Exit(ikHome);
    SDLK_PAGEUP: Exit(ikPageUp);
    SDLK_PAGEDOWN: Exit(ikPageDown);
    SDLK_TAB: Exit(ikTab);
    SDLK_A: Exit(ikA);
    SDLK_B: Exit(ikB);
    SDLK_C: Exit(ikC);
    SDLK_D: Exit(ikD);
    SDLK_E: Exit(ikE);
    SDLK_F: Exit(ikF);
    SDLK_G: Exit(ikG);
    SDLK_H: Exit(ikH);
    SDLK_I: Exit(ikI);
    SDLK_J: Exit(ikJ);
    SDLK_K: Exit(ikK);
    SDLK_L: Exit(ikL);
    SDLK_M: Exit(ikM);
    SDLK_N: Exit(ikN);
    SDLK_O: Exit(ikO);
    SDLK_P: Exit(ikP);
    SDLK_Q: Exit(ikQ);
    SDLK_R: Exit(ikR);
    SDLK_S: Exit(ikS);
    SDLK_T: Exit(ikT);
    SDLK_U: Exit(ikU);
    SDLK_V: Exit(ikV);
    SDLK_W: Exit(ikW);
    SDLK_X: Exit(ikX);
    SDLK_Y: Exit(ikY);
    SDLK_Z: Exit(ikZ);
    SDLK_LEFTBRACKET: Exit(ikLeftBracket);
    SDLK_RIGHTBRACKET: Exit(ikRightBracket);
    SDLK_BACKSLASH: Exit(ikBackSlash);
    SDLK_RETURN: Exit(ikEnter);
    SDLK_CAPSLOCK: Exit(ikCapsLock);
    SDLK_SEMICOLON: Exit(ikSemiColon);
    SDLK_QUOTE: Exit(ikQuote);
    SDLK_LSHIFT, SDLK_RSHIFT: Exit(ikShift);
    SDLK_COMMA: Exit(ikComma);
    SDLK_PERIOD: Exit(ikPeriod);
    SDLK_SLASH: Exit(ikSlash);
    SDLK_LCTRL, SDLK_RCTRL: Exit(ikControl);
    SDLK_LALT, SDLK_RALT: Exit(ikAlt);
    SDLK_SPACE: Exit(ikSpace);
    SDLK_DELETE: Exit(ikDelete);
    SDLK_END: Exit(ikEnd);
    SDLK_LEFT: Exit(ikLeft);
    SDLK_RIGHT: Exit(ikRight);
    SDLK_UP: Exit(ikUp);
    SDLK_DOWN: Exit(ikDown);
    SDLK_NUMLOCK: Exit(ikNumLock);
  end;
  Result:=ikUnknown;
end;

{ TSDLInputDriver }
function TSDLInputDriver.IsLive: Boolean;
var
  Ev: TSDL_Event;
begin
  if Quit then Exit(False);
  FillChar(Ev, SizeOf(Ev), 0);
  while SDL_PollEvent(@Ev) <> 0 do begin
    case Ev.Type_ of
      SDL_KEYDOWN, SDL_KEYUP:
        // Special key to toggle input grab
        if Ev.Key.KeySym.Sym=SDLK_PAUSE then begin
          if Ev.Type_=SDL_KEYDOWN then begin
            if SDL_WM_GrabInput(SDL_GRAB_QUERY)=SDL_GRAB_ON then begin
              SDL_WM_GrabInput(SDL_GRAB_OFF);
              SDL_ShowCursor(SDL_ENABLE);
            end else begin
              SDL_WM_GrabInput(SDL_GRAB_ON);
              SDL_ShowCursor(SDL_DISABLE);
            end;
          end;
        end else
          NotifyHandlersForKey(TranslateKey(Ev.Key.KeySym.Sym),
            Ev.Type_=SDL_KEYDOWN);
      SDL_MOUSEBUTTONDOWN, SDL_MOUSEBUTTONUP: if SDL_WM_GrabInput(SDL_GRAB_QUERY)=SDL_GRAB_ON then
        case Ev.Button.Button of
          SDL_BUTTON_LEFT: NotifyHandlersForKey(ikLeftButton, Ev.Type_=SDL_MOUSEBUTTONDOWN);
          SDL_BUTTON_MIDDLE: NotifyHandlersForKey(ikMiddleButton, Ev.Type_=SDL_MOUSEBUTTONDOWN);
          SDL_BUTTON_RIGHT: NotifyHandlersForKey(ikRightButton, Ev.Type_=SDL_MOUSEBUTTONDOWN);
          4: NotifyHandlersForKey(ikWheelUp, Ev.Type_=SDL_MOUSEBUTTONDOWN);
          5: NotifyHandlersForKey(ikWheelDown, Ev.Type_=SDL_MOUSEBUTTONDOWN);
        end;
      SDL_MOUSEMOTION: if SDL_WM_GrabInput(SDL_GRAB_QUERY)=SDL_GRAB_ON then
        NotifyHandlersForMouse(Ev.Motion.XRel, Ev.Motion.YRel);
      SDL_QUITEV: Quit:=True;
    end;
  end;
  Result:=not Quit;
end;

end.
