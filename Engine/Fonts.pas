{ Font data }
unit Fonts;

interface

uses
  ResMan;

type

  { TBFChar - Information about a single bitmap font character }
  TBFChar = record
    // Character width
    Width: Byte;
    // First bit in the bitstream for this character
    Bit: Integer;
  end;

  { TBitFont - Bitmap font resource }
  TBitFont = class(TResource)
  private
    // Property storage
    FChars: array [32..127] of TBFChar;
    FBitStream: array of Byte;
    FHeight: Byte;
    FBaseLine: Byte;
    function GetChars(AIndex: Integer): TBFChar; inline;
    function GetBitStream: PByte; inline;
  public
    procedure LoadBitFontFile(AFileName: string);
    // Bitmap font characters (from 32 to 127)
    property Chars[AIndex: Integer]: TBFChar read GetChars;
    // Font Bitstream
    property BitStream: PByte read GetBitStream;
    // Font height
    property Height: Byte read FHeight;
    // Font baseline
    property BaseLine: Byte read FBaseLine;
  end;

implementation

uses
  Classes, DataLoad;

{ TBitFont }
function TBitFont.GetChars(AIndex: Integer): TBFChar;
begin
  Result:=FChars[AIndex];
end;

function TBitFont.GetBitStream: PByte;
begin
  Result:=@FBitStream[0];
end;

procedure TBitFont.LoadBitFontFile(AFileName: string);
var
  F: TStream;
  I, TotalBits, TotalBytes: Cardinal;
  Magic: array [0..3] of Char;
begin
  F:=GetDataStream(AFileName);
  if not Assigned(F) then Exit;
  F.ReadBuffer(Magic, 4);
  if Magic <> 'BFFF' then begin
    F.Free;
    Exit;
  end;
  F.ReadBuffer(FHeight, 1);
  F.ReadBuffer(FBaseLine, 1);
  TotalBits:=0;
  for I:=32 to 127 do with FChars[I] do begin
    F.ReadBuffer(Width, 1);
    Bit:=TotalBits;
    TotalBits += Width*Height;
  end;
  TotalBytes:=TotalBits div 8;
  if TotalBytes*8 < TotalBits then Inc(TotalBytes);
  SetLength(FBitStream, TotalBytes);
  F.ReadBuffer(FBitStream[0], Length(FBitStream));
  F.Free;
end;

end.
