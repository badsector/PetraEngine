// OpenGL  rasterizer
unit GLRaster;

interface

uses
  GL, Maths, Colors, Raster, HWRaster, Textures, GfxDrv;

type
  { TOpenGLTexture - OpenGL-specific data for a hardware accelerated texture }
  TOpenGLTexture = class(TRasterTexture)
  private
    // OpenGL texture name
    GLName: GLuint;
  public
    // TRasterTexture methods
    procedure SetData(AWidth, AHeight: Integer; AFiltered: Boolean; APalette: TColorPalette; const Texels); override;
  public
    destructor Destroy; override;
  end;

  { TOpenGLRasterizer - OpenGL-based rasterizer }
  TOpenGLRasterizer = class(THardwareRasterizer)
  protected
    // THardwareRasterizer methods
    procedure BeforeBatchesDraw; override;
    procedure AfterBatchesDraw; override;
    procedure DrawBatch(const Batch: TTriBatch); override;
  public
    // TRasterizer methods
    function CreateTexture: TRasterTexture; override;
    function BeginFrame: Boolean; override;
    procedure FinishFrame; override;
    procedure Clear(R, G, B: Single); override;
    procedure SetMatrices(Projection, View: TMatrix); override;
    procedure SetFog(FogR, FogG, FogB, FogStart, FogEnd: Single); override;
    procedure RenderQuad(X1, Y1, S1, T1, X2, Y2, S2, T2: Integer; Blend: Single; Texture: TTexture); override;
    procedure DrawHUDImage(X, Y: Integer; Texture: TTexture; SX, SY, SW, SH: Integer); override;
  end;

implementation

{ TOpenGLTexture }

procedure TOpenGLTexture.SetData(AWidth, AHeight: Integer; AFiltered: Boolean;
  APalette: TColorPalette; const Texels);
var
  RGBTexels: array of Byte;
  TexelBytes: PByte;
  X, Y: Integer;
  Components: Integer = 3;
begin
  // Check if there is transparency
  TexelBytes:=PByte(@Texels);
  for X:=0 to AWidth*AHeight - 1 do if TexelBytes[X]=255 then begin
    Components:=4;
    Break;
  end;
  // Build RGB texel data from the passed texels and palette
  SetLength(RGBTexels, AWidth*AHeight*Components);
  for Y:=0 to AHeight - 1 do
    for X:=0 to AWidth - 1 do with APalette do begin
      RGBTexels[(Y*AWidth + X)*Components]:=Red[TexelBytes[Y*AWidth + X]];
      RGBTexels[(Y*AWidth + X)*Components + 1]:=Green[TexelBytes[Y*AWidth + X]];
      RGBTexels[(Y*AWidth + X)*Components + 2]:=Blue[TexelBytes[Y*AWidth + X]];
    end;
  // Fill alpha for transparent textures and black them for bilinear filtering
  if Components=4 then begin
    for Y:=0 to AHeight - 1 do
      for X:=0 to AWidth - 1 do with APalette do begin
        if TexelBytes[Y*AWidth + X]=255 then begin
          RGBTexels[(Y*AWidth + X)*4]:=0;
          RGBTexels[(Y*AWidth + X)*4 + 1]:=0;
          RGBTexels[(Y*AWidth + X)*4 + 2]:=0;
          RGBTexels[(Y*AWidth + X)*4 + 3]:=0;
        end else begin
          RGBTexels[(Y*AWidth + X)*4 + 3]:=255;
        end;
      end;
  end;
  // Upload the texture data
  glBindTexture(GL_TEXTURE_2D, GLName);
  if AFiltered then begin
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  end else begin
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  end;
  if Components=3 then
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, AWidth, AHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, @RGBTexels[0])
  else
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, AWidth, AHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, @RGBTexels[0])
end;

destructor TOpenGLTexture.Destroy;
begin
  // Delete the texture
  glDeleteTextures(1, @GLName);
  inherited Destroy;
end;

{ TOpenGLRasterizer }

procedure TOpenGLRasterizer.BeforeBatchesDraw;
begin
  glEnable(GL_FOG);
  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
  glEnableClientState(GL_COLOR_ARRAY);
  glEnableClientState(GL_VERTEX_ARRAY);
end;

procedure TOpenGLRasterizer.AfterBatchesDraw;
begin
  glDisableClientState(GL_VERTEX_ARRAY);
  glDisableClientState(GL_COLOR_ARRAY);
  glDisableClientState(GL_TEXTURE_COORD_ARRAY);
  glDisable(GL_FOG);
end;

procedure TOpenGLRasterizer.DrawBatch(const Batch: TTriBatch);
begin
  with Batch do begin
    glBindTexture(GL_TEXTURE_2D, TOpenGLTexture(HWTexture).GLName);
    glInterleavedArrays(GL_T2F_C3F_V3F, 0, @Vertices[0]);
    glDrawArrays(GL_TRIANGLES, 0, Count div 8);
  end;
end;

function TOpenGLRasterizer.CreateTexture: TRasterTexture;
begin
  Result:=TOpenGLTexture.Create;
  with TOpenGLTexture(Result) do begin
    glGenTextures(1, @GLName);
    glBindTexture(GL_TEXTURE_2D, GLName);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  end;
end;

function TOpenGLRasterizer.BeginFrame: Boolean;
begin
  Result:=inherited BeginFrame;
  glClear(GL_DEPTH_BUFFER_BIT);
  glEnable(GL_TEXTURE_2D);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);
end;

procedure TOpenGLRasterizer.FinishFrame;
begin
  inherited FinishFrame;
end;

procedure TOpenGLRasterizer.Clear(R, G, B: Single);
begin
  glClearColor(R, G, B, 0);
  glClear(GL_COLOR_BUFFER_BIT);
  DiscardBatches;
end;

procedure TOpenGLRasterizer.SetMatrices(Projection, View: TMatrix);
begin
  glMatrixMode(GL_PROJECTION);
  glLoadMatrixf(@Projection.M11);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity;
  glRotatef(180, 0, 1, 0);
  glMultMatrixf(@View.M11);
end;

procedure TOpenGLRasterizer.SetFog(FogR, FogG, FogB, FogStart, FogEnd: Single);
var
  FogColor: array [0..3] of Single;
begin
  FlushBatches;
  FogColor[0]:=FogR;
  FogColor[1]:=FogG;
  FogColor[2]:=FogB;
  FogColor[3]:=0.0;
  glFogfv(GL_FOG_COLOR, @FogColor[0]);
  glFogi(GL_FOG_MODE, GL_LINEAR);
  glFogf(GL_FOG_START, FogStart);
  glFogf(GL_FOG_END, FogEnd);
end;

procedure TOpenGLRasterizer.RenderQuad(X1, Y1, S1, T1, X2, Y2, S2, T2: Integer;
  Blend: Single; Texture: TTexture);
begin
  // Flush any existing triangles
  FlushBatches;
  // Setup blending
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  // Setup texture and color
  glBindTexture(GL_TEXTURE_2D, TOpenGLTexture(Texture.HardwareTexture).GLName);
  glColor4f(1, 1, 1, 0.75);
  // Save current matrices and load identity matrices
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();
  // Disable use of depth buffer
  glDisable(GL_DEPTH_TEST);
  glDepthMask(GL_FALSE);
  // Draw the quad
  glBegin(GL_QUADS);
  glTexCoord2f(S1/Texture.Width, T1/Texture.Height);
  glVertex2f(X1/RealHRes*2-1, (1-Y1/RealVRes)*2-1);
  glTexCoord2f(S1/Texture.Width, T2/Texture.Height);
  glVertex2f(X1/RealHRes*2-1, (1-Y2/RealVRes)*2-1);
  glTexCoord2f(S2/Texture.Width, T2/Texture.Height);
  glVertex2f(X2/RealHRes*2-1, (1-Y2/RealVRes)*2-1);
  glTexCoord2f(S2/Texture.Width, T1/Texture.Height);
  glVertex2f(X2/RealHRes*2-1, (1-Y1/RealVRes)*2-1);
  glEnd();
  // Enable use of depth buffer
  glEnable(GL_DEPTH_TEST);
  glDepthMask(GL_TRUE);
  // Restore matrices
  glPopMatrix();
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);
  // Disable blending
  glDisable(GL_BLEND);
end;

procedure TOpenGLRasterizer.DrawHUDImage(X, Y: Integer; Texture: TTexture; SX, SY, SW, SH: Integer);
var
  AspectFix, AspectOfs: Single;
begin
  // Flush any existing triangles
  FlushBatches;
  // Setup blending
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  // Setup texture and color
  glBindTexture(GL_TEXTURE_2D, TOpenGLTexture(Texture.HardwareTexture).GLName);
  glColor3f(1, 1, 1);
  // Save current matrices and load identity matrices
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();
  // Disable use of depth buffer
  glDisable(GL_DEPTH_TEST);
  glDepthMask(GL_FALSE);
  // Calculate aspect ratio fix so that the HUD is drawn at the 4:3 portion
  AspectFix:=(4/3)/(RealHRes/RealVRes);
  AspectOfs:=(RealHRes - RealHRes*AspectFix)/RealHRes;
  // Draw the quad
  glBegin(GL_QUADS);
  glTexCoord2f(SX/Texture.Width, SY/Texture.Height);
  glVertex2f(AspectOfs + X/HRes*2*AspectFix-1, (1-Y/VRes)*2-1);
  glTexCoord2f(SX/Texture.Width, (SY + SH)/Texture.Height);
  glVertex2f(AspectOfs + X/HRes*2*AspectFix-1, (1-(Y + SH)/VRes)*2-1);
  glTexCoord2f((SX + SW)/Texture.Width, (SY + SH)/Texture.Height);
  glVertex2f(AspectOfs + (X + SW)/HRes*2*AspectFix-1, (1-(Y + SH)/VRes)*2-1);
  glTexCoord2f((SX + SW)/Texture.Width, SY/Texture.Height);
  glVertex2f(AspectOfs + (X + SW)/HRes*2*AspectFix-1, (1-Y/VRes)*2-1);
  glEnd();
  // Enable use of depth buffer
  glEnable(GL_DEPTH_TEST);
  glDepthMask(GL_TRUE);
  // Restore matrices
  glPopMatrix();
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);
  // Disable blending
  glDisable(GL_BLEND);
end;

end.

