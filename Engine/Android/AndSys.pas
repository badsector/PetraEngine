{ Android system }
unit AndSys;
interface

uses
  SysDrv;

type
  { TAndroidSystemDriver - Android system driver }
  TAndroidSystemDriver = class(TSystemDriver)
  private
    BaseTime: QWord;
  protected
    // TSystemDriver methods
    function GetMilliseconds: Integer; override;
  end;

implementation

uses
  SysUtils;

{ TAndroidSystemDriver }
function TAndroidSystemDriver.GetMilliseconds: Integer;
begin
  if BaseTime=0 then BaseTime:=GetTickCount64 - 1;
  Result:=Integer(GetTickCount64 - BaseTime);
end;

end.
