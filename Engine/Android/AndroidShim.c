/* Header stuff */
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <dlfcn.h>
#include <android_native_app_glue.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h> 
#include <EGL/egl.h>
#include <GLES/gl.h>
#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>
#include <android/log.h>
#include <math.h>

/* Colors are scaled a bit because they are too dark */
#define COLOR_SCALE(x) x

/* Logging */
#define LOG_TAG "PostApocalypticPetra"
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN, LOG_TAG, __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)
#define LOGD(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)

/* Globals */
static EGLDisplay display;
static EGLSurface surface;
static EGLContext context;
static struct android_app* appstate;
static EGLint width, height;
static void* dl;
static SLObjectItf engineObject;
static SLEngineItf engineEngine;
static SLObjectItf outputMixObject;
static SLObjectItf bqPlayerObject;
static SLPlayItf bqPlayerPlay;
static SLAndroidSimpleBufferQueueItf bqPlayerBufferQueue;
static SLVolumeItf bqPlayerVolume;
static int fakeJoyId = -1, fakeJoyStartX, fakeJoyStartY;
static int fingerX, fingerY, fingerId = -1;

/* Procedures (must match TProcs in AndProcs.pas unit) */
#pragma pack(push,1)
struct TProcs
{
    void (*LogString)(char* Str);
    void (*RequestQuit)(void);
    void (*FreeMem)(void* Ptr);
    void* (*GetAssetData)(char* AFileName, int32_t* ASize);
    int32_t (*HasDataFile)(char* AFileName);
    int64_t (*CreateTexture)(void);
    void (*DestroyTexture)(int64_t AHandle);
    void (*SetTextureData)(int64_t AHandle, int32_t Width, int32_t Height, char* RGB);
    void (*GetViewSize)(int32_t* ViewWidth, int32_t* ViewHeight);
    void (*Clear)(float R, float G, float B);
    void (*SetMatrices)(float* Proj, float* View);
    void (*SetFog)(float R, float G, float B, float FogStart, float FogEnd);
    void (*BeforeBatches)(void);
    void (*AfterBatches)(void);
    void (*RenderBatch)(int64_t TexHandle, float* VertexData, int32_t Count);
    void (*RenderQuad)(int64_t TexHandle, float X1, float Y1, float S1, float T1, float X2, float Y2, float S2, float T2);
};
#pragma pack(pop)

/* Input event constants */
#define IEV_LEFTPAD 1
#define IEV_RIGHTPAD 2
#define IEV_BUTTON_LEFT 3
#define IEV_BUTTON_RIGHT 4
#define IEV_BUTTON_UP 5
#define IEV_BUTTON_DOWN 6
#define IEV_BUTTON_O 7
#define IEV_BUTTON_U 8
#define IEV_BUTTON_Y 9
#define IEV_BUTTON_A 10
#define IEV_BUTTON_L 11
#define IEV_BUTTON_R 12

/* Pascal procs exposed via the Pascal side library */
static void (*Initialize)(struct TProcs* procs);
static void (*RunCycle)(void);
static void (*Shutdown)(void);
static void (*InputEvent)(int32_t Ev, int32_t A, int32_t B);
static void (*AudioCallback)(void** Buffer, int32_t* Size);

static void f_LogString(char* Str)
{
    LOGI("%s", Str);
}

static void f_RequestQuit(void)
{
    exit(0);
}

static void f_FreeMem(void* Ptr)
{
    free(Ptr);
}

static void* f_GetAssetData(char* AFileName, int32_t* ASize)
{
    AAsset* asset = AAssetManager_open(appstate->activity->assetManager, AFileName, AASSET_MODE_BUFFER);
    void* buffer;
    if (!asset)
    {
        LOGE("Requested asset '%s' but was not found!!!", AFileName);
        return NULL;
    }
    *ASize = (int32_t)AAsset_getLength(asset);
    LOGI("Requested asset '%s' and was found (%i bytes)", AFileName, (int)*ASize);
    buffer = malloc(*ASize);
    memcpy(buffer, AAsset_getBuffer(asset), *ASize);
    AAsset_close(asset);
    return buffer;
}

int32_t f_HasDataFile(char* AFileName)
{
    AAsset* asset = AAssetManager_open(appstate->activity->assetManager, AFileName, AASSET_MODE_STREAMING);
    if (!asset)
    {
        LOGW("Checked for asset '%s' but it was not found", AFileName);
        return 0;
    }
    AAsset_close(asset);
    return 1;
}

static int64_t f_CreateTexture(void)
{
    GLuint id;
    glGenTextures(1, &id);
    glBindTexture(GL_TEXTURE_2D, id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    return id;
}

static void f_DestroyTexture(int64_t AHandle)
{
    GLuint id = (GLuint)AHandle;
    glDeleteTextures(1, &id);
}

static void f_SetTextureData(int64_t AHandle, int32_t Width, int32_t Height, char* RGB)
{
    glBindTexture(GL_TEXTURE_2D, (GLuint)AHandle);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, Width, Height, 0, GL_RGB, GL_UNSIGNED_BYTE, RGB);
}

static void f_GetViewSize(int32_t* ViewWidth, int32_t* ViewHeight)
{
    *ViewWidth = (int32_t)width;
    *ViewHeight = (int32_t)height;
}

static void f_Clear(float R, float G, float B)
{
    glClearColor(COLOR_SCALE(R), COLOR_SCALE(G), COLOR_SCALE(B), 0.0);
    glClear(GL_COLOR_BUFFER_BIT);
}

static void f_SetMatrices(float* Proj, float* View)
{
    glMatrixMode(GL_PROJECTION);
    glLoadMatrixf(Proj);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glRotatef(180, 0, 1, 0);
    glMultMatrixf(View);
}

static void f_SetFog(float R, float G, float B, float FogStart, float FogEnd)
{
    float color[4] = {R, G, B, 1.0f};
    glFogfv(GL_FOG_COLOR, color);
    glFogf(GL_FOG_MODE, GL_LINEAR);
    glFogf(GL_FOG_START, FogStart);
    glFogf(GL_FOG_END, FogEnd);
}

static void f_BeforeBatches(void)
{
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glEnable(GL_FOG);
}

static void f_AfterBatches(void)
{
    glDisable(GL_FOG);
}

static void f_RenderBatch(int64_t TexHandle, float* VertexData, int32_t Count)
{
    glBindTexture(GL_TEXTURE_2D, (GLuint)TexHandle);
    glTexCoordPointer(2, GL_FLOAT, 36, VertexData);
    glColorPointer(4, GL_FLOAT, 36, VertexData + 2);
    glVertexPointer(3, GL_FLOAT, 36, VertexData + 6);
    glDrawArrays(GL_TRIANGLES, 0, Count);
}

static void f_RenderQuad(int64_t TexHandle, float X1, float Y1, float S1, float T1, float X2, float Y2, float S2, float T2)
{
    float pos[8], st[8], rgb[16];
    pos[0] = X1/(float)width*2.0f - 1.0f;
    pos[1] = (1.0f- Y1/(float)height)*2.0f - 1.0f;
    pos[2] = X1/(float)width*2.0f - 1.0f;
    pos[3] = (1.0f- Y2/(float)height)*2.0f - 1.0f;
    pos[4] = X2/(float)width*2.0f - 1.0f;
    pos[5] = (1.0f- Y1/(float)height)*2.0f - 1.0f;
    pos[6] = X2/(float)width*2.0f - 1.0f;
    pos[7] = (1.0f- Y2/(float)height)*2.0f - 1.0f;
    st[0] = S1/(float)256.0;//Texture.Width;
    st[1] = T1/(float)256.0;//Texture.Height;
    st[2] = S1/(float)256.0;//Texture.Width;
    st[3] = T2/(float)256.0;//Texture.Height;
    st[4] = S2/(float)256.0;//Texture.Width;
    st[5] = T1/(float)256.0;//Texture.Height;
    st[6] = S2/(float)256.0;//Texture.Width;
    st[7] = T2/(float)256.0;//Texture.Height;
    rgb[0] = rgb[1] = rgb[2] = 
    rgb[4] = rgb[5] = rgb[6] = 
    rgb[8] = rgb[9] = rgb[10] = 
    rgb[12] = rgb[13] = rgb[14] = 1.0;
    rgb[3] = rgb[7] = rgb[11] = rgb[15] = 0.65f;
    glEnable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glBindTexture(GL_TEXTURE_2D, (GLuint)TexHandle);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glVertexPointer(2, GL_FLOAT, 0, pos);
    glTexCoordPointer(2, GL_FLOAT, 0, st);
    glColorPointer(4, GL_FLOAT, 0, rgb);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    glPopMatrix();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glDisable(GL_BLEND);
}

static void audio_callback(SLAndroidSimpleBufferQueueItf bq, void *context)
{
    void* buffer;
    int32_t size;
    AudioCallback(&buffer, &size);
    (*bqPlayerBufferQueue)->Enqueue(bqPlayerBufferQueue, buffer, size);
}

static void do_init(void)
{
    EGLint attribs[11], configs, format;
    EGLConfig config;
    char* fullpath;
    struct TProcs procs;
    SLDataLocator_AndroidSimpleBufferQueue loc_bufq = {SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE, 2};
    SLDataFormat_PCM format_pcm = {
        SL_DATAFORMAT_PCM,
        2,
        SL_SAMPLINGRATE_22_05,
        SL_PCMSAMPLEFORMAT_FIXED_16,
        SL_PCMSAMPLEFORMAT_FIXED_16,
        SL_SPEAKER_FRONT_LEFT | SL_SPEAKER_FRONT_RIGHT,
        SL_BYTEORDER_LITTLEENDIAN
    };
    SLDataSource audioSrc = {&loc_bufq, &format_pcm};
    const SLInterfaceID ids[2] = {SL_IID_BUFFERQUEUE, SL_IID_VOLUME};
    const SLboolean req[2] = {SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE};
    
    if (display) return;
    
    /* Setup procs to expose to the Pascal side */
    procs.LogString = f_LogString;
    procs.RequestQuit = f_RequestQuit;
    procs.FreeMem = f_FreeMem;
    procs.GetAssetData = f_GetAssetData;
    procs.HasDataFile = f_HasDataFile;
    procs.CreateTexture = f_CreateTexture;
    procs.DestroyTexture = f_DestroyTexture;
    procs.SetTextureData = f_SetTextureData;
    procs.GetViewSize = f_GetViewSize;
    procs.Clear = f_Clear;
    procs.SetMatrices = f_SetMatrices;
    procs.SetFog = f_SetFog;
    procs.BeforeBatches = f_BeforeBatches;
    procs.AfterBatches = f_AfterBatches;
    procs.RenderBatch = f_RenderBatch;
    procs.RenderQuad = f_RenderQuad;
    
    /* Locate and load Pascal library */
    fullpath = malloc(strlen(appstate->activity->internalDataPath) + 128);
    sprintf(fullpath, "%s/../lib/libCSLib.so", appstate->activity->internalDataPath);
    LOGI("Attempting to load Pascal library from '%s'", fullpath);
    dl = dlopen(fullpath, RTLD_LAZY);
    free(fullpath);
    if (!dl) exit(1);
    Initialize = dlsym(dl, "Initialize");
    RunCycle = dlsym(dl, "RunCycle");
    Shutdown = dlsym(dl, "Shutdown");
    InputEvent = dlsym(dl, "InputEvent");
    AudioCallback = dlsym(dl, "AudioCallback");
    if (!Initialize) LOGE("Initialize proc missing");
    if (!RunCycle) LOGE("RunCycle proc missing");
    if (!Shutdown) LOGE("Shutdown proc missing");
    if (!InputEvent) LOGE("InputEvent proc missing");
    if (!AudioCallback) LOGE("AudioCallback proc missing");
    
    /* Initialize OpenGL ES */
    attribs[0] = EGL_SURFACE_TYPE;
    attribs[1] = EGL_WINDOW_BIT;
    attribs[2] = EGL_BLUE_SIZE;
    attribs[3] = 4;
    attribs[4] = EGL_GREEN_SIZE;
    attribs[5] = 4;
    attribs[6] = EGL_RED_SIZE;
    attribs[7] = 4;
    attribs[8] = EGL_DEPTH_SIZE;
    attribs[9] = 16;
    attribs[10] = EGL_NONE;
    display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    eglInitialize(display, 0, 0);
    eglBindAPI(EGL_OPENGL_ES_API);
    eglChooseConfig(display, attribs, &config, 1, &configs);
    if (!configs) exit(1);
    eglGetConfigAttrib(display, config, EGL_NATIVE_VISUAL_ID, &format);
    surface = eglCreateWindowSurface(display, config, appstate->window, 0);
    context = eglCreateContext(display, config, 0, 0);
    eglMakeCurrent(display, surface, surface, context);
    eglQuerySurface(display, surface, EGL_WIDTH, &width);
    eglQuerySurface(display, surface, EGL_HEIGHT, &height);
    
    /* Initialize OpenSL ES */
    LOGI("Creating OpenGL ES Engine");
    slCreateEngine(&engineObject, 0, NULL, 0, NULL, NULL);
    (*engineObject)->Realize(engineObject, SL_BOOLEAN_FALSE);
    LOGI("Obtaining OpenGL ES Engine Interface");
    (*engineObject)->GetInterface(engineObject, SL_IID_ENGINE, &engineEngine);
    LOGI("Creating OpenGL ES Output Mix");
    (*engineEngine)->CreateOutputMix(engineEngine, &outputMixObject, 0, 0, 0);
    (*outputMixObject)->Realize(outputMixObject, SL_BOOLEAN_FALSE);
    LOGI("Creating OpenGL ES Audio Player");
    SLDataLocator_OutputMix loc_outmix = {SL_DATALOCATOR_OUTPUTMIX, outputMixObject};
    SLDataSink audioSnk = {&loc_outmix, NULL};
    (*engineEngine)->CreateAudioPlayer(engineEngine, &bqPlayerObject, &audioSrc, &audioSnk, 2, ids, req);
    (*bqPlayerObject)->Realize(bqPlayerObject, SL_BOOLEAN_FALSE);
    LOGI("Obtaining Audio Player interfaces");
    (*bqPlayerObject)->GetInterface(bqPlayerObject, SL_IID_PLAY, &bqPlayerPlay);
    (*bqPlayerObject)->GetInterface(bqPlayerObject, SL_IID_BUFFERQUEUE, &bqPlayerBufferQueue);
    LOGI("Registering Player Buffer Queue Callback");
    (*bqPlayerBufferQueue)->RegisterCallback(bqPlayerBufferQueue, audio_callback, NULL);
    (*bqPlayerObject)->GetInterface(bqPlayerObject, SL_IID_VOLUME, &bqPlayerVolume);

    /* Initialize the game */
    LOGI("Initializing Pascal library");
    Initialize(&procs);
    LOGI("Pascal library initialized");

    /* Start audio playback */
    LOGI("Starting Audio Playback");
    (*bqPlayerPlay)->SetPlayState(bqPlayerPlay, SL_PLAYSTATE_PLAYING);
    audio_callback(NULL, NULL);
    audio_callback(NULL, NULL);
    audio_callback(NULL, NULL);
}

static void do_cycle(void)
{
    // Check finger event
    static int lastPos = -1;
    static const int actions[4] = {IEV_BUTTON_U, IEV_BUTTON_A, IEV_BUTTON_O, IEV_BUTTON_Y};
    if (fingerId != -1)
    {
        int pos = fingerY / (height / 4);
        if (pos != lastPos)
        {
            if (lastPos != -1) InputEvent(actions[lastPos], 0, 0);
            if (pos >= 0 && pos <= 3)
                InputEvent(actions[pos], 1, 0);
            else
                pos = -1;
            lastPos = pos;
        }
    }
    else if (lastPos != -1)
    {
        InputEvent(actions[lastPos], 0, 0);
        lastPos = -1;
    }
    // Prepare for render
    glViewport(0, 0, width, height);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glEnable(GL_TEXTURE_2D);
    if (RunCycle) RunCycle();
    eglSwapBuffers(display, surface);
}

static void do_shutdown(void)
{
    if (!display) return;
    LOGI("Shutting down");
    if (Shutdown) Shutdown();
    (*bqPlayerObject)->Destroy(bqPlayerObject);
    (*outputMixObject)->Destroy(outputMixObject);
    (*engineObject)->Destroy(engineObject);
    eglMakeCurrent(display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
    eglDestroyContext(display, context);
    eglDestroySurface(display, surface);
    eglTerminate(display);
    display = 0;
    LOGI("Shutdown complete");
}

static int32_t on_input(struct android_app* app, AInputEvent* event)
{
    int32_t type = AInputEvent_getType(event);
    int32_t source = AInputEvent_getSource(event);
    // Ignore events if on_init isn't called yet
    if (!display) return 0;
    // Motion event (axis)
    if (type == AINPUT_EVENT_TYPE_MOTION)
    {
        int32_t action = AMotionEvent_getAction(event);
        // Handle joystick/gamepad input
        if ((source & AINPUT_SOURCE_JOYSTICK) &&
            (action == AMOTION_EVENT_ACTION_MOVE))
        {
            int i, historySize = AMotionEvent_getHistorySize(event);
            float x, y;
            for (i=0; i < historySize; i++)
            {
                x = AMotionEvent_getHistoricalAxisValue(event, AMOTION_EVENT_AXIS_X, 0, i);
                y = AMotionEvent_getHistoricalAxisValue(event, AMOTION_EVENT_AXIS_Y, 0, i);
                InputEvent(IEV_LEFTPAD, (int)(x*65535.0f), (int)(y*65535.0f));
            }
            x = AMotionEvent_getAxisValue(event, AMOTION_EVENT_AXIS_X, 0);
            y = AMotionEvent_getAxisValue(event, AMOTION_EVENT_AXIS_Y, 0);
            InputEvent(IEV_LEFTPAD, (int)(x*65535.0f), (int)(y*65535.0f));
            return 1;
        }
        // Handle touch events
        if (source == AINPUT_SOURCE_TOUCHSCREEN)
        {
            int index = (action & 0xFF00) >> 8;
            int id = AMotionEvent_getPointerId(event, index);
            int x = AMotionEvent_getX(event, index);
            int y = AMotionEvent_getY(event, index);
            switch (action & 0xFF)
            {
            case AMOTION_EVENT_ACTION_DOWN:
            case AMOTION_EVENT_ACTION_POINTER_DOWN:
                if (x < width / 2)
                {
                    fakeJoyId = id;
                    fakeJoyStartX = x;
                    fakeJoyStartY = y;
                    InputEvent(IEV_LEFTPAD, 0, 0);
                }
                else if (fingerId == -1)
                {
                    fingerId = id;
                    fingerX = x;
                    fingerY = y;
                }
                break;
            case AMOTION_EVENT_ACTION_UP:
            case AMOTION_EVENT_ACTION_POINTER_UP:
                if (id == fakeJoyId)
                {
                    fakeJoyId = -1;
                    InputEvent(IEV_LEFTPAD, 0, 0);
                }
                else if (id == fingerId)
                {
                    fingerId = -1;
                }
                break;
            case AMOTION_EVENT_ACTION_MOVE:
                if (id == fakeJoyId)
                {
                    float size = ((float)((width / 2) < height ? (width / 2) : height))*0.3f;
                    float dx = ((float)(x - fakeJoyStartX))/size;
                    float dy = ((float)(y - fakeJoyStartY))/size;
                    if (dx < -1.0f) dx = -1.0f; else if (dx > 1.0f) dx = 1.0f;
                    if (dy < -1.0f) dy = -1.0f; else if (dy > 1.0f) dy = 1.0f;
                    InputEvent(IEV_LEFTPAD, (int)(dx*65535.0f), (int)(dy*65535.0f));
                }
                else if (id == fingerId)
                {
                    fingerX = x;
                    fingerY = y;
                }
                break;
            }
        }
    }
    // Key event
    else if (type == AINPUT_EVENT_TYPE_KEY)
    {
        int32_t code = AKeyEvent_getKeyCode(event);
        //LOGI("Received key event: %d\n", code);
        switch (code)
        {
        case AKEYCODE_DPAD_UP: code = IEV_BUTTON_UP; break;
        case AKEYCODE_DPAD_DOWN: code = IEV_BUTTON_DOWN; break;
        case AKEYCODE_DPAD_LEFT: code = IEV_BUTTON_LEFT; break;
        case AKEYCODE_DPAD_RIGHT: code = IEV_BUTTON_RIGHT; break;
        case AKEYCODE_BUTTON_A: code = IEV_BUTTON_O; break;
        case AKEYCODE_BUTTON_X: code = IEV_BUTTON_U; break;
        case AKEYCODE_BUTTON_Y: code = IEV_BUTTON_Y; break;
        case AKEYCODE_BUTTON_B: code = IEV_BUTTON_A; break;
        case AKEYCODE_BUTTON_L1:
        case AKEYCODE_BUTTON_L2: code = IEV_BUTTON_L; break;
        case AKEYCODE_BUTTON_R1:
        case AKEYCODE_BUTTON_R2: code = IEV_BUTTON_R; break;
        default: code = 0;
        }
        if (code)
        {
            InputEvent(code, AKeyEvent_getAction(event) == AKEY_EVENT_ACTION_DOWN, 0);
            return 1;
        }
    }
    return 0;
}

static void on_appcmd(struct android_app* app, int32_t cmd)
{
    switch (cmd)
    {
    case APP_CMD_INIT_WINDOW:
        do_init();
        break;
    case APP_CMD_PAUSE:
    case APP_CMD_STOP:
    case APP_CMD_TERM_WINDOW:
        do_shutdown();
        exit(0);
        break;
    default: break;
    }
}

void android_main(struct android_app* state)
{
    appstate = state;
    state->onAppCmd = on_appcmd;
    state->onInputEvent = on_input;
    while (1)
    {
        struct android_poll_source* source = NULL;
        int ident, events;
        while ((ident=ALooper_pollAll(0, 0, &events, (void**)&source)) >= 0)
        {
            if (source) source->process(state, source);
            if (state->destroyRequested)
            {
                do_shutdown();
            }
        }
        if (display) do_cycle();
    }
}
