{ Android input }
unit AndInput;
interface

uses
  InputDrv;

type

  { TAndroidInputDriver - Android input driver }
  TAndroidInputDriver = class(TInputDriver)
  private
    // Fake joystick keys
    FakeJoyKeys: array [TInputKey] of Boolean;
    // Ensure the given key is pressed or released by generating the appropriate
    // events and updating FakeJoyKeys
    procedure EnsureFakeKey(AKey: TInputKey; State: Boolean);
  public
    // TInputDriver methods
    function IsLive: Boolean; override;
  public
    // Called by CSLib when input events occur on the C side
    procedure NotifyInputEvent(Ev, A, B: Int32);
  end;

implementation

uses
  Maths;

const
  // Input events (must match the values in AndroidShim.c)
  IEV_LEFTPAD = 1;
  IEV_RIGHTPAD = 2;
  IEV_BUTTON_LEFT = 3;
  IEV_BUTTON_RIGHT = 4;
  IEV_BUTTON_UP = 5;
  IEV_BUTTON_DOWN = 6;
  IEV_BUTTON_O = 7;
  IEV_BUTTON_U = 8;
  IEV_BUTTON_Y = 9;
  IEV_BUTTON_A = 10;
  IEV_BUTTON_L = 11;
  IEV_BUTTON_R = 12;

{ TAndroidInputDriver }
procedure TAndroidInputDriver.EnsureFakeKey(AKey: TInputKey; State: Boolean);
begin
  if FakeJoyKeys[AKey] <> State then begin
    FakeJoyKeys[AKey]:=State;
    NotifyHandlersForKey(AKey, State);
  end;
end;

function TAndroidInputDriver.IsLive: Boolean;
begin
  Result:=True;
end;

procedure TAndroidInputDriver.NotifyInputEvent(Ev, A, B: Int32);
var
  Motion: TVector;
begin
  case Ev of
    IEV_LEFTPAD: begin
      // Create a motion vector from the left pad position
      Motion:=Vector(A/65535, -B/65535, 0);
      // If the vector isn't long enough, stop the motion
      if Motion.Length < 0.09 then begin
        EnsureFakeKey(ikLeft, False);
        EnsureFakeKey(ikRight, False);
        EnsureFakeKey(ikUp, False);
        EnsureFakeKey(ikDown, False);
        EnsureFakeKey(ikShift, False);
      end else begin
        // Handle backwards motion
        if Motion.Y < -0.1 then begin
          // Down key should only be sent if the joystick has moved enough to
          // avoid accidental backsteps
          EnsureFakeKey(ikDown, Motion.Y < -0.7);
          // Other keys should be off
          EnsureFakeKey(ikUp, False);
          EnsureFakeKey(ikShift, False);
        end else begin // Handle forward motion
          EnsureFakeKey(ikUp, Motion.Y > 0.3);
          EnsureFakeKey(ikShift, Motion.Y < 0.55);
        end;
        // Handle rotation
        EnsureFakeKey(ikLeft, Motion.X < -0.34);
        EnsureFakeKey(ikRight, Motion.X > 0.34);
      end;
    end;
    IEV_BUTTON_UP: NotifyHandlersForKey(ikUp, A <> 0);
    IEV_BUTTON_DOWN: NotifyHandlersForKey(ikDown, A <> 0);
    IEV_BUTTON_LEFT: NotifyHandlersForKey(ikLeft, A <> 0);
    IEV_BUTTON_RIGHT: NotifyHandlersForKey(ikRight, A <> 0);
    IEV_BUTTON_O: NotifyHandlersForKey(ikControl, A <> 0);
    IEV_BUTTON_U: NotifyHandlersForKey(ikEscape, A <> 0);
    IEV_BUTTON_Y: NotifyHandlersForKey(ikAlt, A <> 0);
    IEV_BUTTON_A: NotifyHandlersForKey(ikEnter, A <> 0);
    IEV_BUTTON_L: NotifyHandlersForKey(ikEnd, A <> 0);
    IEV_BUTTON_R: NotifyHandlersForKey(ikSpace, A <> 0);
  end;
end;

end.

