{ Android graphics }
unit AndGfx;
interface

uses
  Colors, Textures, Maths, GfxDrv, Raster, HWRaster, Renderer;

type
  { TAndroidTexture - Hardware texture for Android }
  TAndroidTexture = class(TRasterTexture)
  private
    // Texture handle
    Handle: Int64;
  public
    // TRasterTexture methods
    procedure SetData(AWidth, AHeight: Integer; AFilter: Boolean; APalette: TColorPalette; const Texels); override;
  public
    destructor Destroy; override;
  end;

  { TAndroidRasterizer - Rasterizer that calls into the shim procs }
  TAndroidRasterizer = class(THardwareRasterizer)
  protected
    // THardwareRasterizer methods
    procedure BeforeBatchesDraw; override;
    procedure AfterBatchesDraw; override;
    procedure DrawBatch(const Batch: TTriBatch); override;
  public
    // TRasterizer methods
    function CreateTexture: TRasterTexture; override;
    function BeginFrame: Boolean; override;
    procedure FinishFrame; override;
    procedure Clear(R, G, B: Single); override;
    procedure SetMatrices(Projection, View: TMatrix); override;
    procedure SetFog(FogR, FogG, FogB, FogStart, FogEnd: Single); override;
    procedure RenderQuad(X1, Y1, S1, T1, X2, Y2, S2, T2: Integer; Blend: Single; Texture: TTexture); override;
    procedure DrawHUDImage(X, Y: Integer; Texture: TTexture; SX, SY, SW, SH: Integer); override;
  end;

  { TAndroidGraphicsDriver - Android graphics driver }
  TAndroidGraphicsDriver = class(TGraphicsDriver)
  private
    // Rasterizer
    FRasterizer: TAndroidRasterizer;
  public
    // TGraphicsDriver methods
    procedure Initialize; override;
    procedure Shutdown; override;
    function BeginFrame: Boolean; override;
    procedure FinishFrame; override;
    procedure SetColorPalettePart(const APalette: TColorPalette; First, Last: Integer); override;
    procedure SetGamma(AGamma: TGammaValue); override;
    function GetRasterizer: TRasterizer; override;
  public
    constructor Create;
    destructor Destroy; override;
  end;

implementation

uses
  SysUtils, AndProcs;

{ TAndroidTexture }
destructor TAndroidTexture.Destroy;
begin
  // Destroy texture handle
  Procs.DestroyTexture(Handle);
  inherited Destroy;
end;

procedure TAndroidTexture.SetData(AWidth, AHeight: Integer; AFilter: Boolean; APalette: TColorPalette; const Texels);
var
  RGBTexels: array of Byte;
  TexelBytes: PByte;
  X, Y: Integer;
begin
  // Build RGB texel data from the passed texels and palette
  SetLength(RGBTexels, AWidth*AHeight*3);
  TexelBytes:=PByte(@Texels);
  for Y:=0 to AHeight - 1 do
    for X:=0 to AWidth - 1 do with APalette do begin
      RGBTexels[(Y*AWidth + X)*3]:=Red[TexelBytes[Y*AWidth + X]];
      RGBTexels[(Y*AWidth + X)*3 + 1]:=Green[TexelBytes[Y*AWidth + X]];
      RGBTexels[(Y*AWidth + X)*3 + 2]:=Blue[TexelBytes[Y*AWidth + X]];
    end;
  // Upload the texture data
  Procs.SetTextureData(Handle, AWidth, AHeight, @RGBTexels[0]);
end;

procedure TAndroidRasterizer.BeforeBatchesDraw;
begin
  Procs.BeforeBatches;
end;

procedure TAndroidRasterizer.AfterBatchesDraw;
begin
  Procs.AfterBatches;
end;

procedure TAndroidRasterizer.DrawBatch(const Batch: TTriBatch);
begin
  Procs.RenderBatch(TAndroidTexture(Batch.HWTexture).Handle, @Batch.Vertices[0], Batch.Count div 9);
end;

{ TAndroidRasterizer }
function TAndroidRasterizer.CreateTexture: TRasterTexture;
begin
  Result:=TAndroidTexture.Create;
  TAndroidTexture(Result).Handle:=Procs.CreateTexture();
end;

function TAndroidRasterizer.BeginFrame: Boolean;
begin
  Result:=inherited BeginFrame;
end;

procedure TAndroidRasterizer.FinishFrame;
begin
  inherited FinishFrame;
end;

procedure TAndroidRasterizer.Clear(R, G, B: Single);
begin
  Procs.Clear(R, G, B);
  DiscardBatches;
end;

procedure TAndroidRasterizer.SetMatrices(Projection, View: TMatrix);
begin
  Procs.SetMatrices(@Projection.M11, @View.M11);
end;

procedure TAndroidRasterizer.SetFog(FogR, FogG, FogB, FogStart, FogEnd: Single);
begin
  FlushBatches;
  Procs.SetFog(FogR, FogG, FogB, FogStart, FogEnd);
end;

procedure TAndroidRasterizer.RenderQuad(X1, Y1, S1, T1, X2, Y2, S2,
  T2: Integer; Blend: Single; Texture: TTexture);
begin
  FlushBatches;
  Procs.RenderQuad(
    TAndroidTexture(Texture.HardwareTexture).Handle,
    X1, Y1, S1, T1, X2, Y2, S2, T2);
end;

procedure TAndroidRasterizer.DrawHUDImage(X, Y: Integer; Texture: TTexture; SX, SY, SW, SH: Integer);
begin
end;

{ TAndroidGraphicsDriver }
constructor TAndroidGraphicsDriver.Create;
begin
  inherited Create;
  FRasterizer:=TAndroidRasterizer.Create;
end;

destructor TAndroidGraphicsDriver.Destroy;
begin
  FreeAndNil(FRasterizer);
  inherited Destroy;
end;

procedure TAndroidGraphicsDriver.Initialize;
begin
end;

procedure TAndroidGraphicsDriver.Shutdown;
begin
end;

function TAndroidGraphicsDriver.BeginFrame: Boolean;
begin
  Result:=True;
end;

procedure TAndroidGraphicsDriver.FinishFrame;
begin
end;

procedure TAndroidGraphicsDriver.SetColorPalettePart(
  const APalette: TColorPalette; First, Last: Integer);
begin
end;

procedure TAndroidGraphicsDriver.SetGamma(AGamma: TGammaValue);
begin
  // Not implemented for Android
end;

function TAndroidGraphicsDriver.GetRasterizer: TRasterizer;
begin
  Result:=FRasterizer;
end;

end.

