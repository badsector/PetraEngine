@ECHO OFF

REM Android paths
set BUILDTOOLSPATH=%ANDROID_HOME%\build-tools\30.0.0
set NDKPATH=%ANDROID_HOME%\ndk\21.3.6528147

REM Recreate directories
rmdir /s /q assets
rmdir /s /q bin
rmdir /s /q lib
rmdir /s /q obj
rmdir /s /q jsrc
mkdir assets
mkdir bin
mkdir lib
mkdir lib\armeabi-v7a
mkdir obj
mkdir jsrc

REM Delete any existing APK
del /f *.apk

REM Build the Pascal shared library
del libCSLib.so

ppcrossarm -vm6058 -dPETRAHW -dNODIRECTFILEIO -Tandroid -Mobjfpc -Sh -Sc -B -Fu.. -Fu../../Game -O2 -XX -g- CSLib

IF NOT EXIST "libCSLib.so" GOTO DONE

REM Build the C Android Shim library
call ndk-build APP_PLATFORM=android-16 NDK_PROJECT_PATH=%~dp0 NDK_APPLICATION_MK=%~dp0\Application.mk APP_BUILD_SCRIPT=%~dp0\Android.mk NDK_LIBS_OUT=lib

IF NOT EXIST "lib\armeabi-v7a\libAndroidShim.so" GOTO DONE

REM Copy the Pascal library to the lib directory
copy libCSLib.so lib\armeabi-v7a

REM Build resources
%BUILDTOOLSPATH%\aapt package -f -m -S res -J jsrc -M AndroidManifest.xml -I %ANDROID_HOME%/platforms/android-16/android.jar
javac -source 1.5 -target 1.5 -d obj -classpath %ANDROID_HOME%\platforms\android-16\android.jar -sourcepath jsrc jsrc/com/runtimeterror/PostApocalypticPetra/R.java

REM Copy assets
mkdir assets\Data
xcopy /e /y ..\..\Data\* assets\Data\
copy ..\..\palcache.dat assets\Data\

REM Create dexfile
call %BUILDTOOLSPATH%\dx --dex --output=bin/classes.dex obj

REM Create APK
%BUILDTOOLSPATH%\aapt package -f -M AndroidManifest.xml -S res -I %ANDROID_HOME%/platforms/android-16/android.jar -F PostApocalypticPetra.unsigned.apk bin
%BUILDTOOLSPATH%\aapt add PostApocalypticPetra.unsigned.apk lib/armeabi-v7a/libAndroidShim.so
%BUILDTOOLSPATH%\aapt add PostApocalypticPetra.unsigned.apk lib/armeabi-v7a/libCSLib.so
zip -9r PostApocalypticPetra.unsigned.apk assets

REM Sign the APK
call signtheapk.bat PostApocalypticPetra.signed.apk PostApocalypticPetra.unsigned.apk

REM Align the APK
%BUILDTOOLSPATH%\zipalign -f 4 PostApocalypticPetra.signed.apk PostApocalypticPetra.apk

REM Remove unnecessary files
del PostApocalypticPetra.unsigned.apk
del PostApocalypticPetra.signed.apk

:DONE
