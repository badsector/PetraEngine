{ The entire game as a Pascal library to be used via Android Shim }
library CSLib;
uses
  AndProcs, AndInput, AndSnd, Math, Engine, GfxDrv, Settings, DataLoad;

{ Called to initialize the game }
procedure Initialize(constref AProcs: TProcs); cdecl;
var
  ViewWidth, ViewHeight: Int32;
begin
  // Disable FPU exceptions
  SetExceptionMask([exInvalidOp, exDenormalized, exZeroDivide, exOverflow, exUnderflow, exPrecision]);
  // Store procs
  Procs:=AProcs;
  // Store viewport size
  Procs.GetViewSize(@ViewWidth, @ViewHeight);
  RealHRes:=ViewWidth;
  RealVRes:=ViewHeight;
  // Initialize
  AndroidLog('Loading settings');
  LoadSettings;
  AndroidLog('Initializing engine');
  InitializeEngine;
  AndroidLog('Loading palette');
  GRenderer.ColorPalette.LoadJASCPalette(DataDirectoryPrefix + 'colors.pal');
  // Initialize the game
  AndroidLog('Initializing the game');
  GGame.Initialize;
  AndroidLog('Initialization complete');
end;

{ Called to run a single engine cycle }
procedure RunCycle; cdecl;
begin
  RunEngineCycle;
end;

{ Called to shutdown the game }
procedure Shutdown; cdecl;
begin
  ShutdownEngine;
end;

{ Called to process an input event }
procedure InputEvent(Ev, A, B: Int32); cdecl;
begin
  TAndroidInputDriver(GInput).NotifyInputEvent(Ev, A, B);
end;

{ Called to fill audio buffer }
procedure AudioCallback(Buffer: PPointer; Size: PInt32); cdecl;
begin
  TAndroidSoundDriver(GSound).Callback(Buffer, Size);
end;

exports
  Initialize,
  RunCycle,
  Shutdown,
  InputEvent,
  AudioCallback;

end.
