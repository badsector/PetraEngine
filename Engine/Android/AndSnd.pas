{ Android sound }
unit AndSnd;
interface

uses
  SndDrv;

type
  { TAndroidSoundDriver - Android sound driver }
  TAndroidSoundDriver = class(TSoundDriver)
  protected
    // TSoundDriver methods
    procedure InitializeDriver; override;
    procedure LockBuffers; override;
    procedure UnlockBuffers; override;
  public
    destructor Destroy; override;
    // Called by CSLib to handle audio callback
    procedure Callback(Buffer: PPointer; Size: PInt32);
  end;

implementation

uses
  AndProcs;

var
  Crit: TRTLCriticalSection;

{ TAndroidSoundDriver }
destructor TAndroidSoundDriver.Destroy;
begin
  DoneCriticalSection(Crit);
  inherited Destroy;
end;

procedure TAndroidSoundDriver.InitializeDriver;
begin
  InitCriticalSection(Crit);
  Available:=True;
end;

procedure TAndroidSoundDriver.LockBuffers;
begin
  EnterCriticalSection(Crit);
end;

procedure TAndroidSoundDriver.UnlockBuffers;
begin
  LeaveCriticalSection(Crit);
end;

procedure TAndroidSoundDriver.Callback(Buffer: PPointer; Size: PInt32);
begin
  LockBuffers;
  if SilentCounter <> 0 then
    FillChar(Buffers[NextBuffer], SizeOf(Buffers[0]), 0)
  else if NeedMix then
    MixNow;
  Buffer^:=@Buffers[NextBuffer][0];
  Size^:=SizeOf(Buffers[NextBuffer]);
  NextBuffer:=(NextBuffer + 1) mod MaxSoundBuffers;
  NeedMix:=True;
  Dec(Mixed);
  UnlockBuffers;
end;

end.
