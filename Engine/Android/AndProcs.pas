{ Procedures passed from the C side }
unit AndProcs;
interface

type
  { TProcs - Contains the procedure, must be equivalent to TProcs on C side }
  TProcs = packed record
    // Write to log
    LogString: procedure(Str: PChar); cdecl;
    // Request to quit
    RequestQuit: procedure; cdecl;
    // Free C-side pointer
    FreeMem: procedure(Ptr: Pointer); cdecl;
    // Obtain asset data, returns nil if failed
    GetAssetData: function(AFileName: PChar; ASize: PInt32): Pointer; cdecl;
    // Return a non-zero value if the given data file exists
    HasDataFile: function(AFileName: PChar): Int32; cdecl;
    // Create a new texture handle
    CreateTexture: function: Int64; cdecl;
    // Destroy the given texture handle
    DestroyTexture: procedure(AHandle: Int64); cdecl;
    // Set texture data
    SetTextureData: procedure(AHandle: Int64; Width, Height: Int32; RGB: PByte); cdecl;
    // Get rendering viewport size
    GetViewSize: procedure(ViewWidth, ViewHeight: PInt32); cdecl;
    // Clear the framebuffer with the given color
    Clear: procedure(R, G, B: Single); cdecl;
    // Set the projection and view matrices
    SetMatrices: procedure(Proj, View: PSingle); cdecl;
    // Set fog
    SetFog: procedure(R, G, B, FogStart, FogEnd: Single); cdecl;
    // Prepare for rendering batches
    BeforeBatches: procedure; cdecl;
    // Finish rendering batches
    AfterBatches: procedure; cdecl;
    // Render a batch
    RenderBatch: procedure(TexHandle: Int64; VertexData: PSingle; Count: Int32); cdecl;
    // Render a screen space quad
    RenderQuad: procedure(TexHandle: Int64; X1, Y1, S1, T1, X2, Y2, S2, T2: Single); cdecl;
  end;

var
  // The procedures passed from the C side
  Procs: TProcs;

// Write to Android log
procedure AndroidLog(AString: string);

implementation

procedure AndroidLog(AString: string);
begin
  Procs.LogString(PChar(AString));
end;

end.
