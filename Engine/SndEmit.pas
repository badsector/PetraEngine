{ Sound emitter entities }
unit SndEmit;

interface

uses
  Maths, World, Sounds;

type
  { TSoundEmitter - A sound emitter in 3D space }
  TSoundEmitter = class(TEntity)
  private
    // Property storage
    FSoundFile: string;
    FMaxDistance: Single;
    FVolume: Single;
    FStopped: Boolean;
    procedure SetSoundFile(AValue: string);
  private
    // The sound clip
    Clip: TSoundClip;
    // Sound channel
    Channel: Integer;
  protected
    // TEntity methods
    procedure AddSceneNodes; override;
    procedure RemoveSceneNodes; override;
    procedure Update; override;
    {$IFDEF EDITOR}
    procedure EditorUpdate; override;
    procedure Frame; override;
    {$ENDIF}
    function GetAABox: TAABox; override;
  public
    // TEntity methods
    function PerformCommand(ACommand: string): Boolean; override;
  public
    constructor Create; override;
  published
    // The sound name
    property SoundFile: string read FSoundFile write SetSoundFile;
    // Distance at which the sound isn't audible
    property MaxDistance: Single read FMaxDistance write FMaxDistance;
    // Sound volume
    property Volume: Single read FVolume write FVolume;
    // Stopped or not
    property Stopped: Boolean read FStopped write FStopped;
  end;

implementation

uses
  Serial, Engine;

{ TSoundEmitter }     
constructor TSoundEmitter.Create;
begin
  inherited Create;
  Channel:=-1;
  FMaxDistance:=6;
  FVolume:=1;
end;

procedure TSoundEmitter.SetSoundFile(AValue: string);
begin
  if FSoundFile=AValue then Exit;
  FSoundFile:=AValue;
  RemovedFromWorld;
  Clip:=TSoundClip(GResourceManager.GetResource(AValue));
end;

procedure TSoundEmitter.AddSceneNodes;
begin
end;

procedure TSoundEmitter.RemoveSceneNodes;
begin
  // Release the sound channel, if set
  if Channel <> -1 then begin
    GSound.SetChannelClip(Channel, nil, 0);
    Channel:=-1;
  end;
end;

procedure TSoundEmitter.Update;
var
  CameraToEmitter: TVector;
  Dist, FrontVolume, LeftVolume, RightVolume: Single;
begin
  if Assigned(Clip) and not Stopped then begin
    // Calculate camera-to-emitter vector
    CameraToEmitter:=Transform.Translation.Subbed(GRenderer.Camera.Position);
    // Find distance of the emitter from the camera
    Dist:=CameraToEmitter.LengthSq;
    // Check if we're too far away
    if Dist >= FMaxDistance*FMaxDistance then begin
      // Release channel if we have one
      if Channel <> -1 then begin
        GSound.SetChannelClip(Channel, nil, 0);
        Channel:=-1;
      end;
    end else begin
      // Allocate channel if needed
      if Channel=-1 then begin
        Channel:=GSound.UnusedChannel;
        if Channel <> -1 then begin
          GSound.SetChannelClip(Channel, Clip, 0);
        end;
      end;
      // If we have a channel, update the left and right volumes based on the
      // emitter's position relative to the camera
      if Channel <> -1 then begin
        Dist:=1 - Sqrt(Dist)/MaxDistance;
        Dist:=Dist*Dist;
        FrontVolume:=Abs(CameraToEmitter.Dot(GRenderer.Camera.Direction));
        LeftVolume:=Clamp((Max(0, CameraToEmitter.Dot(GRenderer.CameraLeft)) + FrontVolume*0.45)*Dist);
        RightVolume:=Clamp((Max(0, CameraToEmitter.Dot(GRenderer.CameraRight)) + FrontVolume*0.45)*Dist);
        if LeftVolume < 0.1 then LeftVolume:=0;
        if RightVolume < 0.1 then RightVolume:=0;
        GSound.SetChannelVolume(Channel,
          Round(LeftVolume*Volume*255.0),
          Round(RightVolume*Volume*255.0)
          );
      end;
    end;
  end else if Channel <> -1 then begin
    GSound.SetChannelClip(Channel, nil, 0);
    Channel:=-1;
  end;
end;

{$IFDEF EDITOR}
procedure TSoundEmitter.EditorUpdate;
begin
  Update;
end;

procedure TSoundEmitter.Frame;
begin
  if GPlayMode=pmEditor then GRenderer.RenderAABox(AABox);
end;
{$ENDIF}

function TSoundEmitter.GetAABox: TAABox;
begin
  Result:=inherited GetAABox;
  Result.Grow(0.2);
end;

function TSoundEmitter.PerformCommand(ACommand: string): Boolean;
begin
  if inherited PerformCommand(ACommand) then Exit(True);
  if ACommand='start' then begin
    Stopped:=False;
    Exit(True);
  end;
  if ACommand='stop' then begin
    Stopped:=True;
    Exit(True);
  end;
  if ACommand='toggle' then begin
    Stopped:=not Stopped;
    Exit(True);
  end;
  Result:=False;
end;

initialization
  RegisterSerializableClass(TSoundEmitter);
end.
